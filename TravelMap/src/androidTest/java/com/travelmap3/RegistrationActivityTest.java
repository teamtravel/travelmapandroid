package com.travelmap3;

import android.support.v7.widget.AppCompatEditText;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;

import com.robotium.solo.Solo;
import com.travelmap3.auth.RegistrationActivity;

import java.util.Random;


public class RegistrationActivityTest extends ActivityInstrumentationTestCase2 {

    private Solo solo;
    private Button btn_login;
    private AppCompatEditText login;
    private AppCompatEditText pass;
    private AppCompatEditText passConfirm;
    private AppCompatEditText name;

    public RegistrationActivityTest() {
        super(RegistrationActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        //setUp() is run before a test case is started.
        //This is where the solo object is created.
        solo = new Solo(getInstrumentation(), getActivity());
        btn_login = (Button) solo.getView(R.id.buttonSendRegistration);
        login = (AppCompatEditText) solo.getView(R.id.editEmail);
        name = (AppCompatEditText) solo.getView(R.id.editNikName);
        pass = (AppCompatEditText) solo.getView(R.id.editPass);
        passConfirm = (AppCompatEditText) solo.getView(R.id.editConfirmPass);
        solo.setWiFiData(true);
    }

    @Override
    public void tearDown() throws Exception {
        //tearDown() is run after a test case has finished.
        //finishOpenedActivities() will finish all the activities that have been opened during the test execution.
        solo.finishOpenedActivities();
    }

    public void testInputLogin_Email() throws Exception {
        solo.unlockScreen();
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, "qwertyl.ru");
        solo.enterText(pass, "password123");
        solo.enterText(passConfirm, "password123");
        solo.enterText(name, "Andrew");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Email не корректный");
        assertTrue("Email некорректный", b);
    }
    public void testInputNoLengthNikName() throws Exception {
        solo.unlockScreen();
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, "qwertyBugNotEmail.ru");
        solo.enterText(pass, "password123");
        solo.enterText(passConfirm, "password123");
        solo.enterText(name, "");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Длина имени меньше трех символов");
        assertTrue("Длина имени меньше трех символов", b);
    }
    public void testInputEmptyLogin() throws  Exception {
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, " ");
        solo.enterText(pass, "zxzxzx");
        solo.enterText(name, "Andrew");
        solo.enterText(passConfirm, "zxzxzx");
        solo.clickOnView(btn_login);
        solo.sleep(1200);
        boolean b = solo.searchText("Не введен");
        assertTrue("Пустое поле для email не выполнено", b);
    }
    public void testInputEmptyPass() throws Exception {
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, "mail@mail.ru ");
        solo.enterText(pass, " ");
        solo.enterText(name, "Andrew");
        solo.enterText(passConfirm, "");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Не введен пароль");
        assertTrue("Не введен пароль", b);
    }
    public  void testInputEmptyConfirmPass() throws  Exception {
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, " mail@mail.ru");
        solo.enterText(pass, "zxzxz");
        solo.enterText(name, "Andrew");
        solo.enterText(passConfirm, "");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Не введен пароль");
        assertTrue("Не введен пароль", b);
    }

    public void testInputNoLengthPass() throws Exception {
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, " mail@mail.ru");
        solo.enterText(pass, "zx");
        solo.enterText(name, "Andrew");
        solo.enterText(passConfirm, "zx");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Пароль меньше 5 символов");
        assertTrue("Пароль меньше 5 символов", b);
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
    }
    public void testInput_onSucces() throws Exception {
        solo.assertCurrentActivity("Expected OldLoginActivity activity", "RegistrationActivity");
        solo.enterText(login, "mail@mail.ru");
        solo.enterText(pass, "123123");
        Random r = new Random();
        int a = r.nextInt(100);
        solo.enterText(name, "Andrew Go"+a);
        solo.enterText(passConfirm, "123123");
        solo.clickOnView(btn_login);
        boolean b = solo.searchText("Успешно");
//        solo.assertCurrentActivity("Expected OldLoginActivity activity", "OldLoginActivity");
        assertTrue("Успешно", b);
        assertTrue("заполненые поля", ((EditText)solo.getView(R.id.user)).getText().toString().compareTo("mail@mail.ru")==0);
        assertTrue("заполненые поля", ((EditText)solo.getView(R.id.pass)).getText().toString().compareTo("123123")==0);
    }
}