package com.travelmap3;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.travelmap3.tracker.TrackerDB;
import com.travelmap3.tracker.TrackerMapsActivity;
import com.travelmap3.tracker.TravelStory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;

/**
 * Created by Andre on 15.07.2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class TrackerStoryUnitTest {
    @Rule
    public ActivityTestRule<TrackerMapsActivity> mActivityRule = new ActivityTestRule<>(
            TrackerMapsActivity.class);
    Context ctx;
    TrackerDB mTrackerDB;
    private int numberTrip = 1;

    @Before
    public void before() {
        Instrumentation instrumentation
                = InstrumentationRegistry.getInstrumentation();
        ctx = instrumentation.getTargetContext();
        mTrackerDB = new TrackerDB(ctx);
        mTrackerDB.clear();

    }

    @Test
    public void testTrackerDB_initWithName() {
        double lat = 23.321;
        double lon = 123.123;
        long time = System.currentTimeMillis();
        mTrackerDB.saveDB(lat, lon, time, numberTrip, "First Name");
        ArrayList<TravelStory> list = mTrackerDB.getListStory();
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), "First Name");
        assertEquals(list.get(0).getLatLonStart(), "23.321 123.123");
        assertEquals(list.get(0).getTimeStart(), time);
        assertEquals(list.get(0).getNumber(), numberTrip);
        double latFinish = 10.123;
        double lonFinish = 11.23;
        long timef = System.currentTimeMillis() + 10;
        double distance = 829234.12;
        double speed = 23.21;
        mTrackerDB.update(latFinish, lonFinish, timef, numberTrip, distance, speed);
        list.clear();
        list = mTrackerDB.getListStory();
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), "First Name");
        assertEquals(list.get(0).getLatLonStart(), "23.321 123.123");
        assertEquals(list.get(0).getTimeStart(), time);
        assertEquals(list.get(0).getNumber(), numberTrip);
        assertEquals(list.get(0).getTimeFinish(), timef);
        assertEquals(list.get(0).getLatLonFinish(), "10.123 11.23");
        assertEquals(list.get(0).getSpeed(), speed);
        assertEquals(list.get(0).getDistance(), distance);

    }

    @Test
    public void testTrackerDb_initNullName() {
        double lat = 213.321;
        double lon = 55.55;
        long time = System.currentTimeMillis();
        mTrackerDB.saveDB(lat, lon, System.currentTimeMillis(), numberTrip, null);
        ArrayList<TravelStory> list = mTrackerDB.getListStory();
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), "");
        assertEquals(list.get(0).getLatLonStart(), "213.321 55.55");
        assertEquals(list.get(0).getTimeStart(), time);
        assertEquals(list.get(0).getNumber(), numberTrip);
        double latFinish = 10.123;
        double lonFinish = 11.23;
        long timef = System.currentTimeMillis() + 10;
        double distance = 829234.12;
        double speed = 23.21;
        mTrackerDB.update(latFinish, lonFinish, timef, numberTrip, distance, speed);
        list.clear();
        list = mTrackerDB.getListStory();
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getName(), "");
        assertEquals(list.get(0).getLatLonStart(), "213.321 55.55");
        assertEquals(list.get(0).getTimeStart(), time);
        assertEquals(list.get(0).getNumber(), numberTrip);
        assertEquals(list.get(0).getTimeFinish(), timef);
        assertEquals(list.get(0).getLatLonFinish(), "10.123 11.23");
        assertEquals(list.get(0).getSpeed(), speed);
        assertEquals(list.get(0).getDistance(), distance);
    }



}
