package com.travelmap3;

import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;

import com.travelmap3.menu.MainMenuActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by tema_ on 03.06.2016.
 */

public class StartHomeCardInfoFragmentTest{

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<>(MainMenuActivity.class);

    @Test
    public void testSwipeUpdate_StartHomeInfoFragment(){
        onView(withId(R.id.scrollView)).perform(ViewActions.swipeUp());
        onView(withId(R.id.scrollView)).perform(ViewActions.swipeDown());
        onView(withId(R.id.swipe)).perform(ViewActions.swipeDown());
    }

    @Test
    public void testSwipeUpdateWithEmptyDB_StartHomeInfoFragment(){
//        NearestObjectDB placeDb = new NearestObjectDB();
//        placeDb.save(new ArrayList<ObjectPlace>());
//        onView(withId(R.id.swipe)).perform(ViewActions.swipeDown());
//        onView(withId(R.id.scrollView)).perform(ViewActions.swipeUp());
//        onView(withId(R.id.scrollView)).perform(ViewActions.swipeDown());
//        onView(withId(R.id.places_show_more_button)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));
    }
}
