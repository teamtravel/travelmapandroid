package com.travelmap3;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.travelmap3.interesplace.Place;
import com.travelmap3.interesplace.PlaceDB;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.Gps;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashSet;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Andre on 20.06.2016.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class InterestingPlaceUnitTest {
    private static final String LOG = "Test";
    @Rule
    public ActivityTestRule<MainMenuActivity> mActivityRule = new ActivityTestRule<>(
            MainMenuActivity.class);
    Context ctx;

    @Before
    public void before() {
        Instrumentation instrumentation
                = InstrumentationRegistry.getInstrumentation();
        ctx = instrumentation.getTargetContext();
    }

    @Test
    public void testSaveAndTake_db() {
        PlaceDB db = new PlaceDB(ctx);
        db.clear();
        for (int i = 1; i < 10; i++) {
            Place place = Place.newBuilder()
                    .setGps(new Gps(i,i,i))
                    .setNumberTrip(i)
                    .setText("text")
                    .setTitle("title")
                    .build();
            db.save(place, PlaceDB.ACTION.INSERT);
        }
        HashSet<Place> set = db.getRecords();
        assertNotNull(set);
        assertEquals(9, set.size());
        for (Place place : set) {
            assertNotNull(place);
            assertFalse(place.getText().isEmpty());
            assertEquals(place.getText(), "text");
            assertEquals(place.getTitle().compareTo("title"), 0);
            assertNotNull(place.getGps());
            assertTrue(place.getGps().getLatitude() < 10 && place.getGps().getLongitude() > 0);

        }
        Place newPlace = Place.newBuilder()
                .setGps(new Gps(1,2,3))
                .setNumberTrip(2)
                .setText("NEW")
                .setTitle("title")
                .build();
        db.save(newPlace, PlaceDB.ACTION.UPDATE);//Заменить заголовок у тех обьектов у кого время = 3
        HashSet<Place> set2 = db.getRecords();
        assertTrue(set2.size() == 9);
        int l = 0;
        for (Place place : set2) {
            Log.e(LOG, place.getText());
            if (place.getTitle().compareTo("title") == 0
                    && place.getText().compareTo("NEW") == 0) l++;
        }
        assertEquals(l, 1);

        HashSet<Place> set3 = new HashSet<>();
        Place deletePlace = Place.newBuilder()
                .setGps(new Gps(1,2,3))
                .setNumberTrip(2)
                .setText("NEW")
                .setTitle("title")
                .build();
        db.save(deletePlace, PlaceDB.ACTION.DELETE);
        set3 = db.getRecords();
        assertNotNull(set3);
        assertEquals(8,set3.size());
        l = 0;
        for (Place place : set3) {
            if (place.getTitle().compareTo("title") == 0
                    && place.getText().compareTo("NEW") == 0) l++;
        }
        assertTrue(l == 0);

        db.clear();
        set = db.getRecords();
        assertNotNull(set);
        assertTrue(set.isEmpty());

    }
}

