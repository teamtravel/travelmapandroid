package com.travelmap3;

import android.app.Instrumentation;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.travelmap3.menu.MySettingsActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class InternetConnectTest {
    Context ctx;
    @Rule
    public ActivityTestRule<MySettingsActivity> mActivityRule = new ActivityTestRule<>(
            MySettingsActivity.class);


    @Before
    public void before() {
        Instrumentation instrumentation
                = InstrumentationRegistry.getInstrumentation();
        ctx = instrumentation.getTargetContext();

    }
    @Test
    public void testIsActiveWiFiConnectionWhenWiFiOnMobOffDefault() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivityRule.getActivity());
        onView(withText(R.string.send_data_only_wifi)).check(matches(notNullValue()));


        ConnectivityManager dataManager  = (ConnectivityManager) mActivityRule.getActivity().getSystemService(ctx.CONNECTIVITY_SERVICE);
        NetworkInfo wifi1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mob1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
        dataMtd.setAccessible(true);
        dataMtd.invoke(dataManager, false);
        WifiManager wifi = (WifiManager) mActivityRule.getActivity().getSystemService(ctx.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
        Log.e("mLOG", wifi.isWifiEnabled()+"");

        boolean connection;


        if(!sp.getBoolean("usedWifi", false)){
            connection = wifi1.isAvailable();
        } else {
            connection = mob1.isAvailable()||wifi1.isAvailable();
        }


        assertTrue(connection);//Если не нажата кнопка НЕТ моб. инета НО включен вайфай, должно быть True
        //assertFalse(mob1.isAvailable());
    }
    @Test
    public void testIsActiveWiFiConnectionWhenWiFiOffMobOffDefault() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivityRule.getActivity());
        onView(withText(R.string.send_data_only_wifi)).check(matches(notNullValue()));
        ConnectivityManager dataManager  = (ConnectivityManager) mActivityRule.getActivity().getSystemService(ctx.CONNECTIVITY_SERVICE);
        NetworkInfo mob1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
        dataMtd.setAccessible(true);
        dataMtd.invoke(dataManager, false);
        WifiManager wifi = (WifiManager) mActivityRule.getActivity().getSystemService(ctx.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
        Log.e("mLOG", wifi.isWifiEnabled()+"");

        boolean connection;


        if(!sp.getBoolean("usedWifi", false)){
            connection = wifi1.isAvailable();
        } else {
            connection = mob1.isAvailable()||wifi1.isAvailable();
        }

        assertFalse(connection);//Если не нажата кнопка НЕТ моб. инета НЕ включен вайфай, должно быть False
    }
    @Test
    public void testIsActiveWiFiConnectionWhenWiFiOffMobOnDefault() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivityRule.getActivity());
        onView(withText(R.string.send_data_only_wifi)).check(matches(notNullValue()));
        //onView(withText(R.string.send_data_only_wifi)).perform(click());

        ConnectivityManager dataManager  = (ConnectivityManager) mActivityRule.getActivity().getSystemService(ctx.CONNECTIVITY_SERVICE);
        NetworkInfo mob1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);

        dataMtd.setAccessible(true);
        dataMtd.invoke(dataManager, true);

        WifiManager wifi = (WifiManager) mActivityRule.getActivity().getSystemService(ctx.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
        Log.e("mLOG", wifi.isWifiEnabled()+"");

        boolean connection;


        if(!sp.getBoolean("usedWifi", false)){
            connection = wifi1.isAvailable();
        } else {
            connection = mob1.isAvailable()|wifi1.isAvailable();
        }

        assertTrue(connection);//Если не нажата кнопка ЕСТЬ моб. инет НЕ включен вайфай, должно быть True
        //assertTrue(wifi1.isAvailable());
    }


    @Test
    public void testIsActiveWiFiConnectionWhenWiFiOn() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        ConnectivityManager dataManager  = (ConnectivityManager) mActivityRule.getActivity().getSystemService(ctx.CONNECTIVITY_SERVICE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivityRule.getActivity());
        NetworkInfo wifi1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mob1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);

        dataMtd.setAccessible(true);
        dataMtd.invoke(dataManager, false);
        onView(withText(R.string.send_data_only_wifi)).check(matches(notNullValue()));
        onView(withText(R.string.send_data_only_wifi)).perform(click());
        //onView(withText(R.string.send_data_only_wifi)).perform(click());


        WifiManager wifi = (WifiManager) mActivityRule.getActivity().getSystemService(ctx.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
        Log.e("mLOG", wifi.isWifiEnabled()+"");

        boolean connection;


        if(!sp.getBoolean("usedWifi", false)){
            connection = wifi1.isAvailable();
        } else {
            connection = mob1.isAvailable()||wifi1.isAvailable();
        }

        assertTrue(connection); // Если нажата кнопка и влючен инет wifi то TRUE
        //assertTrue(!mob1.isAvailable());

        //onView(withText(R.string.send_data_only_wifi)).perform(click());
    }

    @Test
    public void testIsActiveWiFiConnectionWhenWiFiOff() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

        ConnectivityManager dataManager  = (ConnectivityManager) mActivityRule.getActivity().getSystemService(ctx.CONNECTIVITY_SERVICE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mActivityRule.getActivity());
        NetworkInfo wifi1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mob1 = dataManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);

        dataMtd.setAccessible(true);
        dataMtd.invoke(dataManager, false);
        onView(withText(R.string.send_data_only_wifi)).check(matches(notNullValue()));
        onView(withText(R.string.send_data_only_wifi)).perform(click());

        WifiManager wifi = (WifiManager) mActivityRule.getActivity().getSystemService(ctx.WIFI_SERVICE);
        wifi.setWifiEnabled(false);
        Log.e("mLOG", wifi.isWifiEnabled()+"");
        boolean connection;


        if(!sp.getBoolean("usedWifi", false)){
            connection = wifi1.isAvailable();
        } else {
            connection = mob1.isAvailable()||wifi1.isAvailable();
        }

        assertFalse(connection); // Если нажата кнопка и ВыклИнет wifi то FALSE
        //onView(withText(R.string.send_data_only_wifi)).perform(click());
    }

}
