package com.travelmap3.vk;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.model.PeopleModel;
import com.travelmap3.utils.Preferences;

import java.util.ArrayList;

/**
 * show vk groupslist
 */
@SuppressLint("NewApi")
public class MenuVkGroupsDialogsFragment extends Fragment {
    private ListView list;
    private VKGroupsAdapter adapter;
    private ArrayList<PeopleModel> groupList;
    private Toolbar mToolbar;

    public MenuVkGroupsDialogsFragment() {
    }

    /**
     * @param newlist ArrayList
     */
    public void setList(ArrayList<PeopleModel> newlist) {
        this.groupList = newlist;
        Preferences.getInstance().saveArrayList(Preferences.VK_NAME, groupList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        App application = (App) getActivity().getApplication();
//    Tracker mTracker = application.getDefaultTracker();
//    mTracker.setScreenName("Screen VkGroupsDialogsFragment");
//    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        View rootView = inflater.inflate(R.layout.fragment_vk_groups, container, false);
        list = (ListView) rootView.findViewById(R.id.list);
        adapter = new VKGroupsAdapter(this.groupList, getActivity());
        list.setAdapter(adapter);
        if (groupList.size() == 0) {
            Toast.makeText(getActivity(), " У вас по видимому нет собственных групп", Toast.LENGTH_LONG)
                    .show();
        }
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.vk_groups_dialog_title);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.vk_white));
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationContentDescription(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFragment();
            }
        });
        return rootView;
    }

    private void closeFragment() {
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }
}
