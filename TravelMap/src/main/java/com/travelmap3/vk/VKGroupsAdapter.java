package com.travelmap3.vk;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.travelmap3.R;
import com.travelmap3.model.PeopleModel;
import com.travelmap3.utils.Preferences;

import java.util.ArrayList;

public class VKGroupsAdapter extends BaseAdapter {

  private static final String LOG = "VKGroupsAdapter";
  private ArrayList<PeopleModel> data;
  private LayoutInflater inflater = null;

  private PeopleModel nameGroupVK;
  private Context mContext;

  public VKGroupsAdapter(ArrayList dataList, Context context) {
    data = dataList;
    mContext = context;
    inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

  }

  public int getCount() {
    return data.size();
  }

  public Object getItem(int position) {
    return position;
  }

  public long getItemId(int position) {
    return position;
  }

  //TODO Holder add or recycler view
  @Override
  public View getView(final int position, View convertView, final ViewGroup parent) {
    View view = convertView;
    if (convertView == null) {
      view = inflater.inflate(R.layout.list_vk_grorup_row, null);
    }
    nameGroupVK = data.get(position);

    ImageView image = (ImageView) view.findViewById(R.id.iconGroup);

    Glide.with(mContext)
        .load(nameGroupVK.getPhoto50())
        .diskCacheStrategy(DiskCacheStrategy.RESULT)
        .into(image);

    TextView name = (TextView) view.findViewById(R.id.titleVkGroup);
    TextView hidden = (TextView) view.findViewById(R.id.titleVkGroupId);

    name.setText(nameGroupVK.getFirstName());
    hidden.setText(nameGroupVK.getRowID());
    final CheckBox checkBoxGroupChoice = (CheckBox) view.findViewById(R.id.checkBoxGroupChoice);
    checkBoxGroupChoice.setChecked(false);

    if (nameGroupVK.isChecked()) {
      checkBoxGroupChoice.setChecked(true);
    }

    view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CheckBox checkBox = (CheckBox) v.findViewById(R.id.checkBoxGroupChoice);
        checkBox.setChecked(!checkBox.isChecked());
        String[] strPrefGroups = new String[data.size()];
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parent.getChildCount(); ++i) {
          checkBox = (CheckBox) parent.getChildAt(i).findViewById(R.id.checkBoxGroupChoice);
          if (checkBox.isChecked()) {
            TextView nextChild = (TextView) parent.getChildAt(i).findViewById(R.id.titleVkGroupId);
            strPrefGroups[i] = nextChild.getText().toString();
            if (i != parent.getChildCount() - 1) {
              sb.append(nextChild.getText().toString()).append(",");
            } else {
              sb.append(nextChild.getText().toString());
            }

          }
        }
        Preferences.getInstance().saveCheckBoxGroupChoice(implode(",", strPrefGroups));
      }
    });
    return view;
  }

  private static String implode(String separator, String... data) {
    StringBuilder sb = new StringBuilder();
    if (data.length == 1) {
      sb.append(data[0]);
    } else {
      for (String aData : data) {
        if (aData != null) {
          sb.append(aData);
          sb.append(separator);
        }
      }
    }
    return sb.toString();
  }
}