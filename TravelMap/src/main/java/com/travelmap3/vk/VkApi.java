package com.travelmap3.vk;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.BuildConfig;
import android.util.Log;

import com.travelmap3.App;
import com.travelmap3.MainActivity;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.api.VKUploadServiceAPI;
import com.travelmap3.model.PeopleModel;
import com.travelmap3.model.User;
import com.travelmap3.notes.Notes.ImageLocations;
import com.travelmap3.utils.Crypto;
import com.travelmap3.utils.FormatJSON;
import com.travelmap3.utils.Preferences;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static com.travelmap3.api.VKUploadServiceAPI.UploadServerResponseModel;

/**
 * vk sdk wrapper class
 * Created by dev4 on 05.06.2015.
 */
public class VkApi {

    private static final String LOG = "VK_API ";
    private static final String[] sMyScope = new String[]{VKScope.WALL, VKScope.EMAIL,
            VKScope.STATS, VKScope.NOHTTPS, VKScope.OFFLINE, VKScope.GROUPS, "email", VKScope.PHOTOS};
    private static String postToId = "";
    private static IVkPoster vkposter;

    private static int reqNumbOfPics = 0;       //требуемое кол-во картинок, которое должно быть загружено на ВК-сервер

    //объект списка содержит параметры, которые пришли в ответ после удачного сохранения картинки на ВК-сервере
    private static List<VKParameters> successImageUploadingParameters = new ArrayList<VKParameters>();
    private static boolean isImagePostingOk = true;
    private static Object lock = new Object();

    //объект списка содержит айдишники необходимые для завершающего шага в процессе добавления поста с фотками на стену
    private static List<VKResponse> successParametersSendingResponses = new ArrayList<VKResponse>();
    private static boolean isParametersSendingOk = true;

    /**
     * init vk_app and auth user
     */
    public static void start(final Activity activity) {
        if (!VKSdk.isLoggedIn()) {
            VKSdk.login(activity, sMyScope);
        } else if (Preferences.getInstance().loadToken() != null) {
            activity.startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
        } else {
            VKSdk.login(activity, sMyScope);
//            VKSdk.logout();
//            activity.startActivity(new Intent(activity, NewLoginActivity.class));
//            activity.finish();
        }
//            // try auth to server
//            storeByServerUserInfo(activity, new IVkPoster() {
//                @Override
//                public void onSuccess(Object obj) {
//                    activity.startActivity(new Intent(activity, MainMenuActivity.class));
//                    activity.finish();
//                }
//
//                @Override
//                public void onError(String er) {
//
//                }
//            });
    }

    /**
     * init & auth user or posting to wall
     */
    public static void wallPost(final String text, final Activity act, final IVkPoster callback, final List<ImageLocations> imageLocationsList) {

        try {
            if (!isLoggedIn()) {
                start(act);
                vkposter = new IVkPoster() {
                    @Override
                    public void onSuccess(Object obj) {
                        postToWall(text, callback, imageLocationsList, act);
                    }

                    @Override
                    public void onError(String error) {
                    }
                };
            } else {
                postToWall(text, callback, imageLocationsList, act);
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }

    }

    private static Boolean postToWall(final String text, final IVkPoster callback, final List<ImageLocations> imageLocationsList, final Activity activity) {
        isImagePostingOk = true;
        isParametersSendingOk = true;

//    Tracker mTracker = App.getInstance().getDefaultTracker();
//    mTracker.send(new HitBuilders.EventBuilder()
//        .setCategory("Отправка поста в вк")
//        .setAction("Постинг")
//        .build());

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext());

        Boolean checkBoxMyWall = sp.getBoolean("publish_vk_wall", false);
        Boolean publishVkGroupWall = sp.getBoolean("publish_vk_group_wall", false);

        if (!publishVkGroupWall && !checkBoxMyWall) {
            return false;
        }

        VKApi.photos().getWallUploadServer().executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                String uploadUrl = null;

                try {
                    uploadUrl = response.json.getJSONObject("response").getString("upload_url");
                } catch (JSONException e) {
                    callback.onError("Error during wall post");
                    return;
                }

                Log.d("VKRequestLi", "Upload url: " + uploadUrl);
                VKUploadServiceAPI uploadService = RetrofitService.createVkUploadService(VKUploadServiceAPI.class, uploadUrl);

                reqNumbOfPics = imageLocationsList.size();

                for (final ImageLocations image : imageLocationsList) {
                    TypedFile typedFile = new TypedFile("multipart/form-data", new File(image.getFilePath()));

                    uploadService.upload(typedFile, new Callback<UploadServerResponseModel>() {

                        @Override
                        public void success(UploadServerResponseModel myResponseModel, Response response) {
                            Log.d(LOG, "Image " + image.getFilePath() + " was successfully posted to VK");

                            VKParameters vkParams = new VKParameters();
                            vkParams.put("photo", myResponseModel.getPhoto());
                            vkParams.put("server", myResponseModel.getServer());
                            vkParams.put("hash", myResponseModel.getHash());

                            performFinalStepOfSavingImages(vkParams);
                        }

                        private void performFinalStepOfSavingImages(VKParameters vkParams) {
                            synchronized (lock) {
                                if (!isImagePostingOk) {
                                    Log.e(LOG, "Since something go wrong during saving of image to VK server - stop whole wall posting process");
                                    return;
                                }

                                successImageUploadingParameters.add(vkParams);

                                if (successImageUploadingParameters.size() != reqNumbOfPics) {
                                    return;
                                }
                            }

                            Log.d(LOG, "All images were successfully posted to VK, number of images: " + reqNumbOfPics);

                            for (VKParameters params : successImageUploadingParameters) {
                                VKApi.photos().saveWallPhoto(params).executeWithListener(new VKRequest.VKRequestListener() {
                                    @Override
                                    public void onComplete(VKResponse response) {
                                        super.onComplete(response);
                                        performFinishStepOfWallPosting(response);
                                    }

                                    private void performFinishStepOfWallPosting(VKResponse response) {
                                        synchronized (lock) {
                                            if (!isParametersSendingOk) {
                                                Log.e(LOG, "Since something go wrong during sending of image parameters VK server - stop whole wall posting process");
                                                return;
                                            }

                                            successParametersSendingResponses.add(response);

                                            if (successParametersSendingResponses.size() != reqNumbOfPics) {
                                                return;
                                            }
                                        }

                                        List<String> attachmentParams = new ArrayList<String>(reqNumbOfPics);

                                        for (VKResponse resp : successParametersSendingResponses) {
                                            String photoId = null;
                                            String ownerId = null;
                                            try {
                                                photoId = resp.json.getJSONArray("response").getJSONObject(0).getString("id");
                                                ownerId = resp.json.getJSONArray("response").getJSONObject(0).getString("owner_id");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                                return;
                                            }

                                            String aParam = VKApiConst.PHOTO + ownerId + "_" + photoId;

                                            attachmentParams.add(aParam);
                                        }

                                        String allAttachParamsInOneString = android.text.TextUtils.join(",", attachmentParams);
                                        Log.d(LOG, "Result string with attachments params: " + allAttachParamsInOneString);

                                        double longitude = 0;
                                        double latitude = 0;
                                        try {
                                            longitude = App.getInstance(activity).getComponent().uController().get().getLongitude();
                                            latitude = App.getInstance(activity).getComponent().uController().get().getLatitude();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        // SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(); older
                                        Boolean publishVkSignature = Preferences.getInstance().loadPublishVlSing();

                                        Boolean checkBoxMyWall = Preferences.getInstance().loadPublishVkWall();

                                        Boolean publishVkGroupWall = Preferences.getInstance().loadVkGroupWall();

                                        Boolean publishVkGroupWallByOwner = Preferences.getInstance().loadVkOwner();

                                        String signature = "";
                                        if (publishVkSignature) {
                                            signature = "\n\n\n Опубликованно с помощью travelmap.su "; // Потом можно дробавить id пользователя в ссылку на его
                                        }

                                        if (checkBoxMyWall) {
                                            postToId = VKAccessToken.currentToken().userId;
                                        }

                                        String prefixId = "";
                                        String fromGroupVal = "";
                                        String groupIds;
                                        if (publishVkGroupWall
                                                && (groupIds = Preferences.getInstance().loadCheckBoxGroupChoice()).length() > 0) {
                                            prefixId = "-";
                                            postToId = groupIds;
                                            if (publishVkGroupWallByOwner) {
                                                fromGroupVal = "0";
                                            }
                                        }

                                        final String[] separated = postToId.split(",");
                                        for (int j = 0; j < separated.length; j++) {
                                            VKRequest request = VKApi.wall().post(VKParameters.from(
                                                    VKApiConst.OWNER_ID, prefixId + separated[j],
                                                    VKApiConst.MESSAGE, text + signature,
                                                    VKApiConst.FROM_GROUP, 1/*admin*/,
                                                    VKApiConst.LAT, latitude,
                                                    VKApiConst.LONG, longitude,
                                                    VKApiConst.ATTACHMENTS, allAttachParamsInOneString));
                                            request.attempts = 8;
                                            final Integer count = j;
                                            request.executeWithListener(new VKRequest.VKRequestListener() {
                                                @Override
                                                public void onComplete(VKResponse response) {
                                                    Log.w(LOG, "Wall response  " + response.responseString);
                                                    successImageUploadingParameters.clear();
                                                    successParametersSendingResponses.clear();
                                                    callback.onSuccess(separated[count]);
                                                }

                                                @Override
                                                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                                                    successImageUploadingParameters.clear();
                                                    successParametersSendingResponses.clear();
                                                    Log.e(LOG, "attemptFailed during request of url for uploading. Attempt number: " + attemptNumber);
                                                    callback.onError("Attempt to make post failed");
                                                }

                                                @Override
                                                public void onError(VKError error) {
                                                    successImageUploadingParameters.clear();
                                                    successParametersSendingResponses.clear();
                                                    Log.e(LOG, "Error during request of url for upload: " + error.errorMessage);
                                                    callback.onError("Error during wall post");
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                                        super.attemptFailed(request, attemptNumber, totalAttempts);
                                        synchronized (lock) {
                                            isParametersSendingOk = false;
                                            successImageUploadingParameters.clear();
                                            successParametersSendingResponses.clear();
                                        }
                                        Log.e(LOG, "attemptFailed during request of url for uploading. Attempt number: " + attemptNumber);
                                        callback.onError("Attempt to make post failed");
                                    }

                                    @Override
                                    public void onError(VKError error) {
                                        super.onError(error);
                                        synchronized (lock) {
                                            isParametersSendingOk = false;
                                            successImageUploadingParameters.clear();
                                            successParametersSendingResponses.clear();
                                        }
                                        Log.e(LOG, "Error during request of url for upload: " + error.errorMessage);
                                        callback.onError("Error during wall post");
                                    }
                                });
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("Callback", "Fail to post photo");
                            synchronized (lock) {
                                isImagePostingOk = false;
                                successImageUploadingParameters.clear();
                            }
                            callback.onError("Error during wall post");
                        }
                    });
                }
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                super.attemptFailed(request, attemptNumber, totalAttempts);
                Log.e(LOG, "attemptFailed during request of url for uploading. Attempt number: " + attemptNumber);
                callback.onError("Attempt to make post failed");
            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Log.e(LOG, "Error during request of url for upload: " + error.errorMessage);
                callback.onError("Error during wall post");
            }
        });

        return true;
    }

    /**
     * save user info from vk only after vk auth!
     */
    public static void storeByServerUserInfo(final String password, final IVkPoster callback) {

        VKRequest request = VKApi.users()
                .get(VKParameters.from("fields",
                        "status, bdate, photo_50, photo_100, photo_200, city, verified"));

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                String email = VKAccessToken.currentToken().email;// == null ? null : VKAccessToken.currentToken().email;
                try {
                    User loginUser = getLogin(response, email, password);
                    callback.onSuccess(loginUser);
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError("Exception");
                }
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.w(LOG, "storeUserInfo failed on " + attemptNumber);
            }

            @Override
            public void onError(VKError error) {
                Log.w(LOG, "storeUserInfo failed on " + error.toString());
            }
        });
    }

    @NonNull
    private static User getLogin(VKResponse response, String email, String password) throws JSONException {
        if (password == null) throw new JSONException("Error password");
        if (BuildConfig.DEBUG) Log.e(LOG,
                VKAccessToken.currentToken().accessToken + " " + VKAccessToken.currentToken().secret
                        + " " + VKAccessToken.currentToken().userId);
        final User socialUser = new FormatJSON().takeVkUser(
                response.json.getJSONArray("response"));
        if (email == null) {
            email = socialUser.getVkId();
        }
        socialUser.setEmail(email);
        socialUser.setPassword(new Crypto().getHashPass(password));
        return socialUser;
    }


    public static void openGroupsDialog(final Activity act) {
        if (!isLoggedIn()) {
            start(act);
        } else {
            groupsDialog(act);
        }
    }

    public static void selectedCheckBoxMyWall(final Activity act) {
        if (!isLoggedIn()) {
            start(act);
        }
    }

    private static void groupsDialog(final Activity act) {
        VKRequest request = VKApi.groups()
                .get(VKParameters.from("user_id", VKAccessToken.currentToken().userId, "filter",
                        "admin, editor, moder", "extended", 1));
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    ArrayList<PeopleModel> list = new FormatJSON().takeVkGroupsResponse(response.json);
                    MenuVkGroupsDialogsFragment fragment = new MenuVkGroupsDialogsFragment();
                    fragment.setList(list);
                    act.getFragmentManager()
                            .beginTransaction()
                            .replace(android.R.id.content, fragment)
                            .commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.w(LOG, "Groups.get response failed on " + attemptNumber);
            }

            @Override
            public void onError(VKError error) {

            }
        });
    }

    public static boolean isLoggedIn() {
        return VKSdk.isLoggedIn();
    }

    public static void logout() {
        if (isLoggedIn()) {
            VKSdk.logout();
        }
    }

    public static IVkPoster getVkPoster() {
        return vkposter;
    }

    public interface IVkPoster {
        void onSuccess(Object obj);

        void onError(String error);
    }
}
