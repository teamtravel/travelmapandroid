package com.travelmap3;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;

import com.sendbird.android.SendBird;
import com.travelmap3.dagger.AppComponent;
import com.travelmap3.dagger.AppModule;
import com.travelmap3.dagger.DaggerAppComponent;
import com.travelmap3.dagger.InternetModule;
import com.travelmap3.dagger.NoteModule;
import com.travelmap3.dagger.PlaceModule;
import com.travelmap3.dagger.SharedModule;
import com.travelmap3.dagger.TrackerModule;
import com.travelmap3.dagger.UserModule;
import com.travelmap3.utils.Preferences;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;


/**
 * Created by An on 13.11.2015.
 */
public class App extends Application {
    private static final String GIT_SHA_KEY = "GIT_SHA_KEY";
    private static final String BUILD_TIME_KEY = "BUILD_TIME_KEY";

    public static final String APP_ID = "F947B524-EE98-4771-962C-18F81A9E7A28";

    public static App getInstance(Context context) {
        return (App) context.getApplicationContext();
    }

    private AppComponent component;

    public void onCreate() {
        Fabric.with(this, new Crashlytics());
        Crashlytics.setString(GIT_SHA_KEY, "1");
        Crashlytics.setString(BUILD_TIME_KEY, "" + System.currentTimeMillis());
        Crashlytics.getInstance().setDebugMode(BuildConfig.DEBUG);

        // Create an InitializerBuilder
        Stetho.InitializerBuilder initializerBuilder =
                Stetho.newInitializerBuilder(this);
        // Enable Chrome DevTools
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this)
        );
        // Enable command line interface
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this)
        );
        // Use the InitializerBuilder to generate an Initializer
        Stetho.Initializer initializer = initializerBuilder.build();
        // Initialize Stetho with the Initializer
        Stetho.initialize(initializer);

        //Fabric.with(this, new Crashlytics());
        Preferences.getInstance(this);
        try {
            // MultiDex.install(this);
            VKAccessTokenTracker.startTracking();
            VKSdk.initialize(this);
            SendBird.init(this, APP_ID);
            SendBird.DEBUG = BuildConfig.DEBUG;
            component = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .internetModule(new InternetModule())
                    .noteModule(new NoteModule())
                    .placeModule(new PlaceModule())
                    .sharedModule(new SharedModule())
                    .trackerModule(new TrackerModule())
                    .userModule(new UserModule()).build();

            component.updateUsers().getWithServer();

            //   component.uController();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate();
    }
//
//    synchronized public Tracker getDefaultTracker() {
//        if (mTracker == null) {
//            if (BuildConfig.DEBUG)
//                mTracker = GoogleAnalytics.getInstance(this).newTracker("UA-72941235-1"); // false id app
//            else
//                mTracker = GoogleAnalytics.getInstance(this).newTracker("UA-72949855-1"); // true ID app
//        }
//        return mTracker;
//    }


    com.vk.sdk.VKAccessTokenTracker VKAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                //todo check if user auth by vk and relogin
            }
        }
    };

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public AppComponent getComponent() {
        return component;
    }
}

