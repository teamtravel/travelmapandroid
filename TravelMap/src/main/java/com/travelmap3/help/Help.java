package com.travelmap3.help;

public class Help {

  private String message;
  private long time;

  public Help() {
    message = "";
    time = System.currentTimeMillis();
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public long getTime() {
    return time;
  }

  public void setTime(long time) {
    this.time = time;
  }

}
