/**
 *
 */
package com.travelmap3.help;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.utils.Preferences;

/**
 * @author andre
 */

@SuppressLint("NewApi")
public class MenuHelpFragment extends Fragment {

  private final String LOG = this.getClass().getName();

  private EditText helpText;
  private Preferences preferences;
  private TextView textWarning;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View rootView = inflater.inflate(R.layout.fragment_menu_help, container,
        false);

    helpText = (EditText) rootView.findViewById(R.id.textHelp);
    textWarning = (TextView) rootView.findViewById(R.id.textWarning);
    final Button buttonSendHelp = (Button) rootView.findViewById(R.id.sendHelp);
    preferences = Preferences.getInstance();
    if (preferences.isClickHelp()) {
      buttonSendHelp.setText(R.string.help_you_can_cancel_request);
    }

    buttonSendHelp.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        // TODO можно сделать проверку на частоту отправки сообщения
        // этого пользователя.
        // Например если сегодня пользователь уже оптравлял, то больше
        // нельзя.Тут просто надо cleatClickHelp сделать
        try {
          if (preferences.isClickHelp()) {

            ifYouWantToCancel(buttonSendHelp);

          } else {

            ifYouWantToSend(buttonSendHelp);
          }
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

    });
    setRetainInstance(true);

    App application = (App) getActivity().getApplication();
//    Tracker mTracker = application.getDefaultTracker();
//    mTracker.setScreenName("Screen Help");
//    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    return rootView;
  }

  protected void ifYouWantToCancel(Button btn) throws Exception {
    //  new TransferDifferentData(getActivity(), LINK.HELP).send("cancel");
    Toast.makeText(getActivity(), R.string.help_call_of_help_canceled, Toast.LENGTH_SHORT)
        .show();
    btn.setText(R.string.help_call_for_help);
    btn.setActivated(false);
    btn.setEnabled(false);
    preferences.clearClickHelp();
  }

  protected void ifYouWantToSend(Button btn) throws Exception {
    if (helpText.getText().length() < 5) {
      textWarning.setText(R.string.help_reason_description);
    } else {
      //  new TransferDifferentData(getActivity(), LINK.HELP)
      //        .send(makeHelp(helpText.getText().toString()));
      Toast.makeText(getActivity(), R.string.help_was_successfully_sent, Toast.LENGTH_SHORT)
          .show();
      preferences.saveClickHelp();
      btn.setText(R.string.help_cancel_request);
    }
  }

  protected Help makeHelp(String mes) {
    Log.d(LOG, "makeHelp");
    Help h = new Help();
    h.setMessage(mes);
    return h;
  }
}