package com.travelmap3.service;

import android.util.Log;

import com.travelmap3.BuildConfig;
import com.travelmap3.UserController;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.interesplace.ManagerPLace;
import com.travelmap3.interesplace.Place;
import com.travelmap3.interesplace.PlaceDB;
import com.travelmap3.notes.Notes;
import com.travelmap3.notes.NotesController;
import com.travelmap3.notes.NotesDB;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;

import java.util.ArrayList;
import java.util.HashSet;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by An on 01.01.2016.
 * Class for check information in local bd deffered com.travelmap3.message, notes, tracker
 * location,
 * and send data on server
 * Алгоритм.
 * Для каждых данных есть своей метод отправки.
 * В таком методе, мы проверяем есть ли в бд отложенные сообщения, берем их, отпраляем, если ответ
 * удачный то помечаем в бд как успешно отправленные.
 */
public class NetworkManagerDeffered {
    private static final String LOG = "NetworkManagerDeffered";
    private final UserController userController;

    public NotesDB mNoteDB;

    public PlaceDB mPlacesDB;

    public InternetConnect internetConnect;

    ManagerPLace managerPLace;

    NotesController notesController;

    @Inject
    public NetworkManagerDeffered(NotesDB notesDB, PlaceDB db, InternetConnect internetConnect, ManagerPLace managerPLace, UserController userController, NotesController notesController) {
        this.mNoteDB = notesDB;
        this.mPlacesDB = db;
        this.internetConnect = internetConnect;
        this.managerPLace = managerPLace;
        this.userController = userController;
        this.notesController = notesController;
    }

    public void send() {
        message();
        notes();
        tracker();
        // places();
    }

    private void places() {
        if (mPlacesDB.isNotSync()) {

            Log.i(LOG, "true data in local bd places");
            // NotesController nc = new NotesController(App.applicationContext);
            HashSet<Place> notesArrayList = mPlacesDB.getDifferedRecord();
            if (notesArrayList != null) {
                for (final Place place : notesArrayList) {
                    place.setId(userController.get().getId());
                    if (internetConnect.isActiveWiFiConnection()) {
                        managerPLace.sendServer(place, new CallBackResponse() {
                            @Override
                            public void onSuccess(Object obj) {
                                managerPLace.safeServerFlag(place.getGps().getTime());
                            }

                            @Override
                            public void onError(Object obj) {
                                Log.e("LOG", "Error NetWork ManagerPLace Differed Post Places");
                            }
                        });
                    }
                }
            }
        } else {
            Log.i(LOG, "false data in local bd");
        }
    }

    private void tracker() {

    }

    private void message() {
//        if (mMessageDB.isNotSync()) {
//
//        }
    }

    private void notes() {
        if (mNoteDB.isNotSync()) {
            // SEND DATA
            Log.i(LOG, "true data in local bd notes");
            // NotesController nc = new NotesController(App.applicationContext);
            Notes note = null;
            ArrayList<Notes> notesArrayList = null;
            notesArrayList = mNoteDB.getDifferedRecord();
            if (notesArrayList != null) {
                int i = 0;
                while (i < notesArrayList.size()) {
                    note = notesArrayList.get(i);
                    i++;
                    Log.e(LOG, " controllerPlace get name notes:" + note.getTextTitleNote());
                    note.setUid(userController.get().getId());
                    if (internetConnect.isActiveWiFiConnection()) {
                        sendNote(note);
                    }
                }
            }
        } else {
            Log.e(LOG, "false data in local bd");
        }
    }


    private void sendNote(final Notes n) {
        notesController.set(n);
        RetrofitService.getInstanceAPI()
                .postNote(Preferences.getInstance().loadToken(), Config.VERSION_API, n,
                        new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                notesController.saveServerFlag(true);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                if (BuildConfig.DEBUG) {
                                    Log.e(LOG, " Ошибка");
                                }
                            }
                        });
    }
}