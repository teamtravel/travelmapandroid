package com.travelmap3.service;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GpsTrackerServices extends IntentService {

    private static final String LOG = "GpsTrackerServices";
    private LocationRequest locationRequest;
    private GoogleApiClient locationClient;
    private LocationListener locationListener;

    public GpsTrackerServices() {
        super(LOG);
    }


    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.wtf(LOG, "onStartCommand GpsTracker");
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            location();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void usesLocationChanged(Location location) {
        if (location != null) {
            if (BuildConfig.DEBUG)    Log.e(LOG, "position: " + location.getLatitude() + ", " + location.getLongitude() + " accuracy: " + location.getAccuracy());

            // we have our desired accuracy of 400 meters so lets quit this service,
            // onDestroy will be called and stop our location uodates
            if (location.getAccuracy() < 400.0f) {
                location.setTime(System.currentTimeMillis());
                sendLocationDataToWebsite(location);
            }
        }
    }


    private void location() {
        int  DISPLACEMENT = 1500; //1.5km;
            locationRequest = new LocationRequest().setInterval(40000 /*33 * 2 min*/).setFastestInterval(150000).setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY).setSmallestDisplacement(DISPLACEMENT);
        locationClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Location loc = LocationServices.FusedLocationApi.getLastLocation(locationClient);
                Log.i(LOG, "loc" + (loc == null ? "null" : loc.getAccuracy()));
                usesLocationChanged(loc);
                startLocationUpdates();
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.i("MainUI", connectionResult.toString());
            }
        }).addApi(LocationServices.API).build();

        if (locationClient != null)
            if (!locationClient.isConnected())
                locationClient.connect();
    }

    private void startLocationUpdates() {
        if (locationClient != null && locationListener == null)
            if (locationClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(locationClient, locationRequest, locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        Location loc = LocationServices.FusedLocationApi.getLastLocation(locationClient);
                        Log.i(LOG, "loc" + (loc == null ? "null" : loc.getAccuracy()));
                        Log.i("LocationUpdated", "" + location.toString());
                        usesLocationChanged(loc);
                    }
                });
    }

    private void stopLocationUpdates() {
        if (locationClient != null && locationListener != null)
            LocationServices.FusedLocationApi.removeLocationUpdates(locationClient, locationListener);
        locationListener = null;
    }

    private void sendLocationDataToWebsite(final Location location) {

        if (isMoveLocation(location)) {
            Log.d(LOG, " SendGpsTrackerToServer ");
            App.getInstance(this).getComponent().uController().get().setLatitude(location.getLatitude());
            App.getInstance(this).getComponent().uController().get().setLongitude(location.getLongitude());
            App.getInstance(this).getComponent().trackerController().save(location);
            try {
                RetrofitService.getInstanceAPI().postLocation(Preferences.getInstance().loadToken(), Config.VERSION_API, App.getInstance(this).getComponent().uController().get(), new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        // Save in database server flag = true and save last time updating
                        App.getInstance(GpsTrackerServices.this).getComponent().trackerController().saveServerFlag(location.getTime());
                        Preferences.getInstance().saveLastUpdatingGpsServer(System.currentTimeMillis());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        try {
                            if (BuildConfig.DEBUG) Log.e(LOG, "faile send gps tracker " + error + " status " + error.getResponse().getStatus());
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Проверка на то двигался ли чувак или просто стоит на месте или около одного места. Фильтруем кароч
     * @param location
     * @return
     */
    private boolean isMoveLocation(Location location) {
        return Math.round(App.getInstance(this).getComponent().uController().get().getLongitude() * 1000) != Math.round(1000 * location.getLongitude()) &&
                Math.round(10000 * App.getInstance(this).getComponent().uController().get().getLatitude()) != Math.round(location.getLatitude() * 10000);
    }

}

