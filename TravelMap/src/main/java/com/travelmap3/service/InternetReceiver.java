package com.travelmap3.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.travelmap3.App;
import com.travelmap3.utils.InternetConnect;

public class InternetReceiver extends BroadcastReceiver {
    private static final String LOG = "BroadcastReceiver";
    private InternetConnect internetConnect;


    private NetworkManagerDeffered networkManagerDeffered;

    public InternetReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        this.networkManagerDeffered = App.getInstance(context).getComponent().networkManagerDif();
        this.internetConnect = App.getInstance(context).getComponent().getInternetConnect();
        Bundle extras = intent.getExtras();
        NetworkInfo info = (NetworkInfo) extras
                .getParcelable("networkInfo");
        NetworkInfo.State state = info.getState();
        if (state == NetworkInfo.State.CONNECTED &&
                internetConnect.isActiveWiFiConnection()) {
            /*
            Появился интернет,
            Проверяем есть ли отложенные сообщения, записи, трекер.
            И посылаем на сервер, если успешно то ставим пометку что успешно.
             */

            networkManagerDeffered.send();
        }
    }
}
