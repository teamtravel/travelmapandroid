package com.travelmap3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.travelmap3.api.UploadManager;
import com.travelmap3.auth.NewLoginActivity;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.Gps;
import com.travelmap3.model.User;
import com.travelmap3.notes.MapGoogleNoteActivity;
import com.travelmap3.notes.Notes;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.service.GpsTrackerServices;
import com.travelmap3.utils.Crypto;
import com.travelmap3.utils.Preferences;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends FragmentActivity {

    public static final String LOG = "MainActivity";
    private static final int REQUEST_FINE_LOCATION = 11;
    public static final String CHANGE_ADAPTER = "ChangeAdapter";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        work2();
        FirebaseCrash.report(new Exception("jaVasCript:/*-/*`/*\\`/*'/*\"/**/(/* */oNcliCk=alert() )//%0D%0A%0d%0a//</stYle/</titLe/</teXtarEa/</scRipt/--!>\\x3csVg/<sVg/oNloAd=alert()//>\\x3e"));
   }

    private void work2() {
        // init vk app and token tracker
        Preferences pref = Preferences.getInstance();
//        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
//        for (String s : fingerprints) {// || VkApi.isLoggedIn()
//            Log.e("LOG", s + " ");
//        }

        if (pref.loadToken() != null) {
            startActivity(new Intent(MainActivity.this, MainMenuActivity.class));
            App.getInstance(this).getComponent().networkManagerDif().send();// При запуске отправляем отложенные сообщения.
            finish();
        } else
            startActivity(new Intent(MainActivity.this, NewLoginActivity.class));
        finish();
    }

    private void testCloud() {
        dialogShow();
    }

//    private void testMapInsertTrackerLine() {
//        Location location = new Location("GPS");
//
//        for (int i = 1; i < 10; i++) {
//            location.setLatitude(55 + (double) ((i * i) / 4.0));
//            location.setLongitude(55 + (double) (i + 5) / 6.0);
//            TrackerController.getInstance().save(location);
//        }
//        for (int i = 1; i < 10; i++) {
//            location.setLatitude(12 + (double) ((i) / 4.0));
//            location.setLongitude(12 + (double) (i * i) / 3.0);
//            TrackerController.getInstance().save(location);
//        }
//    }

    private void dialogShow() {
        final String[] items = new String[]{"SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setTitle("Меню");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                }
            }
        });
        final android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    @Deprecated
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        Bitmap bitmap50 = null;
        Bitmap bitmap100 = null;
        Bitmap bitmap200 = null;
        if (requestCode == 1) {
            Uri selectedImageUri = data.getData();
            InputStream imageStream = null;
            InputStream imageStream100 = null;
            InputStream imageStream200 = null;
            try {
                imageStream = getApplicationContext().getContentResolver().openInputStream(selectedImageUri);
                bitmap50 = cropImage(imageStream, 50, 50, selectedImageUri);
                imageStream100 = getApplicationContext().getContentResolver().openInputStream(selectedImageUri);
                bitmap100 = cropImage(imageStream100, 100, 100, selectedImageUri);
                imageStream200 = getApplicationContext().getContentResolver().openInputStream(selectedImageUri); // необходимо использовать новый поток
                bitmap200 = cropImage(imageStream200, 200, 200, selectedImageUri);

                Log.e("LOG", bitmap100.getHeight() + " " + bitmap200.getHeight() + " " + bitmap50.getHeight());
                if (imageStream != null) {
                    imageStream.close();
                    assert imageStream100 != null;
                    imageStream100.close();
                    assert imageStream200 != null;
                    imageStream200.close();
                }
                try {
                    String url50 = new UploadManager().uploadBitmap(getApplicationContext(), bitmap50);
                    String url100 = new UploadManager().uploadBitmap(getApplicationContext(), bitmap100);
                    String url200 = new UploadManager().uploadBitmap(getApplicationContext(), bitmap200);

                    Log.e("LOG", url100 + " " + url200 + " " + url50);
                    User user = App.getInstance(this).getComponent().uController().get();
                    user.setPhoto200(url200);
                    user.setPhoto100(url100);
                    user.setPhoto50(url50);
                    App.getInstance(this).getComponent().uController().setUser(user, new CallBackResponse() {
                        @Override
                        public void onSuccess(Object obj) {
                            Toast.makeText(getApplicationContext(), "Аватарка обновленна!", Toast.LENGTH_SHORT).show();
                        }

                        @Nullable
                        @Override
                        public void onError(Object obj) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    assert imageStream != null;
                    imageStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public Bitmap cropImage(InputStream in, int w, int h, Uri selectedImageUri) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        BitmapFactory.decodeStream(in, null, options);
        in.close();
        int origWidth = options.outWidth; //исходная ширина
        int origHeight = options.outHeight; //исходная высота
        int bytesPerPixel = 2; //соответствует RGB_555 конфигурации
        int maxSize = 320 * 640 * bytesPerPixel; //Максимально разрешенный размер Bitmap
        int desiredSize = w * h * bytesPerPixel; //Максимально разрешенный размер Bitmap для заданных width х height
        if (desiredSize < maxSize) maxSize = desiredSize;
        int scale = 1; //кратность уменьшения
        int origSize = origWidth * origHeight * bytesPerPixel;
//высчитываем кратность уменьшения
        if (origWidth > origHeight) {
            scale = Math.round((float) origHeight / (float) h);
        } else {
            scale = Math.round((float) origWidth / (float) w);
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        in = getApplicationContext().getContentResolver().openInputStream(selectedImageUri);//Ваш InputStream. Важно - открыть его нужно еще раз, т.к второй раз читать из одного и того же InputStream не разрешается (Проверено на ByteArrayInputStream и FileInputStream).
        Bitmap bitmap = BitmapFactory.decodeStream(in, null, options); //Полученный Bitmap
        Log.e(LOG, "h nad w " + bitmap.getHeight() + " " + bitmap.getWidth() + " " + bitmap.getConfig().toString());
        return bitmap;
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "OKE", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "ONO", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
//
//    private void testTips() {
//        Preferences.getInstance().clear();
//        TipsCreate.getInstance().show(TIPS.INTERES_PLACE, getFragmentManager());
//        TipsCreate.getInstance().show(TIPS.TRACKER, getFragmentManager());
//        TipsCreate.getInstance().show(TIPS.PROFILE, getFragmentManager());
//        TipsCreate.getInstance().show(TIPS.NOTE_MENU, getFragmentManager());
//        TipsCreate.getInstance().show(TIPS.NOTE_FRAGMENT, getFragmentManager());
//        TipsCreate.getInstance().show(TIPS.MESSAGE, getFragmentManager());
//    }


//    private void testNetworkManager() {
//        new NetworkManagerDeffered().send();
//    }

//    private void testGetNote() {
//        SyncNote.getServer("1", 60,60);
//    }

//    private void testXmlParseWithAPINTP(int radius, double lat, double lon) {
//        String hash = "EhcKQwzD";
//        String login = "travelmap";
//        String xml = "\t\n" +
//                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
//                "<request action=\"get-objects-for-update\" >\n" +
//                "<point radius=\"" + radius + "\">" + lat + "," + lon + "</point>\n" +
//                "<attributes>\n" +
//                "<addressRegion />\n" +
//                "<photo/>\n" +
//                "<type/>\n" +
//                "<addressArea/>\n" +
//                "<addressLocality/>\n" +
//                "<streetAddress/>\n" +
//                "<name/>\n" +
//                "<url/>\n" +
//                "<geo/>\n" +
//                "<telephone/>\n" +
//                "<startDate/>\n" +
//                "<endDate/>\n" +
//                "<review/>\n" +
//                "<addressRegion/>\n" +
//                "</attributes>\n" +
//                "</request>";
//        RetrofitService.getInstanceNTP().getObjectPlace(login, hash, xml, new Callback<Response>() {
//            @Override
//            public void success(Response response, Response response2) {
//
//                RussiaTravelXmlParser rp = new RussiaTravelXmlParser();
//                InputStream is = new ByteArrayInputStream(parseBody(response2.getBody()).getBytes());
//                try {
//                    List<ObjectPlace> ar = rp.parse(is, 50, 55);
//                    ObjectPlace object = ar.get(0);
//
//                } catch (XmlPullParserException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                try {
//                    if (error.getKind() == RetrofitError.Kind.HTTP) {
//                        //Log.e("LOG", "1"+error.getResponse().getStatus());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
//
//    protected String parseBody(TypedInput result) {
//        BufferedReader reader = null;
//        StringBuilder sb = new StringBuilder();
//        try {
//
//            reader = new BufferedReader(new InputStreamReader(result.in()));
//
//            String line;
//            while ((line = reader.readLine()) != null) {
//                sb.append(line);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return sb.toString();
//    }


    private void testFullMapNotes() {
        for (int i = 0; i < 8; i++) {
            Notes notes = new Notes();
            notes.setAccess((short) 1);
            notes.setCommand(Notes.CREATE);
            int randomNum = 1 + (int) (Math.random() * i * 10);
            notes.setGps(new Gps(30.98812 + randomNum, 28.982761 - (double) (randomNum) / 2));
            notes.setTextNote("Пытаемся найти продуктовый магазин, но пока нам так ни кто и не сказал где он..");
            notes.setTextTitleNote("День пятый... ");
            notes.setTimeCreate(System.currentTimeMillis() + i * 25);
            //App.getInstance().saveInfo(notes);
        }
        startActivity(new Intent(MainActivity.this, MapGoogleNoteActivity.class));
    }


    public void testServiceGpsTracker() {
        Intent intent = new Intent(this, GpsTrackerServices.class);
        startService(intent.putExtra("name", 1));
    }

    private void testHash() {
        try {
            Crypto crypto = new Crypto();

            if ("3f2cd8e57b096fe7e4a78a5627e34ca3f885ad65a56e61c287cf4211bbc5949f".compareTo(new Crypto().getHashPass("password")) == 0)
                Log.w(LOG, "true");
            else Log.w(LOG, "false");
            Log.w(LOG, crypto.md5("password"));
            Log.w(LOG, crypto.md5TestMethod("password"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}
