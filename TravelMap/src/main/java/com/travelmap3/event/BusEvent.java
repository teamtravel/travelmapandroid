package com.travelmap3.event;

/**
 * Created by Andre on 01.02.2016.
 */
public class BusEvent {
  public final String message;

  public BusEvent(String message) {
    this.message = message;
  }
}
