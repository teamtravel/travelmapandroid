package com.travelmap3.event;

import java.util.List;

import static com.travelmap3.notes.Notes.ImageLocations;

/**
 * Created by tema_ on 26.06.2016.
 */
public class BusEventNoteImage {
    public String[] imagesURl;
    private List<ImageLocations> imageLocationsList;

    public BusEventNoteImage(String[] urls) {
        this.imagesURl = urls;
    }

    public BusEventNoteImage(List<ImageLocations> imageLocationsList) {
        this.imageLocationsList = imageLocationsList;
    }

    public List<ImageLocations> getImageLocationsList() {
        return imageLocationsList;
    }
}
