package com.travelmap3.event;

import android.location.Location;

/**
 * Created by Andre on 01.02.2016.
 */
public class LocationEvent {
  public final Location location;

  public LocationEvent(Location location) {
    this.location = location;
  }
}
