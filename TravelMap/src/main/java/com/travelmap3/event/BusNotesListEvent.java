package com.travelmap3.event;

import com.travelmap3.notes.Notes;

import java.util.ArrayList;

/**
 * Created by Andre on 22.03.2016.
 */
public class BusNotesListEvent {
  public final ArrayList<Notes> list;

  public BusNotesListEvent(ArrayList<Notes> notes) {
    list = notes;
  }
}
