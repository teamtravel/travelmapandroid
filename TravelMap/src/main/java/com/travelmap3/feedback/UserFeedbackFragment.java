package com.travelmap3.feedback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by root on 12/4/15.
 */
public class UserFeedbackFragment extends android.support.v4.app.Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_menu_user_review, container, false);
        final TextView textFeedBack = (TextView) root.findViewById(R.id.editTextDescrFeedback);
        root.findViewById(R.id.btnSendFeedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textFeedBack.getText().length() > 5) {
                    sendTextToServer(textFeedBack.getText().toString());
                    root.findViewById(R.id.btnSendFeedback).setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), R.string.feedback_text_too_small_for_review,
                            Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        setUpToolbar(root);

//    mTracker = App.getInstance(getActivity()).getDefaultTracker();
//    mTracker.setScreenName("Screen Feedback");
//    mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        return root;
    }

    private void sendTextToServer(String text) {
        RetrofitService.getInstanceAPI()
                .postFeedback(Preferences.getInstance().loadToken(), Config.VERSION_API,
                        new Feedback(text, App.getInstance(getActivity()).getComponent().uController().get().getId()),
                        new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                if (response != null) {
                                    if (response.getStatus() == 200) {
//                    mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction(getString(R.string.feedback_screen_action_title))
//                        .setLabel(getString(R.string.feedback_successfully_sent_to_server))
//                        .build());
                                        Toast.makeText(getActivity(), R.string.feedback_sent,
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
//                mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory("ui_action")
//                    .setAction(getString(R.string.feedback_screen_action_title))
//                    .setLabel(getString(R.string.feedback_not_successfully_sent_to_server))
//                    .build());
                                Toast.makeText(getActivity(), R.string.feedback_error, Toast.LENGTH_SHORT)
                                        .show();

                            }
                        });
    }

    private void setUpToolbar(View rootView) {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_burger);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }
}
