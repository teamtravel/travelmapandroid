package com.travelmap3.feedback;

/**
 * Created by An on 03.01.2016.
 */
public class Feedback {

  private String text;
  private int id;

  public Feedback(String text, Integer id) {
    setId(id);
    setText(text);
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
