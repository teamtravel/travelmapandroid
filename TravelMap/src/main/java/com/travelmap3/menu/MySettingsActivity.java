package com.travelmap3.menu;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.service.GpsTrackerServices;
import com.travelmap3.utils.Preferences;
import com.travelmap3.vk.VkApi;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

public class MySettingsActivity extends PreferenceActivity {
    private static final int itemActivityResultCode = 121;// for resultCode when returning from settings/map activity
    private static final String LOG = " Settings Activity";
    CheckBoxPreference friendMap;
    CheckBoxPreference commonMap;
    CheckBoxPreference gpsTracker;
    CheckBoxPreference publish_vk_group_wall;
    CheckBoxPreference publishVkWall;
    Preference publish_group_selector;
    Preference clear_app_cache;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.activity_my_settings);
        setEnableForCommonMap();
        offOnGpsTrackerStartService();
        setGroupPostSettings();
        App application = (App) getApplication();
//        mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen Settings");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    @SuppressWarnings("deprecation")
    private void offOnGpsTrackerStartService() {

        gpsTracker = (CheckBoxPreference) findPreference(Preferences.CHECK_BOX_SEND_GPS_TRACKER);
        gpsTracker.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (gpsTracker.isChecked()) {
                    startService(new Intent(getApplicationContext(), GpsTrackerServices.class));
                } else {
                    stopService(new Intent(getApplicationContext(), GpsTrackerServices.class));
                }
                return false;
            }
        });

    }

    private void setGroupPostSettings() {
        publish_vk_group_wall = (CheckBoxPreference) findPreference("publish_vk_group_wall");
        publish_group_selector = findPreference("publish_group_selector");
        publishVkWall = (CheckBoxPreference) findPreference("publish_vk_wall");
        clear_app_cache = findPreference("clear_app_cache");
        publish_group_selector.setEnabled(publish_vk_group_wall.isChecked());

        publish_vk_group_wall.setEnabled(!publishVkWall.isChecked());
        publishVkWall.setEnabled(!publish_vk_group_wall.isChecked());

        publish_vk_group_wall.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                publish_group_selector.setEnabled(publish_vk_group_wall.isChecked());
                publishVkWall.setEnabled(!publish_vk_group_wall.isChecked());
                publishVkWall.setChecked(false);
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction("Экран Settings")
//                        .setLabel("Вк на стену группы")
//                        .build());
                return false;
            }
        });
        publishVkWall.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                publish_vk_group_wall.setEnabled(!publishVkWall.isChecked());
                publish_vk_group_wall.setChecked(false);
                VkApi.selectedCheckBoxMyWall(MySettingsActivity.this);
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction("Экран Settings")
//                        .setLabel("Вк на стену пользователя")
//                        .build());
                return false;
            }
        });
        publish_group_selector.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction("Экран Settings")
//                        .setLabel("Вк выбор групп")
//                        .build());
                if (publish_vk_group_wall.isChecked()) {
                    VkApi.openGroupsDialog(MySettingsActivity.this);
                }
                return true;
            }
        });
        clear_app_cache.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                // trimCache();
                return true;
            }
        });
    }

    @SuppressWarnings("deprecation")
    private void setEnableForCommonMap() {
        friendMap = (CheckBoxPreference) findPreference("setViewOnFriend");
        commonMap = (CheckBoxPreference) findPreference("setViewOnMap");

        commonMap.setEnabled(friendMap.isChecked());

        friendMap.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction("Экран Settings")
//                        .setLabel("Вкл.Выкл показ на карте")
//                        .build());
                commonMap.setEnabled(friendMap.isChecked());
                commonMap.setChecked(false);
                /*
                 * Можно отображать на общей карте только тогда, когда
                 * установлено условие, что можно отображать и для друзей Нельзя
                 * отображать, на общей, и не отображать у друзей. Смысла это ни
                 * имеет! Иначе надо попровить вторую строчку.
                 */
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VkApi.openGroupsDialog(MySettingsActivity.this);
            }

            @Override
            public void onError(VKError error) {

                Toast.makeText(MySettingsActivity.this, R.string.my_settings_activity_vk_sign_error,
                        Toast.LENGTH_SHORT)
                        .show();
                publishVkWall.setChecked(false);
                publish_vk_group_wall.setChecked(false);
                publish_vk_group_wall.setEnabled(!publishVkWall.isChecked());
                publishVkWall.setEnabled(!publish_vk_group_wall.isChecked());
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        setResult(itemActivityResultCode);
        super.onBackPressed();
    }
}
