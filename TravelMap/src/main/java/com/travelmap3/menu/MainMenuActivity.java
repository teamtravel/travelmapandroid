package com.travelmap3.menu;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sendbird.android.SendBird;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.event.LocationEvent;
import com.travelmap3.feedback.UserFeedbackFragment;
import com.travelmap3.home.NearestObject.NearestObjectFragment;
import com.travelmap3.home.StartHomeFragment;
import com.travelmap3.interesplace.InterestingPlaceMapActivity;
import com.travelmap3.maps.GoogleMapCustomMarkerClusteringActivity;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.model.User;
import com.travelmap3.newchat.SendBirdChannelListActivity;
import com.travelmap3.newchat.SendBirdChatActivity;
import com.travelmap3.newchat.SendBirdMessagingActivity;
import com.travelmap3.newchat.SendBirdMessagingChannelListActivity;
import com.travelmap3.notes.MenuNotesFragment;
import com.travelmap3.notes.NoteActivity;
import com.travelmap3.notes.SyncNote;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.profile.ProfileActivity;
import com.travelmap3.service.GpsTrackerServices;
import com.travelmap3.tracker.TrackerMapsActivity;
import com.travelmap3.utils.CircleTransform;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;
import com.travelmap3.vk.VkApi;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by An on 28.09.2015.
 */


public class MainMenuActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private static final String LOG = "MainMenuActivity";
    public static final int ITEM_ACTIVITY_RESULT_CODE = 121;//for resultCode when returning from settings/map activity // NOT USED
    private static final int REQUEST_FINE_LOCATION = 2;
    public static final int TWO_WEEK = 1209600000;
    public static final String FRAGMENT_ROUTER = "fragment_show";
    private static String tagFrag = "newNote";
    private static String notesFragTag = "notesFrag";
    Preferences pref;
    private List<ObjectPlace> mObjectPlace;
    private Fragment mFragment;
    private DrawerLayout mDrawerLayout;
    private GoogleApiClient mGoogleApiClient;
    private NavigationView navigationView;
    private LocationRequest mLocationRequest;
    private LocationListener locationListener;
    private boolean isInMainFragment = true, isInNoteEditFragment = false;//used to return to main frag if backpressed
    public static final int REQUEST_SENDBIRD_CHAT_ACTIVITY = 100;
    public static final int REQUEST_SENDBIRD_CHANNEL_LIST_ACTIVITY = 101;
    public static final int REQUEST_SENDBIRD_MESSAGING_ACTIVITY = 200;
    public static final int REQUEST_SENDBIRD_MESSAGING_CHANNEL_LIST_ACTIVITY = 201;
    public static final int REQUEST_SENDBIRD_USER_LIST_ACTIVITY = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        pref = Preferences.getInstance();
        Log.i("LOG", "time live token: " + (System.currentTimeMillis() - Preferences.getInstance().loadTimeAuthUser()) + " " + 1209600000);
        if (App.getInstance(this).getComponent().uController().get() == null || System.currentTimeMillis() - Preferences.getInstance().loadTimeAuthUser() > TWO_WEEK) {
            Log.e("LOGGER", "EXIT");
            exitFromProgram();
            return;
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        View vi = navigationView.getHeaderView(0);
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startProfile();
            }
        });
        setMyDataUI();

        if (savedInstanceState == null && ifFirstEntryUser())
            startProfile(); // При первом входе после регистрации заходим в профиль!
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                displayView(menuItem.getItemId());
                return true;
            }
        });
        if (checkPlayServices() && checkPermissions()) {
            initLocation();
        }

        getNoteSynchronized(App.getInstance(this).getComponent().uController().get()); // получаем все записи дневника пользователя
        getPlacesUser();

    }


    /**
     * Функция для синхронизации записей дневников.
     * Запускаем после тогда как мы зашли в приложения первый раз
     * И подтягиваем все данные
     */
    static int flag_note = 1;

    public void getNoteSynchronized(User user) {

        if (flag_note == 1)
            try {
                new SyncNote(App.getInstance(this).getComponent().notesDB()).getServer(user.getToken(), user.getId(), user.getId());
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Ошибка синхронизации дневника", Toast.LENGTH_SHORT).show();
            } finally {
                flag_note = 2;
            }

    }

    private void getPlacesUser() {
        App.getInstance(this).getComponent().controller().getServerMyPlaces(this, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Log.i(LOG, "Получили мои места с сервера");
            }

            @Override
            public void onError(Object obj) {
                Log.e(LOG, "Не получили мои места с сервера");

            }
        });
    }

    private void initLocation() {
        int DISPLACEMENT = 1500; //1.5km;
        mLocationRequest = new LocationRequest().setInterval(3300000).setFastestInterval(1500).setPriority(LocationRequest.PRIORITY_LOW_POWER).setSmallestDisplacement(DISPLACEMENT);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle bundle) {
                        usesLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
                        startLocationUpdates();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.e("LOG", "location onConnectionSuspended");
                    }
                }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                        Log.w(LOG, " onConnectionFailed");
                        if (connectionResult.hasResolution()) {
                            Log.i("onConnectionApi", "Location services connection failed with code " + connectionResult.getErrorCode());
                        }
                    }
                }).addApi(LocationServices.API).build();
    }

    private void startProfile() {
        Intent intent = ProfileActivity.newIntent(this, App.getInstance(this).getComponent().uController().get().getId());
        startActivityForResult(intent, REQUEST_SENDBIRD_MESSAGING_ACTIVITY);
    }


    private void displayView(int id) {
        mFragment = null;
        String tagFrag = "";
        switch (id) {
            case R.id.navigation_item_message:
                isInMainFragment = false;
                openChat();
                break;
            case R.id.navigation_item_notes:
                isInMainFragment = false;
                mFragment = new MenuNotesFragment();
                tagFrag = notesFragTag;
                break;
            case R.id.navigation_item_tracker:
                isInMainFragment = false;
                openTrackerMapActivity();
                break;
            case R.id.navigation_item_exit:
                isInMainFragment = false;
                showExitDialog();
                break;
            case R.id.navigation_item_map:
                isInMainFragment = false;
                openMapActivity();
                break;
            case R.id.navigation_item_setting:
                isInMainFragment = false;
                openSettingsActivity();
                break;
            case R.id.navigation_item_user_review:
                isInMainFragment = false;
                mFragment = new UserFeedbackFragment();
                break;
            case R.id.navigation_item_interesting_place:
                isInMainFragment = false;
                openStartIntPlaceActivity();
                break;
            default:
                isInMainFragment = true;
                mFragment = new StartHomeFragment();
                disconnectApiClient();
                break;
        }

        if (mFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, mFragment, tagFrag)
                    .commit();
            if (id == R.id.navigation_main)
                connectApiClient();// При переходе, для быстрого обновления сначала отключает APi, а потом подключаем.
        }

    }


    private void openChat() {
        startMessagingChannelList();
        //startUserList();
    }

    private void startMessagingChannelList() {
        Intent intent = new Intent(MainMenuActivity.this, SendBirdMessagingChannelListActivity.class);
        Bundle args = SendBirdMessagingChannelListActivity.makeSendBirdArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName());
        intent.putExtras(args);

        startActivityForResult(intent, REQUEST_SENDBIRD_MESSAGING_CHANNEL_LIST_ACTIVITY);
    }

    private void joinMessaging(String channelUrl) {
        Intent intent = new Intent(MainMenuActivity.this, SendBirdMessagingActivity.class);
        Bundle args = SendBirdMessagingActivity.makeMessagingJoinArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName(), channelUrl);
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_MESSAGING_ACTIVITY);
    }

    private void startChannelList() { // GROUP CHANNEL
        Intent intent = new Intent(MainMenuActivity.this, SendBirdChannelListActivity.class);
        Bundle args = SendBirdChannelListActivity.makeSendBirdArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName());
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_CHANNEL_LIST_ACTIVITY);
    }

    private void startChat(String channelUrl) { // GROUP CHANNEL
        Intent intent = new Intent(MainMenuActivity.this, SendBirdChatActivity.class);
        Bundle args = SendBirdChatActivity.makeSendBirdArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName(), channelUrl);
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_CHAT_ACTIVITY);
    }

    private void startMessaging(String[] targetUserIds) {// GROUP CHANNEL and not only
        Intent intent = new Intent(MainMenuActivity.this, SendBirdMessagingActivity.class);
        Bundle args = SendBirdMessagingActivity.makeMessagingStartArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName(), targetUserIds);
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_MESSAGING_ACTIVITY);
    }


    private void openStartIntPlaceActivity() {
        startActivity(new Intent(this, InterestingPlaceMapActivity.class));
    }

    private void showExitDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Выход:")
                .setMessage("Вы действительно хотите выйти из TravelMap ?")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        exitFromProgram();
                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void openSettingsActivity() {
        startActivityForResult(new Intent(this, MySettingsActivity.class), ITEM_ACTIVITY_RESULT_CODE);
    }

    public void openMapActivity() {
        try {
//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory("ui_action")
//                    .setAction("Экран Диалогов")
//                    .setLabel("")
//                    .build());

        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivityForResult(new Intent(getApplicationContext(), GoogleMapCustomMarkerClusteringActivity.class), REQUEST_SENDBIRD_CHAT_ACTIVITY);
    }

    public void openTrackerMapActivity() {
        startActivity(new Intent(getApplicationContext(), TrackerMapsActivity.class));
    }

    private void signOut() {
        try {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Log.e("LOG OUT", status.toString() + "");
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    } // for testing


    private void exitFromProgram() {
        stopService(new Intent(this, GpsTrackerServices.class));
        App.getInstance(this).getComponent().controller().clearDB();
        App.getInstance(this).getComponent().notesController().clearDB();
        VkApi.logout();
        signOut();
        disconnect();
        pref.clear();
        pref.removeUserPreferences();
        finish();
    }

    private void disconnect() {
        SendBird.disconnect();
    }

    /**
     * if this first enter user in app then load /Activity with profile
     */
    private boolean ifFirstEntryUser() {
        if (pref.loadFirstEntryUser()) {
            pref.saveFirstEntryUser();
            return true;
        }
        return false;
    }


    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else
                Toast.makeText(this, "This device is not supported.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    private void checkMySettinsGpsTracker() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getBoolean(Preferences.CHECK_BOX_SEND_GPS_TRACKER, true) && checkPermissions() && pref.isModeTravel())
            startService(new Intent(this, GpsTrackerServices.class));

    }

    private void setMyDataUI() {
        TextView headerName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.drawer_header_text);
        ImageView imageAvatar = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.drawer_image_view);
        Glide.with(this).load(App.getInstance(this).getComponent().uController().get().getPhoto100()).transform(new CircleTransform(this)).into(imageAvatar);
        headerName.setText(App.getInstance(this).getComponent().uController().get().getFullName());

        // View router!=)
        if (getIntent() != null && getIntent().getIntExtra(FRAGMENT_ROUTER, 0) == 1) {
            displayView(R.id.navigation_item_notes);
        } else {
            displayView(R.id.navigation_main);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_map_open).setVisible(false);
        menu.findItem(R.id.action_btn_save).setVisible(false);
        menu.findItem(R.id.action_btn_copy_text).setVisible(false);
        menu.findItem(R.id.action_map_note).setVisible(false);
        menu.findItem(R.id.action_search_dialogs).setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callActivitySettings(MenuItem mi) {
        mi.setIntent(new Intent(this, MySettingsActivity.class));
    }


    public void onMakeNewNote(View v) {
        Intent intent = new Intent(this, NoteActivity.class);
        startActivity(intent);
        isInNoteEditFragment = true;
    }

    public void openNearestObjectFragment(List<ObjectPlace> objectPlaces) {
        mObjectPlace = objectPlaces;
        final Fragment frag = new NearestObjectFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, frag, tagFrag)
                .addToBackStack("")
                .commit();
    }

    public List<ObjectPlace> getObjectPlaces() {
        return mObjectPlace;
    }

    //Закрывать Drawer по нажатию системной кнопки «Назад»:

    @Override
    public void onBackPressed() {
        navigationView.getMenu().close();
        if (mDrawerLayout.isDrawerOpen(navigationView)) {
            mDrawerLayout.closeDrawers();
        } else if (!isInMainFragment && !isInNoteEditFragment)
            //sets checked item in navView and returns to main fragment
            navigationView.getMenu().performIdentifierAction(R.id.navigation_main, 0);
        else if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else {
            super.onBackPressed();

            //just to set value false anyway
            isInNoteEditFragment = false;

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disconnectApiClient();
    }


    @Override
    protected void onStart() {
        super.onStart();
        //connectApiClient();
    }

    private void connectApiClient() {
        if (mGoogleApiClient != null) {
            Log.e("LOG", "google connect");
            mGoogleApiClient.connect();
        }
    }

    private void disconnectApiClient() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            Log.e("LOG", "google disconnect");
        }
        //  stopLocationUpdates(); // GoogleApiClient is not connected yet.
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        stopLocationUpdates();
        if (mGoogleApiClient != null)
            if (mGoogleApiClient.isConnected())
                mGoogleApiClient.disconnect();


    }

    protected void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (mGoogleApiClient != null && locationListener != null)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
        locationListener = null;
        Log.e("LOG", "google stoplocation");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /**
         * Handle vk auth result
         */
        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VkApi.getVkPoster().onSuccess(null);
            }

            @Override
            public void onError(VKError error) {
                // User didn't pass Authorization
            }
        };
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_SENDBIRD_MESSAGING_CHANNEL_LIST_ACTIVITY && data != null) {
            joinMessaging(data.getStringExtra("channelUrl"));
        }

        if (resultCode == RESULT_OK && (requestCode == REQUEST_SENDBIRD_CHAT_ACTIVITY || requestCode == REQUEST_SENDBIRD_USER_LIST_ACTIVITY) && data != null) {
            startMessaging(data.getStringArrayExtra("userIds"));
        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_SENDBIRD_CHANNEL_LIST_ACTIVITY && data != null) {
            startChat(data.getStringExtra("channelUrl"));
        }
    }

    private void SendGpsToServer(final Location location) throws Exception {

        Log.e("LOG", "google send location gps");
        RetrofitService.getInstanceAPI().postLocation(App.getInstance(this).getComponent().uController().get().getToken(), Config.VERSION_API, App.getInstance(this).getComponent().uController().get(), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                Log.i(LOG, "code update location= " + response.getStatus());
                pref.saveLastUpdatingGpsServer(System.currentTimeMillis());
            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    if (error != null)
                        Log.e(LOG, "faile update location" + error + " status " + error.getResponse().getStatus());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void changeInfoIntoForHomeFragment(Location loc) throws Exception {
        if (loc != null) {
            if (pref.isModeTravel()) {//и идет путешествие
                EventBus.getDefault().post(new LocationEvent(loc)); // Сообщаем фрагменту что надо обновиться
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkMySettinsGpsTracker();
        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected())
                mGoogleApiClient.connect();
            Log.e("LOG", "google resume connect");
        }


    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mLocationRequest == null)
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        usesLocation(location);
                        Log.i("LocationUpdated", "" + location.toString());
                    }
                });
    }

    private void usesLocation(Location location) {
        if (location == null) return;
        // Если мы находимся в путешествии и еще не инициализированный данные пользователя то изменяем данные.
        // Изменяем наш фрагмент с текущими данными.
        // При изменение наших координат( если они не совпадают с предыдущими) отправляем на сервак. Math.round - отбрасывает мелкие измеения.
        // Сохраняем последние полученные текущие координаты
        if (pref.isModeTravel() || (App.getInstance(this).getComponent().uController().get().getLatitude() == 0 && App.getInstance(this).getComponent().uController().get().getLongitude() == 0))
            try {
                boolean b1 = (Math.round(location.getLatitude() * 100000) == Math.round(App.getInstance(this).getComponent().uController().get().getLatitude() * 100000));
                boolean b2 = (Math.round(location.getLongitude() * 10000) == Math.round(App.getInstance(this).getComponent().uController().get().getLongitude() * 10000));
//            Log.d("LOCATION",  b1 + " " + b2);
//            Log.e("LOCATION", Math.round(location.getLatitude() * 100000) + " " +Math.round(App.getInstance(this).getComponent().uController().get().getLatitude() * 100000));
//            Log.e("LOCATION",Math.round(location.getLongitude() * 10000) + " " +  Math.round(App.getInstance(this).getComponent().uController().get().getLongitude() * 10000));
                if (!(Math.round(location.getLatitude() * 100000) == Math.round(App.getInstance(this).getComponent().uController().get().getLatitude() * 100000)
                        && Math.round(location.getLongitude() * 10000) == Math.round(App.getInstance(this).getComponent().uController().get().getLongitude() * 10000))) {
                    saveDBTracker(location);
                    try {
                        Log.e("LOG", "location sending");
                        App.getInstance(this).getComponent().uController().updateLocation(location); // там юзер отправляется с координатами
                        SendGpsToServer(location); // отправляем на сервер трекер
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                    if (pref.loadStartLongitude() == 0 && pref.loadStartLatitude() == 0 && location.getLatitude() != 0)
                        pref.saveStartLocation(location);
                    changeInfoIntoForHomeFragment(location);

                }
                App.getInstance(this).getComponent().uController().updateLocation(location); // сохраняем юзера координаты в синглтон на случай если метод выше не сработает
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    private void saveDBTracker(Location location) {
        Log.e("LOG", "google save database location");
        App.getInstance(this).getComponent().trackerController().save(location);
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermissions();
            return false;
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initLocation();
                }
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
