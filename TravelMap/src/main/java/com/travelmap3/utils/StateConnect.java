package com.travelmap3.utils;

/**
 * Created by andrey.gusenkov on 27/12/2016.
 * Для отслеживания состояния работы мессенджера.
 * Важно его всегда поддерживатьв режиме коннекта.
 * И перед отправкой всегда проверять
 */

public enum StateConnect {
    DISCONNECTED,
    CONNECTING,
    CONNECTED
}
