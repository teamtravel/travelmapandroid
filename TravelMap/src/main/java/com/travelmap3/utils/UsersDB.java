package com.travelmap3.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.db.SqliteDBHelper;
import com.travelmap3.model.User;

import java.util.HashSet;

import javax.inject.Inject;

/**
 * Класс реализует сохранение и считывания всех юзеров которые видны общей
 * карте для последующего их отображения на карте. Таблица USERS. Поля: все поля
 * класса User.
 * Пока только поля status, Latitude, longitude, Email, firstname;
 * Константы вынесены в интерфейс IuserDataBase
 *
 * @author An
 */

/**
 * Класс реализует сохранение и считывания только юзеров которые находится близко от нас(10-50 км).
 * Для последующего рассчета информации о приближающихся путешествеников. Таблица DB_TABLE_USERS.
 * Поля: все поля  класса User.
 * Пока только поля status, Latitude, longitude, Email, firstname;
 *
 * @author An
 */
public class UsersDB extends SqliteDBHelper {



    private final String LOG = this.getClass().getName();
    User user;
    Context context;

    @Inject
    public UsersDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }


    public void insertFast(HashSet<User> set) {
        // you can use INSERT only
        String sql = "INSERT OR REPLACE INTO " + DB_TABLE_USERS + " ( " +
                DB_COLUMN_ID + ", "
                + DB_COLUMN_LAT + ", "
                + DB_COLUMN_EMAIL + ", "
                + DB_COLUMN_LONG + ", "
                + DB_COLUMN_NIKNAME + ", "
                + DB_COLUMN_STATUS + ", "
                + DB_COLUMN_ABOUTME + ", "
                + DB_COLUMN_BIRTHDAY + ", "
                + DB_COLUMN_FROM_CITY + ", "
                + DB_COLUMN_LASTNAME + ", "
                + DB_COLUMN_PHOTO_50 + ", "
                + DB_COLUMN_PHOTO_100 + ", "
                + DB_COLUMN_PHOTO_200 + ", "
                + DB_COLUMN_LASTTIME + ", "
                + DB_COLUMN_TELEGRAM + ", "
                + DB_COLUMN_VISIBLE + " ) VALUES (  ?, ?, ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ,?, ? , ? , ?)";
        SQLiteDatabase db = this.getWritableDatabase();

        /*
         * According to the docs http://developer.android.com/reference/android/database/sqlite/SQLiteDatabase.html
         * Writers should use beginTransactionNonExclusive() or beginTransactionWithListenerNonExclusive(SQLiteTransactionListener)
         * to start a transaction. Non-exclusive mode allows database file to be in readable by other threads executing queries.
         */
        db.beginTransactionNonExclusive();
        // placeDb.beginTransaction();

        SQLiteStatement stmt = db.compileStatement(sql);
        int id = Preferences.getInstance().loadUserId();
        for (User user : set) {
            if (user.getId() != id) {
                stmt.bindString(1, DB_COLUMN_ID + " # " + user.getId());
                stmt.bindString(2, DB_COLUMN_LAT + " # " + user.getLatitude());
                stmt.bindString(3, DB_COLUMN_EMAIL + " # " + user.getEmail());
                stmt.bindString(4, DB_COLUMN_LONG + " # " + user.getLongitude());
                stmt.bindString(5, DB_COLUMN_NIKNAME + " # " + user.getFirstName());
                stmt.bindString(6, DB_COLUMN_STATUS + " # " + user.getStatusText());
                stmt.bindString(7, DB_COLUMN_ABOUTME + " # " + user.getAbout());
                stmt.bindString(8, DB_COLUMN_BIRTHDAY + " # " + user.getBirthday());
                stmt.bindString(9, DB_COLUMN_FROM_CITY + " # " + user.getFrom_city());
                stmt.bindString(10, DB_COLUMN_LASTNAME + " # " + user.getLastName());
                stmt.bindString(11, DB_COLUMN_PHOTO_50 + " # " + user.getPhoto50());
                stmt.bindString(12, DB_COLUMN_PHOTO_100 + " # " + user.getPhoto100());
                stmt.bindString(13, DB_COLUMN_PHOTO_200 + " # " + user.getPhoto200());
                stmt.bindString(14, DB_COLUMN_LASTTIME + " # " + user.getLastTime());
                stmt.bindString(15, DB_COLUMN_TELEGRAM + " # " + user.getTelegram());
                stmt.bindString(16, DB_COLUMN_VISIBLE + " # " + (user.getVisible() ? 0 : 1));
                stmt.bindString(17, DB_COLUMN_NEARBY_DISTANCE + " # " + distance(user));

                stmt.execute();
                stmt.clearBindings();
            }
        }
        stmt.close();
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insert(HashSet<User> set) {
        /*
         * Перед сохранение удаляем всех старых юзеров. Пробегаем в цикле по
         * всему HashSet и сохраняем все поля конкретного Юзера.
         */
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            clearAllT(DB_TABLE_USERS);

            int id = Preferences.getInstance().loadUserId();
            for (User user : set) {
                if (user.getId() != id) {
                    ContentValues contentValues = new ContentValues();
                    //16 number field
                    contentValues.put(DB_COLUMN_ID, user.getId());
                    contentValues.put(DB_COLUMN_LAT, user.getLatitude());
                    contentValues.put(DB_COLUMN_EMAIL, user.getEmail());
                    contentValues.put(DB_COLUMN_LONG, user.getLongitude());
                    contentValues.put(DB_COLUMN_NIKNAME, user.getFirstName());
                    contentValues.put(DB_COLUMN_STATUS, user.getStatusText());
                    contentValues.put(DB_COLUMN_ABOUTME, user.getAbout());
                    contentValues.put(DB_COLUMN_BIRTHDAY, user.getBirthday());
                    contentValues.put(DB_COLUMN_FROM_CITY, user.getFrom_city());
                    contentValues.put(DB_COLUMN_LASTNAME, user.getLastName());
                    contentValues.put(DB_COLUMN_PHOTO_50, user.getPhoto50());
                    contentValues.put(DB_COLUMN_PHOTO_100, user.getPhoto100());
                    contentValues.put(DB_COLUMN_PHOTO_200, user.getPhoto200());
                    contentValues.put(DB_COLUMN_LASTTIME, user.getLastTime());
                    contentValues.put(DB_COLUMN_TELEGRAM, user.getTelegram() == null ? "" : user.getTelegram());
                    contentValues.put(DB_COLUMN_VISIBLE, user.getVisible() ? 0 : 1);
                    contentValues.put(DB_COLUMN_NEARBY_DISTANCE, distance(user));
                    long printfCode = getWriteDB(contentValues);
                    // Log.i(LOG, "Map set in placeDb get write return = " + printfCode);
                } //else Log.wtf(LOG, "Сервер прислал меня");

            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    private Float distance(User user) {
        if (BuildConfig.DEBUG)
            Log.i(LOG, "Растояние между пользователем " + user.getFullName() + " " + user.getId() + "!=" + Preferences.getInstance().loadUserId() + "и мной, равно = " + new CalculateForGps().distance(user.getLatitude(), user.getLongitude(),
                    App.getInstance(context).getComponent().uController().get().getLatitude(),
                    App.getInstance(context).getComponent().uController().get().getLongitude()));
        return
                new CalculateForGps().distance(user.getLatitude(), user.getLongitude(),
                        App.getInstance(context).getComponent().uController().get().getLatitude(),
                        App.getInstance(context).getComponent().uController().get().getLongitude());
    }

    private long getWriteDB(ContentValues contentValues) {
        return getWritableDatabase().insert(DB_TABLE_USERS, null, contentValues);
    }


    public HashSet<User> get() {
        return getUsers();
    }

    public HashSet<User> get(String uid) {
        return getUsers(uid);
    }


    private HashSet<User> getUsers(String... args) {
        HashSet<User> set = new HashSet<User>();

        Cursor c = null;
        try {
            String where = null;
            if (args.length > 0) where = DB_COLUMN_ID + "=" + args[0];
            c = getReadableDatabase().query(DB_TABLE_USERS, null, where, null, null, null, null);

            if (c.moveToFirst()) {

                int lon = c.getColumnIndex(DB_COLUMN_LONG);
                int email = c.getColumnIndex(DB_COLUMN_EMAIL);
                int id = c.getColumnIndex(DB_COLUMN_ID);
                int lat = c.getColumnIndex(DB_COLUMN_LAT);
                int status = c.getColumnIndex(DB_COLUMN_STATUS);
                int nikname = c.getColumnIndex(DB_COLUMN_NIKNAME);
                int lastname = c.getColumnIndex(DB_COLUMN_LASTNAME);

                int lasttime = c.getColumnIndex(DB_COLUMN_LASTTIME);
                int birthday = c.getColumnIndex(DB_COLUMN_BIRTHDAY);
                int about = c.getColumnIndex(DB_COLUMN_ABOUTME);
                int userphoto50 = c.getColumnIndex(DB_COLUMN_PHOTO_50);
                int userphoto100 = c.getColumnIndex(DB_COLUMN_PHOTO_100);
                int userphoto200 = c.getColumnIndex(DB_COLUMN_PHOTO_200);
                int fromcity = c.getColumnIndex(DB_COLUMN_FROM_CITY);
                int visible = c.getColumnIndex(DB_COLUMN_VISIBLE);
                int telegram = c.getColumnIndex(DB_COLUMN_TELEGRAM);
                int distance = c.getColumnIndex(DB_COLUMN_NEARBY_DISTANCE);

                do {
                    set.add(
                            getUser(c, lon, email, id, lat, status, nikname, lastname, lasttime, birthday, about, userphoto50, userphoto100, userphoto200, fromcity, visible, distance, telegram));
                } while (c.moveToNext());

            } else {
                Log.i(LOG, "No Columns");
            }
        } catch (Exception e) {
            Log.i(LOG, "error" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }


        return set;
    }

    private User getUser(Cursor c, int lon, int email, int id, int lat, int status, int nikname, int lastname, int lasttime, int birthday, int about, int userphoto50, int userphoto100, int userphoto200, int fromcity, int visible, int distance, int telegramm) {

        user = null;
        user = new User();
        user.setId(c.getInt(id));
        user.setEmail(c.getString(email));
        user.setStatusText(c.getString(status));
        user.setLongitude(c.getString(lon));
        user.setLatitude(c.getString(lat));
        user.setFirstName(c.getString(nikname));
        user.setPhoto50(c.getString(userphoto50));
        user.setPhoto100(c.getString(userphoto100));
        user.setPhoto200(c.getString(userphoto200));
        user.setAbout(c.getString(about));
        user.setFromCity(c.getString(fromcity));
        user.setBirthday(c.getString(birthday));
        user.setLastName(c.getString(lastname));
        user.setLastTime(c.getLong(lasttime));
        user.setVisible(c.getInt(visible) != 0); // Так как нельзя boolean сохранять
        user.setPosition(new LatLng(user.getLatitude(), user.getLongitude()));
        user.setDistanceTo(c.getFloat(distance));
        user.setTelegram(c.getString(telegramm));
        return user;
    }

    public void close() {
        Log.i(LOG, "getWritableDatabase close");
        try {
            getWritableDatabase().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashSet<User> getNearby(int limit) {
        HashSet<User> nearByUsers = new HashSet<>();

        if (limit <= 0)
            return nearByUsers;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;
        try {
            db.beginTransaction();
            c = getReadableDatabase().query(DB_TABLE_USERS, null, null, null, null, null, DB_COLUMN_NEARBY_DISTANCE, String.valueOf(limit));
            while (c.moveToNext()) {

                int lon = c.getColumnIndex(DB_COLUMN_LONG);
                int email = c.getColumnIndex(DB_COLUMN_EMAIL);
                int id = c.getColumnIndex(DB_COLUMN_ID);
                int lat = c.getColumnIndex(DB_COLUMN_LAT);
                int status = c.getColumnIndex(DB_COLUMN_STATUS);
                int nikname = c.getColumnIndex(DB_COLUMN_NIKNAME);
                int lastname = c.getColumnIndex(DB_COLUMN_LASTNAME);

                int lasttime = c.getColumnIndex(DB_COLUMN_LASTTIME);
                int birthday = c.getColumnIndex(DB_COLUMN_BIRTHDAY);
                int about = c.getColumnIndex(DB_COLUMN_ABOUTME);
                int userphoto50 = c.getColumnIndex(DB_COLUMN_PHOTO_50);
                int userphoto100 = c.getColumnIndex(DB_COLUMN_PHOTO_100);
                int userphoto200 = c.getColumnIndex(DB_COLUMN_PHOTO_200);
                int fromcity = c.getColumnIndex(DB_COLUMN_FROM_CITY);
                int visible = c.getColumnIndex(DB_COLUMN_VISIBLE);
                int distance = c.getColumnIndex(DB_COLUMN_NEARBY_DISTANCE);
                int telegram = c.getColumnIndex(DB_COLUMN_TELEGRAM);

                User nearby = getUser(c, lon, email, id, lat, status, nikname, lastname, lasttime, birthday, about, userphoto50, userphoto100, userphoto200, fromcity, visible, distance, telegram);
                nearByUsers.add(nearby);
            }
            c.close();
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.i(LOG, "error" + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
            if (c != null && !c.isClosed())
                c.close();
        }
        db.close();
        return nearByUsers;
    }
}
