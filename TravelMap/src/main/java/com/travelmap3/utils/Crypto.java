package com.travelmap3.utils;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.squareup.okhttp.internal.Util.UTF_8;

/**
 * Created by An on 23.07.2015.
 * Class Crypto for implementation hash and crypto algorithm
 * SHA256, MD5
 */
public class Crypto {

    public static final String SALT = "travelmap=)";

    public String hashFromBitmap(Bitmap bitmap) throws Exception {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                builder.append(bitmap.getPixel(i, j));
            }
        }
        return sha256(builder.toString());
    }

    public String sha256(String value) throws Exception {

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(value.getBytes(UTF_8));

        byte byteData[] = md.digest();

        StringBuilder hexString = new StringBuilder();
        for (byte aByteData : byteData) {
            String hex = Integer.toHexString(0xff & aByteData);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public String md5TestMethod(String str) {
        MessageDigest md5;
        StringBuilder hexString = new StringBuilder();
        try {
            md5 = MessageDigest.getInstance("md5");
            md5.reset();
            md5.update(str.getBytes(UTF_8));
            byte messageDigest[] = md5.digest();
            for (byte aMessageDigest : messageDigest) {
                hexString.append(Integer.toHexString(0xFF & aMessageDigest));
            }
        } catch (NoSuchAlgorithmException e) {
            return e.toString();
        }
        return hexString.toString();
    }

    public String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes(UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            // TODO: Исправить пустой Catch-блок
        }
        return null;
    }

    public String getHashPass(String pass) {
        try {
            return sha256(md5(pass));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String base64(String text) throws UnsupportedEncodingException {
        byte[] data = text.getBytes("UTF-8");
        return Base64.encodeToString(data, Base64.DEFAULT);
    }
}
