package com.travelmap3.utils;

import android.content.SharedPreferences;

import com.travelmap3.BuildConfig;

/**
 * Created by Andre on 02.02.2016.
 */
public class MySharedPreferences {
  public static final String NAME_FILE = BuildConfig.APPLICATION_ID + BuildConfig.BUILD_TYPE + 1;
  protected SharedPreferences sharedPref;
  protected SharedPreferences.Editor sharedPrefEditor;

  public void clear() {
    sharedPrefEditor.clear().commit();
  }
}
