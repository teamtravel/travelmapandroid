package com.travelmap3.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by An on 22.03.2015.
 */

public class TextFormat {

    /**
     * Формат вывода dd MMMM yy hh:mm:ss
     * @param time
     * @return
     */
    public static String fullDate(long time) {
        Date d = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMMM-yy hh:mm:ss", Locale.ROOT);
        return simpleDateFormat.format(d);
    }
    /**
     * Формат вывода dd.mm.yy
     * @param time
     * @return
     */
    static public String date_ddMMyy(long time) {
        Date d = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy",  Locale.ROOT);
        return simpleDateFormat.format(d);
    }
    static public String date_d(long time) {
        Date d = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
        return simpleDateFormat.format(d);
    }
    /**
     * Вывод на экран только первые n символов+"...";
    */
    static public String partialOutput(String text, int length){
        StringBuilder builder = new StringBuilder();
        if ( text==null ) return "";
        if (text.length()<length) return text;
        for (int i=0;i<length;i++)
            builder.append(text.charAt(i));
        builder.append("...");
        return builder.toString();
    }

    /**
     *
     * @param time
     * @return time in format h:mm a(English)
     */
    static public String dateTime(long time) {
        Date d = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a");
        return simpleDateFormat.format(d).replace("после полудня","pm").replace("до полудня","am");
    }
/*
 name of month
 */
static public String date_MMM(long time) {
        Date d = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        return simpleDateFormat.format(d).replace(".","");
    }


    static public String timeSpentOnTheRoad(long timeMillis){

        if (timeMillis > 1451649600 ) return "0 минут";
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("UTC"));
        c.setTimeInMillis(timeMillis);
        // 1970 - год начала отсчета в новом инстансе календаря.
        int mYear = c.get(Calendar.YEAR)-1970;
        int mMonth = c.get(Calendar.MONTH);
        int mWeek = (c.get(Calendar.DAY_OF_MONTH)-1)/7;
        int mDay = (c.get(Calendar.DAY_OF_MONTH)-1)%7;
        int mHour = c.get(Calendar.HOUR);
        int mMin = c.get(Calendar.MINUTE);

        StringBuilder sb = new StringBuilder();
        sb.append(timeFormatter("year", mYear))
                .append(timeFormatter("month", mMonth))
                .append(timeFormatter("week", mWeek))
                .append(timeFormatter("day", mDay))
                .append(timeFormatter("hour", mHour))
                .append(timeFormatter("min", mMin));
        return sb.toString().trim();
    }



    static public String timeSpentOnTheRoadInDays(long timeMillis){
        long days = TimeUnit.MILLISECONDS.toDays(timeMillis);
        return timeFormatter("day", days).trim();
    }

    /**
     * Используется для выбора соответсвтующего слова при выводе даты
     * "{kol}month {kol} day {kol} hours {kol} min"

     * @param timeUnit - year, month, week, day, hour, min
     * @param time  - значение времени для еденицы timeUnit
     *
     * */

    private static String timeFormatter(String timeUnit, long time){

        if (time == 0)
            return "";

        String[] yearPrint = new String[]{"лет", "год", "года"};
        String[] monthPrint = new String[]{"месяцев", "месяц", "месяца"};
        String[] weekPrint = new String[]{"недель", "неделю", "недели"};
        String[] dayPrint = new String[]{"дней", "день", "дня"};
        String[] hourPrint = new String[]{"часов", "час", "часа"};
        String[] minutePrint = new String[]{"минут", "минуту", "минуты"};


        int remainder = (int) (time % 10);
        int tenRemainder = (int) (time % 100);

        String result = null;


        // для чисел оканчивающихся от 11 до 19

        if (tenRemainder > 10 && tenRemainder < 20) {
            switch (timeUnit) {
                case "year":
                    result = yearPrint[0];
                    break;
                case "month":
                    result = monthPrint[0];
                    break;
                case "week":
                    result = weekPrint[0];
                    break;
                case "day":
                    result = dayPrint[0];
                    break;
                case "hour":
                    result = hourPrint[0];
                    break;
                case "min":
                    result = minutePrint[0];
                    break;
            }
            return time + " " + result + " ";
        }

        // для остальных чисел
        switch (timeUnit){
            case "year":
                if (remainder == 1)
                    result = yearPrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = yearPrint[2];
                else
                    result = yearPrint[0];
                break;
            case "month":
                if (remainder == 1)
                    result = monthPrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = monthPrint[2];
                else
                    result = monthPrint[0];
                break;
            case "week":
                if (remainder == 1)
                    result = weekPrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = weekPrint[2];
                else
                    result = weekPrint[0];
                break;
            case "day":
                if (remainder == 1)
                    result = dayPrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = dayPrint[2];
                else
                    result = dayPrint[0];
                break;
            case "hour":
                if (remainder == 1)
                    result = hourPrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = hourPrint[2];
                else
                    result = hourPrint[0];
                break;
            case "min":
                if (remainder == 1)
                    result = minutePrint[1];
                else if (remainder > 1 && remainder < 5)
                    result = minutePrint[2];
                else
                    result = minutePrint[0];
                break;
        }

        return time + " " + result + " ";
    }
    static public String getTimeHh(long time) {
        return (new SimpleDateFormat("HH:mm", Locale.getDefault())).format(new Date(time));
     }
    public static long convertDataToLong(String data) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = formatter.parse(data);
        return  date.getTime();
    }
}
/*

Date and Time Pattern	Result
"yyyy.MM.dd G 'at' HH:mm:ss z"    	2001.07.04 AD at 12:08:56 PDT
"EEE, MMM d, ''yy"	                Wed, Jul 4, '01
"h:mm a"	                        12:08 PM
"hh 'o''clock' a, zzzz"	            12 o'clock PM, Pacific Daylight Time
"K:mm a, z"	                        0:08 PM, PDT
"yyyyy.MMMMM.dd GGG hh:mm aaa"	    02001.July.04 AD 12:08 PM
"EEE, d MMM yyyy HH:mm:ss Z"	    Wed, 4 Jul 2001 12:08:56 -0700
"yyMMddHHmmssZ"	                    010704120856-0700
"yyyy-MM-dd'T'HH:mm:ss.SSSZ"	    2001-07-04T12:08:56.235-0700
"yyyy-MM-dd'T'HH:mm:ss.SSSXXX"  	2001-07-04T12:08:56.235-07:00
"YYYY-'W'ww-u"	                    2001-W27-3
http://stackoverflow.com/questions/5369682/get-current-time-and-date-on-android
 */