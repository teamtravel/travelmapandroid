package com.travelmap3.utils;

import com.travelmap3.BuildConfig;

import java.util.Locale;

public class Config {

  public static final String SERVER = BuildConfig.SERVER; //"http://52.25.189.139:55555"; // сервер Go // Установка с грэдла
  //  public static final String SERVER = "http://192.168.1.5:55555";// локальный dev сервер Go Al
  // public static final String SERVER = "http://192.168.21.44:8000";
    /*
 Версия API сервера
 */
  public static final String VERSION_API = "v1";
  /**
   * Константа для получения всех пользователей что бы отоброзить их на карте
   * И информацию о них.
   */
  public static final String GET_USERS = "/users";

  /**
   * Для сохранения профиля
   */
  public static final String POST_PROFILE = "/profile";
  /**
   * Для сохранения записей дневника
   */
  public static final String SAVE_NOTES = "/note";
  /**
   * Для сохранения сообщений
   */
  public static final String SAVE_MESSAGES = "/message";
  /**
   * Для сохранения сообщений о помощи!
   */
  public static final String SAVE_HELP = "/help";
  /**
   * Для сохранения трекера путешествия пользователя
   */
  public static final String SAVE_GPS_TRACKER = "/tracker";
  /**
   * Для использование апи национального туристического портала. Сайт стремный. Контент может быть
   * плохой.
   */
  public static final String SERVER_API_RUSSIAN_TRAVEL = "http://api.russia.travel";
  /*
* A user-agent string that's sent to the HTTP site. It includes information about the device
* and the build that the device is running.
*/
  public static final String USER_AGENT = "Mozilla/5.0 (Linux; U; Android "
      + android.os.Build.VERSION.RELEASE + ";"
      + Locale.getDefault().toString() + "; " + android.os.Build.DEVICE
      + "/" + android.os.Build.ID + ")";
  public static final String CLOUD_NAME = "travelmaps";
  public static final String API_KEY_CLOUDDINARY = "121592342522876";
  public static final String API_SECRET_CLOUDDINARY = "a8PyyGmUT2dO4Z1u7WQm5HNS_5Q";
}