package com.travelmap3.utils;

import android.content.Context;
import android.util.Log;

import com.travelmap3.model.User;

import java.util.HashSet;
import java.util.concurrent.ExecutionException;

/**
 * Теперь что бы получить всех ближайших юзеров надо сделать выборку с сортировкой по расстоянию из
 * таблицы где у нас хранятся юзеры все.
 * NearbyTravelerController updUser = new NearbyTravelerController(getActivity());
 * HashSet set = updUser.currentData(); -Здесь собсвенно и происходит данная выборка.
 * В итоге получаем сет с 2 ближайшими юзерам.
 * Количество юзеров указывается в  getFromDB()  NearbyTravelerController.class
 */
public class NearbyTravelerController {
  private static final String LOG = "NearbyTraveler";
  private static final long MIN_TIME_UPDATE_DEBUG = 9000;
  private Context context;
  private Preferences pref;
  private UsersDB db;

  public NearbyTravelerController(Context context) {
    this.context = context;
    pref = Preferences.getInstance();
    db = new UsersDB(context);
  }


  public boolean isTime() {
    return System.currentTimeMillis() - pref.loadLastTimeUpdateNearby() > MIN_TIME_UPDATE_DEBUG;
  }

  public HashSet<User> getWithServer() throws InterruptedException, ExecutionException {
    return null;
  }

  public void saveTime() {
    pref.saveTimeUpdateNearby(System.currentTimeMillis());
  }


  public boolean insertInDB(HashSet<User> set) {
    Log.w(LOG, "set in PlaceDB users nearby");
    if (set != null) {
      db.insert(set);
      return true;
    } else {
      return false;
    }
  }


  public HashSet currentData() {
    return getFromDB();
  }

  private HashSet<User> getFromDB() {
    // Достаем пока 2 Юзеров
    return db.getNearby(2);
  }

}
