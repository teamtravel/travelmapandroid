package com.travelmap3.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelmap3.App;
import com.travelmap3.model.PeopleModel;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class Preferences extends MySharedPreferences {

    public static final String CHECK_BOX_GROUP_CHOICE = "check_box_group_choice";
    public static final String CHECK_BOX_SEND_GPS_TRACKER = "sendGpsTracker";
    //User
    public static final String USER_DATA_JSON = "user_data_json";
    public static final String USER_ID = "userId";
    public static final String USER_REG_ID = "gcm_reg_id";
    public static final String VK_NAME = "vkr";
    private static final String UPDATE_INFO_PROFILE = "update_me_profile";
    private static final String TOKEN = " tokken";
    private static final String NOTE_AMOUNT = "NoteAmout";
    private static final String DIALOG_AMOUNT = "DialogMes";
    private static final String FIRST_ENTRY_USER = "first_entry_user";
    private static final String UPDATE_INFO_ABOUT_USERS = "update_info_about_users";
    private static final String USER_EMAIL = "user";
    private static final String MODE_TRAVEL = "ModeTravel";
    private static final String CLICK_HELP = "ClickHelp";
    private static final String LAST_TIME_UPDATE_NEARBY = "TimeUpdateNearby";
    private static final String LAST_TIME_UPDATE_MAP = "TimeUpdateMap";
    private static final String TIME_START_TRAVEL = "startTravelTime";
    private static final String START_LATITUDE = "latitude";
    private static final String START_LONGITUDE = "longitude";
    private static final String LAST_LONGITUDE = "last_longitude";
    private static final String LAST_LATITUDE = "last_gps_latitude";
    private static final String DISTANCE_PASSED = "treveler_distance";
    private static final String LAST_TIME_UPDATING_GPS = "gps_last";
    private static final String LAST_TIME_UPDATING_PLACE_OBJECT_LAT = "objectplaceupdalon";
    private static final String LAST_TIME_UPDATING_PLACE_OBJECT_LON = "objectplaceupdal";
    private static final String NUMBER_TRIP = "number_trip";
    private static final String TIME_LAST_UPDATE_PLACES = "time_last_places";
    private static final String TIME_LAST_AUTH = "auth_u";
    private static final String TIME_LAST_UPDATE_PLACES_MY = "TIME_LAST_UPDATE_PLACES_MY";

    private static volatile Preferences Instance = null;

    private Preferences(Context context) {
        try {
            sharedPref = context.getSharedPreferences(NAME_FILE, Context.MODE_PRIVATE);
            sharedPrefEditor = sharedPref.edit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Preferences getInstance(App app) {
        context = app.getApplicationContext();
        return getInstance();
    }

    private static Context context;

    public static Preferences getInstance() {
        Preferences localInstance = Instance;
        if (localInstance == null) {
            synchronized (Preferences.class) {
                localInstance = Instance;
                if (localInstance == null)
                    Instance = localInstance = new Preferences(context);
            }
        }
        return localInstance;
    }

    public void saveToken(String tokken) {
        sharedPrefEditor.putString(TOKEN + loadUserId(), tokken);
        sharedPrefEditor.commit();
    }

    public Boolean loadPublishVlSing() {
        return sharedPref.getBoolean("publish_vk_signature", true);
    }
    public String loadToken() {
        return sharedPref.getString(TOKEN + loadUserId(), null);
    }


    public void removeUserPreferences() {
        sharedPrefEditor.remove(USER_DATA_JSON + loadUserId()).remove(USER_EMAIL + loadUserId())
                .remove(USER_REG_ID + loadUserId()).remove(TOKEN + loadUserId()).apply();
    }


    public void saveModeTravel() {
        sharedPrefEditor.putBoolean(MODE_TRAVEL + loadUserId(), true).apply();
    }

    public boolean isModeTravel() {
        return sharedPref.getBoolean(MODE_TRAVEL + loadUserId(), false);
    }

    public void clearModeTravel() {
        sharedPrefEditor.putBoolean(MODE_TRAVEL + loadUserId(), false);
        sharedPrefEditor.apply();
    }

    public void saveClickHelp() {
        sharedPrefEditor.putBoolean(CLICK_HELP, true);
        sharedPrefEditor.apply();
    }

    public void clearClickHelp() {
        sharedPrefEditor.remove(CLICK_HELP).commit();
    }

    public boolean isClickHelp() {
        return sharedPref.contains(CLICK_HELP);
    }

    public void saveTimeUpdateNearby(long currentTimeMillis) {
        sharedPrefEditor.putLong(LAST_TIME_UPDATE_NEARBY, currentTimeMillis);
        sharedPrefEditor.apply();

    }

    public Long loadLastTimeUpdateNearby() {
        return sharedPref.getLong(LAST_TIME_UPDATE_NEARBY, 0);
    }

    public void saveTimeUpdateMap(long currentTimeMillis) {
        sharedPrefEditor.putLong(LAST_TIME_UPDATE_MAP + loadUserId(), currentTimeMillis);
        sharedPrefEditor.apply();

    }

    public Long loadLastTimeUpdateMap() {
        return sharedPref.getLong(LAST_TIME_UPDATE_MAP + loadUserId(), 0);
    }

    public long loadTimeStartTravel() {
        return sharedPref.getLong(TIME_START_TRAVEL, 60);
    }

    public void saveTimeStartTravel(long currentTimeMl) {
        sharedPrefEditor.putLong(TIME_START_TRAVEL, currentTimeMl);
        sharedPrefEditor.apply();
    }

    public void clearTimeStartTravel() {
        sharedPrefEditor.remove(TIME_START_TRAVEL).commit();
    }


    public void saveStartLocation(Location loc) {
        sharedPrefEditor.putFloat(START_LATITUDE, (float) loc.getLatitude());
        sharedPrefEditor.putFloat(START_LONGITUDE, (float) loc.getLongitude());
        sharedPrefEditor.apply();
    }

    public float loadStartLatitude() {
        return sharedPref.getFloat(START_LATITUDE, 0);
    }

    public float loadStartLongitude() {
        return sharedPref.getFloat(START_LONGITUDE, 0);
    }

    public void clearStartLocation() {
        sharedPrefEditor.remove(START_LATITUDE).commit();
        sharedPrefEditor.remove(START_LONGITUDE).commit();
    }

    public void saveLastLocation(Location loc) {
        sharedPrefEditor.putFloat(LAST_LATITUDE, (float) loc.getLatitude());
        sharedPrefEditor.putFloat(LAST_LONGITUDE, (float) loc.getLongitude());
        sharedPrefEditor.apply();
    }

    public float loadLastLatitude() {
        return sharedPref.getFloat(LAST_LATITUDE, 0);
    }

    public float loadLastLongitude() {
        return sharedPref.getFloat(LAST_LONGITUDE, 0);
    }

    public void clearLastLocation() {
        sharedPrefEditor.remove(LAST_LATITUDE).commit();
        sharedPrefEditor.remove(LAST_LONGITUDE).commit();
    }

    public void savePassedDistance(float dist) {
        sharedPrefEditor.putFloat(DISTANCE_PASSED + loadUserId(), dist);
        sharedPrefEditor.apply();
    }

    public float loadPassedDistance() {
        return sharedPref.getFloat(DISTANCE_PASSED + loadUserId(), 0);
    }

    public void clearPassedDistance() {
        sharedPrefEditor.remove(DISTANCE_PASSED + loadUserId()).commit();
    }


    public void saveAmountNote(int sizeNote) {
        sharedPrefEditor.putInt(NOTE_AMOUNT, sizeNote);
        sharedPrefEditor.apply();
    }

    public int loadAmountNote() {
        return sharedPref.getInt(NOTE_AMOUNT, 0);
    }

    public void saveAmountDialogMessage(int size) {
        sharedPrefEditor.putInt(DIALOG_AMOUNT, size);
        sharedPrefEditor.apply();
    }

    public int loadAmountDialogMessage() {
        return sharedPref.getInt(DIALOG_AMOUNT, 0);
    }


    public void saveCheckBoxGroupChoice(String str) {
        sharedPrefEditor.putString(CHECK_BOX_GROUP_CHOICE, str);
        sharedPrefEditor.apply();
    }

    public String loadCheckBoxGroupChoice() {
        return sharedPref.getString(CHECK_BOX_GROUP_CHOICE, "");
    }


    public String getString(String key) {
        return sharedPref.getString(key + loadUserId(), "");
    }

    public void putString(String k, String v) throws Exception {
        if (loadUserId() == 0) throw new Exception("user id equlas 0");
        sharedPrefEditor.putString(k + loadUserId(), v);
        sharedPrefEditor.apply();
    }

    public boolean loadFirstEntryUser() {
        return sharedPref.getBoolean(FIRST_ENTRY_USER + loadUserId(), true);
    }

    public void saveFirstEntryUser() {
        sharedPrefEditor.putBoolean(FIRST_ENTRY_USER + loadUserId(), false);
        sharedPrefEditor.apply();
    }

    public void saveTimeUpdateInfoAboutUsers() {
        sharedPrefEditor.putLong(UPDATE_INFO_ABOUT_USERS, System.currentTimeMillis());
        sharedPrefEditor.apply();
    }

    public long loadTimeUpdateInfoAboutUsers() {
        return sharedPref.getLong(UPDATE_INFO_ABOUT_USERS, 0);
    }

    public void saveTimeUpdateProfile() {
        sharedPrefEditor.putLong(UPDATE_INFO_PROFILE, System.currentTimeMillis()).apply();
    }

    public long loadTimeUpdateProfile() {
        return sharedPref.getLong(UPDATE_INFO_PROFILE, 0);
    }

    public void saveUserId(Integer id) {
        sharedPrefEditor.putInt(USER_ID, id);
        sharedPrefEditor.commit();// обязательно commit() a не apply()
    }

    public Integer loadUserId() {
        return sharedPref.getInt(USER_ID, 0);
    }

    public Boolean pushEnabled() {
        return !sharedPref.getString(USER_REG_ID + String.valueOf(loadUserId()), "").isEmpty();
    }


    public long loadLastUpdatingGpsServer() {
        return sharedPref.getLong(LAST_TIME_UPDATING_GPS + loadUserId(), 0);
    }

    public void saveLastUpdatingGpsServer(long time) {
        sharedPrefEditor.putLong(LAST_TIME_UPDATING_GPS + loadUserId(), time);
        sharedPrefEditor.apply();
    }

    public void saveArrayList(String cons, ArrayList<PeopleModel> list) {
        sharedPrefEditor.putInt(cons + "list_size", list.size()); /* sKey is an array */

        for (int i = 0; i < list.size(); i++) {
            sharedPrefEditor.remove(cons + "list_1" + i);
            sharedPrefEditor.remove(cons + "list_2" + i);
            sharedPrefEditor.putString(cons + "list_1_" + i, list.get(i).getFirstName());
            sharedPrefEditor.putString(cons + "list_2_" + i, list.get(i).getRowID());
        }

        sharedPrefEditor.apply();
    }

    public ArrayList<PeopleModel> loadArrayList(String cons) {
        ArrayList<PeopleModel> list = new ArrayList();
        list.clear();
        int size = sharedPref.getInt(cons + "list_size", 0);

        PeopleModel p = new PeopleModel();
        for (int i = 0; i < size; i++) {
            p = new PeopleModel();
            p.setFirstName(sharedPref.getString(cons + "list_1_" + i, null));
            p.rowID = sharedPref.getString(cons + "list_2_" + i, null);
            list.add(p);
        }
        return list;
    }

    public LatLng loadOldLocationUpdatePlaceLocation() {
        return new LatLng(sharedPref.getFloat(LAST_TIME_UPDATING_PLACE_OBJECT_LAT, 0), sharedPref.getFloat(LAST_TIME_UPDATING_PLACE_OBJECT_LON, 0));
    }

    public void saveOldLocationUpdatePlaceLocation(LatLng l) {
        Log.e("DATA", l.latitude + " " + l.longitude);
        sharedPrefEditor.putFloat(LAST_TIME_UPDATING_PLACE_OBJECT_LAT, (float) l.latitude);
        sharedPrefEditor.commit();
        sharedPrefEditor.putFloat(LAST_TIME_UPDATING_PLACE_OBJECT_LON, (float) l.longitude);
        sharedPrefEditor.apply();
    }

    public void saveNumberTrip(int number) {
        sharedPrefEditor.putInt(NUMBER_TRIP + loadUserId(), number);
        sharedPrefEditor.apply();
    }

    public int loadNumberTrip() {
        return sharedPref.getInt(
                NUMBER_TRIP + loadUserId(), 0);
    }

    public long loadTimeLastUpdatePlacesAll() {
        return sharedPref.getLong(
                TIME_LAST_UPDATE_PLACES, 0);
    }
    public void saveTimeLastUpdatePlacesAll() {
        sharedPrefEditor.putLong(TIME_LAST_UPDATE_PLACES, System.currentTimeMillis());
        sharedPrefEditor.apply();
    }
    public long loadTimeLastUpdatePlacesMy() {
        return sharedPref.getLong(
                TIME_LAST_UPDATE_PLACES_MY, 0);
    }
    public void saveTimeLastUpdatePlacesMy() {
        sharedPrefEditor.putLong(TIME_LAST_UPDATE_PLACES_MY, System.currentTimeMillis());
        sharedPrefEditor.apply();
    }
    public void saveTimeAuthUser() {
        sharedPrefEditor.putLong(TIME_LAST_AUTH, System.currentTimeMillis());
        sharedPrefEditor.apply();
    }
    public long loadTimeAuthUser() {
        return sharedPref.getLong(
                TIME_LAST_AUTH, 0);
    }
    public void clearTimeAuthUser() {
        sharedPrefEditor.remove(TIME_LAST_AUTH).commit();
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public Boolean loadPublishVkWall() {
        return sharedPref.getBoolean("publish_vk_wall", false);
    }

    public Boolean loadVkGroupWall() {
        return sharedPref.getBoolean("publish_vk_group_wall", false);
    }

    public Boolean loadVkOwner() {
        return sharedPref.getBoolean("publish_vk_group_wall_by_owner", false);
    }
}
