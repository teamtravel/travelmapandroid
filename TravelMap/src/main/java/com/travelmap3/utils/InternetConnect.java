package com.travelmap3.utils;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import javax.inject.Inject;

public class InternetConnect implements IConnect {
    private final ConnectivityManager connectivityManager;
    private final SharedPreferences sp;
    private String LOG = this.getClass().getName();
    private NetworkInfo wifi;
    private NetworkInfo mob;

    @Inject
    public InternetConnect(ConnectivityManager connectivityManager, SharedPreferences sp) {
        wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mob = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        this.connectivityManager = connectivityManager;
        this.sp = sp;
    }



    /**
     * Проверка на подключение к интернету, учитывая настройки пользователя!
     */
    public boolean isActiveWiFiConnection() {
        mob = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        Log.e("mLOG", "settingsWifi: " + sp.getBoolean("usedWifi", false) + " wifi:" + checkWifi() + " mob:" + checkMob());
        if (sp.getBoolean("usedWifi", false)) {
            return checkWifi();//Если в настройках сейчас стоит отправка только по вайфаю, тогда проверяем соединение по вайфай
        } else {
            return checkMob() || checkWifi();//иначе проверяем соединение по мобильному инету
        }

    }

    private boolean checkMob() {

        if (connectivityManager == null) {
            return false;
        } else {
            if (mob != null) return mob.isConnectedOrConnecting();
            else return false;

        }
    }

    private boolean checkWifi() {
        if (connectivityManager == null) {
            return false;
        } else {
            return wifi.isConnectedOrConnecting();
        }
    }

    public final boolean isInternetOn() {
        if (connectivityManager == null || connectivityManager.getNetworkInfo(0) == null) {
            return checkWifi();
        }

        if (connectivityManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED
                ||
                connectivityManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING
                ||
                connectivityManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING
                ||
                connectivityManager.getNetworkInfo(1).getState()
                        == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet
            return true;

        } else if (
                connectivityManager.getNetworkInfo(0).getState()
                        == android.net.NetworkInfo.State.DISCONNECTED ||
                        connectivityManager.getNetworkInfo(1).getState()
                                == android.net.NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

}
