package com.travelmap3.utils;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.travelmap3.model.PeopleModel;
import com.travelmap3.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class FormatJSON {

    private final String LOG = this.getClass().getName();
    private Gson gson;

    public FormatJSON() {
        gson = new GsonBuilder().create();
    }

    public String takeJsonForUser(User obj) {
        return gson.toJson(obj);
    }

    public HashSet<User> takeHashSetUser(String strJson) throws JSONException {
        if (strJson == null) {
            throw new RuntimeException("String Json null");
        }
        JSONArray users = new JSONArray(strJson);
        Log.i(LOG, "Count users = " + users.length());
        HashSet<User> setUser = new HashSet<User>();
        User user;
        for (int i = 0; i < users.length(); i++) {
            user = new User();
            JSONObject jsonObj = users.getJSONObject(i);
            user.setId(jsonObj.getInt("id"));
            //  user.setEmail(jsonObj.getString("user_email"));
            user.setFirstName(jsonObj.getString("firstname"));
            user.setLastName(jsonObj.getString("lastname"));
            user.setLatitude(jsonObj.getString("latitude"));
            user.setLongitude(jsonObj.getString("longitude"));
            user.setLastTime(jsonObj.getLong("lasttime"));
            user.setStatusText(jsonObj.getString("status"));
            user.setVisible(jsonObj.getString("visible").equalsIgnoreCase("true"));
            user.setAbout(jsonObj.getString("about"));
            user.setPhoto50(jsonObj.getString("photo50"));
            user.setPhoto100(jsonObj.getString("photo100"));
            user.setPhoto200(jsonObj.getString("photo200"));
            user.setFromCity(jsonObj.getString("from_city"));
            user.setPosition(new LatLng(user.getLatitude(), user.getLongitude()));
            setUser.add(user);
            Log.i(LOG, "User id= " + user.getId());
        }
        return setUser;
    }

    /**
     * Вставляй выше!! Че тупишь! Точняк!!
     */
    public User takeUser(String json) {
        if (json.isEmpty()) return new User();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        User user = gson.fromJson(json, User.class);
        return user;
    }

    public ArrayList<PeopleModel> takeVkGroupsResponse(JSONObject response) throws JSONException {
        ArrayList<PeopleModel> list = new ArrayList<PeopleModel>();
        JSONObject jsonObject;
        JSONArray data = response.getJSONObject("response").getJSONArray("items");
        String[] sep = Preferences.getInstance().loadCheckBoxGroupChoice().split(",");
        for (int i = 0; i < data.length(); i++) {
            jsonObject = data.getJSONObject(i);
            Boolean checked = Arrays.asList(sep).contains(jsonObject.getString("id"));
            PeopleModel map = new PeopleModel(checked, jsonObject.getString("id"),
                    jsonObject.getString("name"), jsonObject.getString("photo_100"));
            list.add(map);
        }
        return list;
    }

    public User takeVkUser(JSONArray data) throws JSONException {
        JSONObject res = data.getJSONObject(0);
        User user = new User();
        user.setFirstName(res.getString("first_name"));
        user.setLastName(res.getString("last_name"));
        user.setVkId(res.getString("id"));
        Log.w(LOG, "vk user id from source  " + res);
        if (res.has("city")) {
            user.setFromCity(res.getJSONObject("city").getString("title"));
        }
        if (res.has("bdate")) {
            user.setBirthday(res.getString("bdate"));
        }
        if (res.has("status")) {
            user.setStatusText(res.getString("status"));
        }
        if (res.has("photo_50")) {
            user.setPhoto50(res.getString("photo_50"));
        }
        if (res.has("photo_100")) {
            user.setPhoto100(res.getString("photo_100"));
        }
        if (res.has("photo_200")) {
            user.setPhoto200(res.getString("photo_200"));
        }
        return user;
    }

}
