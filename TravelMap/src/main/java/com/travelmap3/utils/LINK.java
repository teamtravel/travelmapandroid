package com.travelmap3.utils;

/**
 * @author An
 *         Ссылки для отправки на сервер
 */
public enum LINK {
    NOTES(Config.SAVE_NOTES),
    MESSAGE(Config.SAVE_MESSAGES),
    HELP(Config.SAVE_HELP),
    ALL,
    LOCATION(Config.SAVE_GPS_TRACKER),
    PROFILE(Config.POST_PROFILE),
    USERS(Config.GET_USERS);

    private String value;

    LINK() {
    }

    LINK(String s) {
        this.value = Config.SERVER + s;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}