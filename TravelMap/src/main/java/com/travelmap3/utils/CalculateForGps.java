/**
 *
 */
package com.travelmap3.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

/**
 * Класс для подсчетов различных значиний по GPS
 *
 * @author An
 */
public class CalculateForGps {

    private Preferences pref;
    private String LOG = this.getClass().getName();
    private long startTime;
    private long timePassed;

    public CalculateForGps() {
        pref = Preferences.getInstance();
        startTime = pref.loadTimeStartTravel();
        timePassed = timePassed();
    }

    private static float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++) {
            pow *= 10;
        }
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }

    public static String timeLastUpdate() {
        return new TextFormat().timeSpentOnTheRoad(
                System.currentTimeMillis() - Preferences.getInstance().loadLastUpdatingGpsServer());
    }

    /*
    Подсчет средней скорости, которая рассчитывается по формуле.
    speedMid = (distanceBetweenLocationStartAndCurrent() / (currentTime - startTime))
    где, distanceBetweenLocationStartAndCurrent  - это расстояние между началом путешествия и текущими координатами.
    Начальные данные сохраняются в Preferences. И хранятся на сервере!
    currentTime - текущие время в милисекундах;
    startTime - время старта

     */
    public float speedMidleMetr(Location loc) throws RuntimeException {
        float speedMid = 0;
        if (timePassed == 0) {
            return 0;
        }
        speedMid = distanceBetweenStartAndCurrentPosition(loc) / (timePassed);
        return round(speedMid, 3);
    }

    private long timePassed() {
        return System.currentTimeMillis() - startTime;
    }

    /* Функция вычисляет
    расстояние между текущим местоположение и точкой старта путешествия.
     */
    public float distanceBetweenStartAndCurrentPosition(Location currentLocation) {
        float lat = pref.loadLastLatitude(); //
        float lon = pref.loadLastLongitude();
        if (lat == 0 && lon == 0) {//Если это начало пути, то предыдущая метка, это старт путешествия.
            lat = pref.loadStartLatitude();
            lon = pref.loadStartLongitude();
        }
           /*
         Подсчитываем сколько мы проехали в от последней отметки до текущей.
          Сохраняем в passedDistance = distancePassed+distanceCurrent;
          Сохраняем текущее положение устанавливая его в LastLocation;
          Отправляем.
         */
        float passedDistance = pref.loadPassedDistance();
        float distance = distance(lat, lon, currentLocation);
        if (distance > 10.00f && currentLocation.getAccuracy() < 500.0) { //Условием, убираем погрешности которые возникают когда человек стоит на месте
            pref.savePassedDistance(passedDistance + distance);
            pref.saveLastLocation(currentLocation);
            return passedDistance + distance;
        } else {
            return passedDistance;
        }
    }

    public float distance(double lat1, double lon1, Location loc) {
        Location start = new Location("GPS");
        start.setLatitude(lat1);
        start.setLongitude(lon1);
        return loc.distanceTo(start);
    }

    /**
     * @param latMe
     * @param lonMe
     * @param lat2
     * @param lon2
     * @return
     */
    public float distance(double latMe, double lonMe, double lat2, double lon2) {
        Location position = new Location("GPS");
        position.setLatitude(lat2);
        position.setLongitude(lon2);
        return Math.round(distance(latMe, lonMe, position));
    }

    public String timeSpentOnTheRoadInDays() {
        return new TextFormat().timeSpentOnTheRoad(timePassed);
    }

    public Float getSpeedAv(Float distance) {
        return (float) Math.round(distance / timePassed);
    }

    /**
     * Example computing distance between two poasition
     *
     * @param positionA
     * @param positionB
     */
    private void showDistance(LatLng positionA, LatLng positionB) {
        double distance = SphericalUtil.computeDistanceBetween(positionA, positionB);
    }

    private String formatNumber(double distance) {
        String unit = "m";
        if (distance < 1) {
            distance *= 1000;
            unit = "mm";
        } else if (distance > 1000) {
            distance /= 1000;
            unit = "km";
        }
        return String.format("%4.3f%s", distance, unit);
    }

}
