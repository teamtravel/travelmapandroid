package com.travelmap3.interesplace;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.travelmap3.db.IDataBase;
import com.travelmap3.db.SqliteDBHelper;
import com.travelmap3.model.Gps;

import java.util.HashSet;

/**
 * Created by Andre on 20.06.2016.
 */
public class PlaceDB extends SqliteDBHelper implements IDataBase {
    private static final String LOG = "dbinteres";
    private static ContentValues cv;

    public PlaceDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static void save(HashSet<Place> placeSet) {

    }

    public boolean isNotSync() {
        Cursor cursor = getWritableDatabase().rawQuery("select count(*) from " + DB_TABLE_INTERECSING_PLACES + " where  " + DB_COLUMN_FLAG_SERVER_SAFE + "=0", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
            if (cursor.getInt(0) > 0) {
                return true;
            }
        }
        cursor.close();
        return false;
    }

    public long updateServerFlag(long timeKey) {
        cv = new ContentValues();
        cv.put(DB_COLUMN_FLAG_SERVER_SAFE, 1);
        return update(cv, timeKey);
    }

    public long save(Place place, ACTION act) {
        Log.e("LOG save", place.getTitle() + " " + act.name());
        cv = new ContentValues();
        cv.put(DB_COLUMN_TEXT, place.getText());
        cv.put(DB_COLUMN_TITLE, place.getTitle());
        cv.put(DB_COLUMN_NUMBER_TRIP, place.getNumbertrip());
        cv.put(DB_COLUMN_LONG, place.getGps().getLongitude());
        cv.put(DB_COLUMN_LAT, place.getGps().getLatitude());
        cv.put(DB_COLUMN_TIME, place.getGps().getTime());
        cv.put(DB_COLUMN_FLAG_SERVER_SAFE, 0);
        cv.put(DB_COLUMN_IMAGURL, "");
        cv.put(DB_COLUMN_PRIVATES, place.getPrivates() ? 1 : 0);
        switch (act) {
            case INSERT: {
                return insert(cv);
            }
            case UPDATE: {
                return update(cv, place.getGps().getTime());
            }
            case DELETE: {
                return delete(cv, place.getGps().getTime());
            }
        }
        return -1;//ERROR
    }

    private long update(ContentValues cv, long time) {
        String[] filter = new String[]{String.valueOf(time)};
        return getWritableDatabase().update(DB_TABLE_INTERECSING_PLACES, cv, DB_COLUMN_TIME + "=? ", filter);
    }

    private long delete(ContentValues cv, long time) {
        return getWritableDatabase().delete(DB_TABLE_INTERECSING_PLACES, DB_COLUMN_TIME + "=? ", new String[]{String.valueOf(time)});
    }

    private long insert(ContentValues cv) {
        return getWritableDatabase().insertWithOnConflict(DB_TABLE_INTERECSING_PLACES, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public HashSet<Place> getRecords() {
        HashSet<Place> set = new HashSet<>();
        Cursor c = null;
        try {
            c = getReadableDatabase().query(DB_TABLE_INTERECSING_PLACES, null, null,
                    null, null, null, null);
            set.addAll(getAll(c));
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (c != null) c.close();
        }
        return set;
    }

    public HashSet<Place> getMyRecords() {
        HashSet<Place> set = new HashSet<>();
        Cursor c = null;
        try {
            c = getReadableDatabase().query(DB_TABLE_INTERECSING_PLACES, null, DB_COLUMN_NUMBER_TRIP + ">-1",
                    null, null, null, null);
            set.addAll(getAll(c));
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (c != null) c.close();
        }
        return set;
    }

    public HashSet<Place> getAll(Cursor cursor) {

        HashSet<Place> set = new HashSet<>();
        if (cursor.moveToFirst()) try {

            int text = cursor.getColumnIndex(DB_COLUMN_TEXT);
            int title = cursor.getColumnIndex(DB_COLUMN_TITLE);
            int lat = cursor.getColumnIndex(DB_COLUMN_LAT);
            int lon = cursor.getColumnIndex(DB_COLUMN_LONG);
            int time = cursor.getColumnIndex(DB_COLUMN_TIME);
            int number = cursor.getColumnIndex(DB_COLUMN_NUMBER_TRIP);
            int image = cursor.getColumnIndex(DB_COLUMN_IMAGURL);

            do {
                Place.Builder placeBuild = Place.newBuilder();
                placeBuild.setNumberTrip(cursor.getInt(number));
                placeBuild.setGps(new Gps(cursor.getDouble(lat), cursor.getDouble(lon), cursor.getLong(time)));
                placeBuild.setCreated(cursor.getLong(time));
                placeBuild.setText(cursor.getString(text));
                placeBuild.setImageurl(cursor.getString(image));
                placeBuild.setTitle(cursor.getString(title));
                set.add(placeBuild.build());
            } while (cursor.moveToNext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        else {
            Log.d(LOG, "No columns");
        }
        return set;
    }

    public void clear() {
        clearAllT(DB_TABLE_INTERECSING_PLACES);
    }

    public HashSet<Place> getDifferedRecord() {
        HashSet<Place> list = new HashSet<>();
        String[] whereClause = {"0"};
        Cursor cursor = getReadableDatabase().query(DB_TABLE_INTERECSING_PLACES, null, DB_COLUMN_FLAG_SERVER_SAFE + "=?", whereClause, null, null, null, null);
        if (cursor.moveToFirst()) {
            list = getAll(cursor);
        }
        cursor.close();
        return list;
    }

    public enum ACTION {
        INSERT,
        UPDATE,
        DELETE
    }
}
