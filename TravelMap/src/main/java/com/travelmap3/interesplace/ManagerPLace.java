package com.travelmap3.interesplace;

import com.travelmap3.UserController;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andre on 20.06.2016.
 */
public class ManagerPLace {

    public PlaceDB placeDb;
    private UserController uController;

    @Inject
    public ManagerPLace(PlaceDB dbPlace, UserController userController) {
        this.placeDb = dbPlace;
        this.uController = userController;
    }


    public void getServer(final CallBackResponse callBackResponse) {
        RetrofitService.getInstanceAPI().getPlaces(Config.VERSION_API, new Callback<HashSet<Place>>() {
            @Override
            public void success(HashSet<Place> response, Response response2) {

                for (Place place : response) {
                    place.setNumbertrip(-1);
                    saveDb(place);
                }
                Preferences.getInstance().saveTimeLastUpdatePlacesAll();
                callBackResponse.onSuccess(response);
            }

            @Override
            public void failure(RetrofitError error) {
                callBackResponse.onError(error);
            }
        });

    }

    public void sendServer(final Place place, final CallBackResponse callBackResponse) {
        RetrofitService.getInstanceAPI().postPlace(uController.get().getToken(),
                Config.VERSION_API,
                place,
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        callBackResponse.onSuccess("Успешно сохранено");
                        safeServerFlag(place.getGps().getTime());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        String s = "Server error";
                        if (error != null && error.getResponse() != null) {
                            s = error.getKind() + "\n" + error.getResponse().getReason();
                        }
                        callBackResponse.onError(s);
                    }
                });
    }

    public void delete(final Place place, final CallBackResponse callBackResponse) {
        RetrofitService.getInstanceAPI().deletePlace(uController.get().getToken(), Config.VERSION_API, place, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        long error = deleteDb(place);
                        if (error < 1) {
                            callBackResponse.onError("Error delete place on database");
                        } else {
                            callBackResponse.onSuccess("Успех");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        callBackResponse.onError("Error delete place on server");
                    }
                }
        );
    }

    public long saveDb(Place place) {
        return placeDb.save(place, PlaceDB.ACTION.INSERT);
    }

    public long updateDb(Place place) {
        return placeDb.save(place, PlaceDB.ACTION.UPDATE);
    }

    public long deleteDb(Place place) {
        return placeDb.save(place, PlaceDB.ACTION.DELETE);
    }

    public HashSet<Place> takeDb() {
        return placeDb.getMyRecords();
    }

    public HashSet<Place> takeAllDb() {
        return placeDb.getRecords();
    }

    public void safeServerFlag(long time) {
        placeDb.updateServerFlag(time);
    }

    public void getServer(String token, Integer id, final CallBackResponse callback) {
        if (checkCachePlaces()) {
            RetrofitService.getInstanceAPI().getPlaces(token, Config.VERSION_API, id, new Callback<HashSet<Place>>() {
                @Override
                public void success(HashSet<Place> places, Response response) {
                    //Для синхронизации обьектов что приходят с сервера.
                    Set<Place> placeDbMyRecords = placeDb.getMyRecords();
                    for (Place place : places) {
                        int flag = 0;
                        for (Place data : placeDbMyRecords) {
                            if (!place.getTitle().equals(data.getTitle()) /*&& place.getGps().getTime() == data.getGps().getTime()*/) {
                                flag++;
                            }
                        }
                        if (flag == placeDbMyRecords.size())
                            placeDb.save(place, PlaceDB.ACTION.INSERT);
                    }
                    callback.onSuccess("Успех");
                    Preferences.getInstance().saveTimeLastUpdatePlacesMy();
                }

                @Override
                public void failure(RetrofitError error) {
                    callback.onError("Проблема");
                }
            });
        }
    }

    private boolean checkCachePlaces() {
        return System.currentTimeMillis() - Preferences.getInstance().loadTimeLastUpdatePlacesMy() > 100000000; // 27 hr. 46 min. 40 sec.
    }

    public void deleteAll() {
        placeDb.clear();
    }
}
