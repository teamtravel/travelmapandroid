package com.travelmap3.interesplace;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.dagger.AppComponent;
import com.travelmap3.model.Gps;
import com.travelmap3.notes.MODE;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.tips.TIPS;
import com.travelmap3.tips.TipsCreate;
import com.travelmap3.utils.TextFormat;

import java.util.HashSet;

/**
 * Класс для показа всех интересных мест - отметок  пользователя на карте.
 * Возможность: Читать.
 * При выборе на определенный маркет открывается текстовое окно.
 * Created by An on 21.06.2016.
 */
public class InterestingPlaceMapActivity extends Activity implements OnMapReadyCallback {
    public static final int LENGTH_CUT_TEXT = 10;
    private final static String LOG = "MapGoogleNoteActivity";
    private static final float ZOOM_CAMERA = 5;
    private GoogleMap map;
    private HashSet<Place> mPlaceSet;
    private MODE mode; // Режим для перемещения маркера по карте
    ControllerPlace controllerPlace;
    AppComponent component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_map);
        TipsCreate.getInstance().show(TIPS.INTERES_PLACE, getFragmentManager(), this);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        final ToggleButton btnToggle = (ToggleButton) findViewById(R.id.toggleButton);
        btnToggle.setVisibility(View.VISIBLE);
        mapFragment.getMapAsync(this);
        btnToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawMapPlace(btnToggle.isChecked());
            }
        });
        App application = (App) getApplication();
        component = application.getComponent();
        controllerPlace = component.controller();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen InterestingPlaceMap");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /**
     * Запрос к базе данных. Делаем через кеширование, сроком 10 часов.
     * получаем mPlace и выводим на экран. Обновляем карту.
     *
     * @param checked
     */
    private void drawMapPlace(boolean checked) {
        if (checked)
            try {
                mPlaceSet = controllerPlace.getMyPlaces();
                map.clear();
                initAddMarkers();
            } catch (Exception e) {
                e.printStackTrace();
            }
        else {
            component.controller().getAllPlaces(new CallBackResponse() {
                @Override
                public void onSuccess(Object obj) {
                    try {
                        mPlaceSet = (HashSet<Place>) obj;
                        initAddMarkers();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(Object obj) {
                    Toast.makeText(getApplication(), "Error connect internet", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        this.map = map;
        setMode(MODE.NORM);
        try {
            mPlaceSet = controllerPlace.getMyPlaces();
            LatLng latLng = initAddMarkers();
            if (component.uController().get().getLatitude() != 0) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(component.uController().get().getLatitude(),
                                component.uController().get().getLongitude()), ZOOM_CAMERA));
            } else {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(latLng.latitude, latLng.longitude), ZOOM_CAMERA));
            }
        } catch (Exception e) {
            Toast.makeText(InterestingPlaceMapActivity.this, "Оуу ошибка..", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                addMarkerPlace(latLng);
            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
                    dialog(marker);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    public void dialog(final Marker marker) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(InterestingPlaceMapActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_interes_place_box, null);
        alertDialog.setView(view);
        alertDialog.setTitle("");
        final EditText text = (EditText) view.findViewById(R.id.editTextBox);
        final EditText title = (EditText) view.findViewById(R.id.editTitle);
        TextView tvDateNote = (TextView) view.findViewById(R.id.tvDateNote);
        final CheckBox privates = (CheckBox) view.findViewById(R.id.checkBoxPrivatesPlace);
        final Place place = searchNoteByMarker(marker);
        if (place != null) {
            text.setText(place.getText());
            title.setText(place.getTitle());
            tvDateNote.setText("Созданно: " + TextFormat.fullDate(place.getGps().getTime()));
            alertDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            updatePlace(text, title, marker.getPosition(), privates, place.getGps().getTime());
                            dialog.cancel();
                        }
                    }
            );
            alertDialog.setNeutralButton("Удалить", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deletePlace(place);
                            marker.setAlpha(0);
                            dialog.cancel();
                        }
                    }
            );
//        alertDialog.setNeutralButton("Переместить", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                setMode(MODE.MOVE);
//                Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
//                        "Выберите место на карте для заметки", Snackbar.LENGTH_SHORT).show();
//                dialog.cancel();
//            }
//        });
            alertDialog.show();
        }
    }

    private void updatePlace(EditText text, EditText title, LatLng position, CheckBox privates, long time) {
        final Place place = Place.newBuilder()
                .setText(text.getText().toString())
                .setTitle(title.getText().toString())
                .setGps(new Gps(position, time))
                .setCreated(time)
                .setId(component.uController().get().getId())
                .setNumberTrip(component.trackerController().getNumberTrip())
                .setPrivates(!privates.isChecked()) // По дефолту для всех видна, Если не чекнута значит только для юзера
                .build();

        controllerPlace.updateMyPlace(place, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(InterestingPlaceMapActivity.this, "Сохранено", Toast.LENGTH_SHORT).show();
                // А здесь ее отключить
            }

            @Override
            public void onError(Object obj) {
                Toast.makeText(InterestingPlaceMapActivity.this, "Ошибка сохранения" + (String) obj.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletePlace(Place place) {
        controllerPlace.deleteMyPlace(place, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(InterestingPlaceMapActivity.this, "Успешно", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Object obj) {
                Toast.makeText(InterestingPlaceMapActivity.this, "Ошибка: " + (String) obj, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Place searchNoteByMarker(Marker marker) {
        if (marker == null || mPlaceSet == null) {
            throw new IllegalArgumentException();
        }
        for (Place place : mPlaceSet) {
            if (marker.getPosition().latitude == place.getGps().getLatitude()
                    && marker.getPosition().longitude == place.getGps().getLongitude()) {
                return place;
            }
        }
        return null;
    }

    private void addMarkerPlace(final LatLng latLng) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(InterestingPlaceMapActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_interes_place_box, null);
        alertDialog.setView(view);
        alertDialog.setTitle("Еще одно крутое место");
        final EditText text = (EditText) view.findViewById(R.id.editTextBox);
        final EditText title = (EditText) view.findViewById(R.id.editTitle);
        final TextView date = (TextView) view.findViewById(R.id.tvDateNote);
        final CheckBox privates = (CheckBox) view.findViewById(R.id.checkBoxPrivatesPlace);
        date.setText(TextFormat.date_ddMMyy(System.currentTimeMillis()));
        alertDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                placeManager(text, title, latLng, privates);
                dialog.cancel();
            }
        });
        alertDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void placeManager(EditText text, EditText title, LatLng latLng, CheckBox privates) {
        final Place place = Place.newBuilder()
                .setText(text.getText().toString())
                .setId(App.getInstance(this).getComponent().uController().get().getId())
                .setTitle(title.getText().toString())
                .setCreated(System.currentTimeMillis())
                .setGps(new Gps(latLng, System.currentTimeMillis()))
                .setNumberTrip(component.trackerController().getNumberTrip()) // TODO привязать к истории странствий.
                .setPrivates(!privates.isChecked()) // По дефолту для всех видна, Если не чекнута значит только для юзера
                .build();
        //TODO Добавить крутящуюся хуйнюшку )
        map.addMarker(new MarkerOptions()
                .position(new LatLng(place.getGps().getLatitude(), place.getGps().getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue)));
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {
                    dialog(marker);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
        controllerPlace.saveMyPlace(place, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(InterestingPlaceMapActivity.this, "Сохранено", Toast.LENGTH_SHORT).show();
                // А здесь ее отключить
            }

            @Override
            public void onError(Object obj) {
                //  Toast.makeText(InterestingPlaceMapActivity.this, "Ошибка сохранения" + (String) obj.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private LatLng initAddMarkers() throws Exception {
        LatLng latLng = new LatLng(55, 55); // this default random value
        for (Place place : mPlaceSet) {
            latLng = new LatLng(place.getGps().getLatitude(), place.getGps().getLongitude());
            map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .snippet(TextFormat.partialOutput(place.getTitle(), LENGTH_CUT_TEXT))
                    .title(TextFormat.date_ddMMyy(place.getGps().getTime()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_blue))
            );
        }
        return latLng;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public MODE getMode() {
        return mode;
    }

    public void setMode(MODE mode) {
        this.mode = mode;
    }

}
