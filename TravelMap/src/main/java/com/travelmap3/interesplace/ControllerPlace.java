package com.travelmap3.interesplace;

import android.content.Context;
import android.util.Log;

import com.travelmap3.App;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;

import java.util.HashSet;

import javax.inject.Inject;

/**
 * Created by Andre on 20.06.2016.
 */
public class ControllerPlace {
    public ManagerPLace managerPLace;

    private InternetConnect internetConnect;

    @Inject
    public ControllerPlace(ManagerPLace managerPLace, InternetConnect internetConnect) {
        this.managerPLace = managerPLace;
        this.internetConnect = internetConnect;
    }

    public HashSet<Place> getMyPlaces() {
        return managerPLace.takeDb();
    }

    /**
     * Сохраняем место. Реализуем следующую логику.
     * Сохраняем в бд - если ошибка бросаем вывод что косяк.
     * И отправляем на сервер, кидаем в колбэк. успех или не успех.
     *
     * @param place
     * @param callBackResponse
     */
    void saveMyPlace(Place place, CallBackResponse callBackResponse) {
        long error = managerPLace.saveDb(place);
        if (error < 1) {
            callBackResponse.onError("Save database error");
        } else {
            managerPLace.sendServer(place, callBackResponse);
            callBackResponse.onSuccess("Успех");
        }
    }

    public void updateMyPlace(Place place, CallBackResponse callBackResponse) {
        long error = managerPLace.updateDb(place);
        if (error < 1) {
            callBackResponse.onError("Save database error");
        } else {
            managerPLace.sendServer(place, callBackResponse);
        }
    }

    /**
     * Если удаляем с сервера, то удаляемся и с бд. Иначе никак.
     *
     * @param place
     * @param callBackResponse
     */
    public void deleteMyPlace(Place place, CallBackResponse callBackResponse) {
        managerPLace.delete(place, callBackResponse);
    }


    /**
     * Получаем все места всех пользователей от сервера
     *
     * @return
     */
    public void getAllPlaces(CallBackResponse callBackResponse) {

        Log.d("LOG place", "get places, cash:" + checkCachePlaces() + " inet: " + internetConnect.isActiveWiFiConnection());
        if (checkCachePlaces() && internetConnect.isActiveWiFiConnection()) {
            managerPLace.getServer(callBackResponse);
            Log.d("LOG place", "get serv");
        } else {
            callBackResponse.onSuccess(managerPLace.takeAllDb());
            Log.d("LOG place", "get placeDb");
        }
    }

    private boolean checkCachePlaces() {
        return System.currentTimeMillis() - Preferences.getInstance().loadTimeLastUpdatePlacesAll() > 100000;
    }

    public void getServerMyPlaces(Context context, CallBackResponse callback) {
        managerPLace.getServer(App.getInstance(context).getComponent().uController().get().getToken(), App.getInstance(context).getComponent().uController().get().getId(), callback);
    }

    public void clearDB() {
        managerPLace.deleteAll();
    }
}
