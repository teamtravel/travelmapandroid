package com.travelmap3.interesplace;

import com.google.gson.annotations.SerializedName;
import com.travelmap3.model.Gps;

/**
 * Created by Andre on 20.06.2016.
 */

public class Place {
    private int uid;
    private String text; // описание обьекта интересного для пользователя
    private int numbertrip; // Счетчик путешествий
    private Gps gps;
    private String title;
    private boolean privates;// Если ссыль приватная.
    private String imageurl;// ССЫЛКА НА КАРТИНКУ
    private boolean isExpiredToken;
    @SerializedName("created")
    private long created;

    private Place() {
        isExpiredToken = false;
    }

    public static Builder newBuilder() {
        return new Place().new Builder();
    }

    public String getImageUrl() {
        return imageurl;
    }

    public boolean getPrivates() {
        return privates;
    }

    public String getText() {
        return text;
    }

    public int getNumbertrip() {
        return numbertrip;
    }

    public Gps getGps() {
        return gps;
    }

    public String getTitle() {
        return title;
    }

    public void setNumbertrip(int numbertrip) {
        this.numbertrip = numbertrip;
    }

    public boolean isExpiredToken() {
        return isExpiredToken;
    }

    public int getId() {
        return uid;
    }

    public void setId(Integer id) {
        this.uid = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public class Builder {

        private Builder() {
            // private constructor
        }

        public Builder setText(String text) {
            Place.this.text = text;
            return this;
        }

        public Builder setId(int id) {
            Place.this.uid = id;
            return this;
        }

        public Builder setTitle(String title) {
            Place.this.title = title;
            return this;
        }

        public Builder setNumberTrip(int n) {
            Place.this.numbertrip = n;
            return this;
        }

        public Builder setCreated(long created) {
            Place.this.created = created;
            return this;
        }

        public Builder setImageurl(String url) {
            Place.this.imageurl = url;
            return this;
        }

        public Builder setPrivates(boolean p) {
            Place.this.privates = p;
            return this;
        }

        public Builder setGps(Gps gps) {
            Place.this.gps = gps;
            return this;
        }

        public Place build() {
            return Place.this;
        }
    }

}
