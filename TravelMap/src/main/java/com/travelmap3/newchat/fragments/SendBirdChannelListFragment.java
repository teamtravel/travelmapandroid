package com.travelmap3.newchat.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sendbird.android.ChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.model.Channel;
import com.travelmap3.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;


public  class SendBirdChannelListFragment extends Fragment {

    private SendBirdChannelListHandler mHandler;
    private ListView mListView;
    private ChannelListQuery mChannelListQuery;
    private ChannelListQuery mChannelListSearchQuery;
    private SendBirdChannelAdapter mCurrentAdapter;
    private SendBirdChannelAdapter mAdapter;
    private SendBirdChannelAdapter mSearchAdapter;
    private EditText mEtxtSearch;

    public  interface SendBirdChannelListHandler {
        void onChannelSelected(Channel channel);
    }

    public void setSendBirdChannelListHandler(SendBirdChannelListHandler handler) {
        mHandler = handler;
    }

    public SendBirdChannelListFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sendbird_fragment_channel_list, container, false);
        initUIComponents(rootView);
        mChannelListQuery = SendBird.queryChannelList();
        mChannelListQuery.next(new ChannelListQuery.ChannelListQueryResult() {
            @Override
            public void onResult(Collection<Channel> channels) {
                mAdapter.addAll(channels);
                if (channels.size() <= 0) {
                    Toast.makeText(getActivity(), "No channels were found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Exception e) {
            }
        });

        showChannelList();

        return rootView;

    }

    private void initUIComponents(View rootView) {
        mListView = (ListView) rootView.findViewById(R.id.list);
        mAdapter = new SendBirdChannelAdapter(getActivity());
        mSearchAdapter = new SendBirdChannelAdapter(getActivity());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Channel channel = mCurrentAdapter.getItem(position);
                if (mHandler != null) {
                    mHandler.onChannelSelected(channel);
                }
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount >= (int) (totalItemCount * 0.8f)) {
                    loadMoreChannels();
                }
            }
        });

        mEtxtSearch = (EditText) rootView.findViewById(R.id.etxt_search);
        mEtxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 0) {
                    showChannelList();
                } else {
                    showSearchList();
                }
            }
        });
        mEtxtSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    search(mEtxtSearch.getText().toString());
                }
                return false;
            }
        });
    }

    private void loadMoreChannels() {
        if (mCurrentAdapter == mAdapter) {
            if (mChannelListQuery != null && mChannelListQuery.hasNext() && !mChannelListQuery.isLoading()) {
                mChannelListQuery.next(new ChannelListQuery.ChannelListQueryResult() {
                    @Override
                    public void onResult(Collection<Channel> channels) {
                        mAdapter.addAll(channels);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Exception e) {
                    }
                });
            }
        } else if (mCurrentAdapter == mSearchAdapter) {
            if (mChannelListSearchQuery != null && mChannelListSearchQuery.hasNext() && !mChannelListSearchQuery.isLoading()) {
                mChannelListSearchQuery.next(new ChannelListQuery.ChannelListQueryResult() {
                    @Override
                    public void onResult(Collection<Channel> channels) {
                        mSearchAdapter.addAll(channels);
                        mSearchAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Exception e) {
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mAdapter.notifyDataSetChanged();
        mSearchAdapter.notifyDataSetChanged();
    }

    private void showChannelList() {
        mCurrentAdapter = mAdapter;
        mListView.setAdapter(mCurrentAdapter);
        mCurrentAdapter.notifyDataSetChanged();
    }

    private void showSearchList() {
        mCurrentAdapter = mSearchAdapter;
        mListView.setAdapter(mCurrentAdapter);
        mCurrentAdapter.notifyDataSetChanged();
    }

    private void search(String keyword) {
        if (keyword == null || keyword.length() <= 0) {
            showChannelList();
            return;
        }

        showSearchList();

        mChannelListSearchQuery = SendBird.queryChannelList(keyword);
        mChannelListSearchQuery.next(new ChannelListQuery.ChannelListQueryResult() {
            @Override
            public void onResult(Collection<Channel> channels) {
                mSearchAdapter.clear();
                mSearchAdapter.addAll(channels);
                mSearchAdapter.notifyDataSetChanged();
                if (channels.size() <= 0) {
                    Toast.makeText(getActivity(), "No channels were found.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(Exception e) {
            }
        });
    }

    public static class SendBirdChannelAdapter extends BaseAdapter {
        private final LayoutInflater mInflater;
        private final ArrayList<Channel> mItemList;


        public SendBirdChannelAdapter(Context context) {
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mItemList = new ArrayList<Channel>();
        }

        @Override
        public int getCount() {
            return mItemList.size();
        }

        @Override
        public Channel getItem(int position) {
            return mItemList.get(position);
        }

        public void clear() {
            mItemList.clear();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void add(Channel channel) {
            mItemList.add(channel);
            notifyDataSetChanged();
        }

        public void addAll(Collection<Channel> channels) {
            mItemList.addAll(channels);
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;

            if (convertView == null) {
                viewHolder = new ViewHolder();

                convertView = mInflater.inflate(R.layout.sendbird_view_channel, parent, false);
                viewHolder.setView("selected_container", convertView.findViewById(R.id.selected_container));
                viewHolder.getView("selected_container").setVisibility(View.GONE);
                viewHolder.setView("img_thumbnail", convertView.findViewById(R.id.img_thumbnail));
                viewHolder.setView("txt_topic", convertView.findViewById(R.id.txt_topic));
                viewHolder.setView("txt_desc", convertView.findViewById(R.id.txt_desc));

                convertView.setTag(viewHolder);
            }

            Channel item = getItem(position);
            viewHolder = (ViewHolder) convertView.getTag();

            Glide.with(mInflater.getContext()).load(item.getCoverUrl()).into(viewHolder.getView("img_thumbnail", ImageView.class));

            viewHolder.getView("txt_topic", TextView.class).setText("#" + item.getUrlWithoutAppPrefix());
            viewHolder.getView("txt_desc", TextView.class).setText("" + item.getMemberCount() + ((item.getMemberCount() <= 1) ? " Member" : " Members"));

            if (item.getUrl().equals(SendBird.getChannelUrl())) {
                viewHolder.getView("selected_container").setVisibility(View.VISIBLE);
            } else {
                viewHolder.getView("selected_container").setVisibility(View.GONE);
            }

            return convertView;
        }

        private static class ViewHolder {
            private Hashtable<String, View> holder = new Hashtable<String, View>();

            public void setView(String k, View v) {
                holder.put(k, v);
            }

            public View getView(String k) {
                return holder.get(k);
            }

            public <T> T getView(String k, Class<T> type) {
                return type.cast(getView(k));
            }
        }
    }
}
