package com.travelmap3.newchat.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.sendbird.android.MemberCountQuery;
import com.sendbird.android.MemberListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.model.Member;
import com.travelmap3.R;
import com.travelmap3.newchat.adapter.MemberListAdapter;

import java.util.Collection;

import static com.travelmap3.newchat.SendBirdMemberListActivity.CHANNEL_URL;
import static com.travelmap3.newchat.SendBirdMemberListActivity.NICKNAME;
import static com.travelmap3.newchat.SendBirdMemberListActivity.UUID;

/**
 * Created by andrey.gusenkov on 27/01/2017.
 */

public class SendBirdMemberListFragment extends Fragment {
    private ListView mListView;
    private MemberListAdapter mAdapter;
    private String mChannelUrl;
    private SendBirdMemberListFragmentHandler mHandler;

    public interface SendBirdMemberListFragmentHandler {
        public void onTitleChanged(String title);
    }

    public void setHandler(SendBirdMemberListFragmentHandler handler) {
        mHandler = handler;
    }

    public SendBirdMemberListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sendbird_fragment_member_list, container, false);

        initSendBird(getArguments());
        initUIComponents(rootView);

        return rootView;

    }

    private void initSendBird(Bundle extras) {
        if (extras != null) {
            String uuid = extras.getString(UUID);
            String nickname = extras.getString(NICKNAME);
            String gcmRegToken = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("SendBirdGCMToken", "");
            mChannelUrl = extras.getString(CHANNEL_URL);
            SendBird.login(SendBird.LoginOption.build(uuid).setUserName(nickname).setGCMRegToken(gcmRegToken));
        }
    }


    private void initUIComponents(View rootView) {
        mListView = (ListView) rootView.findViewById(R.id.list);
        mAdapter = new MemberListAdapter(getActivity());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
        mListView.setAdapter(mAdapter);
        loadTitle();
        loadMembers();
    }

    private void loadTitle() {
        SendBird.queryMemberCount(mChannelUrl).get(new MemberCountQuery.MemberCountQueryResult() {
            @Override
            public void onResult(int total, int online, int accumulative) {
                if (mHandler != null) {
                    mHandler.onTitleChanged("Total " + total + " / " + "Online " + online);
                }
            }

            @Override
            public void onError(SendBirdException e) {
                Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void loadMembers() {
        mAdapter.clear();
        mAdapter.notifyDataSetChanged();

        SendBird.queryMemberList(mChannelUrl, false).get(new MemberListQuery.MemberListQueryResult() {
            @Override
            public void onResult(Collection<Member> collection) {
                mAdapter.addAll(collection);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(SendBirdException e) {
                Toast.makeText(getActivity(), "Error " + e.getCode() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
