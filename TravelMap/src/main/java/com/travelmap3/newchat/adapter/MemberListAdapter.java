package com.travelmap3.newchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sendbird.android.model.Member;
import com.travelmap3.App;
import com.travelmap3.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by andrey.gusenkov on 25/01/2017.
 */

public class MemberListAdapter extends BaseAdapter {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ArrayList<Member> mItemList;

    public MemberListAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = new ArrayList<Member>();
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Member getItem(int position) {
        return mItemList.get(position);
    }

    public void clear() {
        mItemList.clear();
    }

    public Member remove(int index) {
        return mItemList.remove(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addAll(Collection<Member> members) {
        mItemList.addAll(members);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.sendbird_view_member, parent, false);
            viewHolder.setView("root_view", convertView);
            viewHolder.setView("img_thumbnail", convertView.findViewById(R.id.img_thumbnail));
            viewHolder.setView("txt_name", convertView.findViewById(R.id.txt_name));
            viewHolder.setView("txt_status", convertView.findViewById(R.id.txt_status));
            convertView.setTag(viewHolder);
        }

        final Member item = getItem(position);
        viewHolder = (ViewHolder) convertView.getTag();

        Glide.with(mContext).load(getPhotoLink(item.getId(), item.getImageUrl())).into(viewHolder.getView("img_thumbnail", ImageView.class));

        viewHolder.getView("txt_name", TextView.class).setText(item.getName());
        if (item.isOnline()) {
            viewHolder.getView("txt_status", TextView.class).setText("Online");
        } else {
            if (item.getLastSeenAt() == 0) {
                // Undefined. Never seen.
                viewHolder.getView("txt_status", TextView.class).setText("");
            } else {
                viewHolder.getView("txt_status", TextView.class).setText(new SimpleDateFormat("dd/MMM/yyyy").format(new Date(item.getLastSeenAt())));
            }
        }
        return convertView;
    }

    private class ViewHolder {
        private Hashtable<String, View> holder = new Hashtable<String, View>();

        public void setView(String k, View v) {
            holder.put(k, v);
        }

        public View getView(String k) {
            return holder.get(k);
        }

        public <T> T getView(String k, Class<T> type) {
            return type.cast(getView(k));
        }
    }

    private String getPhotoLink(String uuid, String def) {
        String photoLink = def;
        int id = 0;
        try {
            id = Integer.valueOf(uuid);
        } catch (Exception e) {
            id = 0;
            e.printStackTrace();
        }

        if (id != 0)
            photoLink = App.getInstance(mContext).getComponent().uController().getUserById(id).getPhoto50();
        return photoLink;
    }
}
