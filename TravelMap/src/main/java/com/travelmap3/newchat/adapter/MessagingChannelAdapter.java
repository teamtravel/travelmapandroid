package com.travelmap3.newchat.adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sendbird.android.SendBird;
import com.sendbird.android.model.BroadcastMessage;
import com.sendbird.android.model.FileLink;
import com.sendbird.android.model.Message;
import com.sendbird.android.model.MessageModel;
import com.sendbird.android.model.MessagingChannel;
import com.travelmap3.App;
import com.travelmap3.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by andrey.gusenkov on 25/01/2017.
 */


public class MessagingChannelAdapter extends BaseAdapter {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ArrayList<MessagingChannel> mItemList;

    public static List<MessagingChannel> sortMessagingChannels(List<MessagingChannel> messagingChannels) {
        Collections.sort(messagingChannels, new Comparator<MessagingChannel>() {
            @Override
            public int compare(MessagingChannel lhs, MessagingChannel rhs) {
                long lhsv = lhs.getLastMessageTimestamp();
                long rhsv = rhs.getLastMessageTimestamp();
                return (lhsv == rhsv) ? 0 : (lhsv < rhsv) ? 1 : -1;
            }
        });

        return messagingChannels;
    }


    public MessagingChannelAdapter(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = new ArrayList<MessagingChannel>();
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public MessagingChannel getItem(int position) {
        return mItemList.get(position);
    }

    public void clear() {
        mItemList.clear();
    }

    public MessagingChannel remove(int index) {
        return mItemList.remove(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(MessagingChannel channel) {
        mItemList.add(channel);
        notifyDataSetChanged();
    }

    public void addAll(List<MessagingChannel> channels) {
        mItemList.addAll(channels);
        notifyDataSetChanged();
    }

    public void replace(MessagingChannel newChannel) {
        for (MessagingChannel oldChannel : mItemList) {
            if (oldChannel.getId() == newChannel.getId()) {
                mItemList.remove(oldChannel);
                break;
            }
        }

        mItemList.add(0, newChannel);
        notifyDataSetChanged();
    }

    private static String getDisplayMemberNames(List<MessagingChannel.Member> members) { //// TODO: 25/01/2017  must to  Fullname by ID
        if (members.size() < 2) {
            return "No Members";
        }

        StringBuffer names = new StringBuffer();
        for (MessagingChannel.Member member : members) {
            if (member.getId().equals(SendBird.getUserId())) {
                continue;
            }

            names.append(", " + member.getName());
        }
        return names.delete(0, 2).toString();
    }

    private static String getDisplayTimeOrDate(Context context, long milli) {
        Date date = new Date(milli);

        if (System.currentTimeMillis() - milli > 60 * 60 * 24 * 1000l) {
            return DateFormat.getDateFormat(context).format(date);
        } else {
            return DateFormat.getTimeFormat(context).format(date);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.sendbird_view_messaging_channel, parent, false);
            viewHolder.setView("img_thumbnail", convertView.findViewById(R.id.img_thumbnail));
            viewHolder.setView("txt_topic", convertView.findViewById(R.id.txt_topic));
            viewHolder.setView("txt_member_count", convertView.findViewById(R.id.txt_member_count));
            viewHolder.setView("txt_unread_count", convertView.findViewById(R.id.txt_unread_count));
            viewHolder.setView("txt_date", convertView.findViewById(R.id.txt_date));
            viewHolder.setView("txt_desc", convertView.findViewById(R.id.txt_desc));

            convertView.setTag(viewHolder);
        }

        MessagingChannel item = getItem(position);
        viewHolder = (ViewHolder) convertView.getTag();

        Glide.with(mContext).load(getDisplayCoverImageUrl(item.getMembers())).into(viewHolder.getView("img_thumbnail", ImageView.class));

        viewHolder.getView("txt_topic", TextView.class).setText(getDisplayMemberNames(item.getMembers()));

        if (item.getUnreadMessageCount() > 0) {
            viewHolder.getView("txt_unread_count", TextView.class).setVisibility(View.VISIBLE);
            viewHolder.getView("txt_unread_count", TextView.class).setText("" + item.getUnreadMessageCount());
        } else {
            viewHolder.getView("txt_unread_count", TextView.class).setVisibility(View.INVISIBLE);
        }

        if (item.isGroupMessagingChannel()) {
            viewHolder.getView("txt_member_count", TextView.class).setVisibility(View.VISIBLE);
            viewHolder.getView("txt_member_count", TextView.class).setText("" + item.getMemberCount());
        } else {
            viewHolder.getView("txt_member_count", TextView.class).setVisibility(View.GONE);
        }

        if (item.hasLastMessage()) {
            MessageModel message = item.getLastMessage();
            if (message instanceof Message) {
                viewHolder.getView("txt_date", TextView.class).setText(getDisplayTimeOrDate(mContext, message.getTimestamp()));
                viewHolder.getView("txt_desc", TextView.class).setText("" + ((Message) message).getMessage());
            } else if (message instanceof BroadcastMessage) {
                viewHolder.getView("txt_date", TextView.class).setText(getDisplayTimeOrDate(mContext, message.getTimestamp()));
                viewHolder.getView("txt_desc", TextView.class).setText("" + ((BroadcastMessage) message).getMessage());
            } else if (message instanceof FileLink) {
                viewHolder.getView("txt_date", TextView.class).setText(getDisplayTimeOrDate(mContext, message.getTimestamp()));
                viewHolder.getView("txt_desc", TextView.class).setText("Файл...");
            } else {
                viewHolder.getView("txt_date", TextView.class).setText("");
                viewHolder.getView("txt_desc", TextView.class).setText("");
            }
        } else {
            viewHolder.getView("txt_date", TextView.class).setText("");
            viewHolder.getView("txt_desc", TextView.class).setText("");
        }

        return convertView;
    }

    private static class ViewHolder {
        private Hashtable<String, View> holder = new Hashtable<String, View>();

        public void setView(String k, View v) {
            holder.put(k, v);
        }

        public View getView(String k) {
            return holder.get(k);
        }

        public <T> T getView(String k, Class<T> type) {
            return type.cast(getView(k));
        }
    }

    private String getDisplayCoverImageUrl(List<MessagingChannel.Member> members) {
        for (MessagingChannel.Member member : members) {
            if (member.getId().equals(SendBird.getUserId())) {
                continue;
            }

            return getPhotoLink(member.getId(), member.getImageUrl());
        }

        return "";
    }

    private String getPhotoLink(String uuid, String def) {
        String photoLink = def;
        int id = 0;
        try {
            id = Integer.valueOf(uuid);
        } catch (Exception e) {
            id = 0;
            e.printStackTrace();
        }

        if (id != 0)
            photoLink = App.getInstance(mContext).getComponent().uController().getUserById(id).getPhoto50();
        return photoLink;
    }
}
