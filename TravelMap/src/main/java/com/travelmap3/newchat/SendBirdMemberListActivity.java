package com.travelmap3.newchat;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.travelmap3.R;
import com.travelmap3.newchat.fragments.SendBirdMemberListFragment;

public class SendBirdMemberListActivity extends FragmentActivity {
    public static final String CHANNEL_URL = "channelUrl";
    public static final String NICKNAME = "nickname";
    public static final String UUID = "uuid";
    private SendBirdMemberListFragment mSendBirdMemberListFragment;

    private ImageButton mBtnClose;
    private TextView mTxtTitle;
    private View mTopBarContainer;

    public static Bundle makeSendBirdArgs(String uuid, String nickname, String channelUrl) {
        Bundle args = new Bundle();
        args.putString(UUID, uuid);
        args.putString(NICKNAME, nickname);
        args.putString(CHANNEL_URL, channelUrl);
        return args;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sendbird_member_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initUIComponents();
        initFragment();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resizeMenubar();
    }


    private void resizeMenubar() {
        ViewGroup.LayoutParams lp = mTopBarContainer.getLayoutParams();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lp.height = (int) (28 * getResources().getDisplayMetrics().density);
        } else {
            lp.height = (int) (48 * getResources().getDisplayMetrics().density);
        }
        mTopBarContainer.setLayoutParams(lp);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.sendbird_slide_in_from_top, R.anim.sendbird_slide_out_to_bottom);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void initFragment() {
        mSendBirdMemberListFragment = new SendBirdMemberListFragment();
        mSendBirdMemberListFragment.setArguments(getIntent().getExtras());
        mSendBirdMemberListFragment.setHandler(new SendBirdMemberListFragment.SendBirdMemberListFragmentHandler() {
            @Override
            public void onTitleChanged(String title) {
                mTxtTitle.setText(title);
            }
        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mSendBirdMemberListFragment)
                .commit();
    }

    private void initUIComponents() {
        mTopBarContainer = findViewById(R.id.top_bar_container);
        mTxtTitle = (TextView) findViewById(R.id.txt_title);

        mBtnClose = (ImageButton) findViewById(R.id.btn_close);

        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        resizeMenubar();
    }



}
