package com.travelmap3.newchat;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.sendbird.android.SendBird;
import com.sendbird.android.model.Channel;
import com.travelmap3.R;
import com.travelmap3.newchat.fragments.SendBirdChannelListFragment;


public class SendBirdChannelListActivity extends FragmentActivity {
    public static final String NICKNAME = "nickname";
    public static final String UUID = "uuid";
    private SendBirdChannelListFragment mSendBirdChannelListFragment;

    private ImageButton mBtnClose;
    private ImageButton mBtnSettings;

    private View mTopBarContainer;

    public static Bundle makeSendBirdArgs(String uuid, String nickname) {
        Bundle args = new Bundle();
        args.putString(UUID, uuid);
        args.putString(NICKNAME, nickname);
        return args;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sendbird_channel_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initSendBird(getIntent().getExtras());
        initFragment();
        initUIComponents();
    }

    private void initSendBird(Bundle extras) {
        String uuid = extras.getString(UUID);
        String nickname = extras.getString(NICKNAME);
        String gcmRegToken = PreferenceManager.getDefaultSharedPreferences(this).getString("SendBirdGCMToken", "");
        SendBird.login(SendBird.LoginOption.build(uuid).setUserName(nickname).setGCMRegToken(gcmRegToken));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resizeMenubar();
    }


    private void resizeMenubar() {
        ViewGroup.LayoutParams lp = mTopBarContainer.getLayoutParams();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lp.height = (int) (28 * getResources().getDisplayMetrics().density);
        } else {
            lp.height = (int) (48 * getResources().getDisplayMetrics().density);
        }
        mTopBarContainer.setLayoutParams(lp);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.sendbird_slide_in_from_top, R.anim.sendbird_slide_out_to_bottom);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void initFragment() {
        mSendBirdChannelListFragment = new SendBirdChannelListFragment();
        mSendBirdChannelListFragment.setSendBirdChannelListHandler(new SendBirdChannelListFragment.SendBirdChannelListHandler() {
            @Override
            public void onChannelSelected(Channel channel) {
                Intent data = new Intent();
                data.putExtra("channelUrl", channel.getUrl());
                setResult(RESULT_OK, data);
                finish();
            }
        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mSendBirdChannelListFragment)
                .commit();
    }

    private void initUIComponents() {
        mTopBarContainer = findViewById(R.id.top_bar_container);


        mBtnClose = (ImageButton) findViewById(R.id.btn_close);
        mBtnSettings = (ImageButton) findViewById(R.id.btn_settings);

        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBtnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        resizeMenubar();
    }


}
