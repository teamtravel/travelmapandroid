package com.travelmap3.newchat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.sendbird.android.MessagingChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdNotificationHandler;
import com.sendbird.android.model.Mention;
import com.sendbird.android.model.MessagingChannel;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.maps.GoogleMapCustomMarkerClusteringActivity;
import com.travelmap3.newchat.adapter.MessagingChannelAdapter;

import java.util.List;

import static com.travelmap3.menu.MainMenuActivity.REQUEST_SENDBIRD_MESSAGING_ACTIVITY;
import static com.travelmap3.menu.MainMenuActivity.REQUEST_SENDBIRD_USER_LIST_ACTIVITY;


public class SendBirdMessagingChannelListActivity extends FragmentActivity {
    private SendBirdMessagingChannelListFragment mSendBirdMessagingChannelListFragment;

    private ImageButton mBtnClose;
    private ImageButton mBtnSettings;
    private TextView mTxtChannelUrl;
    private View mTopBarContainer;
    private String mUserId;
    private String mNickname;
    private String mGcmRegToken;
    private CircleProgressBar progress;

    public static Bundle makeSendBirdArgs(String uuid, String nickname) {
        Bundle args = new Bundle();
        args.putString("uuid", uuid);
        args.putString("nickname", nickname);
        return args;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.sendbird_slide_in_from_bottom, R.anim.sendbird_slide_out_to_top);
        setContentView(R.layout.activity_sendbird_messaging_channel_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mUserId = getIntent().getExtras().getString("uuid");
        mNickname = getIntent().getExtras().getString("nickname");
        mGcmRegToken = PreferenceManager.getDefaultSharedPreferences(SendBirdMessagingChannelListActivity.this).getString("SendBirdGCMToken", "");

        initFragment();
        initUIComponents();
        initSendBird();

        //Toast.makeText(this, "Long press the channel to leave.", Toast.LENGTH_LONG).show();
    }

    private void initSendBird() {
        SendBird.login(SendBird.LoginOption.build(mUserId).setUserName(mNickname).setGCMRegToken(mGcmRegToken));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resizeMenubar();
    }


    private void resizeMenubar() {
        ViewGroup.LayoutParams lp = mTopBarContainer.getLayoutParams();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lp.height = (int) (28 * getResources().getDisplayMetrics().density);
        } else {
            lp.height = (int) (48 * getResources().getDisplayMetrics().density);
        }
        mTopBarContainer.setLayoutParams(lp);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_SENDBIRD_USER_LIST_ACTIVITY && data != null) {
            startMessaging(data.getStringArrayExtra("userIds"));
        }
    }

    private void startMessaging(String[] targetUserIds) {
        Intent intent = new Intent(this, SendBirdMessagingActivity.class);
        Bundle args = SendBirdMessagingActivity.makeMessagingStartArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName(), targetUserIds);
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_MESSAGING_ACTIVITY);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SendBird.join("");
//        SendBird.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        SendBird.disconnect();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.sendbird_slide_in_from_top, R.anim.sendbird_slide_out_to_bottom);
    }

    private void initFragment() {
        mSendBirdMessagingChannelListFragment = new SendBirdMessagingChannelListFragment();
        mSendBirdMessagingChannelListFragment.setSendBirdMessagingChannelListHandler(new SendBirdMessagingChannelListFragment.SendBirdMessagingChannelListHandler() {
            @Override
            public void onMessagingChannelSelected(MessagingChannel messagingChannel) {
                Intent intent = new Intent(SendBirdMessagingChannelListActivity.this, SendBirdMessagingActivity.class);
                Bundle args = SendBirdMessagingActivity.makeMessagingJoinArgs(mUserId, mNickname, messagingChannel.getUrl());
                intent.putExtras(args);
                startActivity(intent);
            }

        });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mSendBirdMessagingChannelListFragment)
                .commit();
    }

    private void startUserList() {
        Intent intent = new Intent(SendBirdMessagingChannelListActivity.this, SendBirdUserListActivity.class);
        Bundle args = SendBirdUserListActivity.makeSendBirdArgs(App.getInstance(this).getComponent().uController().get().getStringId(), App.getInstance(this).getComponent().uController().get().getFullName());
        intent.putExtras(args);
        startActivityForResult(intent, REQUEST_SENDBIRD_USER_LIST_ACTIVITY);
    }

    private void initUIComponents() {
        mTopBarContainer = findViewById(R.id.top_bar_container);
        mTxtChannelUrl = (TextView) findViewById(R.id.txt_channel_url);

        mBtnClose = (ImageButton) findViewById(R.id.btn_close);
        mBtnSettings = (ImageButton) findViewById(R.id.btn_settings);
        (findViewById(R.id.btn_map_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), GoogleMapCustomMarkerClusteringActivity.class), REQUEST_SENDBIRD_USER_LIST_ACTIVITY);
            }
        });
        (findViewById(R.id.btn_search_user)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startUserList();
            }
        });
        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        resizeMenubar();
    }

    public static class SendBirdMessagingChannelListFragment extends Fragment {
        private SendBirdMessagingChannelListHandler mCoolback;
        private ListView mListView;
        private MessagingChannelAdapter mAdapter;
        private MessagingChannelListQuery mMessagingChannelListQuery;

        public interface SendBirdMessagingChannelListHandler {
            void onMessagingChannelSelected(MessagingChannel channel);
        }
        
        public void setSendBirdMessagingChannelListHandler(SendBirdMessagingChannelListHandler handler) {
            mCoolback = handler;
        }

        public SendBirdMessagingChannelListFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.sendbird_fragment_messaging_channel_list, container, false);
            initUIComponents(rootView);
            return rootView;
        }

        private void initUIComponents(View rootView) {

            mListView = (ListView) rootView.findViewById(R.id.list);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MessagingChannel channel = mAdapter.getItem(position);
                    if (mCoolback != null) {
                        mCoolback.onMessagingChannelSelected(channel);
                    }
                }
            });
            mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if (firstVisibleItem + visibleItemCount >= (int) (totalItemCount * 0.8f)) {
                        loadNextChannels();
                    }
                }
            });
            mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    final MessagingChannel channel = mAdapter.getItem(position);
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Leave")
                            .setMessage("Do you want to leave this channel?")
                            .setPositiveButton("Leave", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mAdapter.remove(position);
                                    mAdapter.notifyDataSetChanged();
                                    SendBird.endMessaging(channel.getUrl());
                                }
                            })
                            .setNeutralButton("Hide", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mAdapter.remove(position);
                                    mAdapter.notifyDataSetChanged();
                                    SendBird.hideMessaging(channel.getUrl());
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).create().show();
                    return true;
                }
            });

            mAdapter = new MessagingChannelAdapter(getActivity());
            mListView.setAdapter(mAdapter);
        }


        private void loadNextChannels() {
            if (mMessagingChannelListQuery == null || mMessagingChannelListQuery.isLoading()) {
                return;
            }

            if (mMessagingChannelListQuery.hasNext()) {
                mMessagingChannelListQuery.next(new MessagingChannelListQuery.MessagingChannelListQueryResult() {
                    @Override
                    public void onResult(List<MessagingChannel> messagingChannels) {
                        mAdapter.addAll(messagingChannels);
                    }

                    @Override
                    public void onError(int i) {
                    }
                });
            }
        }

        @Override
        public void onResume() {
            super.onResume();

            SendBird.registerNotificationHandler(new SendBirdNotificationHandler() {
                @Override
                public void onMessagingChannelUpdated(MessagingChannel messagingChannel) {
                    mAdapter.replace(messagingChannel);
                }

                @Override
                public void onMentionUpdated(Mention mention) {

                }
            });


            if (mMessagingChannelListQuery == null) {
                mMessagingChannelListQuery = SendBird.queryMessagingChannelList();
                mMessagingChannelListQuery.setLimit(30);
            }

            mMessagingChannelListQuery.next(new MessagingChannelListQuery.MessagingChannelListQueryResult() {
                @Override
                public void onResult(List<MessagingChannel> list) {
                    mAdapter.clear();
                    mAdapter.addAll(list);
                    mAdapter.notifyDataSetChanged();

                    SendBird.join("");
                    SendBird.connect();
                }

                @Override
                public void onError(int i) {

                }
            });
        }

        @Override
        public void onPause() {
            super.onPause();
            if (mMessagingChannelListQuery != null) {
                mMessagingChannelListQuery.cancel();
                mMessagingChannelListQuery = null;
            }

            SendBird.disconnect();
        }
    }


}
