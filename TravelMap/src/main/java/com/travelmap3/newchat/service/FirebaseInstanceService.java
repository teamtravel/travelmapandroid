package com.travelmap3.newchat.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.handler.RegisterPushTokenHandler;
import com.sendbird.android.shadow.com.google.gson.JsonElement;

/**
 * Created by andrey.gusenkov on 27/01/2017.
 */

public class FirebaseInstanceService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("SERVICE TOKEN", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Get updated InstanceID token.
        SendBird.registerPushToken(new RegisterPushTokenHandler() {
            @Override
            public void onError(SendBirdException e) {
                Log.d("SERVICE TOKEN", e.getCode() + " onError");
            }

            @Override
            public void onSuccess(JsonElement jsonElement) {
                Log.d("SERVICE TOKEN", jsonElement.toString() + " json");
            }
        });
    }
}
