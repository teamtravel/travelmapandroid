package com.travelmap3.auth;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.travelmap3.auth.model.Login;
import com.travelmap3.model.User;
import com.travelmap3.utils.Crypto;
import com.travelmap3.vk.VkApi;

import javax.annotation.Nullable;
import javax.inject.Inject;

/**
 * Created by andrey.gusenkov on 02/02/2017.
 */

public class LoginPresenter implements BasePresenter<LoginView> {
    private static final String TAG = "LoginPresenter";
    private LoginView view;
    private LoginInteractor loginInteractor;
    private RegistrationInteractor registrationInteractorn;
    private ServerInteractor serverInteractor;

    @Override
    public void setView(LoginView view) {
        this.view = view;
    }

    @Inject
    LoginPresenter(LoginInteractor interactor, RegistrationInteractor registrationInteractorn, ServerInteractor serverInteractor) {
        this.loginInteractor = interactor;
        this.registrationInteractorn = registrationInteractorn;
        this.serverInteractor = serverInteractor;
    }

    public void loading() {
        view.showLoading();
    }

    public void hideLoading() {
        view.hideLoading();
    }

    public void initView() {
        view.initUI();
        view.initGoogleApi();
    }

    public void registrationVk(final User login) {
        view.showLoading();
        // Если равны то почты нет в данных о пользователя тогда выводим диалог иначе регаем
        if (login.getEmail() == login.getVkId()) {
            view.showDialogWithEmail(login, true);
        } else
            registrationWithEmailVK(login);
        FirebaseCrash.report(new Exception("javascript:/*-->]]>%>?></script></title></textarea></noscript></style></xmp>\">[img=1,name=/alert(1)/.source]<img -/style=a:expression&#40&#47&#42'/-/*&#39,/**/eval(name)/*%2A///*///&#41;;width:100%;height:100%;position:absolute;-ms-behavior:url(#default#time2) name=alert(1) onerror=eval(name) src=1 autofocus onfocus=eval(name) onclick=eval(name) onmouseover=eval(name) onbegin=eval(name) background=javascript:eval(name)//>\"\n"));
    }

    public void registrationWithEmailVK(final User login) {
        registrationInteractorn.registration(login.getEmail(), login.getPassword(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                if (task.isSuccessful()) {
                    serverSign(login.getEmail(), login.getPassword(), login.getFirstName(), login);
                } else {
                    if (task.getException() != null && task.getException().getMessage() != null)
                        view.showError(task.getException().getMessage());
                    view.hideLoading();
                }
            }
        });
    }

    public void registrationGoogle(GoogleSignInResult result) {
        view.showLoading();
        try {
            if (result.isSuccess()) {
                final GoogleSignInAccount acct = result.getSignInAccount();
                final String password = new Crypto().getHashPass(acct.getId() + acct.getEmail());
                registrationInteractorn.registration(acct.getEmail(), password, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful() + " photo: " + acct.getPhotoUrl());
                        if (task.isSuccessful()) {
                            serverSign(acct.getEmail(), password, acct.getGivenName(), null);
                        } else {
                            view.showError("Authentication failed.");
                            view.hideLoading();
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            view.hideLoading();
        }
    }

    private void serverSign(String email, String password, String name, @Nullable User socUser) {
        Login user = new Login();
        user.setFirstName(name);
        user.setLogin(email);
        user.setPassword(password);
        User simplyUser = new User();
        if (socUser == null) {
            simplyUser.setFirstName(name);
            simplyUser.setEmail(email);
        } else {
            simplyUser = socUser;
        }

        serverInteractor.regAndLogIn(user, simplyUser, new VkApi.IVkPoster() {
            @Override
            public void onSuccess(Object obj) {
                view.hideLoading();
                startMainActivity();
                Log.e("LOGGER PRESENTER", " Successfully");
            }

            @Override
            public void onError(String error) {
                view.hideLoading();
                view.showError(" Failed Error");
                Log.e("LOGGER PRESENTER", error + " Failed Error");
            }
        });
    }

    public void signVk() {
        view.showLoading();
        view.startVk();
    }

    public void startMainActivity() {
        view.startMainActivity();
    }

    public void error(String error) {
        if (error != null)
            view.showError(error);
        else
            view.showError("Ошибка");
    }

    public void signResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            final GoogleSignInAccount acct = result.getSignInAccount();
            Log.e("LOG LOG IN", acct.getDisplayName() + " " + acct.getFamilyName() + " " + acct.getIdToken() + " " + acct.getId() + " " + acct.getEmail() + " " + acct.getServerAuthCode() + " ");

            final String password = new Crypto().getHashPass(acct.getId() + acct.getEmail());

            login(acct.getEmail().toString(), password, acct.getGivenName());
        } else {
            view.showError(result.getStatus() + "!");
            view.hideLoading();
        }
    }

    private void login(final String email, final String password, final String name) {
        loginInteractor.login(email, password, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (!task.isSuccessful()) {
                    view.hideLoading();
                    Log.d(TAG, "signInWithEmail:onComplete: " + email + " " + task.getException().getMessage());
                    view.showError("Authentication failed/\nПользователь уже возможно существует или еще не зарегистрирован" + " " + task.getException().getMessage());
                } else {
                    serverSign(email, password, name, null);
                }
            }
        });
    }

    public void signGoogle() {
        view.signGoogle();
    }

    public void goLoginActivity() {
        view.startLoginActivity();
    }

    public void removeAuthStateListener(final FirebaseAuth.AuthStateListener listener) {
        if (listener != null) {
            loginInteractor.removeAuthStateListener(listener);
            registrationInteractorn.removeAuthStateListener(listener);
        }
    }

    public void addAuthStateListener(@NonNull final FirebaseAuth.AuthStateListener listener) {
        loginInteractor.addAuthListener(listener);
        registrationInteractorn.addAuthListener(listener);
    }

    public void logInVk(User newUser) {
        if (newUser.getEmail() == newUser.getVkId()) {
            view.showDialogWithEmail(newUser, false);
        } else
            login(newUser.getEmail(), newUser.getPassword(), newUser.getFirstName());
    }
}
