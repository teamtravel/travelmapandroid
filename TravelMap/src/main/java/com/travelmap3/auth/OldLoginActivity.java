package com.travelmap3.auth;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.auth.model.Login;
import com.travelmap3.interesplace.ControllerPlace;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.User;
import com.travelmap3.notes.SyncNote;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Crypto;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.vk.VkApi;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by An on 27.09.2015.
 */
public class OldLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String LOG = "Login";
    private ImageButton buttonVkLogin;
    private Button buttonSend;
    private CircleProgressBar progress;
    private TextInputLayout passwordOuter, emailOuter;
    private EditText password;
    private EditText email;
    private Button buttonRecovery;
    private boolean isRecoveryOut = false;
    private int width, height;
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public ControllerPlace controllerPlace;
    /*
     * после метода setClickable(false), а затем анимации возврата непрозрачности и setClickable(true)
     * кнопка не работает. Пришлось создать переменную для костыля
     */
    private boolean isVkButtonClickable = true;// for not nonclickable after it is

    private static final int RC_SIGN_IN = 9001;
    InternetConnect internetConnect;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        App application = (App) getApplication();
//        mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen Login");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        internetConnect = App.getInstance(this).getComponent().getInternetConnect();
        controllerPlace = App.getInstance(this).getComponent().controller();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button btnRegistration = (Button) findViewById(R.id.buttonReg);
        email = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.pass);
        progress = (CircleProgressBar) findViewById(R.id.progressLogin);
        buttonRecovery = (Button) findViewById(R.id.btnForgotPassword);
        buttonSend = (Button) findViewById(R.id.buttonSend);
        buttonVkLogin = (ImageButton) findViewById(R.id.vk_auth_button);
        passwordOuter = (TextInputLayout) findViewById(R.id.editText);
        emailOuter = (TextInputLayout) findViewById(R.id.editText2);

        email.requestFocus();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // Set the dimensions of the sign-in button.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_google);
        signInButton.setSize(SignInButton.SIZE_ICON_ONLY);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            email.setText(intent.getStringExtra(EMAIL));
            password.setText(intent.getStringExtra(PASSWORD));
        }
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSend();
            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable text) {
                String result = text.toString().replaceAll(" ", "");
                if (!text.toString().equals(result)) {
                    email.setText(result);
                    email.setSelection(result.length());
                }
            }
        });
        if (btnRegistration != null) {
            btnRegistration.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!internetConnect.isInternetOn()) {
                        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                                R.string.login_unable_connect_to_internet, Snackbar.LENGTH_SHORT).show();
                    } else {
                        startActivity(new Intent(OldLoginActivity.this, RegistrationActivity.class).addFlags(
                                Intent.FLAG_ACTIVITY_SINGLE_TOP));
                    }
                }
            });
        }

        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();  // deprecated
        height = display.getHeight();

    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /**
     * Функция для отправки данных на сервер.
     */
    public void onClickSend() {//send login&password
        if (!isRecoveryOut) {//если не экран восстановления пароля, то на вход
            if (!internetConnect.isInternetOn()) {
                Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.login_unable_connect_to_internet, Snackbar.LENGTH_SHORT).show();
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

            } else {
                String email = this.email.getText().toString();
                String password = this.password.getText().toString();

                if (password.isEmpty()) {
                    Toast.makeText(getApplicationContext(), R.string.login_input_password, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                int response;
                if ((response = Utils.isCorrectEmail(email)) != 0) {
                    if (response == 1) {
                        Toast.makeText(this, R.string.login_login_incorrect, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (response == 2) {
                        Toast.makeText(this, R.string.login_email_field_empty, Toast.LENGTH_SHORT).show();
                        return;
                    }

                }

                final Login login = new Login();
                login.setLogin(email);
                login.setPassword(new Crypto().getHashPass(password));

                progress.setVisibility(View.VISIBLE);

                RetrofitService.getInstanceAPI().postLogin(Config.VERSION_API, login, new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        if (user.getId() > 0) {
                            Toast.makeText(getApplicationContext(), R.string.login_successfully,
                                    Toast.LENGTH_SHORT).show();
                            // сохраняем данные пользователя на устройство
                            // startRegistrationInChatUsers(user);
                            saveDataUsers(user, response);
                            saveTimeAuth();
                            startMenuActivity();

                        } else {
                            if (BuildConfig.DEBUG) {
                                Log.w(LOG, "parse : " + com.travelmap3.utils.Utils.parseBody(response.getBody()) + " ");
                            }
                            Toast.makeText(getApplicationContext(),
                                    R.string.login_these_login_and_password_not_exist,
                                    Toast.LENGTH_LONG).show();
//                            mTracker.send(new HitBuilders.EventBuilder()
//                                    .setCategory("Screen Login")
//                                    .setAction(getString(R.string.login_not_successfully_authorisation))
//                                    .setLabel(getString(R.string.login_these_login_and_password_not_exist))
//                                    .build());
                        }
                        progress.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progress.setVisibility(View.INVISIBLE);
                        try {
//                            mTracker.send(new HitBuilders.EventBuilder()
//                                    .setCategory("Screen Login")
//                                    .setAction(getString(R.string.login_authorisation_error))
//                                    .build());

                            if (checkInError(error)) {
                                return;
                            }
                            if (error.getResponse().getStatus() == 404
                                    || error.getResponse().getStatus() == 401) {
                                Toast.makeText(getApplicationContext(),
                                        R.string.login_these_login_and_password_not_exist,
                                        Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } else {// восстановление
//            mTracker.send(new HitBuilders.EventBuilder()
//                    .setCategory("Screen Login")
//                    .setAction(("Восстановление пароля"))
//                    .build());
            if (!internetConnect.isInternetOn()) {
                Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.login_unable_connect_to_internet, Snackbar.LENGTH_SHORT).show();
            } else {
                final String emailStr = email.getText().toString();

                if (Utils.isCorrectEmail(emailStr) == 0) {
                    progress.setVisibility(View.VISIBLE);
                    try {
                        RetrofitService.getInstanceAPI()
                                .postResetPassword(Config.VERSION_API, emailStr, new Callback<String>() {
                                    @Override
                                    public void success(String user, Response response) {
                                        //ToDo: Тут кидает ошибку при нажатии кнопки "восстановить"
                                        // throws fatal error while click on "reestablish"
                                        Toast.makeText(getApplicationContext(), R.string.login_check_in_mail,
                                                Toast.LENGTH_LONG)
                                                .show();
                                        progress.setVisibility(View.INVISIBLE);
                                        showDialogRecovery();
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        progress.setVisibility(View.INVISIBLE);
                                        if (checkInError(error)) {
                                            return;
                                        }
                                        showDialogRecovery();
                                        Toast.makeText(getApplicationContext(), error.getMessage(),
                                                Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInputFromInputMethod(getCurrentFocus().getWindowToken(),
                            InputMethodManager.SHOW_IMPLICIT);
                }
            }

        }
    }

    private void saveTimeAuth() {
        Preferences.getInstance().saveTimeAuthUser();
    }

    private void saveDataUsers(User user, Response response) {

        List<Header> headerList = response.getHeaders();

        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase("token")) {
                Preferences.getInstance().saveUserId(user.getId());
                Preferences.getInstance().saveToken(header.getValue());
                user.setToken(header.getValue());
            }
        }

        App.getInstance(this).getComponent().uController().setUser(user, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {

            }

            @Override
            public void onError(Object obj) {

                Toast.makeText(getApplicationContext(), R.string.login_error_save_user_data,
                        Toast.LENGTH_SHORT).show();
            }
        });

        getNoteSynchronized(user); // получаем все записи дневника пользователя
        getPlacesUser();

    }

    private void getPlacesUser() {
        controllerPlace.getServerMyPlaces(this, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Log.i(LOG, "Получили мои места с сервера");
            }

            @Override
            public void onError(Object obj) {
                Log.e(LOG, "Не получили мои места с сервера");
            }
        });
    }


    private boolean checkInError(RetrofitError error) {
        if (error == null || error.getResponse() == null) {
            Toast.makeText(getApplicationContext(),
                    R.string.login_check_in_connection,
                    Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public void onForgotClick(View v) {
        isRecoveryOut = !isRecoveryOut;
        if (isRecoveryOut) {
            animateToRecoverPassword();
        } else {
            animateFromRecoverPassword();
        }
        email.requestFocus();
    }

    private void animateToRecoverPassword() {

        passwordOuter.animate().alpha(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                passwordOuter.setClickable(false);
            }

            public void onAnimationEnd(Animator animation) {
            }

            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {
            }
        });
        buttonVkLogin.animate().alpha(0);
        isVkButtonClickable = false;
        buttonSend.animate()
                .translationY(emailOuter.getBottom() - buttonSend.getY())
                .setListener(new Animator.AnimatorListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
                                email, 0);
                    }

                    public void onAnimationCancel(Animator animation) {
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }
                });
        buttonSend.setText(R.string.login_recovery);
        buttonRecovery.setText(R.string.return_back);
        Toast.makeText(getBaseContext(), "У нас проблемы. Для востановления напиши нам на почту travelmap.su@mail.ru",
                Toast.LENGTH_SHORT).show();

    }

    private void animateFromRecoverPassword() {

        buttonRecovery.setText(R.string.login_forgot_password);
        passwordOuter.setClickable(true);
        isVkButtonClickable = true;

        passwordOuter.animate().alpha(1);
        buttonVkLogin.animate().alpha(1);
        buttonSend.animate()
                .translationY(emailOuter.getBottom() - buttonSend.getY())
                .setListener(new Animator.AnimatorListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(
                                email, 0);
                    }

                    public void onAnimationCancel(Animator animation) {
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }
                });

        buttonSend.setText(R.string.login_send);

    }


    private void startMenuActivity() {
        Intent intent = new Intent(OldLoginActivity.this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("LOG IN LOGGER", "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            progress.setVisibility(View.VISIBLE);
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    progress.setVisibility(View.INVISIBLE);
                    handleSignInResult(googleSignInResult);
                }
            });
        }

    }

    public void onVkClick(View view) {
        if (isVkButtonClickable) {
            if (!internetConnect.isInternetOn()) {
                Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.login_unable_connect_to_internet, Snackbar.LENGTH_SHORT).show();
            } else {
                progress.setVisibility(View.VISIBLE);
                VkApi.start(OldLoginActivity.this);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**
         * Handle vk auth result
         */
        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                VkApi.storeByServerUserInfo("password", new VkApi.IVkPoster() {
                    @Override
                    public void onSuccess(Object obj) {
                        progress.setVisibility(View.INVISIBLE);
                        startActivity();
                    }

                    @Override
                    public void onError(String error) {
                        progress.setVisibility(View.INVISIBLE);
                        Toast.makeText(getBaseContext(), getString(R.string.login_error) + error,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onError(VKError error) {
                progress.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplication(), R.string.login_error_while_authorisation,
                        Toast.LENGTH_LONG).show();
            }
        };
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void startActivity() {
        startActivity(new Intent(OldLoginActivity.this, MainMenuActivity.class));
        finish();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("LOGGER LOG IN", "handleSignInResult:" + result.isSuccess() + "\n " + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e("LOG LOG IN", acct.getDisplayName() + " " + acct.getFamilyName() + " " + acct.getIdToken() + " " + acct.getId() + " " + acct.getEmail() + " " + acct.getServerAuthCode() + " ");
            Toast.makeText(this, acct.getDisplayName() + " " + acct.getFamilyName() + " " + acct.getIdToken() + " " + acct.getId(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, result.getStatus() + "", Toast.LENGTH_LONG).show();
        }
    }

    //----------------- recovery --------------------------------
    private void sendRecover(String key, String password) throws Exception {
        RetrofitService.getInstanceAPI().postRecoveryPassword(
                Config.VERSION_API,
                key, new Crypto().getHashPass(password),
                new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        Toast.makeText(getApplicationContext(),
                                R.string.login_password_was_successfully_changed, Toast.LENGTH_SHORT)
                                .show();
                        startActivity(new Intent(getApplicationContext(), OldLoginActivity.class).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage() + " " +
                                getString(R.string.login_poorly), Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }

    private void showDialogRecovery() {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getApplicationContext());
            View view = getLayoutInflater().inflate(R.layout.dialog_recovery_password, null);
            alertDialog.setView(view).setTitle(getString(R.string.login_new_password));
            final EditText newPassword = (EditText) view.findViewById(R.id.editTextNewPassword);
            final EditText newPasswordRepeat = (EditText) view.findViewById(
                    R.id.editTextNewPasswordRepeat);
            final EditText key = (EditText) view.findViewById(R.id.editTextKeyRecovery);

            alertDialog.setPositiveButton(R.string.login_send, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                if (newPassword.getText()
                                        .toString()
                                        .compareTo(newPasswordRepeat.getText().toString()) != 0) {
                                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                                            R.string.login_passwords_are_not_equal, Snackbar.LENGTH_SHORT).show();
                                } else {
                                    sendRecover(key.getText().toString(), newPassword.getText().toString());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            dialog.cancel();
                        }
                    }
            );
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-------------------- recovery end -------------------------

    @Override
    public void onBackPressed() {
        if (isRecoveryOut) {
            // frameRecoveryPswdFragment.setVisibility(View.GONE);
            animateFromRecoverPassword();
            isRecoveryOut = false;
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Функция для синхронизации записей дневников.
     * Запускаем после тогда как мы зашли в приложения первый раз
     * И подтягиваем все данные
     */
    public void getNoteSynchronized(User user) {
        try {
            new SyncNote(App.getInstance(this).getComponent().notesDB()).getServer(user.getToken(), user.getId(), user.getId());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Ошибка синхронизации дневника", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("LOGGER LOG IN", "onConnectionFailed:" + connectionResult);
    }
}
