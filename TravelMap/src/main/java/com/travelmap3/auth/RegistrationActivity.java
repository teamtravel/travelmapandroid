package com.travelmap3.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.BuildConfig;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.auth.model.Login;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Crypto;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RegistrationActivity extends Activity {
  private static final String LOG = "RegistrationActivity";

  private EditText passText;
  private EditText passConfirmText;
  private TextView textAnswer;
  private EditText nickNameText;
  private EditText emailText;
  private CircleProgressBar progress;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
//
//    mTracker = App.getInstance(this).getDefaultTracker();
//    mTracker.setScreenName("Screen Registration");
//    mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_registration);
    emailText = (EditText) findViewById(R.id.editEmail);
    passText = (EditText) findViewById(R.id.editPass);
    passConfirmText = (EditText) findViewById(R.id.editConfirmPass);
    progress = (CircleProgressBar) findViewById(R.id.progress2);
    Button btnSendReg = (Button) findViewById(R.id.buttonSendRegistration);
    textAnswer = (TextView) findViewById(R.id.textAnswer);
    nickNameText = (EditText) findViewById(R.id.editNikName);

    btnSendReg.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        if (passwordAndEmailAreCorrect()) {
          Login user = new Login();
          user.setLogin(emailText.getText().toString());
          user.setPassword(new Crypto().getHashPass(passText.getText().toString()));
          user.setFirstName(nickNameText.getText().toString());
          try {
            progress.setVisibility(View.VISIBLE);

            RetrofitService.getInstanceAPI()
                .postRegistration(Config.VERSION_API, user, new Callback<Response>() {
                  @Override
                  public void success(Response response, Response response2) {
                    if (response.getStatus() == 200) {
//                      mTracker.send(new HitBuilders.EventBuilder()
//                          .setCategory("ui_action")
//                          .setAction(getString(R.string.registration_screen_action_title))
//                          .setLabel(
//                              getString(R.string.registration_successfully_sent_to_server))
//                          .build());
                      progress.setVisibility(View.INVISIBLE);
                      Toast.makeText(RegistrationActivity.this,
                          R.string.registration_successfully,
                          Toast.LENGTH_SHORT)
                          .show();
                      Intent intent = new Intent(RegistrationActivity.this,
                              OldLoginActivity.class);
                      intent.putExtra("email", emailText.getText().toString());
                      intent.putExtra("password", passText.getText().toString());
                      startActivity(intent);
                      finish();
                    }
                  }

                  @Override
                  public void failure(RetrofitError error) {
                    progress.setVisibility(View.INVISIBLE);
//                    mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction(getString(R.string.registration_screen_action_title))
//                        .setLabel(
//                            getString(R.string.registration_not_successfully_sent_to_server))
//                        .build());
                    try {
                      if (error.getResponse().getStatus() == 409) {
                        Toast.makeText(RegistrationActivity.this,
                            R.string.registration_login_is_busy, Toast.LENGTH_SHORT)
                            .show();
                      }
                    } catch (Exception e) {
                      if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                      }
                    }
                  }
                });
          } catch (Exception e) {
            e.printStackTrace();
          } finally {
            progress.setVisibility(View.INVISIBLE);
          }
        } else {
          Toast.makeText(getApplicationContext(), R.string.registration_incorrect_data,
              Toast.LENGTH_SHORT)
              .show();
        }
      }
    });

  }

  private boolean passwordAndEmailAreCorrect() {
    String emailUser = emailText.getText().toString();
    String nickName = nickNameText.getText().toString();
    String password = passText.getText().toString();
    String passwordConfirm = passConfirmText.getText().toString();

    if (emailUser.length() == 0) {
      textAnswer.setText(R.string.registration_email_field_not_fill);
    } else if (password.length() == 0) {
      textAnswer.setText(R.string.registration_password_field_not_fill);
    } else if (nickName.length() < 3) {
      textAnswer.setText(R.string.registration_length_of_name_is_invalid);
    } else if (nickName.equalsIgnoreCase(emailUser)) {
      textAnswer.setText(R.string.registration_name_can_not_equals_email);
    } else if (passwordConfirm.length() == 0) {
      textAnswer.setText(R.string.registration_repeat_password_field_not_fill);
    } else if (passwordConfirm.length() < 5) {
      textAnswer.setText(R.string.registration_length_of_password_is_invalid);
    } else if (passwordConfirm.compareTo(password) != 0) {
      textAnswer.setText(R.string.registration_passwords_are_not_equal);
    } else if (Utils.isCorrectEmail(emailUser) != 0) {
      textAnswer.setText(R.string.email_is_incorrect);
    } else {
      return true;
    }
    return false;
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
  }
}