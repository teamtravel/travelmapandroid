package com.travelmap3.auth;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.User;
import com.travelmap3.vk.VkApi;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import javax.inject.Inject;

/**
 * Created by andrey.gusenkov on 01/02/2017.
 */

public final class NewLoginActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener, FirebaseAuth.AuthStateListener, GoogleApiClient.ConnectionCallbacks, LoginView {
    private static final String TAG = "NewLoginActivity";
    private static final int RC_SIGN_UP = 8002;
    private CircleProgressBar progress;
    private GoogleApiClient mGoogleApiClient;
    ImageButton vkButton;
    ImageButton googleButton;
    private Boolean mAllowNavigation = true;
    private static final int RC_SIGN_IN = 9001;
    private static String passwordVK = "";

    @Inject
    LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle save) {
        super.onCreate(save);
        setContentView(R.layout.activity_new_login);
        App.getInstance(this).getComponent().inject(this);
        presenter.setView(this);
        presenter.initView();

    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.e("LOG OUT", status.toString() + "");
                    }
                });
    } // for testing


    @Override
    public void onStop() {
        super.onStop();
        presenter.removeAuthStateListener(this);
    }

    @Override
    public void initUI() {
        progress = (CircleProgressBar) findViewById(R.id.progressLogin);
        vkButton = (ImageButton) findViewById(R.id.vk_auth_button);
        googleButton = (ImageButton) findViewById(R.id.google_auth_button);

        (findViewById(R.id.older_auth)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.goLoginActivity();
            }
        });

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.signGoogle();
            }
        });

        vkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.signVk();
            }
        });
    }

    @Override
    public void initGoogleApi() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void startVk() {
        showPasswordDialog();
    }

    private void showPasswordDialog() {
        final Dialog dialog = new Dialog(this);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_recovery_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.setTitle("Пароль для TravelMap");

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                presenter.hideLoading();
            }
        });
        dialog.findViewById(R.id.button_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.hideLoading();
                String newPassword = ((EditText) dialog.findViewById(R.id.editTextNewPassword)).getText().toString();
                String newPasswordRepeat = ((EditText) dialog.findViewById(R.id.editTextNewPasswordRepeat)).getText().toString();
                if (checkPassword(newPassword, newPasswordRepeat)) {
                    passwordVK = newPassword;
                    dialog.dismiss();

                    VkApi.start(NewLoginActivity.this);
                } else {
                    dialog.dismiss();
                    Snackbar snack = Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content), R.string.login_passwords_are_not_equal, Snackbar.LENGTH_LONG);
                    View view = snack.getView();
                    TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    tv.setTextColor(Color.WHITE);
                    snack.show();
                }


            }
        });
        dialog.show();
        presenter.hideLoading();


    }

    private void dialogChoiseVk(final User newUser) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_log_in);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });

        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.button_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override // Регистрация
            public void onClick(View v) {
                presenter.registrationVk(newUser);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.button_log_in).setOnClickListener(new View.OnClickListener() {
            @Override// Авторизация
            public void onClick(View v) {
                presenter.logInVk(newUser);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean checkPassword(String newPassword, String newPasswordRepeat) {
        Log.d("LOGGER", newPassword + " " + newPasswordRepeat + " " + newPassword.compareTo(newPasswordRepeat) + " " + newPassword.equals(newPasswordRepeat));
        return newPassword.equals(newPasswordRepeat);
    }

    @Override
    public void signGoogle() {
        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        final Dialog dialog = new Dialog(this);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_log_in);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.button_sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(signInIntent, RC_SIGN_UP);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.button_log_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(signInIntent, RC_SIGN_IN);
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public void startLoginActivity() {
        signOut();
        VkApi.logout();
        //startActivity(new Intent(this, OldLoginActivity.class));
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showDialogWithEmail(final User login, final boolean isRegistration) {
        final Dialog dialog = new Dialog(this);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_recovery_password);
        dialog.setTitle("Введите Email");
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                presenter.hideLoading();
            }
        });
        dialog.findViewById(R.id.edit_new_email).setVisibility(View.VISIBLE);
        dialog.findViewById(R.id.editTextNewPassword).setVisibility(View.GONE);
        dialog.findViewById(R.id.editTextNewPasswordRepeat).setVisibility(View.GONE);
        dialog.findViewById(R.id.button_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // presenter.hideLoading();
                String email = ((EditText) dialog.findViewById(R.id.email)).getText().toString();
                login.setEmail(email);
                if (Utils.isCorrectEmail(email) == 0) {
                    if (isRegistration)
                        presenter.registrationWithEmailVK(login);
                    else presenter.logInVk(login);
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                    Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                            R.string.email_is_incorrect, Snackbar.LENGTH_SHORT).show();
                }


            }
        });
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        /**
         * Handle vk auth result
         */
        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                presenter.loading();
                VkApi.storeByServerUserInfo(passwordVK, new VkApi.IVkPoster() {
                    @Override
                    public void onSuccess(Object obj) {
                        try {
                            presenter.hideLoading();
                            dialogChoiseVk((User) obj);
                            passwordVK = "";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        presenter.hideLoading();
                        presenter.error(getString(R.string.login_error) + error);
                    }
                });

            }

            @Override
            public void onError(VKError error) {
                presenter.hideLoading();
                presenter.error(getString(R.string.login_error_while_authorisation));
            }
        };

        if (requestCode == RC_SIGN_IN) {
            presenter.signResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
        }
        if (requestCode == RC_SIGN_UP) {
            presenter.registrationGoogle(Auth.GoogleSignInApi.getSignInResultFromIntent(data));
        }
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("LOGGER", connectionResult.getErrorMessage() + " ");
        presenter.error(connectionResult.getErrorMessage());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.addAuthStateListener(this);
    }

    @Override
    public void showLoading() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            // User is signed in
            Log.d(TAG, "onAuthStateChanged:signed_in:");
        } else {
            // User is signed out
            Log.d(TAG, "onAuthStateChanged:signed_out");
        }
    }
}
