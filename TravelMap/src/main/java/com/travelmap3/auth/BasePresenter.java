package com.travelmap3.auth;

/**
 * Created by andrey.gusenkov on 02/02/2017.
 */

public interface BasePresenter<T> {
    void setView(T view);
}
