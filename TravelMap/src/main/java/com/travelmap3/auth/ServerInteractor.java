package com.travelmap3.auth;

import android.util.Log;

import com.travelmap3.UserController;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.auth.model.Login;
import com.travelmap3.model.User;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;
import com.travelmap3.vk.VkApi;

import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static com.travelmap3.MainActivity.LOG;
import static com.travelmap3.auth.LoginInteractor.regFlag;

/**
 * Created by andrey.gusenkov on 03/02/2017.
 */
public class ServerInteractor {
    @Inject
    UserController userController;

    @Inject
    public ServerInteractor(UserController controller) {
        this.userController = controller;
    }

    public void regAndLogIn(final Login userLogin, final User socialUser, final VkApi.IVkPoster callback) {
        RetrofitService.getInstanceAPI()
                .postRegistration(Config.VERSION_API, userLogin, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {
                        if (response != null) {
                            if (response.getStatus() == 200) {
                                regFlag = true;
                                loginServer(userLogin, socialUser, callback);
                                Log.d(LOG, "registrationGoogle on vk api");
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        loginServer(userLogin, socialUser, callback);
                        if (error != null && error.getResponse() != null) {
                            if (com.travelmap3.BuildConfig.DEBUG) {
                                Log.e(LOG, "" + error.getResponse().getStatus());
                            }
                        }
                    }
                });
    }

    private void loginServer(Login loginUser, final User soc, final VkApi.IVkPoster callback) {
        RetrofitService.getInstanceAPI().postLogin(Config.VERSION_API, loginUser, new Callback<User>() {
            @Override
            public void success(User user, Response response) {

                CallBackResponse cb = new CallBackResponse() {
                    @Override
                    public void onSuccess(Object obj) {
                        callback.onSuccess(null);
                        saveTimeAuth();
                    }

                    @Override
                    public void onError(Object obj) {
                        callback.onError("Error save data user");
                    }
                };
                if (response != null) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("token")) {
                            Preferences.getInstance().saveUserId(user.getId());
                            Preferences.getInstance().saveToken(header.getValue());
                            user.setToken(header.getValue());
                        }
                    }



                    if (response.getStatus() == 200) {
                        if (regFlag) {
                            soc.setId(user.getId());
                            soc.setToken(user.getToken());

                            userController.setUser(soc, cb);
                        } else {
                            userController.setUser(user, cb);
                        }

                    } else {
                        callback.onError(response.getStatus() + "");
                    }
                } else {
                    callback.onError("Response equalse null");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.onError("failure in login in vk");
            }
        });
    }

    private void saveTimeAuth() {
        Preferences.getInstance().saveTimeAuthUser();
    }


}
