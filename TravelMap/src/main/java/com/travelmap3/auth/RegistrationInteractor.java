package com.travelmap3.auth;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

/**
 * Created by andrey.gusenkov on 03/02/2017.
 */
public class RegistrationInteractor {
    private final FirebaseAuth mAuth;

    @Inject
    public RegistrationInteractor() {
        mAuth = FirebaseAuth.getInstance();
    }

    public void registration(String email, String password, OnCompleteListener<AuthResult> listener) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(listener);
    }

    public void addAuthListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.addAuthStateListener(listener);
    }

    public void removeAuthStateListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.removeAuthStateListener(listener);
    }


}
