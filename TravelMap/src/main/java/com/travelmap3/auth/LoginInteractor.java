package com.travelmap3.auth;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by andrey.gusenkov on 02/02/2017.
 */
@Singleton
public class LoginInteractor {
    private static final String TAG = "LogInteractor";
    public static boolean regFlag = true;
    private final FirebaseAuth mAuth;

    @Inject
    LoginInteractor() {
        mAuth = FirebaseAuth.getInstance();
    }

    public void login(String email, String password, OnCompleteListener<AuthResult> listener) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(listener);
    }

    public void addAuthListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.addAuthStateListener(listener);
    }

    public void removeAuthStateListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.removeAuthStateListener(listener);
    }

}
