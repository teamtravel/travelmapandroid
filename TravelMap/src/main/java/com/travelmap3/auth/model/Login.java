package com.travelmap3.auth.model;

/**
 * Created by An on 22.09.2015.
 */
public class Login {
  private String login;// email
  private String password;
  private String firstName;
  private String idVk;
  private String image;
  private String status;



  public Login() {}

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstName() {
    return firstName != null ? firstName : "";
  }

  public void setFirstName(String firstName) {
    if (firstName != null) {
      this.firstName = firstName;
    }
  }

  public String getIdVk() {
    return idVk;
  }

  public void setIdVk(String idVk) {
    this.idVk = idVk;
  }
}
