package com.travelmap3.auth;

/**
 * Created by An on 11.10.2015.
 */
public final class Utils {

  private Utils() {
    // private empty constructor to unable of creation instance
  }

  public static int isCorrectEmail(String email) {
    if (email.isEmpty()) {
      return 2;
    }
    if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
      // if email is too short of smth like that it returns
      return 1;// Email не корректный
    }
    return 0; // Все хорошо
  }
}
