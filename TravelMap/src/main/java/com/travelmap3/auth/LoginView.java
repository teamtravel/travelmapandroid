package com.travelmap3.auth;

import com.travelmap3.model.User;

/**
 * Created by andrey.gusenkov on 02/02/2017.
 */

public interface LoginView {

    void showDialogWithEmail(User login, boolean isRegistration);

    void showLoading();

    void hideLoading();

    void initUI();

    void initGoogleApi();

    void startVk();

    void showError(String error);

    void signGoogle();

    void startLoginActivity();

    void startMainActivity();

}
