package com.travelmap3.tracker;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.model.Gps;
import com.travelmap3.utils.CalculateForGps;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit.Callback;

/**
 * Created by Andre on 18.01.2016.
 */
public class TrackerController {


    TrackerDB mTrackerDB;

    public int numberTrip;

    public Context context;

    @Inject
    public TrackerController(int num, TrackerDB db, Context context) {
        numberTrip = num;
        mTrackerDB = db;
        this.context = context;

    }

    public boolean save(Location location) {
//        try {
//            if (BuildConfig.DEBUG)
//                Toast.makeText(, "Gps save database " + location.getAccuracy() + " " + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_LONG).show(); // for debug
//        } catch (Exception e) {
//            e.getMessage();
//        }
        return mTrackerDB.insert(location, numberTrip) > 0;
    }

    public ArrayList<LatLng> getTrackLine(int numberTrip) {
        Cursor c = mTrackerDB.query(numberTrip);
        ArrayList<LatLng> listLatLng = new ArrayList<>();
        if (c != null) {
            if (c.moveToFirst())
                do {
                    double lat = c.getDouble(c.getColumnIndex(TrackerDB.DB_COLUMN_LAT));
                    double lon = c.getDouble(c.getColumnIndex(TrackerDB.DB_COLUMN_LONG)); // FIXME добавить остальные поля из бд. Использовать класс Gps
                    listLatLng.add(new LatLng(lat, lon));
                } while (c.moveToNext());
        }
        return listLatLng;
    }

    public void initNewTrip(String nameTrip, Callback callback) {
        double lat = App.getInstance(context).getComponent().uController().get().getLatitude();
        double lon = App.getInstance(context).getComponent().uController().get().getLongitude();
        long time = System.currentTimeMillis();
        TravelStory story = new TravelStory(time, 0, lat + " " + lon, "", nameTrip, 0, 0, numberTrip);
        RetrofitService.getInstanceAPI().postInitStory(App.getInstance(context).getComponent().uController().get().getToken(), Config.VERSION_API, App.getInstance(context).getComponent().uController().get().getId(), story, callback);
        mTrackerDB.saveDB(lat, lon, time, numberTrip, nameTrip);
    }

    public int getNumberTrip() {
        return numberTrip;
    }

    public void setNumberTrip(int numberTrip) {
        this.numberTrip = numberTrip;
    }

    public void finishTrip(Callback callback) {
        double lat = App.getInstance(context).getComponent().uController().get().getLatitude();
        double lon = App.getInstance(context).getComponent().uController().get().getLongitude();
        float distance = Preferences.getInstance().loadPassedDistance();
        long time = System.currentTimeMillis();
        TravelStory story = new TravelStory(0, time, "", lat + " " + lon, null, distance, 0, 0);
        RetrofitService.getInstanceAPI().updateStory(App.getInstance(context).getComponent().uController().get().getToken(), Config.VERSION_API, story, callback);
        mTrackerDB.update(lat, lon, time, numberTrip, distance, new CalculateForGps().getSpeedAv(distance));
    }

    public void saveServerFlag(long time) {
        long error = mTrackerDB.sFlagUpdate(time);
        if (BuildConfig.DEBUG) if (error < 1) Log.e("TrackerController flag", "" + error);
    }

    public ArrayList<Gps> getListGpsNotSendServer() {
        return mTrackerDB.getListGpsServerFlagFalse();
    }

    public ArrayList<TravelStory> getStory() {
        return mTrackerDB.getListStory();
    }

    public void countNumberTrip() {
        numberTrip++;
        Preferences.getInstance().saveNumberTrip(numberTrip); // увеличеваем количество путешествий. Так как сейчас новое началось.
    }

    public boolean deleteTracker(int number, Callback callback) {
        RetrofitService.getInstanceAPI().deleteStory(
                App.getInstance(context).getComponent().uController().get().getToken(),
                Config.VERSION_API,
                App.getInstance(context).getComponent().uController().get().getId(),
                mTrackerDB.getStoryByNumber(),
                callback);
        return mTrackerDB.deleteTrackerAndStory(number);
    }

    public boolean changeNameTacker(String name, int number) {
        //TODO sent to server api put tracker
        return mTrackerDB.update(name, number) > 0;
    }


}
