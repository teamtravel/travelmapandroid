package com.travelmap3.tracker;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.event.BusEvent;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andre on 18.07.2016.
 */
public class InfoMapFragmnet extends Fragment {
    private ArrayList<TravelStory> stories;
    private TravelStoryInfoAdapter adapter;

    public InfoMapFragmnet() {

    }

    public static InfoMapFragmnet getInstance(ArrayList<TravelStory> stories) {
        InfoMapFragmnet fragment = new InfoMapFragmnet();
        fragment.stories = stories;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_cluster_detail, null, false);
        try {
            ListView listView = (ListView) rootView.findViewById(R.id.cluster_info_listview);

            adapter = new TravelStoryInfoAdapter(getActivity(), stories);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    EventBus.getDefault().post(new BusEvent(TrackerMapsActivity.NEW_POLYLINE + position));// post in Maps Activity что бы смениля маршрут
                }
            });
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    dialogChangeNameOrDeleteTracker(stories.get(position).getNumber());
                    return false;
                }
            });

        } catch (Exception e) {
            Toast.makeText(getContext(), "Ошибка в трекере", Toast.LENGTH_SHORT).show();
        }
        return rootView;
    }

    private void dialogChangeNameOrDeleteTracker(final int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setNegativeButton("Удалить трекер путешествия", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                App.getInstance(getActivity()).getComponent().trackerController().deleteTracker(position, new Callback() {
                    @Override
                    public void success(Object o, Response response) {
                        updateListAdapter();
                        Toast.makeText(getContext(), "Успешно удалено", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getContext(), "Не очень удачно удалилось", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();
                dialog.cancel();
            }
        }).setNeutralButton("Изменить заголовок трекера", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View view = inflater.inflate(R.layout.dialog_note_box, null);
                view.findViewById(R.id.editText2Note).setVisibility(View.GONE);
                alertDialog.setView(view);
                alertDialog.setTitle("Новое название трека для путешествия");
                alertDialog.setPositiveButton("Изменить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText textTitle = (EditText) view.findViewById(R.id.editTitle);
                        if (App.getInstance(getActivity()).getComponent().trackerController().changeNameTacker(textTitle.getText().toString(), position)) {
                            Toast.makeText(getContext(), "Успешно", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(getContext(), "Не очень удачно сохранилось...", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                }).show();
                dialog.dismiss();
                dialog.cancel();
            }
        }).show();

    }

    private void updateListAdapter() {
        adapter.notifyDataSetChanged();
    }
}