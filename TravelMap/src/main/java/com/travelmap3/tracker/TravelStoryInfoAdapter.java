package com.travelmap3.tracker;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.travelmap3.R;
import com.travelmap3.utils.TextFormat;

import java.util.ArrayList;

/**
 * Created by Andre on 18.07.2016.
 */
public class TravelStoryInfoAdapter extends ArrayAdapter<TravelStory> {
    private final Activity activity;
    private final ArrayList<TravelStory> stories;

    public TravelStoryInfoAdapter(FragmentActivity activity, ArrayList<TravelStory> stories) {
        super(activity, R.layout.list_item_object, stories);
        this.stories = stories;
        this.activity = activity;
    }

    @Override
    public int getCount() throws NullPointerException {
        return stories==null? 0 :stories.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            rowView = inflater.inflate(R.layout.list_row_story, null, true);
            holder = new ViewHolder();
            holder.name = (TextView) rowView.findViewById(R.id.title);
            holder.data = (TextView) rowView.findViewById(R.id.time);
            holder.number = (TextView) rowView.findViewById(R.id.numbertrip);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.name.setText(stories.get(position).getName().compareTo("")==0? "Путешествие":stories.get(position).getName());
        holder.data.setText(TextFormat.date_ddMMyy(stories.get(position).getTimeStart()) + " " + TextFormat.date_ddMMyy(stories.get(position).getTimeFinish()));
        holder.number.setText(""+stories.get(position).getNumber());

        return rowView;
    }

    static class ViewHolder {
        public TextView name;
        public TextView data;
        public TextView number;

    }
}
