package com.travelmap3.tracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.support.annotation.Nullable;
import android.util.Log;

import com.travelmap3.BuildConfig;
import com.travelmap3.db.SqliteDBHelper;
import com.travelmap3.model.Gps;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Andre on 18.01.2016.
 */
public class TrackerDB extends SqliteDBHelper {

    private final String LOG = this.getClass().getName();

    public TrackerDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    public long insert(Gps gps, int numberTrip) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_NUMBER_TRIP, numberTrip);
        contentValues.put(DB_COLUMN_LONG, gps.getLongitude());
        contentValues.put(DB_COLUMN_TIME, gps.getTime());
        contentValues.put(DB_COLUMN_SPEED, gps.getSpeed());
        contentValues.put(DB_COLUMN_ALTITUDE, gps.getAltitude());
        contentValues.put(DB_COLUMN_LAT, gps.getLatitude());
        contentValues.put(DB_COLUMN_FLAG_SERVER_SAFE, 0);
        long printfCode = insertDB(DB_TABLE_GPS_TRACKER, contentValues);
        if (BuildConfig.DEBUG) Log.e(LOG, "END INSERT");
        return printfCode;
    }

    public long insert(Location loc, int numberTrip) {
        return insert(new Gps(loc), numberTrip);
    }

    public Cursor query(int numberTrip) {
        Cursor cursor = null;
        db = this.getReadableDatabase();
        db.beginTransaction();
        try {
            cursor = db.query(DB_TABLE_GPS_TRACKER, null, DB_COLUMN_NUMBER_TRIP + " =?", new String[]{String.valueOf(numberTrip)}, null, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        return cursor;
    }

    public void saveDB(double latStart, double lonStart, long timeStart, int numberTrip, @Nullable String nameTrip) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_TIME_START, timeStart);
        contentValues.put(DB_COLUMN_LAT_AND_LONG_START, String.valueOf(latStart) + " " + String.valueOf(lonStart));
        contentValues.put(DB_COLUMN_NUMBER_TRIP, numberTrip);
        if (nameTrip != null) contentValues.put(DB_COLUMN_NAME_OBJECT, nameTrip);
        long k = getWritableDatabase().insert(DB_TABLE_STORY_TRAVEL, null, contentValues);
        Log.e("LOG saveDB TRACKER", " code operation equlas " + k);
    }

    public void update(double latFinish, double lonFinnish, long timeFinish, int numberTrip, double distance, double speed) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_TIME_FINISH, timeFinish);
        contentValues.put(DB_COLUMN_LAT_AND_LONG_FINISH, String.valueOf(latFinish) + " " + String.valueOf(lonFinnish));
        contentValues.put(DB_COLUMN_SPEED, speed);
        contentValues.put(DB_COLUMN_DISTANCE, distance);
        long k = getWritableDatabase().update(DB_TABLE_STORY_TRAVEL, contentValues, DB_COLUMN_NUMBER_TRIP + "=?", new String[]{String.valueOf(numberTrip)});
        Log.e("LOG updates TRACKER", " code operation equlas " + k);
    }

    public long update(String nameTracker, int numberTrip) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_NAME_OBJECT, nameTracker);
        long k = getWritableDatabase().update(DB_TABLE_STORY_TRAVEL, contentValues, DB_COLUMN_NUMBER_TRIP + " =?", new String[]{String.valueOf(numberTrip)});
        Log.e("LOG update TRACKER", " code operation equlas " + k);
        return k;
    }

    public void clear() {
        clearAllT(DB_TABLE_STORY_TRAVEL);
        clearAllT(DB_TABLE_GPS_TRACKER);
    }

    public ArrayList<TravelStory> getListStory() {
        Cursor c = null;
        db = getReadableDatabase();
        db.beginTransaction();
        ArrayList<TravelStory> list = new ArrayList<>();
        try {
            c = db.query(DB_TABLE_STORY_TRAVEL, null, null, null, null, null, null);
            list.addAll(getListStoryCursor(c));
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return list;
    }

    private Collection<? extends TravelStory> getListStoryCursor(Cursor c) {
        ArrayList<TravelStory> list = new ArrayList<>();
        if (c.moveToFirst()) {
            int startTime = c.getColumnIndex(DB_COLUMN_TIME_START);
            int finishTime = c.getColumnIndex(DB_COLUMN_TIME_FINISH);
            int finisLatLon = c.getColumnIndex(DB_COLUMN_LAT_AND_LONG_FINISH);
            int startLatlon = c.getColumnIndex(DB_COLUMN_LAT_AND_LONG_START);
            int speed = c.getColumnIndex(DB_COLUMN_SPEED);
            int distance = c.getColumnIndex(DB_COLUMN_DISTANCE);
            int number = c.getColumnIndex(DB_COLUMN_NUMBER_TRIP);
            int name = c.getColumnIndex(DB_COLUMN_NAME_OBJECT);
            TravelStory story;

            do {
                story = new TravelStory();
                story.setDistance(c.getDouble(distance));
                story.setLatLonFinish(c.getString(finisLatLon));
                story.setLatLonStart(c.getString(startLatlon));
                story.setName(c.getString(name));
                story.setSpeed(c.getDouble(speed));
                story.setNumber(c.getInt(number));
                story.setTimeFinish(c.getLong(finishTime));
                story.setTimeStart(c.getLong(startTime));
                list.add(story);
            } while (c.moveToNext());
        }
        return list;
    }

    private Collection<? extends Gps> getListGpsCoordinats(Cursor c) {
        ArrayList<Gps> list = new ArrayList<>();
        if (c.moveToFirst()) {
            int time = c.getColumnIndex(DB_COLUMN_TIME);
            int lat = c.getColumnIndex(DB_COLUMN_LAT);
            int lon = c.getColumnIndex(DB_COLUMN_LONG);
            int alt = c.getColumnIndex(DB_COLUMN_ALTITUDE);
            int speed = c.getColumnIndex(DB_COLUMN_SPEED);
            Gps gps;
            do {
                gps = new Gps();
                gps.setTime(c.getLong(time));
                gps.setLongitude(c.getFloat(lon));
                gps.setLatitude(c.getFloat(lat));
                gps.setAltitude(c.getDouble(alt));
                gps.setSpeed(c.getDouble(speed));
                list.add(gps);
            }
            while (c.moveToNext());
        }
        return list;
    }

    public long sFlagUpdate(long time) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DB_COLUMN_FLAG_SERVER_SAFE, 1);
        return getWritableDatabase().update(DB_TABLE_GPS_TRACKER, contentValues, DB_COLUMN_TIME + "=?", new String[]{String.valueOf(time)});
    }

    public ArrayList<Gps> getListGpsServerFlagFalse() {
        Cursor c = null;
        db = getReadableDatabase();
        db.beginTransaction();
        ArrayList<Gps> list = new ArrayList<>();
        try {
            c = db.query(DB_TABLE_GPS_TRACKER, null, DB_COLUMN_FLAG_SERVER_SAFE + "=0", null, null, null, null);
            list.addAll(getListGpsCoordinats(c));
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return list;
    }

    public boolean deleteTrackerAndStory(int number) {
        long k1 = getWritableDatabase().delete(DB_TABLE_GPS_TRACKER, DB_COLUMN_NUMBER_TRIP + " =?", new String[]{String.valueOf(number)});
        long k2 = getWritableDatabase().delete(DB_TABLE_STORY_TRAVEL, DB_COLUMN_NUMBER_TRIP + " =?", new String[]{String.valueOf(number)});
        Log.e("LOG", "k1=" + k1 + " k2=" + k2);
        return k1 >-1 && k2 >-1;
    }

    public TravelStory getStoryByNumber() {
        return null;
    }
}
