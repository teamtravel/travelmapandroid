package com.travelmap3.tracker;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.event.BusEvent;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.tips.TIPS;
import com.travelmap3.tips.TipsCreate;
import com.travelmap3.utils.Utils;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class TrackerMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final int WIDTH_LINE_TRACKER = 10;
    private static final String LOG_TAG = "myLog";
    private static final String FRAGMENT_TAG = "fragment";
    public static final String NEW_POLYLINE = "new";
    private GoogleMap mMap;
    private Polyline polyline;
    PolylineOptions rectOptions = new PolylineOptions()
            .width(WIDTH_LINE_TRACKER)
            .color(Color.BLUE)
            .geodesic(true);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_map_clastering);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        TipsCreate.getInstance().show(TIPS.TRACKER, getFragmentManager(), this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setMapSettings(mMap);
        try {
            setActionBar();
            writeTrack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_map);
        View back = (View) findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOG", "onClick: base map ");
                startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
            }
        });
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        if (null != bar) {
            bar.setDisplayShowTitleEnabled(false);

        }
    }

    private void setMapSettings(GoogleMap map) {
        map.getUiSettings().setAllGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        double lat = App.getInstance(this).getComponent().uController().get().getLatitude();
        double lon = App.getInstance(this).getComponent().uController().get().getLongitude();
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 5));
        map.setMyLocationEnabled(true);
    }

    private void writeTrack() {
        ArrayList<LatLng> list = App.getInstance(this).getComponent().trackerController().getTrackLine(App.getInstance(this).getComponent().trackerController().getNumberTrip());
        rectOptions.addAll(list);
        polyline = mMap.addPolyline(rectOptions);

        downFragment();
    }

    private void downFragment() {
        ArrayList<TravelStory> travellist = new ArrayList<>();
        travellist = App.getInstance(this).getComponent().trackerController().getStory();
        if (travellist != null && travellist.size() != 0) {
            InfoMapFragmnet fragment = InfoMapFragmnet.getInstance(travellist);
            animateFragment(fragment);
        }
    }

    private void animateFragment(final Fragment fragment) {
        final View fragmentContainer = findViewById(R.id.cluster_info_fragment_container);
        final Fragment oldFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (oldFragment != null) {
            if (fragmentContainer != null) {
                fragmentContainer.animate()
                        .translationY(fragmentContainer.getHeight())
                        .setListener(new Utils.CustomAnim() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.cluster_info_fragment_container, fragment, FRAGMENT_TAG)
                                        .commitAllowingStateLoss();
                                fragmentContainer.animate().translationY(0);
                            }
                        });
            }
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cluster_info_fragment_container, fragment, FRAGMENT_TAG).commit();
            if (fragmentContainer != null) {
                fragmentContainer.animate().translationY(0);
            }
        }
    }

    // This method will be called when a BusEvent is posted
    public void onEvent(BusEvent event) {
        if (event.message.startsWith(TrackerMapsActivity.NEW_POLYLINE)) {
            try {
                polyline.remove();
                rectOptions = new PolylineOptions()
                        .width(WIDTH_LINE_TRACKER)
                        .color(Color.BLUE)
                        .geodesic(true);
                ArrayList<LatLng> list = App.getInstance(this).getComponent().trackerController().getTrackLine(Integer.parseInt(event.message.substring(3)));
                rectOptions.addAll(list);
                polyline = mMap.addPolyline(rectOptions);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
}
