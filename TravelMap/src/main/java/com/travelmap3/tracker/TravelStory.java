package com.travelmap3.tracker;

/**
 * Created by Andre on 15.07.2016.
 */
public class TravelStory {
    private long timeStart;
    private long timeFinish;
    private String latLonStart;
    private String latLonFinish;
    private String name;
    private double distance;
    private double speed;
    private int  number;
    private boolean isExpiredToken;

    public TravelStory(long timeStart, long timeFinish, String latLonStart, String latLonFinish, String name, double distance, double speed, int number) {
        this.timeStart = timeStart;
        this.timeFinish = timeFinish;
        this.latLonStart = latLonStart;
        this.latLonFinish = latLonFinish;
        this.name = name;
        this.distance = distance;
        this.speed = speed;
        this.number = number;
    }
    public TravelStory() {

    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeFinish() {
        return timeFinish;
    }

    public void setTimeFinish(long timeFinish) {
        this.timeFinish = timeFinish;
    }

    public String getLatLonStart() {
        return latLonStart;
    }

    public void setLatLonStart(String latLonStart) {
        this.latLonStart = latLonStart;
    }

    public String getLatLonFinish() {
        return latLonFinish;
    }

    public void setLatLonFinish(String latLonFinish) {
        this.latLonFinish = latLonFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isExpiredToken() {
        return isExpiredToken;
    }

    public void setExpiredToken(boolean expiredToken) {
        isExpiredToken = expiredToken;
    }
}
