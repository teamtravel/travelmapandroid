package com.travelmap3.dagger;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import com.travelmap3.UserController;
import com.travelmap3.auth.NewLoginActivity;
import com.travelmap3.home.NearestObject.DataManagerObjectPlace;
import com.travelmap3.interesplace.ControllerPlace;
import com.travelmap3.interesplace.ManagerPLace;
import com.travelmap3.interesplace.PlaceDB;
import com.travelmap3.maps.UpdateUsers;
import com.travelmap3.notes.Notes;
import com.travelmap3.notes.NotesController;
import com.travelmap3.notes.NotesDB;
import com.travelmap3.profile.DataManagerUserNotes;
import com.travelmap3.service.NetworkManagerDeffered;
import com.travelmap3.tracker.TrackerController;
import com.travelmap3.tracker.TrackerDB;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.UsersDB;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        PlaceModule.class,
        NoteModule.class,
        InternetModule.class,
        SharedModule.class,
        TrackerModule.class,
        UserModule.class,
        NetworkModule.class,
        ObjectPlaceModule.class
})
public interface AppComponent {
    PlaceDB dbPlace();

    NotesDB notesDB();

    Notes notes();

    ConnectivityManager connectivityManager();

    SharedPreferences sharedPreferences();

    TrackerDB trackerDB();

    UsersDB userDB();

    InternetConnect getInternetConnect();

    ControllerPlace controller();

    ManagerPLace manager();

    NetworkManagerDeffered networkManagerDif();

    NotesController notesController();

    UserController uController();

    UpdateUsers updateUsers();

    TrackerController trackerController();

    DataManagerUserNotes dataManagerNotes();

    DataManagerObjectPlace dataManagerObject();

    NewLoginActivity inject(NewLoginActivity view);
}
