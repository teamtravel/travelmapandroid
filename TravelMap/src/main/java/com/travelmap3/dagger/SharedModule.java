package com.travelmap3.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class SharedModule {

    @Provides
    public SharedPreferences providecSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
