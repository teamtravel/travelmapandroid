package com.travelmap3.dagger;

import com.travelmap3.UserController;
import com.travelmap3.interesplace.ManagerPLace;
import com.travelmap3.interesplace.PlaceDB;
import com.travelmap3.notes.NotesController;
import com.travelmap3.notes.NotesDB;
import com.travelmap3.service.NetworkManagerDeffered;
import com.travelmap3.utils.InternetConnect;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrey.gusenkov on 13/12/2016.
 */
@Module
public class NetworkModule {
    @Singleton
    @Provides
    public NetworkManagerDeffered provNetworkManagerDeffered(NotesDB notesDB, PlaceDB db, InternetConnect internetConnect, ManagerPLace managerPLace, UserController userController, NotesController controller) {
        return new NetworkManagerDeffered(notesDB, db, internetConnect, managerPLace, userController, controller);
    }
}
