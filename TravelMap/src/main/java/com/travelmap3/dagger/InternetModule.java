package com.travelmap3.dagger;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import com.travelmap3.utils.InternetConnect;

import javax.inject.Singleton;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class InternetModule {
    @Singleton
    @Provides
    public ConnectivityManager providesConnectivityManager(Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Singleton
    @Provides
    public InternetConnect providesConnect(SharedPreferences sp, ConnectivityManager manager) {
        return new InternetConnect(manager, sp);
    }
}
