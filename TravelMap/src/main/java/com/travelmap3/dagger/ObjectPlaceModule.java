package com.travelmap3.dagger;

import android.content.Context;

import com.travelmap3.home.NearestObject.DataManagerObjectPlace;
import com.travelmap3.home.NearestObjectDB;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by andrey.gusenkov on 14/12/2016.
 */
@Module
public class ObjectPlaceModule {

    @Provides
    @Singleton
    public NearestObjectDB providesObjectModule(Context context) {
        return new NearestObjectDB(context);
    }

    @Provides
    @Singleton
    public DataManagerObjectPlace providesNearestPlace(NearestObjectDB db) {
        return new DataManagerObjectPlace(db);
    }
}
