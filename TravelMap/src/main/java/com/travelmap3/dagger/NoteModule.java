package com.travelmap3.dagger;

import android.content.Context;

import com.travelmap3.UserController;
import com.travelmap3.notes.Notes;
import com.travelmap3.notes.NotesController;
import com.travelmap3.notes.NotesDB;
import com.travelmap3.profile.DataManagerUserNotes;
import com.travelmap3.utils.Preferences;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class NoteModule {

    @Provides
    public NotesDB providesNotesDb(Context context) {
        return new NotesDB(context);
    }

    @Provides
    public Notes providesNote() {
        return new Notes();
    }

    @Provides
    public NotesController providecNotesController(Notes notes, NotesDB notesDB) {
        return new NotesController(notes, notesDB);
    }

    @Provides
    public DataManagerUserNotes providesDataManagerNotes(UserController userController, Preferences preferences) {
        return new DataManagerUserNotes(userController, preferences);
    }


}
