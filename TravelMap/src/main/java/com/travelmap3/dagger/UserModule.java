package com.travelmap3.dagger;

import android.content.Context;

import com.travelmap3.UserController;
import com.travelmap3.maps.UpdateUsers;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.UsersDB;

import javax.inject.Singleton;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class UserModule {

    @Singleton
    @Provides
    public UsersDB providesUserDb(Context context) {
        return new UsersDB(context);
    }

    @Singleton
    @Provides
    public UserController providesUserController(InternetConnect connect, UsersDB usersDB) {
        return new UserController(connect, usersDB, Preferences.getInstance());
    }

    @Provides
    @Singleton
    public UpdateUsers providesUpdateUsers(UsersDB usersDB, InternetConnect internetConnect) {
        return new UpdateUsers(usersDB, internetConnect);
    }


}
