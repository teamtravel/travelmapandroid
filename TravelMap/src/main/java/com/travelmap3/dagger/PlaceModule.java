package com.travelmap3.dagger;

import android.content.Context;

import com.travelmap3.UserController;
import com.travelmap3.interesplace.ManagerPLace;
import com.travelmap3.interesplace.PlaceDB;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class PlaceModule {
    @Provides
    public PlaceDB provideDbPlace(Context context) {
        return new PlaceDB(context);
    }

    @Provides
    public ManagerPLace providesManager(PlaceDB dbPlace, UserController uController) {
        return new ManagerPLace(dbPlace, uController);
    }
    
}
