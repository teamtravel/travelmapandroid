package com.travelmap3.dagger;

import android.content.Context;

import com.travelmap3.tracker.TrackerController;
import com.travelmap3.tracker.TrackerDB;
import com.travelmap3.utils.Preferences;

import javax.inject.Singleton;

import dagger.Provides;

/**
 * Created by andrey.gusenkov on 09/12/2016.
 */
@dagger.Module
public class TrackerModule {

    @Provides
    public TrackerDB provcidesTrackerDb(Context context) {
        return new TrackerDB(context);
    }

    @Provides
    public Preferences providesPreferences() {
        return Preferences.getInstance();
    }

    @Provides
    @Singleton
    public TrackerController providesTrackerController(Preferences preferences, TrackerDB trackerDB, Context context) {
        return new TrackerController(preferences.loadNumberTrip(), trackerDB, context);
    }
}
