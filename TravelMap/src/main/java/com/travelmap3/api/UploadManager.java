package com.travelmap3.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.travelmap3.App;
import com.travelmap3.event.BusEvent;
import com.travelmap3.event.BusEventNoteImage;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Crypto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

import static com.travelmap3.notes.Notes.ImageLocations;

/**
 * Created by Andre on 13.01.2016.
 */
public class UploadManager {
    public static final String URL_PHOTO = "UrlPhoto";
    public static final String NOTES_IMAGE = "UrlImages";

    private static Cloudinary cloudinary;

    static {
        Map<String, String> config = new HashMap<String, String>();
        config.put("cloud_name", Config.CLOUD_NAME);
        config.put("api_key", Config.API_KEY_CLOUDDINARY);
        config.put("api_secret", Config.API_SECRET_CLOUDDINARY);
        cloudinary = new Cloudinary(config);
    }

    public UploadManager() {

    }

    public String uploadBitmap(final Context context, Bitmap bitmap) throws Exception {
        final byte[] bitmapData = getByteArrayFromBitmap(bitmap);
        final String name = new Crypto().hashFromBitmap(bitmap);
        String url = cloudinary.url().generate(name);
        try {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPostExecute(String url) {
                    if (url.length() > 0) {
                        App.getInstance(context).getComponent().uController()
                                .get()
                                .setPhoto200(url);
                        String url100 = "http://res.cloudinary.com/travelmaps/image/upload/w_100,h_100,c_fill/" + name;
                        String url50 = "http://res.cloudinary.com/travelmaps/image/upload/w_50,h_50,c_fill/" + name;
                        App.getInstance(context).getComponent().uController().get().setPhoto100(url100);
                        App.getInstance(context).getComponent().uController().get().setPhoto50(url50);
                    }
                    EventBus.getDefault().post(new BusEvent(URL_PHOTO));
                }

                @Override
                protected String doInBackground(Void... params) {
                    Map map = null;
                    String url = "";
                    try {
                        map = cloudinary.uploader()
                                .upload(bitmapData, ObjectUtils.asMap("public_id", name, "timestamp", "129830123"));
                        url = (String) map.get("url");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return url;
                }
            }.execute();
        } catch (Exception e) {
            return null;
        }
        return url;
    }

    public String[] uploadBitmaps(ArrayList<Bitmap> bitmaps, final ArrayList<String> bitmapPathList) throws Exception {
        int sizeBitmapList = bitmaps.size();
        final byte[][] byteArrays = new byte[sizeBitmapList][];
        final String[] names = new String[sizeBitmapList];
        String[] urls = new String[sizeBitmapList];
        for (int i = 0; i < sizeBitmapList; i++) {
            Bitmap bitmap = bitmaps.get(i);
            byteArrays[i] = getByteArrayFromBitmap(bitmap);
            names[i] = new Crypto().hashFromBitmap(bitmap);
            urls[i] = cloudinary.url().generate(names[i]);
        }

        try {
            new AsyncTask<Integer, Void, List<ImageLocations>>() {
                @Override
                protected List<ImageLocations> doInBackground(Integer... params) {
                    List<ImageLocations> result = new ArrayList<ImageLocations>();
                    Map map;
                    try {
                        for (int i = 0; i < params[0]; i++) {
                            map = cloudinary.uploader()
                                    .upload(byteArrays[i], ObjectUtils.asMap("public_id", names[i], "timestamp", "129830123"));
                            String url = (String) map.get("url");
                            String filePath = bitmapPathList.get(i);
                            result.add(new ImageLocations(url, filePath));
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return result;
                }

                @Override
                protected void onPostExecute(List<ImageLocations> result) {
                    super.onPostExecute(result);
                    EventBus.getDefault().post(new BusEventNoteImage(result));
                }

            }.execute(sizeBitmapList);
        } catch (Exception e) {
            return null;
        }

        return urls;
    }

    private byte[] getByteArrayFromBitmap(Bitmap bitmap) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        final byte[] bitmapData = bos.toByteArray();
        bos.close();
        return bitmapData;
    }


//    public void deleteImage(final String deleteName) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Map m = cloudinary.uploader().destroy(deleteName, ObjectUtils.emptyMap());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//    }
}
