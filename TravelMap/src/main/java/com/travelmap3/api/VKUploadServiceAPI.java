package com.travelmap3.api;

import retrofit.Callback;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.mime.TypedFile;

/**
 * Created by dpivovar on 18.08.2016.
 */
public interface VKUploadServiceAPI {
    @Multipart
    @POST(value = "/")
    void upload(@Part("photo") TypedFile photo,
                Callback<UploadServerResponseModel> cb);


    class UploadServerResponseModel {
        private String server;
        private String photo;
        private String hash;

        public String getServer() {
            return server;
        }

        public void setServer(String server) {
            this.server = server;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }
    }
}
