package com.travelmap3.api;

import com.travelmap3.auth.model.Login;
import com.travelmap3.feedback.Feedback;
import com.travelmap3.interesplace.Place;
import com.travelmap3.model.Device;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.model.User;
import com.travelmap3.notes.Notes;
import com.travelmap3.tracker.TravelStory;

import java.util.ArrayList;
import java.util.HashSet;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by An on 29.09.2015.
 */
public interface RetrofitAPI {
    /*
    Получаем всех юзеров
    */
    @Headers({
            "Accept: application/json",
            "Cache-Control: max-age=14400"
    })
    @GET("/{version}/users")
    void getUsers(@Path("version") String version, Callback<HashSet<User>> callback);


    /*
    Регистрация в системе
     */
    @POST("/{version}/registration")
    void postRegistration(@Path("version") String version,
                          @Body Login login,
                          Callback<Response> callback);

    /**
     * Авторизация в системе
     *
     * @return данные юзера в формате json Model<User>
     */
    @POST("/{version}/login")
    void postLogin(@Path("version") String version,
                   @Body Login auth,
                   Callback<User> callback);

    /**
     * Save regId
     *
     * @return
     */
    @POST("/{version}/gcmreg")
    void postGcmRegId(@Path("version") String version,
                      @Body Device device,
                      Callback<String> callback);

    /**
     * Получаем личную информацию для профайла от сервера
     *
     * @param version  API server go land
     * @param callback my data user
     */
    @GET("/{version}/profile")
    void getProfile(@Header("Authorization") String access_token,
                    @Path("version") String version,
                    Callback<User> callback);

    /**
     * @param version  API server go lanп
     * @param data     info from client
     * @param callback
     */
    @Headers({
            "Content-Type: application/json"
    })
    @POST("/{version}/profile")
    void postProfile(@Header("Authorization") String token,
                     @Path("version") String version,
                     @Body User data,
//                      @Field("data") String data,
                     Callback<Response> callback);

    /**
     * Метод сохраняет данные местоположения на сервере.
     *
     * @param token
     * @param version
     * @param callback
     */
    @Headers({
            "Content-Type: application/json"
    })
    @POST("/{version}/tracker")
    void postLocation(@Header("Authorization") String token,
                      @Path("version") String version,
                      @Body User user,
                      Callback<Response> callback);

    /**
     * Для Отправки записей дневника на сервер
     *
     * @param token
     * @param version  API
     * @param notes    model class Notes
     * @param callback
     */
    @POST("/{version}/note")
    void postNote(@Header("Authorization") String token,
                  @Path("version") String version,
                  @Body Notes notes,
                  Callback<Response> callback);

    /**
     * @param token
     * @param version  API
     * @param uid      - наш id.
     * @param id       - id того у кого хотим получить данные. Если хотим получить юзера указываем наш uid.
     * @param callback
     */

    @GET("/{version}/note/{uid}/{id}")
    void getNote(@Header("Authorization") String token,
                 @Path("version") String version,
                 @Path("uid") int uid,
                 @Path("id") int id,
                 Callback<ArrayList<Notes>> callback);

    @FormUrlEncoded
    @POST("/{version}/reset")
    void postResetPassword(@Path("version") String version,
                           @Field("query") String query,
                           Callback<String> callback);


    @FormUrlEncoded
    @POST("/{version}/recovery")
    void postRecoveryPassword(@Path("version") String version,
                              @Field("password") String query,
                              @Field("key") String key,
                              Callback<Response> callback);

    /**
     * @param lat
     * @param lon
     * @param type
     * @param callback
     */
    @GET("/{version}/sights/{lat}/{lon}/{radius}/{type}")
    void getObjectPlace(@Header("Authorization") String token,
                        @Path("version") String version,
                        @Path("lat") double lat,
                        @Path("lon") double lon,
                        @Path("radius") int r,
                        @Path("type") String type,
                        Callback<ArrayList<ObjectPlace>> callback);

    @GET("/json.json")
    void getObjectPlaceTest(Callback<ArrayList<ObjectPlace>> callback);


    @POST("/{version}/feedback")
    void postFeedback(@Header("Authorization") String token,
                      @Path("version") String version,
                      @Body Feedback f,
                      Callback<Response> callback);

    @POST("/{version}/places")
    void postPlace(@Header("Authorization") String token,
                   @Path("version") String version,
                   @Body Place place,
                   Callback<Response> callback);

    @DELETE("/{version}/places")
    void deletePlace(@Header("Authorization") String token,
                     @Path("version") String version,
                     @Body Place place, // переделать на время timestamp создания
                     Callback<Response> callback);

    @GET("/{version}/places")
    void getPlaces(@Path("version") String version,
                   Callback<HashSet<Place>> callback);

    @GET("/{version}/places/{uid}")
    void getPlaces(@Header("Authorization") String token,
                   @Path("version") String version,
                   @Path("uid") Integer uid,
                   Callback<HashSet<Place>> callback);

    @FormUrlEncoded
    @DELETE("/{version}/notes")
    void deleteNote(@Header("Authorization") String token,
                    @Path("version") String version,
                    @Field("created") long createdNote,
                    @Field("uid") int uid,
                    Callback<Response> callback);

    @POST("/{version}/story/{uid}")
    void postInitStory(@Header("Authorization") String token,
                       @Path("version") String version,
                       @Path("uid") int uid,
                       @Body TravelStory story,
                       Callback<Response> callback);

    @GET("/{version}/story/{uid}")
    void getStory(@Path("version") String version,
                  @Path("uid") int uid,
                  Callback<Response> callback);

    @PUT("/{version}/story")
    void updateStory(@Header("Authorization") String token,
                     @Path("version") String version,
                     @Body TravelStory story,
                     Callback<Response> callback);

    @DELETE("/{version}/story/{uid}")
    void deleteStory(@Header("Authorization") String token,
                     @Path("version") String version,
                     @Path("uid") int uid,
                     @Body TravelStory story,
                     Callback<Response> callback);
}
