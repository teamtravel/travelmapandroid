package com.travelmap3.api;

import com.facebook.stetho.okhttp.StethoInterceptor;
import com.squareup.okhttp.OkHttpClient;
import com.travelmap3.BuildConfig;
import com.travelmap3.utils.Config;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by An on 29.09.2015.
 */
public class RetrofitService {
    private static final String LOG = "Request";
    private static RetrofitAPI service;

    public static RetrofitAPI getInstanceAPI() {
        return service();
    }


    private static RetrofitAPI service() {
        if (service == null) {
            OkHttpClient client = new OkHttpClient();
            client.networkInterceptors().add(new StethoInterceptor());

            RestAdapter retrofit = new RestAdapter.Builder()
                    .setLogLevel(BuildConfig.DEBUG || true ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                    .setEndpoint(Config.SERVER)
                    .setClient(new OkClient(client))
                    .build();
            service = retrofit.create(RetrofitAPI.class);
        }
        return service;
    }

    public static <S> S createVkUploadService(Class<S> serviceClass, String url) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setClient(new OkClient(new OkHttpClient()));
        RestAdapter adapter = builder.setEndpoint(url)
                                     .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                                     .build();
        return adapter.create(serviceClass);
    }
}
