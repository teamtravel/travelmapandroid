/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.travelmap3;

import android.location.Location;
import android.util.Log;

import com.travelmap3.api.RetrofitService;
import com.travelmap3.model.User;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.FormatJSON;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.UsersDB;

import java.util.HashSet;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by An on 02.08.2015.
 * UserController() есть класс синглетон.
 * Т.е он инициализируется всего один раз за запуск приложения и висит в памяти.
 * Это сделано для того что мы очень часто обращаемся к данным пользователя, а если хранить (как
 * было раньше) все данные в преференс
 * то считывания с диска довольно долгая операция и делать ее в основном потоке не гуд.
 * Поэтому мы всего один раз считываем сохраненые данные.
 * Потом работаем с полями UserController и если что то меняем, например координаты то вызываем
 * метод setUser(User,callback)
 * В этот момент он сохранит на диск и update попытается сделать на сервере также изменит
 * статического юзера который висит в памяти.
 * Вот, и все.
 */
public class UserController {
    private static final String LOG = "UserController";
    private static final long MIN_TIME_UPDATE_DEBUG = 90000;
    private Preferences pref;

    private User mUser;

    public UsersDB db;

    public InternetConnect internetConnect;

    @Inject
    public UserController(InternetConnect internetConnect, UsersDB db, Preferences pref) {
        this.db = db;
        this.internetConnect = internetConnect;
        this.pref = pref;
        mUser = get();// инициилизируем при запуске
    }

    /**
     * Установка текущего юзера и сохранение всех данных
     * в локальном хранилище и затем на сервере
     */
    public void setUser(User user, CallBackResponse callBackResponse) {
        mUser = user;
        updateUserOnServer(callBackResponse);
        //todo Если нет интернета, то можно делать пометку что не отправили. После будем в сервисе проверять, что было не отправлено.
        //Если данные успешно ушли на сервер, то обновляем локального пользователя.
        saveUserToDataStore();
        saveTime();
    }

    /**
     * Получение пользователей с карты из БД по id
     */
    public User getUserById(int id) {
        User user = get();
        if (user.getId() == id) {
            return user;
        }
        String UID = Integer.toString(id);
        HashSet<User> set = db.get(UID);
        if (set.size() > 0) {
            user = (User) set.toArray()[0];
        } else {
            //FIXME SERVER CALLBACK
            User userDef = new User();
            userDef.setPhoto50("https://cdn.it-tehnik.ru/wp-content/uploads/default-user.jpg");
            return userDef;
        }

        return user;
    }


    public User get() {
        if (mUser == null) {
            mUser = getUserFromDataStore();
        }
        return mUser;
    }

    private User getUserFromDataStore() {
        return new FormatJSON().takeUser(
                pref.getString(Preferences.USER_DATA_JSON));
    }

    private void saveUserToDataStore() {
        if (mUser != null) {
            try {
                pref.saveUserId(mUser.getId());
                pref.putString(Preferences.USER_DATA_JSON, new FormatJSON().takeJsonForUser(mUser));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void saveTime() {
        pref.saveTimeUpdateProfile();
    }

    public void updateLocation(Location location) {
        mUser.setLatitude(location.getLatitude());
        mUser.setLongitude(location.getLongitude());
        mUser.setSpeed(location.getSpeed());
        mUser.setAltitude(location.getAltitude());
        mUser.setTime(System.currentTimeMillis());
        saveUserToDataStore();
    }


    public User currentData() {
        //        if (isUpdate()) { TODO пока нет сайта. можно опустить синхронизацию.
        //            getWithServer();
        //            Log.i(LOG, "Если времени прошло много, идем на сервер...");
        //        } // тут должно быть одновление в фоне юзера с сервера
        //   setUser(getUserFromDataStore());

        return get();
    }


    public void updateUserOnServer(final CallBackResponse cb) {
        if (mUser != null) {
            RetrofitService.getInstanceAPI().postProfile(pref.loadToken(), Config.VERSION_API, mUser,
                    new Callback<Response>() {
                        @Override
                        public void success(Response response, Response response2) {
                            Log.d(LOG, "User data saved");
                            cb.onSuccess(response);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            cb.onError(error);
                        }
                    });
        }
    }


    public boolean removeInfo(long time) {
        return false;
    }

    /**
     * Метод который проверяет нужно ли делать обновления.
     * Проверяет  наличие интернета с учетом настроек, и проверка когда последний раз обновлялись.
     *
     * @return Boolean
     */
    private boolean isUpdate() {
        return internetConnect.isActiveWiFiConnection() && isTime();
    }


    public boolean isTime() {
        return (System.currentTimeMillis() - pref.loadTimeUpdateProfile()) > MIN_TIME_UPDATE_DEBUG;
    }


    public boolean insertInDB(HashSet<User> user) {
        return false;
    }


    public User getWithServer() {
        //        RetrofitService.getInstanceAPI().getProfile(Preferences.getInstance().loadToken(), Config.VERSION_API, new Callback<User>() {
        //            @Override
        //            public void success(User user, Response response) {
        //                Log.e(LOG, "code = " + response.getStatus());
        //                setUser(user);
        //                saveTime();
        //                saveInfo();
        //            }
        //
        //            @Override
        //            public void failure(RetrofitError error) {
        //                Log.e(LOG, "faile" + error + " status " + error.getResponse().getStatus());
        //            }
        //        });
        return null;
    }
}