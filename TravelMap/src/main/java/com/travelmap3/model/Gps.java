package com.travelmap3.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Gps implements Serializable{
    private static final long serialVersionUID = -4994763529881200165L;
    private double latitude;
    private double longitude;
    private long time;
    private double altitude;
    private double speed;
    protected LatLng position; // Нужены для карты.

    public Gps(Location loc) {
        this.speed = loc.getSpeed();
        altitude = loc.getAltitude();
        latitude = loc.getLatitude();
        longitude = loc.getLongitude();
        position = new LatLng(latitude, longitude);
        time = loc.getTime();
    }
    public Gps (double lat, double lng) {
        position = new LatLng(lat, lng);
        latitude = lat;
        longitude = lng;
    }
    public Gps( double lat, double lon, long time){
        this.time = time;
        latitude = lat;
        longitude = lon;
        position = new LatLng(lat, lon);
    }
    public Gps(LatLng latLng) {
        latitude = latLng.latitude;
        longitude = latLng.longitude;
        position=latLng;
    }

    public Gps(LatLng latLng, long time) {
        latitude = latLng.latitude;
        longitude = latLng.longitude;
        this.time = time;

    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public void setLatitude(String latitude) {
        this.latitude = Double.parseDouble(latitude);
    }
    public void setLongitude(String longitude) {
        this.longitude = Double.parseDouble(longitude);
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public LatLng getPosition() {
        return position;
    }
    public void setPosition(LatLng position){
        this.position = position;
    }

    public Gps() {
        position = null;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
