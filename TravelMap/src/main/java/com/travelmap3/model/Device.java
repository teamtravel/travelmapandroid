package com.travelmap3.model;

/**
 * Created by Al on 01.10.2015.
 */
public class Device {

  private int uid;
  private String regId;
  private String langCode;
  private String deviceModel;
  private String systemVersion;
  private String appVersion;

  public Device(int uid, String regId, String... args) {
    this.uid = uid;
    this.regId = regId;
    if (args.length == 4) {
      langCode = args[0];
      deviceModel = args[1];
      systemVersion = args[2];
      appVersion = args[3];
    }
  }

  public int getUid() {
    return uid;
  }

  public void setUid(int uid) {
    this.uid = uid;
  }

  public String getRegId() {
    return regId;
  }

  public void setRegId(String regId) {
    this.regId = regId;
  }

  public String getLangCode() {
    return langCode;
  }

  public void setLangCode(String langCode) {
    this.langCode = langCode;
  }

  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    this.deviceModel = deviceModel;
  }

  public String getSystemVersion() {
    return systemVersion;
  }

  public void setSystemVersion(String systemVersion) {
    this.systemVersion = systemVersion;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }
}
