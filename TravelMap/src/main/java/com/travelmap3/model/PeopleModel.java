package com.travelmap3.model;


public class PeopleModel extends User {

  public String rowID; // important field
  public Boolean checked = false;
  public int msgCount = 0;

  public PeopleModel(Boolean ch, String... fields) {
    super();
    this.rowID = fields[0];
    this.checked = ch;
    super.setFirstName(fields[1]);
    super.setPhoto50(fields[2]);
  }

  public PeopleModel(String id, String name) {
    this.rowID = id;
    super.setFirstName(name);
  }

  public PeopleModel(String uid, String name, String ph, int msg_cnt) {
    super();
    this.rowID = uid;
    this.msgCount = msg_cnt;
    super.setFirstName(name);
    super.setPhoto50(ph);
  }

  public PeopleModel() {

  }

  public String getRowID() {
    return rowID == null ? "" : rowID;
  }

  public String getFirstName() {
    return super.getFirstName() == null ? "" : super.getFirstName();
  }

  public String getPhoto50() {
    return super.getPhoto50() == null ? "" : super.getPhoto50();
  }

  public Boolean isChecked() {
    return checked;
  }
}
