/**
 *
 */
package com.travelmap3.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.travelmap3.utils.Preferences;

import java.io.Serializable;

/**
 * Основной класс юзера.
 *
 * @author andre
 */

public class User extends Gps implements ClusterItem, Serializable {
    private static final long serialVersionUID = -4994763529881200165L;
    private String status;// status да ладно?)
    private String firstname; // Имя
    private String email; // Email addres user
    private int age; //Сколько лет
    private int profilePhoto;
    private String startTrip; // начальная точка путешествия
    private String endTrip; // конечная точка путешествия
    private boolean help; // позвал ли он напомощь
    private long lastTime;// Время последнего пришествия.
    private float distanceTo;//Растояние до меня.
    private boolean visible;// Отображаемость на карте, в зависимости от настроек
    private String lastName = ""; // Фамилия
    private String birthday = ""; // дата! рождения (long)// todo
    private String about;// дополнительная информация о пользователи
    private String photo50;//50*50
    private String photo100;//100*100
    private String photo200;//Аватарка размером 200*200
    private String from_city;// Город проживания пользователя
    private String timeStart;//Время начала путешествия.
    private String vkId;// Id из Вконтакте
    private String fbId;
    private String twId;
    private int gender; // 1 - мужик, 2 - женщина, 0 - неопределен
    private String study;//место учебы
    private String jobs;// место работы
    private String phone; // number phone
    private String skype; // адрес по скайпу
    private String timeTrip; // время в путeшествие
    private int id;// Id пользователя системы
    private String lang;
    private String token;
    private String telegram;// nickname/number phone/ link - разное может быть
    private boolean isExpiredToken;
    private String password;

    public User(String firstname, String photo50) {
        super(0, 0);
        this.firstname = firstname;
        this.photo50 = photo50;
    }

    public User() {
        super(0, 0);
        profilePhoto = 0;
        status = "";
        firstname = "";
        email = "";
        age = 0;
        startTrip = "";
        endTrip = "";
        help = false;
        lastTime = 0;
        distanceTo = 0.0f;
        visible = true;
        from_city = "";
        vkId = "";
        fbId = "";
        twId = "";
        lang = "Russian";
        phone = "";
        setLatitude(0);
        setLongitude(0);
        token = Preferences.getInstance().loadToken();
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getStatusText() {
        return (this.status == null) ? "" : this.status;
    }

    public void setStatusText(String status) {
        this.status = status;
    }

    @Override
    public LatLng getPosition() {
        return super.getPosition();
    }

    public Gps getGps() {
        return new Gps(getLatitude(), getLongitude());
    }

    public String getFirstName() {
        return (this.firstname == null) ? "" : this.firstname;
    }

    public void setFirstName(String name) {
        this.firstname = name;
    }

    public String getBirthday() {
        return (this.birthday == null) ? "" : this.birthday;
    }

    public void setBirthday(String bday) {
        if (this.birthday != null) {
            this.birthday = bday;
        }
    }

    public String getLastName() {
        return (this.lastName == null) ? "" : this.lastName;
    }

    public void setLastName(String s) {
        this.lastName = s;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return (this.email == null) ? "" : this.email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the startTrip
     */
    public String getStartTrip() {
        return startTrip;
    }

    /**
     * @param startTrip the startTrip to set
     */
    public void setStartTrip(String startTrip) {
        this.startTrip = startTrip;
    }

    /**
     * @return the endTrip
     */
    public String getEndTrip() {
        return endTrip;
    }

    /**
     * @param endTrip the endTrip to set
     */
    public void setEndTrip(String endTrip) {
        this.endTrip = endTrip;
    }

    /**
     * @return the help
     */
    public boolean getHelp() {
        return help;
    }

    /**
     * @param help the help to set
     */
    public void setHelp(boolean help) {
        this.help = help;
    }

    public float getDistanceTo() {
        return Math.round(distanceTo);
    }

    public void setDistanceTo(float distanceTo) {
        this.distanceTo = distanceTo;
    }

    public boolean getVisible() {

        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public long getLastTime() {
        return lastTime;
    }

    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }

    public int getDefaultProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(int profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getPhoto100() {
        return (this.photo100 == null) ? "" : this.photo100;
    }

    public void setPhoto100(String s) {
        this.photo100 = s;
    }

    public String getAbout() {
        return (this.about == null) ? "" : this.about;
    }

    public void setAbout(String s) {
        this.about = s;
    }

    public String getFrom_city() {
        return (this.from_city == null) ? "" : this.from_city;
    }

    public void setFromCity(String s) {
        this.from_city = s;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getVkId() {
        return this.vkId == null ? "" : this.vkId;
    }

    public void setVkId(String s) {
        this.vkId = s;
    }

    public Integer getId() {
        return this.id;
    }

    public String getStringId() {
        return String.valueOf(this.id);
    }

    public void setId(Integer s) {
        this.id = s;
    }

    public String getPhoto50() {
        return photo50;
    }

    public String getPhotoDefault() {
        return "https://cdn.it-tehnik.ru/wp-content/uploads/default-user.jpg";
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public String getPhoto200() {
        return photo200 == null ? "" : photo200;
    }

    public void setPhoto200(String photo200) {
        this.photo200 = photo200;
    }

    public String getStudy() {
        return study == null ? "" : study;
    }

    public void setStudy(String study) {
        this.study = study;
    }

    public String getSkype() {
        return skype == null ? "" : skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getJobs() {
        return jobs == null ? "" : jobs;
    }

    public void setJobs(String jobs) {
        this.jobs = jobs;
    }

    public String getTimeTrip() {
        return timeTrip;
    }

    public void setTimeTrip(String timeTrip) {
        this.timeTrip = timeTrip;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegramm) {
        this.telegram = telegramm;
    }

    public boolean isExpiredToken() {
        return isExpiredToken;
    }

    public void setExpiredToken(boolean expiredToken) {
        isExpiredToken = expiredToken;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
