package com.travelmap3.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ObjectPlace {
    @SerializedName("lat")
    private double latitude;
    @SerializedName("lon")
    private double longitude;
    private String addressRegion;
    @SerializedName("image")
    private String imageUrl;
    private String type;
    private String addressArea;
    private String addressLocality;
    private String streetAddress;
    private String title;
    private String url;
    private String telephone;
    private Date startDate;
    private Date endDate;
    private double distanceToMe; // in meters
    @SerializedName("text")
    private String review;

    public ObjectPlace(String addressArea, String addressLocality, String addressRegion, Date endDate,
                       String imageURL, double latitude, double longitude, String title, String review,
                       Date startDate, String streetAddress, String telephone, String type, String url) {
        this.addressArea = addressArea;
        this.addressLocality = addressLocality;
        this.addressRegion = addressRegion;
        this.endDate = new Date(endDate.getTime());
        this.imageUrl = imageURL;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.review = review;
        this.startDate = new Date(startDate.getTime());
        this.streetAddress = streetAddress;
        this.telephone = telephone;
        this.type = type;
        this.url = url;
    }

    public ObjectPlace() {
    }

    public void setAddressArea(String addressArea) {
        this.addressArea = addressArea;
    }

    public void setAddressLocality(String addressLocality) {
        this.addressLocality = addressLocality;
    }

    public void setAddressRegion(String addressRegion) {
        this.addressRegion = addressRegion;
    }

    public void setEndDate(Date endDate) {
        this.endDate = new Date(endDate.getTime());
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        /*
          * У нас была проблема. Для ее устранения мы решили использовать регулярные выражения.
          * Теперь у нас две проблемы.(c)
          *
          * заменяет «, » и &quot; на ->  "
        */
        this.title = name.replaceAll("(?:«|»|&quot;)", "\"");
    }

    public void setReview(String review) {
        this.review = review;
    }

    public void setStartDate(Date startDate) {
        this.startDate = new Date(startDate.getTime());
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddressArea() {
        return addressArea;
    }

    public String getAddressLocality() {
        return addressLocality;
    }

    public String getAddressRegion() {
        return addressRegion;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getImageUrl() {
//        if (imageUrl == null) {
//            imageUrl = "https://pp.vk.me/c627318/v627318716/cc64/DyhT_P_4JLY.jpg";
//        }
        return imageUrl;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTitle() {
        return title;
    }

    public String getReview() {
        return review;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public double getDistanceToMe() {
        return distanceToMe;
    }

    public void setDistanceToMe(double distanceToMe) {
        this.distanceToMe = distanceToMe;
    }
}
