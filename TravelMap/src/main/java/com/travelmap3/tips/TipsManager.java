package com.travelmap3.tips;

import android.content.SharedPreferences;

import com.travelmap3.utils.MySharedPreferences;

/**
 * Created by Andre on 02.02.2016.
 */
class TipsManager extends MySharedPreferences {

  public TipsManager(SharedPreferences sh) {
    try {
      sharedPref = sh;
      sharedPrefEditor = sharedPref.edit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  boolean isVisit(TIPS t) {
    return sharedPref.getBoolean(t.getValue(), true);
  }

  void saveVisit(TIPS t) {
    sharedPrefEditor.putBoolean(t.getValue(), false);
    sharedPrefEditor.apply();
  }

}
