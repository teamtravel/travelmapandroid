package com.travelmap3.tips;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;

import com.travelmap3.BuildConfig;
import com.travelmap3.utils.MySharedPreferences;

/**
 * Created by Andre on 03.02.2016.
 */

public class TipsCreate {
    private static volatile TipsCreate instance;
    public static TipsCreate getInstance() {
        TipsCreate localInstance = instance;
        if (localInstance == null) {
            synchronized (TipsManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new TipsCreate();
                }
            }
        }
        return localInstance;
    }

    public void show(TIPS t, FragmentManager fragmentManager, Context context) {
        try {
            TipsManager controller = new TipsManager(context.getSharedPreferences(MySharedPreferences.NAME_FILE, Context.MODE_PRIVATE));
            if (controller.isVisit(t)) {
                DialogFragment newFragment = TipsDialog.newInstance(t);
                newFragment.show(fragmentManager, "dialog");
                controller.saveVisit(t);
            }
        }catch (Exception e) {
            if (BuildConfig.DEBUG) e.printStackTrace();
        }
    }
}
