package com.travelmap3.tips;

import java.io.Serializable;

/**
 * Created by Andre on 03.02.2016.
 */
public enum TIPS implements Serializable {
    MESSAGE("messaget"), NOTE_FRAGMENT("note_fragmentt"), NOTE_MENU("note_menut"), PROFILE("profilet"), TRACKER("trackert"), INTERES_PLACE("intplacet");
    private String value;

    TIPS(String s) {
        this.value = s;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
