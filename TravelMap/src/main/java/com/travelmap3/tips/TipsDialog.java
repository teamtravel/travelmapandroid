package com.travelmap3.tips;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.travelmap3.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TipsDialog extends DialogFragment {
  public static TipsDialog newInstance(TIPS t) {
    TipsDialog frag = new TipsDialog();
    Bundle args = new Bundle();
    args.putSerializable("enum", t);
    frag.setArguments(args);
    return frag;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    TIPS tips = (TIPS) getArguments().getSerializable("enum");
    View view;
    switch (tips) {
      case MESSAGE:
        view = getActivity().getLayoutInflater()
            .inflate(R.layout.tips_message, null);
        break;
      case NOTE_FRAGMENT:
        view = getActivity().getLayoutInflater()
            .inflate(R.layout.tips_note_edit, null);
        break;
      case NOTE_MENU:
        view = getActivity().getLayoutInflater()
            .inflate(R.layout.tips_note_menu, null);
        break;
      case PROFILE:
        view = getActivity().getLayoutInflater()
            .inflate(R.layout.tips_profile, null);
        break;
      case INTERES_PLACE:
        view = getActivity().getLayoutInflater()
                .inflate(R.layout.tips_interes_place, null);
                break;
      case TRACKER:
        view = getActivity().getLayoutInflater()
                .inflate(R.layout.tips_tracker, null);
        break;
      default:
        view = getActivity().getLayoutInflater()
            .inflate(R.layout.tips_message, null);
        break;
    }

    return new AlertDialog.Builder(getActivity())
        .setView(view)
        .setPositiveButton(R.string.text_ok,
            new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int whichButton) {
              }
            }
        )
        .create();
  }
}
