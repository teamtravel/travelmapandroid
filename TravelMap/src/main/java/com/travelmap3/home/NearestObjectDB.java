package com.travelmap3.home;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.travelmap3.BuildConfig;
import com.travelmap3.db.IDataBase;
import com.travelmap3.db.SqliteDBHelper;
import com.travelmap3.model.ObjectPlace;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andre on 14.03.2016.
 */
public class NearestObjectDB extends SqliteDBHelper implements IDataBase {

    private static final String LOG = "NEAREST PlaceDB TEST";

    private ContentValues contentValues;

    public NearestObjectDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public long save(ObjectPlace object) {
        contentValues = new ContentValues();
        long code = 0;
        contentValues.put(DB_COLUMN_ADDRESREGION, object.getAddressRegion());
        contentValues.put(DB_COLUMN_IMAGURL, object.getImageUrl());
        contentValues.put(DB_COLUMN_TYPE, object.getType());
        contentValues.put(DB_COLUMN_ADDRESAREA, object.getAddressArea());
        contentValues.put(DB_COLUMN_ADDRESLOCALITY, object.getAddressLocality());
        contentValues.put(DB_COLUMN_STREETADRESS, object.getStreetAddress());
        contentValues.put(DB_COLUMN_NAME_OBJECT, object.getTitle());
        contentValues.put(DB_COLUMN_URL, object.getUrl());
        contentValues.put(DB_COLUMN_TELEPHONE, object.getTelephone());
        contentValues.put(DB_COLUMN_STARTDATA, object.getStartDate() == null ? 0 : object.getStartDate().getTime());
        contentValues.put(DB_COLUMN_END_DATA, object.getEndDate() == null ? 0 : object.getEndDate().getTime());
        contentValues.put(DB_COLUMN_REVIEW, object.getReview());
        contentValues.put(DB_COLUMN_LAT, object.getLatitude());
        contentValues.put(DB_COLUMN_LONG, object.getLongitude());
        contentValues.put(DB_COLUMN_LAT, object.getLatitude());
        contentValues.put(DB_COLUMN_NEARBY_DISTANCE, object.getDistanceToMe());
        return insertDB(DB_TABLE_OBJECT_PLACE, contentValues);
    }

    public long save(List<ObjectPlace> list) {
        long k = 0;
        deleteObjectPlace();
        for (ObjectPlace objectPlace : list)
            k += save(objectPlace);
        return k;
    }

    private void deleteObjectPlace() {
        int p;
        try {
            if ((p = getWritableDatabase().delete(DB_TABLE_OBJECT_PLACE, null, null)) > 0)
                if (BuildConfig.DEBUG) Log.v(LOG, "Delete, KOL ROWS = " + p);
                else if (BuildConfig.DEBUG) Log.v(LOG, "CLEAR PlaceDB object place ROWS=" + p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<ObjectPlace> getArrayObject(Cursor c) {
        List<ObjectPlace> list = new ArrayList<>();
        if (c.moveToFirst()) {
            int id = c.getColumnIndex("_id");
            int addregion = c.getColumnIndex(DB_COLUMN_ADDRESREGION);
            int image = c.getColumnIndex(DB_COLUMN_IMAGURL);
            int type = c.getColumnIndex(DB_COLUMN_TYPE);
            int addarea = c.getColumnIndex(DB_COLUMN_ADDRESAREA);
            int addloc = c.getColumnIndex(DB_COLUMN_ADDRESLOCALITY);
            int atradd = c.getColumnIndex(DB_COLUMN_STREETADRESS);
            int name = c.getColumnIndex(DB_COLUMN_NAME_OBJECT);
            int url = c.getColumnIndex(DB_COLUMN_URL);
            int telephon = c.getColumnIndex(DB_COLUMN_TELEPHONE);
            int end = c.getColumnIndex(DB_COLUMN_END_DATA);
            int start = c.getColumnIndex(DB_COLUMN_STARTDATA);
            int review = c.getColumnIndex(DB_COLUMN_REVIEW);
            int lat = c.getColumnIndex(DB_COLUMN_LAT);
            int lon = c.getColumnIndex(DB_COLUMN_LONG);
            int d = c.getColumnIndex(DB_COLUMN_NEARBY_DISTANCE);
            ObjectPlace object;
            do {
                object = new ObjectPlace();
                object.setAddressArea(c.getString(addarea));
                object.setImageUrl(c.getString(image));
                object.setReview(c.getString(review));
                object.setUrl(c.getString(url));
                object.setStreetAddress(c.getString(atradd));
                object.setAddressRegion(c.getString(addregion));
                object.setType(c.getString(type));
                object.setAddressLocality(c.getString(addloc));
                object.setName(c.getString(name));
                object.setTelephone(c.getString(telephon));
                object.setEndDate(new Date(c.getLong(end)));
                object.setStartDate(new Date(c.getLong(start)));
                object.setLatitude(c.getDouble(lat));
                object.setDistanceToMe(c.getDouble(d));
                object.setLongitude(c.getDouble(lon));
                list.add(object);
                //    Log.w(LOG, "name " + object.getTitle() + " url=" + object.getUrl() + " imageUrl=" + object.getImageUrl() + " type=" + object.getType() + " distance to me="+object.getDistanceToMe());
            } while (c.moveToNext());

        } else {
            Log.w(LOG, "No Columns");
        }
        c.close();
        return list;
    }

    public List<ObjectPlace> getAllRecord() {
        List<ObjectPlace> list = new ArrayList<>();
        Cursor c = null;
        try {
            c = getReadableDatabase().query(DB_TABLE_OBJECT_PLACE, null, null, null, null,null, DB_COLUMN_NEARBY_DISTANCE + " ASC", null);
            list.addAll(getArrayObject(c));
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (c != null) c.close();
        }
        return list;
    }

}
