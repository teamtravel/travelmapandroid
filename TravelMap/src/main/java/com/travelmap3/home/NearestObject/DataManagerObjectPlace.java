package com.travelmap3.home.NearestObject;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.travelmap3.home.NearestObjectDB;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.utils.Preferences;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Andre on 15.03.2016.
 */
public class DataManagerObjectPlace {
    private static final String TAG = DataManagerObjectPlace.class.getSimpleName();
    private static final int RADIUS = 5000; // m
    private static final double DISTANCE = 0.5;
    NearestObjectDB db;

    @Inject
    public DataManagerObjectPlace(NearestObjectDB db) {
        this.db = db;
    }

    /*
     Если юзер изменил свои координаты, то мы идем на сервер за новыми местами. А если он не менял то идем за кэшированными данными.
     */
    public List<ObjectPlace> takeListObjectPlace(Context context, LatLng location,
                                                 final TravelObjectRequests.OnSuccessRequestListener callback) {
        Log.e("LOGGER", "START on SERVER OBJECT");
        if (location == null || (location.latitude == 0 && location.longitude == 0)) {
            location = new LatLng(55, 55); // просто по дефолту Нижнесаитово, Кушнаренковский район, Bashkortostan, Volga Federal District, Russian Federation
        }
        final LatLng myLocation = location;
        if (isChange(myLocation)) {
            sendRequest(context, myLocation, callback);
        } else {
            List<ObjectPlace> allRecords = db.getAllRecord();
            if (!allRecords.isEmpty()) {
                Log.d(TAG, "The database is not empty");
                callback.onSuccessPlacesLoaded(allRecords);
            } else {
                Log.d(TAG, "The database is empty");
                sendRequest(context, myLocation, callback);
            }
        }
        return null;
    }

    private void sendRequest(Context context, final LatLng myLocation, final TravelObjectRequests.OnSuccessRequestListener callback) {
        new TravelObjectRequests().loadObjectsByLocationRequest(context, RADIUS, myLocation.latitude, myLocation.longitude,
                new TravelObjectRequests.OnSuccessRequestListener() {
                    @Override
                    public void onSuccessPlacesLoaded(List<ObjectPlace> objectPlaceList) {
                        Log.d("LOGGER DATAMANAGER", objectPlaceList.size() + "");
                        Preferences.getInstance().saveOldLocationUpdatePlaceLocation(myLocation);
                        if (objectPlaceList.size() > 0) {
                            callback.onSuccessPlacesLoaded(objectPlaceList);
                            db.save(objectPlaceList);
                        } else {
                            callback.onSuccessPlacesLoaded(db.getAllRecord());
                        }
                    }

                    @Override
                    public void onFail() {
                        callback.onFail();
                    }
                });
    }

    /*
     @return NOT(a>x && b<x && a1>x && b1<x )
     */
    private boolean isChange(LatLng myLocation) {
        LatLng oldLocationUpdatePlace = Preferences.getInstance().loadOldLocationUpdatePlaceLocation();
        return !((myLocation.latitude + DISTANCE >= oldLocationUpdatePlace.latitude
                && myLocation.latitude - DISTANCE <= oldLocationUpdatePlace.latitude)
                && (myLocation.longitude + DISTANCE > oldLocationUpdatePlace.longitude
                && myLocation.longitude - DISTANCE <= oldLocationUpdatePlace.longitude)
                && oldLocationUpdatePlace.latitude > 0.0);
    }
}
