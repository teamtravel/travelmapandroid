package com.travelmap3.home.NearestObject;

import android.content.Context;
import android.util.Log;

import com.travelmap3.App;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.utils.Config;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gairbek on 19.02.2016.
 */
public class TravelObjectRequests {

    private static final String TAG = "TravelObjectRequest";


    public void loadObjectsByLocationRequest(Context context, int radius, final double lat, final double lon, final OnSuccessRequestListener success) {
        String type = "type";
        RetrofitService.getInstanceAPI().getObjectPlace(App.getInstance(context).getComponent().uController().get().getToken(), Config.VERSION_API, lon, lat, radius, type, new Callback<ArrayList<ObjectPlace>>() {
            //RetrofitService.getInstanceAPI().getObjectPlaceTest(new Callback<ArrayList<ObjectPlace>>() {
            @Override
            public void success(ArrayList<ObjectPlace> ar, Response response2) {
                try {
                    Log.d("LOG", "Количество полученный элементов  " + ar.size());
                    success.onSuccessPlacesLoaded(ar);
                } catch (Exception e) {
                    success.onFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                success.onFail();
                if (error != null)
                    Log.i(TAG, "Failed to load travel object data from server " + error.getMessage());
            }
        });
    }

    public interface OnSuccessRequestListener {
        void onSuccessPlacesLoaded(List<ObjectPlace> objectPlaceList);

        void onFail();
    }

}
