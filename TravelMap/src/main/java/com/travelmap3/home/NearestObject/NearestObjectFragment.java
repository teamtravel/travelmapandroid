package com.travelmap3.home.NearestObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.menu.MainMenuActivity;

/**
 * Created by Dmitry Borodin on 06.12.2015.
 */
public class NearestObjectFragment extends Fragment {

    //*******************************************************
    // Section: variables
    //*******************************************************

    private View mRootView;


    private RecyclerView mRecyclerView;
    private NearestObjectAdapter mAdapter;
    private DrawerLayout mDrawerLayout;


    //*******************************************************
    // Section: overrides
    //*******************************************************

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Set userName as toolbar title

        mRootView = inflater.inflate(R.layout.fragment_nearest_object, container, false);
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.fragment_nearest_object_recycle);
        initRecycle();
        App application = (App) getActivity().getApplication();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen NearestObjectFragment");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
    }

    @Override
    public void onStart() {
        super.onStart();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    //*******************************************************
    // Section: private methods
    //*******************************************************

    private void initRecycle() {
        mAdapter = new NearestObjectAdapter(((MainMenuActivity) getActivity()).getObjectPlaces(), getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

}
