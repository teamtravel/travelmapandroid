package com.travelmap3.home.NearestObject;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.utils.CalculateForGps;

/**
 * Created by root on 3/8/16.
 */
public class NearestObjectOneFragment extends Fragment {
    private static final String TYPE = "type";
    private static final String DISTANCE = "distance";
    private static final String LATLNG = "latlng";
    private static final String TITLE = "title";
    private static final String IMG_URL = "img_url";
    private static final String DESCRIPTION = "description";
    private TextView title, distance, description, objectType;
    private ImageView img;

    private LatLng userLocation;
    private LatLng placeLocation;

    private SupportMapFragment supportMapFragment;

    public static NearestObjectOneFragment newInstance(ObjectPlace place, double userLat, double userLon) {
        Bundle args = new Bundle();
        args.putString(TITLE, place.getTitle());

        String description = Html.fromHtml(place.getReview()).toString();
        args.putString(DESCRIPTION, description);

        LatLng placeLatLng = new LatLng(place.getLatitude(), place.getLongitude());
        args.putParcelable(LATLNG, placeLatLng);

        float distance =
                (new CalculateForGps().distance(userLat,
                        userLon,
                        place.getLatitude(),
                        place.getLongitude())) / 1000;
        args.putFloat(DISTANCE, distance);

        args.putString(IMG_URL, place.getImageUrl());
        args.putString(TYPE, place.getType());

        NearestObjectOneFragment fragment = new NearestObjectOneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_nearest_object_one, container, false);

        initViews(root);
        userLocation = App.getInstance(getContext()).getComponent().uController().get().getPosition();
        placeLocation = getArguments().getParcelable(LATLNG);

        return root;
    }

    private void initViews(View rootView) {
        title = (TextView) rootView.findViewById(R.id.title);
        title.setText(getArguments().getString(TITLE));
        try {
            description = (TextView) rootView.findViewById(R.id.description);
            description.setText(getArguments().getString(DESCRIPTION));

            distance = (TextView) rootView.findViewById(R.id.distance);
            String distanceTo =
                    getArguments().getFloat(DISTANCE) + getString(R.string.nearest_object_one_kilometers);
            distance.setText(distanceTo);

            img = (ImageView) rootView.findViewById(R.id.image);
            String imgUrl = getArguments().getString(IMG_URL);
            Glide.with(getActivity())
                    .load(imgUrl)
                    .centerCrop()
                    .crossFade(400)
                    .into(img);

            objectType = (TextView) rootView.findViewById(R.id.object_type);
            String typeId = "ID" + getArguments().getString(TYPE);
            String packageName = getContext().getPackageName();
            int resId = getResources().getIdentifier(typeId, "string", packageName);
            String type = getString(resId);
            objectType.setText(type);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initMapFragment();
    }

    private void initMapFragment() {
        supportMapFragment = new SupportMapFragment();
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map, supportMapFragment).commit();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                Marker user = googleMap.addMarker(new MarkerOptions()
                        .position(userLocation)
                        .title(getString(R.string.nearest_object_one_i_am)));
                Marker place = googleMap.addMarker(new MarkerOptions()
                        .position(placeLocation)
                        .title(title.getText().toString()));

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(placeLocation, 11));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
                googleMap.getUiSettings().setAllGesturesEnabled(true);
            }
        });

    }
}
