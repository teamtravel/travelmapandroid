package com.travelmap3.home.NearestObject;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.utils.CalculateForGps;

import java.util.List;

/**
 * Created by Dmitry Borodin on 06.12.2015.
 */
public class NearestObjectAdapter extends RecyclerView.Adapter<NearestObjectAdapter.NearestObjectViewHolder> {

    private List<ObjectPlace> objectPlaceList;
    private AppCompatActivity mActivity;
    public NearestObjectAdapter(List<ObjectPlace> objectPlaceList, Activity activity) {
        this.objectPlaceList = objectPlaceList;
        mActivity = (AppCompatActivity) activity;

    }

    //*******************************************************
    // Section: Overrides
    //*******************************************************

    @Override
    public NearestObjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mActivity).inflate(R.layout.single_recycler_object_item, parent, false);
        return new NearestObjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NearestObjectViewHolder holder, int position) {
        final ObjectPlace place = objectPlaceList.get(position);


        if (place.getTitle() != null) {
            holder.title.setText(place.getTitle());
        }

        if (place.getImageUrl() != null) {
            Glide.with(mActivity).load(place.getImageUrl())
                    .centerCrop()
                    .crossFade(400)
                    .into(holder.image);
        }

        final float distance = (new CalculateForGps().distance(App.getInstance(mActivity).getComponent().uController().get().getLatitude(),
                App.getInstance(mActivity).getComponent().uController().get().getLongitude(),
                place.getLatitude(),
                place.getLongitude())) / 1000;
        holder.description.setText(
            distance + mActivity.getString(R.string.nearest_object_adapter_kilometer));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NearestObjectOneFragment f = NearestObjectOneFragment.newInstance(place, App.getInstance(mActivity).getComponent().uController().get().getLatitude(), App.getInstance(mActivity).getComponent().uController().get().getLongitude());
                mActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame,f).addToBackStack("").commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (objectPlaceList == null || objectPlaceList.size() == 0) {
            return 0;
        }else {
            return objectPlaceList.size();
        }
    }

    //*******************************************************
    // Section: nested
    //*******************************************************

    protected class NearestObjectViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private TextView description;
        private ImageView image;

        public NearestObjectViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.object_title);
            description = (TextView) itemView.findViewById(R.id.object_distance_to);
            image = (ImageView) itemView.findViewById(R.id.object_image);
        }
    }
}
