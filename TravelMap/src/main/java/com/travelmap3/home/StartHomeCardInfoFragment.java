package com.travelmap3.home;

import android.app.Fragment;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.event.LocationEvent;
import com.travelmap3.tracker.TrackerController;
import com.travelmap3.utils.CalculateForGps;
import com.travelmap3.utils.Preferences;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gairbek on 16.02.2016.
 * Card at the top of the layout.
 */
public class StartHomeCardInfoFragment extends Fragment {

    private static final String TAG = StartHomeCardInfoFragment.class.getSimpleName();
    private static final String SHOW_BACK = "show_back";

    private int[] layouts = new int[]{
            R.layout.fragment_home_card_info,
            R.layout.fragment_home_card_info_start_travel_prompt};

    private int sideToShow = 0;

    private TextView distanceTV;
    private TextView speedTv;
    private TextView tripDurationTv;
    private TextView locationTV;

    public Preferences prefs;

    /**
     * @param showBack - true if we want to show back view (prompt for start the trip and record
     *                 data)
     */
    public static StartHomeCardInfoFragment newInstance(boolean showBack) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_BACK, showBack);

        StartHomeCardInfoFragment fragment = new StartHomeCardInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    TrackerController trackerController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().getBoolean(SHOW_BACK)) sideToShow = 1;
        prefs = Preferences.getInstance();
        trackerController = App.getInstance(getActivity()).getComponent().trackerController();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(layouts[sideToShow], container, false);
        if (sideToShow == 1) {
            initViewsForBackSide(v);
        } else {
            initViewsForFrontSide(v);
        }
        return v;
    }

    private void initViewsForFrontSide(View rootView) {
        distanceTV = (TextView) rootView.findViewById(R.id.text_distance);
        speedTv = (TextView) rootView.findViewById(R.id.text_speed);
        tripDurationTv = (TextView) rootView.findViewById(R.id.text_duration);
        locationTV = (TextView) rootView.findViewById(R.id.text_location);
        speedTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.string_average_speed_home_card, Snackbar.LENGTH_SHORT).show();
            }
        });
        distanceTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.string_distance_trip, Snackbar.LENGTH_SHORT).show();
            }
        });
        locationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                        "Отправленно на сервер: " + CalculateForGps.timeLastUpdate() + " назад. ", Snackbar.LENGTH_SHORT).show();
            }
        });
        tripDurationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                        R.string.string_duration_tv, Snackbar.LENGTH_SHORT).show();
            }
        });
        fillTravelData(null);

        rootView.findViewById(R.id.img_flip_card).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard(true);
            }
        });
    }

    private void initViewsForBackSide(View rootView) {
        Button tripToggleButton = (Button) rootView.findViewById(R.id.trip_toggle_button);
        if (!prefs.isModeTravel()) {
            tripToggleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((int) App.getInstance(getActivity()).getComponent().uController().get().getLongitude() == 0 && (int) App.getInstance(getActivity()).getComponent().uController().get().getLatitude() == 0) {
                        Toast.makeText(getActivity(), R.string.warning_about_on_gps_for_start, Toast.LENGTH_LONG).show();
                    } else {
                        prefs.saveModeTravel();
                        prefs.saveTimeStartTravel(System.currentTimeMillis());
                        flipCard(false);
                        trackerController.countNumberTrip();
                        trackerController.initNewTrip(null, new Callback() {
                            @Override
                            public void success(Object o, Response response) {
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }
            });
        } else {
            TextView tv = (TextView) rootView.findViewById(R.id.start_travel_prompt);
            tv.setText(
                    R.string.start_home_card_info_trip_state);

            tripToggleButton.setText(R.string.start_home_card_info_finish_trip);
            tripToggleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        prefs.clearModeTravel();
                        prefs.clearLastLocation();
                        prefs.clearStartLocation();
                        flipCard(false);
                        trackerController.finishTrip(new Callback() {
                            @Override
                            public void success(Object o, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                        App.getInstance(getActivity()).getComponent().uController().get().setLongitude(0);
                        App.getInstance(getActivity()).getComponent().uController().get().setLatitude(0);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            ImageView flipButton = (ImageView) rootView.findViewById(R.id.flip_back);
            flipButton.setVisibility(View.VISIBLE);
            flipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flipCard(false);
                }
            });
        }
    }

    private void flipCard(boolean showBackSide) {
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.card_flip_bottom_in, R.animator.card_flip_up_out)
                .replace(R.id.card_fragment_holder, StartHomeCardInfoFragment.newInstance(showBackSide))
                .commit();
    }

    private void fillTravelData(@Nullable Location location) {
        Log.w(TAG, "Обновление данных");

        CalculateForGps compute = new CalculateForGps();
        DecimalFormat format = new DecimalFormat();
        format.setRoundingMode(RoundingMode.DOWN);

        String updating = getString(R.string.start_home_card_info_updating);

        float distance = Math.round(prefs.loadPassedDistance());
        float speed = 0; // Fill with real value later
        String tripDuration = compute.timeSpentOnTheRoadInDays();
        String loc = App.getInstance(getActivity()).getComponent().uController().get().getLatitude() + " " + App.getInstance(getActivity()).getComponent().uController().get().getLongitude();

        if (location != null) {
            distance = compute.distanceBetweenStartAndCurrentPosition(location);
            String latitude = format.format(location.getLatitude());
            String longitude = format.format(location.getLongitude());
            loc = latitude + " " + longitude;
        }
        speed = compute.getSpeedAv(distance / 1000);
        if (speed == 0) {
            speedTv.setText(updating);
        }
        distanceTV.setText(distance + getString(R.string.start_home_card_info_kilometers));
        speedTv.setText(speed + getString(R.string.start_home_card_info_kilometers_hours));
        tripDurationTv.setText(tripDuration);
        locationTV.setText(loc);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onEvent(LocationEvent event) {
        fillTravelData(event.location);
    }
}
