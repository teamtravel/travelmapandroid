package com.travelmap3.home;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.event.LocationEvent;
import com.travelmap3.home.NearestObject.NearestObjectOneFragment;
import com.travelmap3.home.NearestObject.TravelObjectRequests;
import com.travelmap3.maps.GoogleMapCustomMarkerClusteringActivity;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.ObjectPlace;
import com.travelmap3.model.User;
import com.travelmap3.profile.ProfileActivity;
import com.travelmap3.utils.CalculateForGps;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.NearbyTravelerController;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.Utils;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import de.greenrobot.event.EventBus;

import static com.travelmap3.menu.MainMenuActivity.REQUEST_SENDBIRD_CHAT_ACTIVITY;

/**
 * Created by Gairbek on 14.02.2016.
 * 1. 037882472 2. 039507968
 * 3. 277980561 4. 796640925
 * 5. 856666084 6. 800983343
 * 7. 664045791 8. 132370523
 * 9. 302349810 10. 119462522 for test
 */
public class StartHomeFragment extends Fragment {

    private static final String TAG = StartHomeFragment.class.getSimpleName();

    private View rootView;
    private Preferences prefs;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    private int[] placesImageIds = new int[]{
            R.id.place_img_id_1,
            R.id.place_img_id_2,
            R.id.place_img_id_3,
            R.id.place_img_id_4
    };

    private int[] placesTextIds = new int[]{
            R.id.place_text_id_1,
            R.id.place_text_id_2,
            R.id.place_text_id_3,
            R.id.place_text_id_4
    };

    private int[] profilesImageIds = new int[]{
            R.id.profile_img_id_1,
            R.id.profile_img_id_2
    };

    private int[] profilesTextIds = new int[]{
            R.id.profile_text_id_1,
            R.id.profile_text_id_2
    };
    private InternetConnect internetConnect;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        prefs = Preferences.getInstance();
        internetConnect = App.getInstance(getActivity()).getComponent().getInternetConnect();

    }

    private void initCard() {
        // Если не в режиме путешествия то показываем сторону с кнопкой начала путешествия
        boolean cardInfoShowMode = !prefs.isModeTravel();
        StartHomeCardInfoFragment fragment = StartHomeCardInfoFragment.newInstance(cardInfoShowMode);
        getActivity().getFragmentManager()
                .beginTransaction()
                .replace(R.id.card_fragment_holder, fragment)
                .commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        setUpToolbar();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        ///Обновление по свайпу
        initSwipeRefresh();

        //Загрузка и обновление данных
        updateData();
        return rootView;
    }

    private void viewInit(List<ObjectPlace> objectPlaceList) {
        try {
            if (!getActivity().isFinishing()) {
                initPlaces(objectPlaceList);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition((ViewGroup) rootView);
                }
                progressBar.setVisibility(View.GONE);
                rootView.findViewById(R.id.places_layout).setVisibility(View.VISIBLE);
                ((TextView) rootView.findViewById(R.id.places_text_show_more)).setText(
                        getString(R.string.start_home_fragment_near) + objectPlaceList.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initSwipeRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!internetConnect.isInternetOn()) {
                    Snackbar.make(getActivity().getWindow().getDecorView().findViewById(android.R.id.content),
                            R.string.login_unable_connect_to_internet, Snackbar.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    updateData();
                }
            }
        });
    }

    private void updateData() {
        App.getInstance(getActivity()).getComponent().dataManagerObject().takeListObjectPlace(getActivity(), App.getInstance(getContext()).getComponent().uController().get().getGps().getPosition(),
                new TravelObjectRequests.OnSuccessRequestListener() {
                    @Override
                    public void onSuccessPlacesLoaded(final List<ObjectPlace> objectPlaceList) {
                        try {
                            if (rootView != null && !getActivity().isFinishing()) {
                                rootView.findViewById(R.id.places_show_more_button)
                                        .setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                ((MainMenuActivity) getActivity()).openNearestObjectFragment(objectPlaceList);
                                            }
                                        });
                            }

                            viewInit(objectPlaceList);
                            rootView.findViewById(R.id.button_object_show).setVisibility(View.VISIBLE);
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail() {
                        viewChange();
                        try {
                            Toast.makeText(getActivity(), " Сервис достопримичательностей времено не доступен", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        initCard();
        initProfiles(rootView);
    }

    private void viewChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            TransitionManager.beginDelayedTransition((ViewGroup) rootView);
        }
        progressBar.setVisibility(View.GONE);
        rootView.findViewById(R.id.places_not_found_tv).setVisibility(View.VISIBLE);
    }

    public void openMapActivity(double lat, double lon) {
        Intent intent = new Intent(getActivity(), GoogleMapCustomMarkerClusteringActivity.class);
        intent.putExtra(GoogleMapCustomMarkerClusteringActivity.ZOOM_CAMERA_USERS_NEARBE, 12);
        intent.putExtra(GoogleMapCustomMarkerClusteringActivity.NEARBY_LATITUDE, lat);
        intent.putExtra(GoogleMapCustomMarkerClusteringActivity.NEARBY_LONGITUDE, lon);
        startActivity(intent);
    }

    private void initProfiles(View rootView) {
        HashSet usersSet = new NearbyTravelerController(getContext()).currentData();
        if (usersSet.size() < 2) {
            this.rootView.findViewById(R.id.profiles_holder_layout).setVisibility(View.GONE);
            this.rootView.findViewById(R.id.profiles_not_found_tv).setVisibility(View.VISIBLE);
            return;
        }

        final ArrayList nearestUsers = new ArrayList(usersSet);
        initProfileWindowWithUser((User) nearestUsers.get(0), 0);
        initProfileWindowWithUser((User) nearestUsers.get(1), 1);

        Button btnShowUsersMap = (Button) rootView.findViewById(R.id.btnShowUsersInMap);
        btnShowUsersMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("LOG", "open");
                openMapActivity(((User) nearestUsers.get(0)).getLatitude(),
                        ((User) nearestUsers.get(0)).getLongitude());
            }
        });

    }

    // This method will be called when a BusEvent is posted
    public void onEvent(LocationEvent event) {
        // changeLocationUpdateView(event.location); FIXME
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    private void initProfileWindowWithUser(final User user, int position) {
        ImageView profile = (ImageView) rootView.findViewById(profilesImageIds[position]);
        boolean isPhotoDefault = Utils.isPhotoDefault(user.getPhoto50());
        if (!isPhotoDefault) {
            Glide.with(getActivity())
                    .load(user.getPhoto50())
                    .into(profile);
        } else {
            Glide.with(getActivity())
                    .load(R.drawable.default_photo)
                    .into(profile);
        }
        updatePhotoView(profile);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = ProfileActivity.newIntent(getContext(), user.getId());
                getActivity().startActivityForResult(intent, REQUEST_SENDBIRD_CHAT_ACTIVITY);
            }
        });

        float distanceToInMeters = new CalculateForGps()
                .distance(
                        App.getInstance(getContext()).getComponent().uController().get().getLatitude(),
                        App.getInstance(getContext()).getComponent().uController().get().getLongitude(),
                        user.getLatitude(),
                        user.getLongitude()
                );

        DecimalFormat format = new DecimalFormat();
        format.setRoundingMode(RoundingMode.DOWN);
        String distanceToInKm = format.format(distanceToInMeters / 1000);

        TextView firstProfileText = (TextView) rootView.findViewById(profilesTextIds[position]);
        firstProfileText.setText(user.getFullName() + ", " + distanceToInKm + " км");
    }

    //обновление маштабирования фото в ImageView
    private void updatePhotoView(ImageView image) {
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    private void initPlaces(List<ObjectPlace> list) {
        for (int i = 0; i < list.size() && i < 4; i++) {
            initPlace(list.get(i), i);
        }
    }

    private void initPlace(final ObjectPlace objectPlace, final int position) {
        if (getActivity() == null) {
            return;
        }
        ImageView imageView = (ImageView) rootView.findViewById(placesImageIds[position]);
        Glide.with(getActivity())
                .load(objectPlace.getImageUrl())
                .crossFade(400)
                .into(imageView);
        updatePhotoView(imageView);
        final float distance = (new CalculateForGps().distance(App.getInstance(getContext()).getComponent().uController().get().getLatitude(),
                App.getInstance(getContext()).getComponent().uController().get().getLongitude(),
                objectPlace.getLatitude(),
                objectPlace.getLongitude())) / 1000;
        final String text = objectPlace.getTitle() + ", " + distance + " м";
        ((TextView) rootView.findViewById(placesTextIds[position])).setText(text);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NearestObjectOneFragment f = NearestObjectOneFragment.newInstance(
                        objectPlace,
                        App.getInstance(getActivity()).getComponent().uController().get().getLatitude(),
                        App.getInstance(getActivity()).getComponent().uController().get().getLongitude()
                );
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, f).addToBackStack("").commit();
            }
        });
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_burger);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
