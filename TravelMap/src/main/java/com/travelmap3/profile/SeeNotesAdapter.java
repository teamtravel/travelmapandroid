package com.travelmap3.profile;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.travelmap3.R;
import com.travelmap3.notes.Notes;
import com.travelmap3.utils.TextFormat;

import java.util.ArrayList;

/**
 * Created by Andre on 23.03.2016.
 */
public class SeeNotesAdapter extends RecyclerView.Adapter<SeeNotesAdapter.ViewHolder> {

    int screenWidth;
    private ArrayList<Notes> list;
    private Context context;

    public SeeNotesAdapter(ArrayList<Notes> data, Context c) {
        list = data;
        context = c;
        screenWidth = getScreenWidth();
    }

    @Override
    public SeeNotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.see_notes_card, parent, false);
        return new ViewHolder(v);
    }

    public void setList(ArrayList<Notes> data) {
        this.list = data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mButton.setVisibility(View.VISIBLE);
        holder.mTitle.setText(list.get(position).getTextTitleNote());
        holder.mDate.setText(TextFormat.fullDate(list.get(position).getTimeLastUpdate()));

        final String fullText = list.get(position).getTextNote();

        final TextView tv = holder.mText;
        tv.setText(fullText);

        final StringBuilder text = new StringBuilder();
        text.append(fullText);

        for (int rows = tooMuchTextObjectNear(text, tv); rows > 5;
             rows = tooMuchTextObjectNear(text, tv)) {
            if (rows > 20) {
                text.delete(text.length() / 2, text.length()).append("...");
            } else if (rows > 14) {
                text.delete(text.length() / 4 * 3, text.length()).append("...");
            } else {
                text.delete(text.length() - 10, text.length()).append("...");
            }
        }

        tv.setText(text.toString());

        //todo this should be setted to cardView itself

        holder.mButton.setOnClickListener(new View.OnClickListener() {
            final String defaultText = holder.mButton.getText().toString();
            boolean isTextHidden = true;

            @Override
            public void onClick(View v) {
                if (isTextHidden) {
                    tv.setText(fullText);
                    holder.mButton.setVisibility(View.GONE);
                    // context.getResources().getString(R.string.yes);
                    isTextHidden = false;
                } else {
                    tv.setText(text.toString());
                    holder.mButton.setText(defaultText);
                    isTextHidden = true;
                }
            }
        });


        if (list.get(position).getImageLocationsList() != null && list.get(position).getImageLocationsList().size() > 0) {
            holder.mImage.setData(list.get(position).getImageLocationsList());
        } else {
            holder.mImage.setVisibility(View.GONE);
        }
    }

    private int tooMuchTextObjectNear(StringBuilder sb, TextView tv) {
        Rect bounds = new Rect();
        Paint paint = new Paint();
        paint.setTextSize(tv.getTextSize());
        paint.getTextBounds(sb.toString(), 0, sb.length(), bounds);
        int width = (int) Math.ceil(bounds.width());
        return (int) Math.ceil(width / screenWidth);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private int getScreenWidth() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            return size.x;
        } else {
            return display.getWidth();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mDate;
        public TextView mText;
        public TextView mTitle;
        public SliderImageView mImage;
        public TextView mButton;

        public ViewHolder(View v) {
            super(v);
            mTitle = (TextView) v.findViewById(R.id.title);
            mDate = (TextView) v.findViewById(R.id.dateNote);
            mText = (TextView) v.findViewById(R.id.textViewUsers);
            mImage = (SliderImageView) v.findViewById(R.id.image);
            mButton = (TextView) v.findViewById(R.id.unfoldBtn);
        }
    }


}
