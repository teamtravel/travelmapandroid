package com.travelmap3.profile;

import android.util.Log;

import com.travelmap3.UserController;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.event.BusNotesListEvent;
import com.travelmap3.notes.Notes;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.Preferences;

import java.util.ArrayList;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andre on 22.03.2016.
 */
public class DataManagerUserNotes {
    private final UserController userController;
    private final Preferences preferences;

    @Inject
    public DataManagerUserNotes(UserController userController, Preferences preferences) {
        this.userController = userController;
        this.preferences = preferences;
    }

    public void requestOnServer(int id) {
        RetrofitService.getInstanceAPI().getNote(preferences.loadToken(), Config.VERSION_API, id, userController.get().getId(), new Callback<ArrayList<Notes>>() {
            @Override
            public void success(ArrayList<Notes> list, Response response) {
                Log.w("DataManagerUserNotes", "success requestOnServer");
//                for (Notes n : list) { for test image slider
//                    List<ImageLocations> l = new ArrayList<ImageLocations>();
//                    l.add(new ImageLocations("http://res.cloudinary.com/travelmaps/image/upload/207985b9673c43a1075a2b2137298ad8cf7de523b7e1ffc85f2f5fa0a711e5be", ""));
//                    l.add(new ImageLocations("http://img0.joyreactor.cc/pics/post/full/%D0%B3%D1%83%D0%B3%D0%BB-%D0%BC%D0%B0%D0%BF%D1%81-33592.jpeg", ""));
//                    n.setImageLocationsList(l);
//                }
                EventBus.getDefault().post(new BusNotesListEvent(list));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("LOG", error.getMessage() + " " + error.getKind());
                EventBus.getDefault().post(new BusNotesListEvent(null));
            }
        });
    }
}
