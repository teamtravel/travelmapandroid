package com.travelmap3.profile;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.travelmap3.R;

import java.util.ArrayList;
import java.util.List;

public class ProfilePagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fragmentList = new ArrayList<>();
    private int[] imageResId = {
            R.drawable.ic_person_36dp,
            R.drawable.ic_map_36dp,
            R.drawable.ic_profile_wall_36dp};

    public ProfilePagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(Fragment fragment) {
        fragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public int getImageResId(int num){
        return imageResId[num];
    }
}