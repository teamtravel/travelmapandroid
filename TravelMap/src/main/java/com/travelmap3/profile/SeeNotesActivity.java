package com.travelmap3.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.event.BusNotesListEvent;
import com.travelmap3.notes.Notes;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Andre on 23.03.2016.
 */
public class SeeNotesActivity extends AppCompatActivity {

    ArrayList<Notes> list;
    private RecyclerView mRecyclerView;
    private SeeNotesAdapter mAdapter;
    private CircleProgressBar progress;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_notes);

        frameLayout = (FrameLayout) findViewById(R.id.see_notes_frame);
        progress = (CircleProgressBar) findViewById(R.id.progress);
        frameLayout.setVisibility(View.VISIBLE);
        if (getIntent() != null)
            App.getInstance(this).getComponent().dataManagerNotes().requestOnServer(getIntent().getIntExtra(ProfileActivity.ID, 0)); // result put in onEvent()
        list = new ArrayList<>();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        assert mRecyclerView != null;
        mRecyclerView.setVisibility(View.INVISIBLE);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new SeeNotesAdapter(list, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void onEvent(BusNotesListEvent eventList) {
        Log.e("LOG", "onEvent "+eventList);
        progress.setVisibility(View.GONE);
        if (eventList != null && eventList.list != null) {
            //  list.addAll(fillingList()); //Добавляем в список данные нашего дневника
            frameLayout.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.setList(eventList.list);
            Log.e("LOG", "set List");
        } else {
            if (findViewById(R.id.textNoNotes) != null) {
                findViewById(R.id.textNoNotes).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }
}
