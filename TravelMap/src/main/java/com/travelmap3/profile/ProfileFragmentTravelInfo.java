package com.travelmap3.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.UserController;
import com.travelmap3.model.User;

/**
 * Created by Andrey on 23.10.2015.
 */
public class ProfileFragmentTravelInfo extends Fragment {

    private static final String ARGS_USER_DATA_TRAVEL_MODEL = "userDataJson";
    private static final String LOG = ProfileFragmentTravelInfo.class.getSimpleName();

    private User mUser;

    public static ProfileFragmentTravelInfo newInstance(User user) {
        ProfileFragmentTravelInfo fragmentTravelInfo = new ProfileFragmentTravelInfo();
        Bundle args = new Bundle();
        args.putInt(ARGS_USER_DATA_TRAVEL_MODEL, user.getId());
        fragmentTravelInfo.setArguments(args);
        return fragmentTravelInfo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserController mController = App.getInstance(getActivity()).getComponent().uController();
//        boolean argsContainsUser = getArguments() != null && getArguments().containsKey(ARGS_USER_DATA_TRAVEL_MODEL) && getArguments().getSerializable(ARGS_USER_DATA_TRAVEL_MODEL) != null;
//        if (argsContainsUser){
//            mUser = (User) getArguments().getSerializable(ARGS_USER_DATA_TRAVEL_MODEL);
//        } else {
//            mUser = mController.currentData();
//        }
        int userId = getArguments().getInt(ARGS_USER_DATA_TRAVEL_MODEL);
        mUser = mController.getUserById(userId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_travel_info, container, false);
        return v;
    }

}
