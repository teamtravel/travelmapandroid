package com.travelmap3.profile;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.travelmap3.R;

/**
 * Created by tema_ on 17.07.2016.
 */
public class PanelForSlider  extends LinearLayout{
    private TextView currentImg, countImg;
    private ImageView statusIcon;

    public PanelForSlider(Context context) {
        super(context);
        init(context);
    }

    public PanelForSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PanelForSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PanelForSlider(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.panel_for_slider,this,true);
        currentImg = (TextView)view.findViewById(R.id.panel_for_slider_current_item);
        countImg = (TextView)view.findViewById(R.id.panel_for_slider_count_items);
        statusIcon = (ImageView)view.findViewById(R.id.panel_for_slider_status_icon);
    }

    public void setIcon(int res){
        statusIcon.setImageResource(res);
    }

    public void setCount(int count){
        countImg.setText(String.valueOf(count));
    }

    public void setCurrentNumber(int pos){
        currentImg.setText(String.valueOf(pos));
    }
}
