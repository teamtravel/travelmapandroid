package com.travelmap3.profile;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.travelmap3.R;
import com.travelmap3.notes.Notes;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by tema_ on 16.07.2016.
 */
public class SliderImageView extends RelativeLayout {
    private static final int DEFAULT_SLIDE_INTERVAL = 3500;

    //View
    private ViewPager viewPager;
    private PanelForSlider panel;
    private ImageView statusSlideAnimation;

    //
    private List<Notes.ImageLocations> data;
    private int countItems;
    private Timer swipeTime;

    public SliderImageView(Context context) {
        super(context);
        initView(context);
    }

    public SliderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public SliderImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SliderImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        View v = LayoutInflater.from(context).inflate(R.layout.slider_image_view, this, true);
        viewPager = (ViewPager) v.findViewById(R.id.slide_image_pager);
        panel = (PanelForSlider) v.findViewById(R.id.slide_image_panel);
        statusSlideAnimation = (ImageView) v.findViewById(R.id.slide_image_status_animation);
    }

    public void setData(List<Notes.ImageLocations> data) {
        this.data = data;
        countItems = data.size();
        panel.setCount(countItems);

        SliderAdapter adapter = new SliderAdapter();
        viewPager.setAdapter(adapter);

        if (data.size() > 1) {
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    if (state == 1 && swipeTime != null) {
                        stopTimer();
                    }
                    if (state == 2 && swipeTime == null) {
                        startSwipeTimer();
                    }

                }
            });

            if (swipeTime != null) {
                stopTimer();
                startSwipeTimer();
            }
        }
    }

    private void showPanel(int i) {
        panel.setVisibility(VISIBLE);
        panel.setCurrentNumber(i);
        panel.postDelayed(new Runnable() {
            @Override
            public void run() {
                panel.setVisibility(GONE);
            }
        }, 750);
    }

    private void startSwipeTimer() {
        swipeTime = new Timer();
        swipeTime.schedule(new SwipeTask(), new Random().nextInt(2500) + 3500, DEFAULT_SLIDE_INTERVAL);
    }

    private void stopTimer() {
        swipeTime.cancel();
        swipeTime.purge();
        swipeTime = null;
    }


    private void changeStatusSlide() {
        int panelRes, res;

        if (swipeTime != null) {
            stopTimer();
            panelRes = R.drawable.ic_ab_back; // TODO FIXME
            res = R.drawable.ic_ab_back;
        } else {
            startSwipeTimer();
            panelRes = R.drawable.ic_ab_back;
            res = R.drawable.ic_ab_back;
        }

        panel.setIcon(panelRes);
        statusSlideAnimation.setImageResource(res);
        statusSlideAnimation.setVisibility(VISIBLE);
        statusSlideAnimation.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.slider_status_icon_fade));
        statusSlideAnimation.postDelayed(new Runnable() {
            @Override
            public void run() {
                statusSlideAnimation.setVisibility(GONE);
            }
        }, 1500);
    }


    private class SliderAdapter extends PagerAdapter implements OnClickListener, OnLongClickListener {
        @Override
        public int getCount() {
            return countItems;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            imageView.setOnClickListener(this);
            imageView.setOnLongClickListener(this);
            Glide.with(getContext()).load(data.get(position).getUrlInCloud()).diskCacheStrategy(DiskCacheStrategy.RESULT).into(imageView);
            container.addView(imageView);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public void onClick(View v) {
            showPanel(viewPager.getCurrentItem() + 1);
        }

        @Override
        public boolean onLongClick(View v) {
            changeStatusSlide();
            return true;
        }
    }

    private class SwipeTask extends TimerTask {
        @Override
        public void run() {
            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem((viewPager.getCurrentItem() + 1) % countItems, true);
                }
            });
        }
    }
}
