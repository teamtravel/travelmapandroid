package com.travelmap3.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.model.User;

/**
 * Created by guest on 02.09.15.
 */
public class ProfileFragmentBaseInfo extends Fragment {

    private static final String ARGS_USER_DATA_MODEL = "us";
    private static final String LOG = ProfileFragmentBaseInfo.class.getSimpleName();

    private User mUser;

    public static ProfileFragmentBaseInfo newInstance(User user) {
        ProfileFragmentBaseInfo fragmentBaseInfo = new ProfileFragmentBaseInfo();
        Bundle args = new Bundle();
        args.putInt(ARGS_USER_DATA_MODEL, user.getId());
        fragmentBaseInfo.setArguments(args);
        return fragmentBaseInfo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int userId = (int) getArguments().getInt(ARGS_USER_DATA_MODEL);
        mUser = App.getInstance(getActivity()).getComponent().uController().getUserById(userId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_base_info, container, false);

        if (mUser != null)
            initViews(v);
        return v;
    }

    private void initViews(View v) {
        try {
            String status = mUser.getStatusText();
            String name = mUser.getFirstName() + " " + mUser.getLastName();
            String genderAndAge = String.valueOf(mUser.getAge());
            String languagesSpoken = "";
            String currentDaysOnRoad = "";
            String homeTown = mUser.getFrom_city();
            String skype = mUser.getSkype();
            String vkLink = mUser.getVkId();
            String fbLink = mUser.getFbId();
            String work = mUser.getJobs();
            String school = mUser.getStudy();
            String about = mUser.getAbout();
            String telegram = mUser.getTelegram();

            if (!status.isEmpty())
                setUpViewWithText(v, R.id.profile_fragment_status, status);

            if (!name.isEmpty())
                setUpViewWithText(v, R.id.profile_fragment_name, name);

//        if (!genderAndAge.isEmpty())
//            setUpViewWithText(v, R.id.profile_fragment_sex_age, genderAndAge.compareTo("1")==0?"Муж":"Жен");

            if (!languagesSpoken.isEmpty())
                setUpViewWithText(v, R.id.profile_fragment_languages, languagesSpoken);

            if (!currentDaysOnRoad.isEmpty())
                setUpViewWithText(v, R.id.profile_fragment_languages, currentDaysOnRoad);

            if (!homeTown.isEmpty())
                setUpViewWithText(v, R.id.profile_fragment_hometown, homeTown);

            final String mobile = mUser.getPhone();
            if (!mobile.isEmpty()) {
                TextView textView = (TextView) v.findViewById(R.id.profile_fragment_mobile);
                textView.setText(mobile);
                textView.setVisibility(View.VISIBLE);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder callDialog = new AlertDialog.Builder(getActivity());
                        callDialog.setMessage("Позвонить " + mUser.getFullName() + "?");
                        callDialog.setNegativeButton("Отмена", null);
                        callDialog.setPositiveButton("Позвонить", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // Call intent
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:" + mobile));
                                startActivity(intent);
                            }
                        });
                        callDialog.show();

                    }
                });
                v.findViewById(R.id.profile_fragment_contact_data_prompt).setVisibility(View.VISIBLE);
            }
            if (!skype.isEmpty()) {
                setUpViewWithText(v, R.id.profile_fragment_skype, skype);
                v.findViewById(R.id.profile_fragment_contact_data_prompt).setVisibility(View.VISIBLE);
            }

            if (!vkLink.isEmpty()) {
                setUpViewWithText(v, R.id.profile_fragment_vk_link, vkLink);
                v.findViewById(R.id.profile_fragment_contact_data_prompt).setVisibility(View.VISIBLE);
            }

            if (!fbLink.isEmpty()) {
                v.findViewById(R.id.profile_fragment_contact_data_prompt).setVisibility(View.VISIBLE);
                setUpViewWithText(v, R.id.profile_fragment_fb_link, vkLink);
            }

            if (!work.isEmpty()) {
                setUpViewWithText(v, R.id.profile_fragment_work, work);
            }

            if (!school.isEmpty()) {
                v.findViewById(R.id.profile_fragment_work_prompt).setVisibility(View.VISIBLE);
                setUpViewWithText(v, R.id.profile_fragment_school, school);
            }

            if (!about.isEmpty()) {
                v.findViewById(R.id.profile_fragment_about_prompt).setVisibility(View.VISIBLE);
                setUpViewWithText(v, R.id.profile_fragment_about, about);
            }

            if (!telegram.isEmpty()) {
                v.findViewById(R.id.profile_fragment_telegramm).setVisibility(View.VISIBLE);
                setUpViewWithText(v, R.id.profile_fragment_telegramm, telegram);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpViewWithText(View v, int resId, String text) {
        TextView view = (TextView) v.findViewById(resId);
        view.setText(text);
        view.setVisibility(View.VISIBLE);
    }
}
