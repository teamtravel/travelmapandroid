package com.travelmap3.profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.UserController;
import com.travelmap3.api.UploadManager;
import com.travelmap3.event.BusEvent;
import com.travelmap3.maps.FetchAddressIntentService;
import com.travelmap3.model.User;
import com.travelmap3.tips.TIPS;
import com.travelmap3.tips.TipsCreate;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Utils;

import java.io.IOException;
import java.io.InputStream;

import de.greenrobot.event.EventBus;

/**
 * Created by Gair on 29.08.15.
 */

public class ProfileActivity extends AppCompatActivity {

    private static final String INTENT_USER_DATA = "intentUserDataMODEL";
    private static final String LOG = ProfileActivity.class.getSimpleName();
    public static final String ID = "ID";
    private static UserController controller;
    private static User mUser;
    protected Location mLastLocation;
    CircleProgressBar progress;

    private AddressResultReceiver mResultReceiver;

    public InternetConnect internetConnect;

    public static Intent newIntent(Context context, int userId) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(INTENT_USER_DATA, userId);
        controller = App.getInstance(context).getComponent().uController();
        mUser = controller.getUserById(userId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  int userId = getIntent().getIntExtra(INTENT_USER_DATA, 0);
        setContentView(R.layout.activity_profile);
        TipsCreate.getInstance().show(TIPS.PROFILE, getFragmentManager(), this);
        setUpToolbar();
        initViewPagerAndTabs();
        internetConnect = App.getInstance(this).getComponent().getInternetConnect();
        App application = (App) getApplication();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen Profile activity");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        if (isMyOwnPage()) {
            menu.findItem(R.id.edit_profile).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private boolean isMyOwnPage() {
        return controller.get().getId().equals(mUser.getId());
    }

    private void dialogShow() {
        final String[] items = new String[]{"SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);

        builder.setTitle("Выбрать изображение из:");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 1);
                }
            }
        });
        final android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void initViewPagerAndTabs() {
        try {
            ViewPager viewPager = (ViewPager) findViewById(R.id.profile_viewPager);

            ProfilePagerAdapter pagerAdapter = new ProfilePagerAdapter(getSupportFragmentManager());
            pagerAdapter.addFragment(ProfileFragmentBaseInfo.newInstance(mUser));
            //      pagerAdapter.addFragment(ProfileFragmentTravelInfo.newInstance(mUser));
            //        pagerAdapter.addFragment(new ProfileWallFragment());
            if (viewPager != null) {
                viewPager.setAdapter(pagerAdapter);
            }

            ImageButton btnNotes = (ImageButton) findViewById(R.id.imageButtonNotes);
            if (btnNotes != null) {
                btnNotes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (internetConnect.isActiveWiFiConnection()) {
                            showNotesUsers(mUser.getId());
                        } else {
                            Toast.makeText(getApplicationContext(), "Загрузка сейчас не доступна", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            ImageButton imgButton = (ImageButton) findViewById(R.id.profile_add_new_image);
            progress = (CircleProgressBar) findViewById(R.id.progress);
            if (isMyOwnPage()) {
                if (btnNotes != null) {
                    btnNotes.setVisibility(View.GONE);
                }
                if (imgButton != null) {
                    imgButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_camera_alt_24dp));
                    imgButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogShow();
                        }
                    });
                }

            } else {
                if (btnNotes != null) {
                    btnNotes.setVisibility(View.VISIBLE);
                }
                assert imgButton != null;
                imgButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startChat(mUser.getStringId());
                    }
                });
            }

            TabLayout tabLayout = (TabLayout) findViewById(R.id.profile_tabLayout);
            assert tabLayout != null;
            tabLayout.setupWithViewPager(viewPager);
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                if (tab != null) {
                    tab.setIcon(pagerAdapter.getImageResId(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startChat(String userIds) {
        Intent data = new Intent();
        data.putExtra("userIds", new String[]{userIds});
        setResult(RESULT_OK, data);
        finish();
    }

    private void showNotesUsers(Integer id) {
        Intent intent = new Intent(this, SeeNotesActivity.class);
        intent.putExtra(ID, id);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case (android.R.id.home): {
                onBackPressed();
                break;
            }
            case (R.id.edit_profile): {
                startActivity(new Intent(this, ProfileEditActivity.class));
                break;
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void setUpToolbar() {
        Toolbar tb = (Toolbar) findViewById(R.id.profile_activity_toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Load image into toolbar
        ImageView tbImage = (ImageView) findViewById(R.id.profile_back_image);
        if (Utils.isPhotoDefault(mUser.getPhoto200())) {
            assert tbImage != null;
            Glide.with(this).load(R.drawable.default_photo).into(tbImage);
        } else
            Glide.with(this).load(mUser.getPhoto200()).into(tbImage);
        // Set userName as toolbar title
        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.profile_collapsing);
        assert toolbarLayout != null;
        toolbarLayout.setTitle(mUser.getFullName());

        updateToolbarViewWithLastLocation();
    }

    private void updateToolbarViewWithLastLocation() {
        mLastLocation = getLastLocation();
        if (mLastLocation != null) {
            // Determine whether a Geocoder is available.
            if (!Geocoder.isPresent()) {
                Toast.makeText(this, "No geocoder available",
                        Toast.LENGTH_LONG).show();
                return;
            }
            startIntentService();
        }
    }

    private Location getLastLocation() {
        Location lastLocation = new Location("Current");
        lastLocation.setLatitude(mUser.getLatitude());
        lastLocation.setLongitude(mUser.getLongitude());
        return lastLocation;
    }

    protected void startIntentService() {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        mResultReceiver = new AddressResultReceiver(new Handler());
        intent.putExtra(FetchAddressIntentService.RECEIVER, mResultReceiver);
        intent.putExtra(FetchAddressIntentService.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;

        if (requestCode == 1) {
            Bitmap bitmap200 = null;
            progress.setVisibility(View.VISIBLE);
            Uri selectedImageUri = data.getData();
            InputStream imageStream200 = null;
            try {
                imageStream200 = getApplicationContext().getContentResolver().openInputStream(selectedImageUri); // необходимо использовать новый поток
                bitmap200 = cropImage(imageStream200, 200, 200, selectedImageUri);
                assert imageStream200 != null;
                imageStream200.close();
                String urlImage = new UploadManager().uploadBitmap(getApplicationContext(), bitmap200);
                if (urlImage == null) {
                    progress.setVisibility(View.INVISIBLE);
                    Toast.makeText(this, "Error upload", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void onEvent(BusEvent event) { // Ждем события с uploadManager
        progress.setVisibility(View.INVISIBLE);
        if (event.message.compareTo(UploadManager.URL_PHOTO) == 0) {
            updateUserPhotoServer();
        }
    }


    public void updateUserPhotoServer() {
        progress.setVisibility(View.VISIBLE);
        App.getInstance(this).getComponent().uController().setUser(App.getInstance(this).getComponent().uController().get(), new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(getApplicationContext(), "Аватарка обновленна", Toast.LENGTH_SHORT).show();
                Glide.with(ProfileActivity.this).load(App.getInstance(getApplicationContext()).getComponent().uController().get().getPhoto200()).into((ImageView) findViewById(R.id.profile_back_image));
                progress.setVisibility(View.INVISIBLE);
            }

            @Nullable
            @Override
            public void onError(Object obj) {
                Toast.makeText(getApplicationContext(), "Ошибка загрузки", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public Bitmap cropImage(InputStream in, int w, int h, Uri selectedImageUri) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        BitmapFactory.decodeStream(in, null, options);
        in.close();
        int origWidth = options.outWidth; //исходная ширина
        int origHeight = options.outHeight; //исходная высота
        int bytesPerPixel = 2; //соответствует RGB_555 конфигурации
        int maxSize = 320 * 640 * bytesPerPixel; //Максимально разрешенный размер Bitmap
        int desiredSize = w * h * bytesPerPixel; //Максимально разрешенный размер Bitmap для заданных width х height
        if (desiredSize < maxSize) maxSize = desiredSize;
        int scale = 1; //кратность уменьшения
        int origSize = origWidth * origHeight * bytesPerPixel;
//высчитываем кратность уменьшения
        if (origWidth > origHeight) {
            scale = Math.round((float) origHeight / (float) h);
        } else {
            scale = Math.round((float) origWidth / (float) w);
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        in = getApplicationContext().getContentResolver().openInputStream(selectedImageUri);//Ваш InputStream. Важно - открыть его нужно еще раз, т.к второй раз читать из одного и того же InputStream не разрешается (Проверено на ByteArrayInputStream и FileInputStream).
        Bitmap bitmap = BitmapFactory.decodeStream(in, null, options); //Полученный Bitmap
        Log.e(LOG, "h nad w " + bitmap.getHeight() + " " + bitmap.getWidth() + " " + bitmap.getConfig().toString());
        return bitmap;
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @SuppressLint("ParcelCreator")
    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            TextView tv = (TextView) findViewById(R.id.profile_appbar_city);
            if (resultCode == FetchAddressIntentService.SUCCESS_RESULT) {
                final String streetAddress = resultData.getString("RESULT OK");
                tv.setText(streetAddress);
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showLocationOnMapIntent(streetAddress);
                    }
                });
            }

        }

        private void showLocationOnMapIntent(String streetAddress) {
            final Uri gmmIntentUri = Uri.parse(
                    "geo:"
                            + App.getInstance(getApplicationContext()).getComponent().uController().get().getLatitude()
                            + ","
                            + App.getInstance(getApplicationContext()).getComponent().uController().get().getLongitude()
                            + "?q="
                            + streetAddress);

            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        }
    }


}
