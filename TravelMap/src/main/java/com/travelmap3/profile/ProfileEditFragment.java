package com.travelmap3.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.model.User;

import retrofit.RetrofitError;

/**
 * Created by Gair on 02.09.15.
 */
public class ProfileEditFragment extends Fragment {

    private static final String LOG = ProfileEditFragment.class.getSimpleName();

    private EditText firstName;
    private EditText lastName;
    private EditText age;
    private EditText ownCity;
    private EditText status;
    private EditText about;
    private Spinner gender;
    private EditText languages;
    private EditText jobs;
    private EditText study;
    private EditText phone;
    private EditText skype;
    private EditText vkLink;
    private EditText fbLink;
    private EditText telegramm;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_edit, container, false);
        setHasOptionsMenu(true);
        initializeViews(v);

        App application = (App) getActivity().getApplication();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen ProfileEdit");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        return v;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.profile_save_icon).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.profile_save_icon) {
            App.getInstance(getActivity()).getComponent().uController().setUser(getInputUser(), new CallBackResponse() {
                @Override
                public void onSuccess(Object obj) {
                    Toast.makeText(getActivity(), "Успешно сохранено", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(Object obj) {
                    Toast.makeText(getActivity(), "Ошибка сохранения. Данные сохранены только локально " + ((RetrofitError) obj).getMessage() + " status" + ((RetrofitError) obj).getResponse().getStatus(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews(View v) {
        User user = App.getInstance(getActivity()).getComponent().uController().get();
        firstName = (EditText) v.findViewById(R.id.fragment_profile_edit_firstname);
        firstName.setText(user.getFirstName());

        lastName = (EditText) v.findViewById(R.id.fragment_profile_edit_lastname);
        lastName.setText(user.getLastName());

        age = (EditText) v.findViewById(R.id.fragment_profile_edit_age);
        age.setText(String.valueOf(user.getAge()));

        ownCity = (EditText) v.findViewById(R.id.fragment_profile_edit_ownCity);
        ownCity.setText(user.getFrom_city());

        status = (EditText) v.findViewById(R.id.fragment_profile_edit_status);
        status.setText(user.getStatusText());

        about = (EditText) v.findViewById(R.id.fragment_profile_edit_about);
        about.setText(user.getAbout());

        gender = (Spinner) v.findViewById(R.id.fragment_profile_edit_gender);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.gender_choice, android.R.layout.simple_spinner_item);
        gender.setAdapter(adapter);
        gender.setSelection(1, true);

        languages = (EditText) v.findViewById(R.id.fragment_profile_edit_languages);
        languages.setText(user.getLang());

        jobs = (EditText) v.findViewById(R.id.fragment_profile_edit_jobs);
        jobs.setText(user.getJobs());

        study = (EditText) v.findViewById(R.id.fragment_profile_edit_study);
        study.setText(user.getStudy());

        phone = (EditText) v.findViewById(R.id.fragment_profile_edit_phone);
        phone.setText(user.getPhone());

        skype = (EditText) v.findViewById(R.id.fragment_profile_edit_skype);
        skype.setText(user.getSkype());

        vkLink = (EditText) v.findViewById(R.id.fragment_profile_edit_vkLink);
        vkLink.setText(user.getVkId());

        fbLink = (EditText) v.findViewById(R.id.fragment_profile_edit_fbLink);
        fbLink.setText(user.getFbId());

        telegramm = (EditText) v.findViewById(R.id.fragment_profile_edit_telegramm);
        telegramm.setText(user.getTelegram());
    }

    // Получаем введенные данные в текущего юзера.

    private User getInputUser() {
        User user = App.getInstance(getActivity()).getComponent().uController().get();
        user.setFirstName(firstName.getText().toString());
        user.setLastName(lastName.getText().toString());
        user.setGender(gender.getSelectedItemPosition());
        user.setLang(languages.getText().toString());
        user.setJobs(jobs.getText().toString());
        user.setStudy(study.getText().toString());
        user.setPhone(phone.getText().toString());
        user.setSkype(skype.getText().toString());
        user.setFbId(fbLink.getText().toString());
        user.setVkId(vkLink.getText().toString());
        user.setStatusText(status.getText().toString());
        user.setAge(Integer.valueOf(age.getText().toString()));
        user.setFromCity(ownCity.getText().toString());
        user.setAbout(about.getText().toString());
        user.setTelegram(telegramm.getText().toString());
        return user;
    }
}
