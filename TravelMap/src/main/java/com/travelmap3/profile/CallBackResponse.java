package com.travelmap3.profile;

/**
 * Created by Andre on 17.12.2015.
 */
public interface CallBackResponse {

  void onSuccess(Object obj);

  void onError(Object obj);
}
