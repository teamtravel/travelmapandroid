/**
 *
 */
package com.travelmap3.notes;

import com.google.gson.annotations.SerializedName;
import com.travelmap3.model.Gps;

import java.util.List;

/**
 * @author An
 */
public class Notes {
    public static final String MODIFICATION = "MODIFICATION";
    public static final String DELETE = "DELETE";
    public static final String CREATE = "CREATE";
    public static final String DEFAULT = CREATE;
    @SerializedName("images")
    private List<ImageLocations> imageLocationsList;
    private int id;
    private String textNote;
    private String date_update;
    private String textTitleNote;
    private long timeCreate; // дата написания записки
    private long timeLastUpdate;  // дата последнего обновления записи.
    private Gps gps;// координаты, где был записан этот
    private boolean vk; // Если отпраленно в вк, то true по default false
    private boolean tw;
    private boolean fb;
    private int _id; // for database
    private boolean server; // Если отпраленно на server успешно, то true по default false
    private boolean isExpiredToken;
    /*
     * Access - уровень доступа к записям пользователя.
     * [0]-Ни кто кроме пользователя, не видет запись.
     * [1]-Запись досупна для чтения всем.
     * [2]-для друзей(предполагается со второй версией!)
     */
    private short access;

    private String command; // Модификатор доступа может принимать любое из констант.  TODO ( перенести в Enum)


    public Notes() {
        this.textNote = "";
        this.textTitleNote = "";
        timeCreate = 0;
        timeLastUpdate = 0;
        access = 0;
        vk = false;
        tw = false;
        fb = false;
        command = DEFAULT;
    }

    public String getTextNote() {
        return textNote;
    }

    public void setTextNote(String textNote) {
        this.textNote = textNote;
    }

    public long getTimeCreate() {
        return timeCreate;
    }

    public void setTimeCreate(long timeCreate) {
        this.timeCreate = timeCreate;
    }

    public short getAccess() {
        return access;
    }

    public void setAccess(short access) {
        this.access = access;
    }

    public Gps getGps() {
        return gps;
    }

    public void setGps(Gps gps) {
        this.gps = gps;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public long getTimeLastUpdate() {
        return timeLastUpdate;
    }

    public void setTimeLastUpdate(long timeLastUpdate) {
        this.timeLastUpdate = timeLastUpdate;
    }

    public String getTextTitleNote() {
        if (textTitleNote == null) return "";
        else
            return textTitleNote;
    }

    public void setTextTitleNote(String textTitleNote) {
        this.textTitleNote = textTitleNote;
    }

    public int getUid() {
        return id;
    }

    public void setUid(int uid) {
        this.id = uid;
    }

    public boolean isVk() {
        return vk;
    }

    public int getVk() {
        return vk ? 1 : 0;
    }

    public void setVk(boolean vk) {
        this.vk = vk;
    }

    public boolean isServer() {
        return server;
    }

    public int getServer() {
        return server ? 1 : 0;
    }

    public void setServer(boolean server) {
        this.server = server;
    }

    //TODO add output of images
    public String getStrings() {
        return timeCreate + " " + textTitleNote + " " + textTitleNote + " " + vk + " s:" + server + " i:" + " " + timeLastUpdate + " uid:" + getUid() + " c:" + command + " a:" + access;
    }


    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }

    public boolean isExpiredToken() {
        return isExpiredToken;
    }

    public void setExpiredToken(boolean expiredToken) {
        isExpiredToken = expiredToken;
    }

    public List<ImageLocations> getImageLocationsList() {
        return imageLocationsList;
    }

    public void setImageLocationsList(List<ImageLocations> imageLocationsList) {
        this.imageLocationsList = imageLocationsList;
    }

    public void setId(int id) {
        this._id = id;
    }

    public int getId() {
        return _id;
    }

    /**
     * Этот клас нужен для того чтобы инкапсулировать в себе несколько различных путей к картинке,
     * которая отображается в заметке.
     * urlInCloud - содержит url, по которому в облаке хранится картинка
     * filePath - путь к картинке в файловой системе
     */
    public static class ImageLocations {
        @SerializedName("url_server")
        private String urlInCloud;
        @SerializedName("url_path")
        private String filePath;

        public ImageLocations(String urlInCloud, String filePath) {
            this.urlInCloud = urlInCloud;
            this.filePath = filePath;
        }

        public String getUrlInCloud() {
            return urlInCloud;
        }

        public String getFilePath() {
            return filePath;
        }

        @Override
        public boolean equals(Object v) {
            boolean res = false;

            if (v instanceof ImageLocations) {
                ImageLocations imgLocations = (ImageLocations) v;
                if (imgLocations.getFilePath().equals(filePath) && imgLocations.getUrlInCloud().equals(urlInCloud)) {
                    res = true;
                }
            }

            return res;
        }

        @Override
        public int hashCode() {
            int prime = 17;
            int result = 1;
            int hash1 = filePath == null ? 0 : filePath.hashCode();
            int hash2 = urlInCloud == null ? 0 : urlInCloud.hashCode();
            result = prime * result + hash1;
            result = prime * result + hash2;
            return result;
        }
    }
}
