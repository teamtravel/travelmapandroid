package com.travelmap3.notes;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.model.Gps;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.TextFormat;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Класс для показа всех записей пользователя на карте.
 * Возможность: Читать.
 * При выборе на определенный маркет открывается текстовое окно.
 * Created by An on 20.03.2015.
 */
public class MapGoogleNoteActivity extends Activity implements OnMapReadyCallback {
    public static final int LENGTH_CUT_TEXT_MIN = 10;
    private final static String LOG = "MapGoogleNoteActivity";
    private static final float ZOOM_CAMERA = 4;
    private Marker mMarker;
    private ArrayList<Notes> listNotes;
    private GoogleMap map;
    private MODE mode; // Режим для перемещения маркера по карте

    private NotesController notesController;

    public InternetConnect internetConnect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_map);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        toggleButton.setVisibility(View.GONE);
        mapFragment.getMapAsync(this);


        App application = (App) getApplication();
        notesController = application.getComponent().notesController();
        internetConnect = application.getComponent().getInternetConnect();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen MapGoogleNote");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        this.map = map;
        setMode(MODE.NORM);
        try {
            listNotes = notesController.takeAll();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(listNotes.get(listNotes.size() - 1).getGps().getLatitude(),
                            listNotes.get(listNotes.size() - 1).getGps().getLongitude()), ZOOM_CAMERA));
            initAddMarkers(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                mMarker = marker;
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapGoogleNoteActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_note_box, null);
                alertDialog.setView(view);
                alertDialog.setTitle(marker.getTitle());
                final EditText textNote = (EditText) view.findViewById(R.id.editTextBox);
                final EditText tvTitle = (EditText) view.findViewById(R.id.editTitle);
                TextView tvDateNote = (TextView) view.findViewById(R.id.tvDateNote);
                final Notes note = searchNoteByMarker(marker);
                textNote.setText(note.getTextNote());
                tvTitle.setText(note.getTextTitleNote());
                tvDateNote.setText("Обновленно: " + TextFormat.fullDate(note.getTimeLastUpdate()));
                alertDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                noteManager(textNote, note, tvTitle, Notes.MODIFICATION);
                                dialog.cancel();
                            }
                        }
                );
                alertDialog.setNeutralButton("Переместить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setMode(MODE.MOVE);
                        Snackbar.make(getWindow().getDecorView().findViewById(android.R.id.content),
                                "Выберите место на карте для заметки", Snackbar.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }

        });
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                try {
                    if (getMode() == MODE.MOVE) {
                        setMode(MODE.NORM);
                        noteManager(latLng, mMarker, Notes.MODIFICATION);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                addNoteMarker(latLng);
            }
        });

    }

    private void noteManager(LatLng latLng, Marker marker, String modification) {
        Notes note = searchNoteByMarker(marker);
        note.setGps(new Gps(latLng.latitude, latLng.longitude));
        noteManager(null, note, null, modification);
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        //mMarker.setAlpha((float)0.5);
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void noteManager(EditText textNote, Notes note, EditText tvTitle, String mode) {
        String text;
        String title;
        if (textNote == null) {
            text = note.getTextNote();
        } else {
            text = textNote.getText().toString();
        }
        if (tvTitle == null) {
            title = note.getTextTitleNote();
        } else {
            title = tvTitle.getText().toString();
        }
        if (text.length() != 0 && title.length() != 0) {
            try {
                saveNewNoteAndSendToServer(note, textNote, tvTitle, mode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(MapGoogleNoteActivity.this, "Пустые поля", Toast.LENGTH_SHORT).show();
        }
    }


    private void saveNewNoteAndSendToServer(Notes note, @Nullable EditText textNote,
                                            @Nullable EditText titleNote, String command) throws Exception {
        // TODO заменить и вынести из контроллера класс для составления данных и реализовать по паттерну build https://habrahabr.ru/post/244521/
        notesController.set(note);
        notesController.saveDataTime(note.getTimeCreate(), command);
        if (textNote != null) {
            notesController.get().setTextNote(textNote.getText().toString());
        }
        if (titleNote != null) {
            notesController.get().setTextTitleNote(titleNote.getText().toString());
        }
        notesController.get().setCommand(command);
        notesController.get().setUid(App.getInstance(this).getComponent().uController().get().getId());

        notesController.saveInfo();
        if (notesController.getCodeOperation() == 1) {// Сохранилось локально
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(note.getGps().getLatitude(), note.getGps().getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.notes_marker_48)));

            if (internetConnect.isActiveWiFiConnection()) {
                sendNote(notesController.get());
            }
            listNotes.add(notesController.get()); // обновляем наш список :)
            animateMarker(mMarker, new LatLng(note.getGps().getLatitude(), note.getGps().getLongitude()),
                    true);
        } else {
            Toast.makeText(this, "Ошибка сохранения", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendNote(Notes notes) {
        RetrofitService.getInstanceAPI()
                .postNote(Preferences.getInstance().loadToken(), Config.VERSION_API, notes,
                        new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {
                                Toast.makeText(MapGoogleNoteActivity.this, "Успешно отправленно на сервер",
                                        Toast.LENGTH_SHORT).show();
                                notesController.saveServerFlag(true);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Toast.makeText(MapGoogleNoteActivity.this, "На сервер не получилось сохранить",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
    }

    private Notes searchNoteByMarker(Marker marker) {
        if (marker == null || listNotes == null) {
            throw new IllegalArgumentException();
        }
        for (Notes note : listNotes) {
            if (marker.getPosition().latitude == note.getGps().getLatitude()
                    && marker.getPosition().longitude == note.getGps().getLongitude()) {
                return note;
            }
        }
        return new Notes();
    }

    void addNoteMarker(LatLng latLng) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapGoogleNoteActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_note_box, null);
        alertDialog.setView(view);
        alertDialog.setTitle("Новая запись");
        final EditText textNote = (EditText) view.findViewById(R.id.editTextBox);
        final EditText textTitle = (EditText) view.findViewById(R.id.editTitle);
        final TextView tvDateNote = (TextView) view.findViewById(R.id.tvDateNote);
        tvDateNote.setText(TextFormat.date_ddMMyy(System.currentTimeMillis()));
        final Notes note = new Notes();
        Gps gps = new Gps();
        gps.setLongitude(latLng.longitude);
        gps.setLatitude(latLng.latitude);
        note.setGps(gps);
        alertDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                noteManager(textNote, note, textTitle, Notes.CREATE);
                dialog.cancel();
            }
        }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    private void initAddMarkers(GoogleMap map) throws Exception {

        for (Notes note : listNotes) {
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(note.getGps().getLatitude(), note.getGps().getLongitude()))
                    .snippet(TextFormat.partialOutput(note.getTextTitleNote(), LENGTH_CUT_TEXT_MIN))
                    .title(TextFormat.date_ddMMyy(note.getTimeCreate()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_point))
            );
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public MODE getMode() {
        return mode;
    }

    public void setMode(MODE mode) {
        this.mode = mode;
    }

}
