package com.travelmap3.notes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.travelmap3.R;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.tips.TIPS;
import com.travelmap3.tips.TipsCreate;

import static com.travelmap3.menu.MainMenuActivity.FRAGMENT_ROUTER;

/**
 * Created by Gairbek on 18.01.2016.
 */
public class NoteActivity extends AppCompatActivity {

    public static final String NOTE_ID_EXTRA = "EXTRA_ID_KEY";

    private static final String TAG = "NoteActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        TipsCreate.getInstance().show(TIPS.NOTE_FRAGMENT, getFragmentManager(), this);

        setActionBar();

        if (savedInstanceState == null) {
            NoteFragment fragment;
            Bundle extras = getIntent().getExtras();
            boolean isNoteExists = extras != null && extras.containsKey(NOTE_ID_EXTRA);

            if (isNoteExists) {
                int noteId = (int) extras.get(NOTE_ID_EXTRA);
                fragment = NoteFragment.createInstance(noteId);
            } else {
                fragment = new NoteFragment();
            }

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.activity_note_fragment_container, fragment).commit();
        }
    }

    private void setActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_activity_note);
        View back = (View) findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOG", "onClick: base map ");
                startActivity(new Intent(getApplicationContext(), MainMenuActivity.class).putExtra(FRAGMENT_ROUTER, 1).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        if (null != bar) {
            bar.setDisplayShowTitleEnabled(false);

        }
    }

}
