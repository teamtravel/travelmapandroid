package com.travelmap3.notes;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelmap3.R;
import com.travelmap3.utils.TextFormat;

import java.util.ArrayList;

/**
 * Created by An on 12.03.2015.
 */
public class NoteAdapter extends BaseAdapter {

  public static final int LENGTH_SYMBOL_TITLE_LANDSCAPE = 33;
  public static final int LENGTH_SYMBOL_TEXT_LANDSCAPE = 50;
  public static final int LENGTH_SYMBOL_TITLE_PORTRAIT = 15;
  public static final int LENGTH_SYMBOL_TEXT_PORTRAIT = 28;
  private static LayoutInflater inflater = null;
  private ArrayList<Notes> data;
  private Activity activity;

  public NoteAdapter(Activity activity, ArrayList<Notes> dataList) {
    data = dataList;
    inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    this.activity = activity;
  }

  public void setData(ArrayList<Notes> data) {
    this.data = data;
  }

  @Override
  public int getCount() {
    return data.size();
  }

  @Override
  public Object getItem(int position) {
    return position;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View vi = convertView;
    if (convertView == null) {
      vi = inflater.inflate(R.layout.list_row_notes, null);
    }
    TextView title = (TextView) vi.findViewById(R.id.title);
    TextView text = (TextView) vi.findViewById(R.id.textNote);
    TextView time = (TextView) vi.findViewById(R.id.time);
    TextView month = (TextView) vi.findViewById(R.id.textMonthNote);
    TextView day = (TextView) vi.findViewById(R.id.number_day);
    ImageView vkIcon = (ImageView) vi.findViewById(R.id.imageViewIsVk);
    ImageView serverIcon = (ImageView) vi.findViewById(R.id.imageViewIsSrever);
    Notes note = data.get(position);
    TextFormat textFormat = new TextFormat();
    if (activity.getResources().getConfiguration().orientation
        == Configuration.ORIENTATION_LANDSCAPE) {
      title.setText(
          TextFormat.partialOutput(note.getTextTitleNote(), LENGTH_SYMBOL_TITLE_LANDSCAPE));
      text.setText(TextFormat.partialOutput(note.getTextNote(), LENGTH_SYMBOL_TEXT_LANDSCAPE));
    } else {
      title.setText(TextFormat.partialOutput(note.getTextTitleNote(),
          LENGTH_SYMBOL_TITLE_PORTRAIT));
      text.setText(TextFormat.partialOutput(note.getTextNote(), LENGTH_SYMBOL_TEXT_PORTRAIT));
    }

    time.setText(textFormat.dateTime(note.getTimeCreate()));
    month.setText(textFormat.date_MMM(note.getTimeCreate()));
    day.setText(textFormat.date_d(note.getTimeCreate()));
    if (note.isVk()) {
      vkIcon.setVisibility(View.VISIBLE);
    } else {
      serverIcon.setVisibility(View.GONE);
    }
    if (note.isServer()) {
      serverIcon.setImageDrawable(
              activity.getResources().getDrawable(R.drawable.cloudcheck));
      serverIcon.setVisibility(View.VISIBLE);
    } else {
      serverIcon.setImageDrawable(
              activity.getResources().getDrawable(R.drawable.ic_cloud_off_black_36dp));
      serverIcon.setVisibility(View.VISIBLE);
    }
    return vi;//TODO сделать recView
  }

  public Notes getData(int count) {
    return this.data.get(count);
  }
}
