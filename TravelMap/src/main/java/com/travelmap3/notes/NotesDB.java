package com.travelmap3.notes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.travelmap3.db.IDataBase;
import com.travelmap3.db.SqliteDBHelper;
import com.travelmap3.model.Gps;
import com.travelmap3.notes.Notes.ImageLocations;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class NotesDB extends SqliteDBHelper implements IDataBase {

    private static final String LOG = "NotesDB";
    private ContentValues cv;
    private SQLiteDatabase db;

    public NotesDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public boolean save(Notes notes) {
        cv = new ContentValues();
        long code = 0;
        Log.i("LOG travel", notes.getStrings());
        cv.put(DB_COLUMN_TEXT, notes.getTextNote());
        cv.put(DB_COLUMN_TIME_CREATE, notes.getTimeCreate() == 0 ? notes.getTimeLastUpdate() : notes.getTimeCreate());
        cv.put(DB_COLUMN_TITLE, notes.getTextTitleNote());
        cv.put(DB_COLUMN_LAT, notes.getGps().getLatitude());
        cv.put(DB_COLUMN_LONG, notes.getGps().getLongitude());
        cv.put(DB_COLUMN_ACCESS, notes.getAccess());
        cv.put(DB_COLUMN_FLAG_SERVER_SAFE, notes.isServer() ? 1 : 0);
        cv.put(DB_COLIMN_FLAG_VK_SAFE, notes.isVk() ? 1 : 0);
        cv.put(DB_COLUMN_DATA_UPDATE, notes.getTimeLastUpdate());
        switch (notes.getCommand()) {
            case Notes.CREATE:
                code = insertDB(DB_TABLE_NOTES, cv);
                break;
            case Notes.MODIFICATION:
                code = updateDB(cv);
                break;
            case Notes.DELETE:
                code = deleteDB(notes);//Неиспользуется пока
                break;
            default:
                break;
        }
        return code > 0;
    }

    public void saveVkFlag(boolean b, long timeCreate) {
        cv = new ContentValues();
        cv.put(DB_COLIMN_FLAG_VK_SAFE, b ? 1 : 0);
        cv.put(DB_COLUMN_TIME_CREATE, timeCreate);
        updateDB(cv);
    }

    public void saveServerFlag(boolean b, long timeCreate) {
        cv = new ContentValues();
        cv.put(DB_COLUMN_FLAG_SERVER_SAFE, b ? 1 : 0);
        cv.put(DB_COLUMN_TIME_CREATE, timeCreate);
        updateDB(cv);
    }

    private long deleteDB(Notes notes) {
        db = this.getWritableDatabase();
        int kol = 0;
        db.beginTransaction();
        try {
            kol = db.delete(DB_TABLE_NOTES,
                    DB_COLUMN_TIME_CREATE + "=" + notes.getTimeCreate(),
                    null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
        return kol;
    }

    private long updateDB(ContentValues contentValues) {
        db = this.getWritableDatabase();
        db.beginTransaction();
        int kol = 0;
        Log.i(LOG, "UPDATE time_create=" + cv.get(DB_COLUMN_TIME_CREATE));
        try {
            kol = db.update(DB_TABLE_NOTES, contentValues, DB_COLUMN_TIME_CREATE + "=" + cv.get(DB_COLUMN_TIME_CREATE), null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
        Log.i(LOG, "update Notes in" + kol);
        return kol;
    }


    public ArrayList<Notes> getAllRecord() {
        ArrayList<Notes> list = new ArrayList<Notes>();
        Cursor c = null;
        try {
            c = getReadableDatabase().query(DB_TABLE_NOTES, null, null,
                    null, null, null, null);
            list.addAll(getArrayNotes(c));
            Log.d("LOG GET All Record", "size:=" + list.size() + " ");
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            if (c != null) c.close();
        }
        return list;
    }

    public Notes getNoteById(int id) {
        Cursor c = getReadableDatabase().query(DB_TABLE_NOTES, null, "_id =" + id, null, null, null, null);

        Notes notes = new Notes();

        int columnIdIndex = c.getColumnIndex("_id");
        int lon = c.getColumnIndex(DB_COLUMN_LONG);
        int access = c.getColumnIndex(DB_COLUMN_ACCESS);
        int lat = c.getColumnIndex(DB_COLUMN_LAT);
        int createTime = c.getColumnIndex(DB_COLUMN_TIME_CREATE);
        int update = c.getColumnIndex(DB_COLUMN_DATA_UPDATE);
        int text = c.getColumnIndex(DB_COLUMN_TEXT);
        int title = c.getColumnIndex(DB_COLUMN_TITLE);
        int vk = c.getColumnIndex(DB_COLIMN_FLAG_VK_SAFE);
        int server = c.getColumnIndex(DB_COLUMN_FLAG_SERVER_SAFE);


        if (c.moveToFirst()) {
            notes = new Notes();
            notes.setId(c.getInt(columnIdIndex));
            notes.setTimeCreate(c.getLong(createTime));
            notes.setAccess(c.getShort(access));
            Gps g = new Gps();
            g.setLatitude(c.getString(lat));
            g.setLongitude(c.getString(lon));
            notes.setGps(g);
            notes.setTextNote(c.getString(text));
            notes.setTextTitleNote(c.getString(title));
            notes.setTimeLastUpdate(c.getLong(update));
            notes.setServer(c.getInt(server) == 1);
            notes.setVk(c.getInt(vk) == 1);
            setImagesUrlsAndPaths(notes);
            Log.i("LOG travel get", notes.getStrings());
        }

        Log.d(LOG, "FIND THE NOTE WITH TITLE " + notes.getTextTitleNote());
        c.close();
        return notes;
    }

    private void setImagesUrlsAndPaths(Notes note) {
        if (db == null) {
            db = getReadableDatabase();
        }

        List<ImageLocations> locationsList = new ArrayList<>();

        Cursor c = db.query(DB_TABLE_NOTES_IMAGE, new String[]{DB_COLUMN_IMAGURL, DB_COLUMN_IMAGPATH}, DB_COLUMN_NOTE_ID + " = ?", new String[]{String.valueOf(note.getId())}, null, null, null);
        if (c != null) {
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                ImageLocations locations = new ImageLocations(c.getString(c.getColumnIndex(DB_COLUMN_IMAGURL)),
                        c.getString(c.getColumnIndex(DB_COLUMN_IMAGPATH)));
                locationsList.add(locations);
            }
            c.close();
        }

        note.setImageLocationsList(locationsList);
    }

    private ArrayList<Notes> getArrayNotes(Cursor c) {
        ArrayList<Notes> list = new ArrayList<Notes>();
        if (c.moveToFirst())
            do {
                int id = c.getColumnIndex("_id");
                id = c.getInt(id);
                Log.e("LOG Notes", "note_id=" + id);
                list.add(getNoteById(id));
            }
            while (c.moveToNext());
        return list;
    }


    public void close() {
        Log.i(LOG, "getWritableDatabase");
        getWritableDatabase().close();
    }

    public long delete(long time) {
        db = getWritableDatabase();
        db.beginTransaction();
        int kol = 0;
        try {
            kol = db.delete(DB_TABLE_NOTES, DB_COLUMN_TIME_CREATE + "=" + time, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
        return kol;
    }

    public void sinchronized() {
            /*
             Алгоритм синхронизации.
             Если две записи с одним временем то ничего не делаем.
             Если запись есть на сервере но нет здесь, то добавляем.
             Если запись есть у нас, но нет на сервере то считаем что кидаем на сервер.
             */
    }

    public boolean isNotSync() {
        db = getWritableDatabase();
        Cursor cursor = db.rawQuery("select count(*) from " + DB_TABLE_NOTES + " where  " + DB_COLUMN_FLAG_SERVER_SAFE + "=0", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0 && cursor.getColumnCount() > 0) {
            if (cursor.getInt(0) > 0) {
                return true;
            }
        }
        cursor.close();
        db.close();
        return false;
    }

    public ArrayList<Notes> getDifferedRecord() {
        db = getReadableDatabase();
        ArrayList<Notes> list = new ArrayList<>();
        String[] whereClause = {"0"};
        Cursor cursor = db.query(DB_TABLE_NOTES, null, DB_COLUMN_FLAG_SERVER_SAFE + "=?", whereClause, null, null, null, null);
        if (cursor.moveToFirst()) {
            list = getArrayNotes(cursor);
        }
        cursor.close();
        return list;
    }

    public void clear() {
        clearAllT(DB_TABLE_NOTES);
        clearAllT(DB_TABLE_NOTES_IMAGE);
    }

    public void insertData(ArrayList<Notes> notes) throws ParseException {
        clearAllT(DB_TABLE_NOTES);
        for (Notes note : notes) {
            note.setCommand(Notes.CREATE);
            note.setServer(true);
            save(note);
        }
    }

    //Добавляем изображения
    public void insertNoteImage(int noteId, List<ImageLocations> newImages) {
        db = getWritableDatabase();
        for (int i = 0; i < newImages.size(); i++) {
            String imageUrl = newImages.get(i).getUrlInCloud();
            String imagePath = newImages.get(i).getFilePath();
            ContentValues contentValues = new ContentValues();
            contentValues.put(DB_COLUMN_NOTE_ID, noteId);
            contentValues.put(DB_COLUMN_IMAGURL, imageUrl);
            contentValues.put(DB_COLUMN_IMAGPATH, imagePath);
            db.insert(DB_TABLE_NOTES_IMAGE, null, contentValues);
        }
    }

    //Удаляем изображение
    public void deleteNoteImages(int noteID, List<ImageLocations> deleteImages) {
        for (int i = 0; i < deleteImages.size(); i++) {
            db.delete(DB_TABLE_NOTES_IMAGE, DB_COLUMN_NOTE_ID + " = ? AND " + DB_COLUMN_IMAGURL + " = ?", new String[]{String.valueOf(noteID), deleteImages.get(i).getUrlInCloud()});
        }
    }

    //Получем изображение которые относятся к записи
    public String[] getNoteImages(int noteId) {
        if (db == null) {
            db = getReadableDatabase();
        }
        ArrayList<String> images = new ArrayList<>();
        Cursor c = db.query(DB_TABLE_NOTES_IMAGE, new String[]{DB_COLUMN_IMAGURL}, DB_COLUMN_NOTE_ID + " = ?", new String[]{String.valueOf(noteId)}, null, null, null);
        if (c != null) {
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                images.add(c.getString(c.getColumnIndex(DB_COLUMN_IMAGURL)));
            }
            c.close();
        }
        return images.toArray(new String[images.size()]);
    }


    public int getNoteId(Notes notes) {
        db = getWritableDatabase();
        Cursor cursor = db.query(DB_TABLE_NOTES, new String[]{"_id"}, DB_COLUMN_TIME_CREATE + " = ?", new String[]{String.valueOf(notes.getTimeCreate())}, null, null, null);
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex("_id"));
            cursor.close();
            return id;
        }
        return -1;
    }

    public int[] getNotesId() {
        db = getWritableDatabase();
        int[] ids = null;
        String sql = "SELECT _id FROM " + DB_TABLE_NOTES;
        Cursor c = db.rawQuery(sql, null);
        if (c != null && c.getCount() > 0) {
            ids = new int[c.getCount()];
            if (c.moveToFirst()) {
                int i = 0;
                while (!c.isAfterLast()) {
                    ids[i] = (int) c.getLong(c.getColumnIndex("_id"));
                    c.moveToNext();
                    i++;
                }
            }
            c.close();
        }
        return ids;
    }
}
