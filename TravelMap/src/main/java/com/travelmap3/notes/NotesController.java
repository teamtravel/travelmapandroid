package com.travelmap3.notes;

import android.content.Context;
import android.util.Log;

import com.travelmap3.App;
import com.travelmap3.model.Gps;
import com.travelmap3.notes.Notes.ImageLocations;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * Класс для реализации дополнительно слоя. В котором проверяется корректность данных, введенных от пользователя.
 * TODO: несделана синхронизация с сервером/ Сделать после появления сайта. Использовать паттерн строитель!
 */
public class NotesController {
    private static final String LOG = "NotesController";

    public Notes notes;

    public NotesDB db;

    private int codeOperation = 0;

    @Inject
    public NotesController(Notes notes, NotesDB notesDB) {
        this.notes = notes;
        this.db = notesDB;

    }

    private void saveText(String text) throws Exception {
        if (text == null) throw new Exception("Text null");
        if (text.isEmpty()) throw new Exception("Length less one");
        notes.setTextNote(firstUpperCase(text));
        Log.w(LOG, "text=" + text);
    }

    private void saveTitle(String text) throws Exception {
        if (text == null) throw new Exception("Text title null");
        notes.setTextTitleNote(firstUpperCase(text));
        Log.w(LOG, "text=" + text);
    }

    private String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";//или return word;
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    private void saveGps(Gps g) throws Exception {
        if (g == null) throw new Exception("GPS null");
        notes.setGps(g);
        Log.w(LOG, "getLat=" + g.getLatitude());
    }

    private void saveAccess(short s) throws Exception {
        if (s > 5 || s < 0) throw new Exception("Access > 5 || <0");
        notes.setAccess(s);
        Log.w(LOG, "access=" + s);
    }

    public void saveDataTime(long timeCreate, String command) throws Exception {

        long timeDate = System.currentTimeMillis();

        if (command.compareTo(Notes.CREATE) == 0) notes.setTimeCreate(timeDate);
        else {
            if (timeCreate == 0) throw new Exception("Date null");
            notes.setTimeCreate(timeCreate);//Вызывается только тогда,когда идет MODIFICATION записи.
        }
        notes.setTimeLastUpdate(timeDate);
        Log.w(LOG, "date=" + timeCreate);
    }

    /**
     * @param text
     * @param title
     * @param gps
     * @param access
     * @param date
     * @param command(const Note MODIFICATION,DELETE,CREATE)
     * @throws Exception
     */
    public void make(String text, String title, Gps gps, short access, long date, String command, Context context) throws Exception {
        saveText(text);
        saveGps(gps);
        saveAccess(access);
        saveDataTime(date, command);
        saveTitle(title);
        notes.setUid(App.getInstance(context).getComponent().uController().get().getId());
        notes.setCommand(command);
        notes.setServer(false);
        notes.setVk(false);
    }

    /*
    Мы сначала собираем MAKE, потом можем сохранить в БД например. Или получить- get();
     */

    public Notes get() {
        return notes;
    }

    public void set(Notes notes) {
        this.notes = notes;
    }

    /**
     * Класс для сохранения в базе данных.
     */

    public void saveInfo() {
        if (notes != null) {
            codeOperation = db.save(notes) ? 1 : 0;
        }
        Log.e("LOG save", "code+" + codeOperation);
    }

//    public void saveInfo(Notes notes) {
//        this.notes = notes;
//        saveInfo();
//    }

    public ArrayList<Notes> takeAll() throws NullPointerException {
        ArrayList<Notes> listNote = db.getAllRecord();
        if (listNote == null) throw new NullPointerException("Null note records");
        return listNote;
    }

    public Notes getNoteById(int id) {
        return db.getNoteById(id);
    }

    //for test
    public int[] getNotesId() {
        return db.getNotesId();
    }
    //

    public boolean removeInfo(long time) {
        if (time > 0) {
            try {
                return db.delete(time) > 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public void saveVkFlag(boolean b) {
        db.saveVkFlag(b, notes.getTimeCreate());
    }

    public void saveServerFlag(boolean b) {
        db.saveServerFlag(b, notes.getTimeCreate());
    }

    /**
     * @param notes data from server
     */
    public void sinchronozed(Notes notes) {
        db.sinchronized();
    }

    public int getCodeOperation() {
        return codeOperation;
    }

    public void setCodeOperation(int codeOperation) {
        this.codeOperation = codeOperation;
    }

    public void setImages(List<ImageLocations> imageLocationsList) {
        notes.setImageLocationsList(imageLocationsList);
    }

    public void deleteNoteImagesFromDB(List<ImageLocations> deleteImages) {
        db.deleteNoteImages(db.getNoteId(notes), deleteImages);
    }

    public void saveNewNoteImagesToDB(List<ImageLocations> newImages) {
        db.insertNoteImage(db.getNoteId(notes), newImages);
    }

    public void clearDB() {
        db.clear();
    }
}
