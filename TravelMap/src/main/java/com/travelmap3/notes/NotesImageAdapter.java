package com.travelmap3.notes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.travelmap3.R;
import com.travelmap3.notes.Notes.ImageLocations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tema_ on 20.06.2016.
 */
public class NotesImageAdapter extends RecyclerView.Adapter<NotesImageAdapter.ItemHolder> {
    public interface OnImageClickListener{
        void deleteImage(int position);
    }


    private List<ImageLocations> data;
    private Context context;
    private OnImageClickListener listener;

    public NotesImageAdapter(List<ImageLocations> data,Context context ,OnImageClickListener listener){
        this.data = data;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_image_note,parent,false);
        return new ItemHolder(v);
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, int position) {
        Glide.with(context).load(data.get(position).getUrlInCloud()).into(holder.imageView);

        holder.icDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.deleteImage(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView imageView, icDelete;
        FrameLayout frameLayout;
        public ItemHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.note_image_item);
            icDelete = (ImageView)itemView.findViewById(R.id.note_image_item_delete);
            frameLayout = (FrameLayout)itemView.findViewById(R.id.note_image_item_layout);
        }
    }
}
