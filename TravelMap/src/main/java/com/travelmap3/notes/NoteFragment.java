package com.travelmap3.notes;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.MainActivity;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.api.UploadManager;
import com.travelmap3.event.BusEvent;
import com.travelmap3.event.BusEventNoteImage;
import com.travelmap3.menu.MySettingsActivity;
import com.travelmap3.model.Gps;
import com.travelmap3.model.PeopleModel;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.vk.VkApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.travelmap3.R.id.action_btn_copy_text;
import static com.travelmap3.R.id.action_btn_save;
import static com.travelmap3.notes.Notes.ImageLocations;

/**
 * Created by An on 12.03.2015.
 */
public class NoteFragment extends android.support.v4.app.Fragment implements NotesImageAdapter.OnImageClickListener {

    private static final String LOG = "NoteFragment";
    private static final String NOTE_ID_EXTRA = "NOTE_ID_EXTRA";
    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int PHOTO_REQUEST_CODE = 2;

    private EditText textNote;
    private EditText titleNote;

    private CheckBox access;
    private CheckBox socialNet;
    private String text;
    private String title;
    private long timeCreate;
    private Notes note;
    private String command = Notes.CREATE;
    private NotesController notesController;

    //
    private RecyclerView noteImagesList;
    private NotesImageAdapter adapter;
    private ImageButton addImageButton;
    private CircleProgressBar progressBar;

    private List<ImageLocations> imageLocationsList;
    private List<ImageLocations> imageLocationsListNew;
    private List<ImageLocations> imageLocationsListDelete;

    public InternetConnect internetConnect;

    /**
     * Использовать только при редактировании уже существующей заметки
     * (При создании новой заметки используй конструктор по умолчанию)
     *
     * @param noteId - id
     * @return NoteFragment
     */
    public static NoteFragment createInstance(int noteId) {
        NoteFragment fragment = new NoteFragment();
        Bundle args = new Bundle();
        args.putInt(NOTE_ID_EXTRA, noteId);
        fragment.setArguments(args);
        return fragment;
    }

    public void setNote(Notes notes) {
        this.note = notes;
        timeCreate = note.getTimeCreate();
        text = note.getTextNote();
        title = note.getTextTitleNote();

        List<ImageLocations> imageLocations = notes.getImageLocationsList();
        imageLocationsList = new ArrayList<>(imageLocations);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        internetConnect = App.getInstance(getActivity()).getComponent().getInternetConnect();
        notesController = App.getInstance(getActivity()).getComponent().notesController();
        boolean isReceivedID = getArguments() != null && getArguments().containsKey(NOTE_ID_EXTRA);
        if (isReceivedID) {
            int noteId = (int) getArguments().get(NOTE_ID_EXTRA);
            setNote(notesController.getNoteById(noteId));
        }

        App application = (App) getActivity().getApplication();
//        mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen NoteFragment");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note, container, false);
        initViews(rootView);
        setOnGUI();
        return rootView;
    }

    public void copyTextNote() {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager)
                getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        String copiedText = titleNote.getText().toString() + "  \n" + textNote.getText().toString();
        android.content.ClipData clip = android.content.ClipData.newPlainText("TAG", copiedText);
        clipboard.setPrimaryClip(clip);
        toast(getActivity().getResources().getString(R.string.copy_text_done));
    }

    private void initViews(final View rootView) {
        titleNote = (EditText) rootView.findViewById(R.id.editTextTitle);
        textNote = (EditText) rootView.findViewById(R.id.editTextNote);
        access = (CheckBox) rootView.findViewById(R.id.checkBoxPublic);
        socialNet = (CheckBox) rootView.findViewById(R.id.chechBoxVk);
        noteImagesList = (RecyclerView) rootView.findViewById(R.id.fragment_note_note_images);
        addImageButton = (ImageButton) rootView.findViewById(R.id.fragment_note_add_image);
        progressBar = (CircleProgressBar) rootView.findViewById(R.id.fragment_note_progress);


        if (imageLocationsList == null) {
            imageLocationsList = new ArrayList<>();
        }
        if (imageLocationsList.size() > 0) {
            noteImagesList.setVisibility(View.VISIBLE);
        }
        adapter = new NotesImageAdapter(imageLocationsList, getContext(), this);


        socialNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSocialNet();
            }
        });
        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogGalleryOrPhoto();
                Log.e("LOGGER", "dialog show click");
            }
        });
        noteImagesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        noteImagesList.setAdapter(adapter);


    }


    private void dialogGalleryOrPhoto() {
        final String[] item = {"Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select image:");
        builder.setItems(item, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                galleryIntent();
            }
        });
        builder.show();
    }

    private void photoIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, PHOTO_REQUEST_CODE);
    }

    private void galleryIntent() {
        Log.e("LOGGER", "gallery");
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            gallery.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }
        startActivityForResult(gallery, GALLERY_REQUEST_CODE);
    }

    private boolean checkEmptyFields() {
        if (titleNote.getText().length() == 0) {
            toast("Нет заголовка..!");
            return false;
        }
        if (textNote.getText().length() == 0) {
            toast("Запись пуста");
            return false;
        }
        return true;
    }

    /**
     * Функция спрашивает, точно ли нужно отправлять запись в соц сеть!
     * В такую то группу или стену такогото пользователя.
     * Так же проверяет на пустоту. И предлагает сходить в настройки.
     */
    private void dialogSocialNet() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Boolean checkBoxMyWall = sp.getBoolean("publish_vk_wall", false);
        Boolean publishVkGroupWall = sp.getBoolean("publish_vk_group_wall", false);

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(getActivity().getResources().getString(R.string.titleNoteSendToVK));

        String noSendAndSettings = "Нельзя отправить, так как не извеcтно куда отправлять, необходимо перейти в настройки";
        String message = noSendAndSettings;
        if (checkBoxMyWall) message = "На мою стену";
        if (publishVkGroupWall) {

            //Используем модель PeopleModel так как она подходит под нужды. Firstname - Имя группы, RowId - id группы
            ArrayList<PeopleModel> list = Preferences.getInstance().loadArrayList(Preferences.VK_NAME);
            for (PeopleModel p : list) {
                String s = Preferences.getInstance().loadCheckBoxGroupChoice();
                if (p.getRowID().compareTo(Preferences.getInstance().loadCheckBoxGroupChoice()) == 0) {
                    message = "Группы: " + p.getFirstName();
                }
            }
        }
        alertDialog.setMessage(message);

        if (message.compareTo(noSendAndSettings) == 0) {
            alertDialog.setPositiveButton("Перейти в настройки", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().startActivity(new Intent(getActivity(), MySettingsActivity.class));
                }
            });
        } else
            alertDialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dialog.cancel();
                }
            });

        alertDialog.show();

    }

    //Метод создаёт пост на стене пользователя
    private void sendPostToVk() {
        String vktext = titleNote.getText().toString() + "\n" + textNote.getText().toString();
        VkApi.wallPost(vktext, getActivity(), new VkApi.IVkPoster() {
            @Override
            public void onSuccess(Object obj) {
                toast("Отправлено по адресу: " + obj);
                notesController.saveVkFlag(true);
//                if (!BuildConfig.DEBUG) {
//                    mTracker.send(new HitBuilders.EventBuilder()
//                            .setCategory("ui_action")
//                            .setAction("Экран Записки Дневника")
//                            .setLabel("Отправленно в вк")
//                            .build());
//                }
            }

            @Override
            public void onError(String error) {
                notesController.saveVkFlag(false);
            }
        }, imageLocationsList);
    }

    private void setOnGUI() {
        if (text != null) textNote.setText(text);
        if (title != null) titleNote.setText(title);
        if (note != null) {
            /*
            Если запись уже заполнена значит, мы включаем режим MODIFICATION
             */
            command = Notes.MODIFICATION;
            if (note.getAccess() == 0) access.setChecked(false);
            if (note.isVk()) socialNet.setChecked(true);
        }
    }

    private void toast(String message) {
        Toast t = Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT);
        t.setGravity(17, 3, 3);
        t.show();
    }


    private boolean saveAndSendNote() {
        short checkBoxAccess = 1;
        Log.e("LOG", "send note");
        if (!access.isChecked()) checkBoxAccess = 0;
        Gps gps = App.getInstance(getActivity()).getComponent().uController().get().getGps();
        try {

            notesController.make(textNote.getText().toString(),
                    titleNote.getText().toString(), gps, checkBoxAccess,
                    timeCreate, command, getActivity());
            notesController.saveInfo();
            //Сохраняем в модель изображения которые остались в imageUrls
            notesController.setImages(imageLocationsList);

            //Вносим изминения в базу
            changeInImagesToNote();
            if (internetConnect.isActiveWiFiConnection()) {
                sendNote(notesController.get());
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Метод внесение изминений связаный с изображениями
     * Удаление изображений
     * Добавление изображений
     */
    private void changeInImagesToNote() {
        if (imageLocationsListDelete != null && imageLocationsListDelete.size() > 0) {
            notesController.deleteNoteImagesFromDB(imageLocationsListDelete);
        }

        if (imageLocationsListNew != null && imageLocationsListNew.size() > 0) {
            notesController.saveNewNoteImagesToDB(imageLocationsListNew);
        }
    }

    private void sendNote(Notes notes) {
        Log.e("LOG", "send note retrofit");
        RetrofitService.getInstanceAPI().postNote(App.getInstance(getActivity()).getComponent().uController().get().getToken(), Config.VERSION_API, notes, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                // Сохраняем в бд что сохранилось на сервере
                saveFlagOnBD();
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("ui_action")
//                        .setAction("Экран Записки Дневника")
//                        .setLabel("Отправленно на сервер")
//                        .build());

                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                try {
                    toast("Ошибка" + error.getResponse().getStatus());
                    //Log.e(LOG, error.getResponse().getReason() + " " + error.getMessage());
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(action_btn_save).setVisible(true);
        menu.findItem(R.id.action_map_open).setVisible(false);
        menu.findItem(R.id.action_map_note).setVisible(false);
        menu.findItem(R.id.action_search_dialogs).setVisible(false);
        menu.findItem(R.id.action_btn_copy_text).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case action_btn_save: {
            /*
                 Проверка на ввод данных.
                 Если успешно сохранили то выводим об успешности операции.
                 Если успешно отправили то выводим что успешно отправилось.
                 */
                if (!checkEmptyFields()) return true;
                String answer = "Error";
                if (saveAndSendNote()) {
                    answer = "Запись успешно сохранилась в телефоне";
                    item.setTitle("Сохранено");
                    item.setEnabled(false);
                    if (socialNet.isChecked()) sendPostToVk();
                }
                toast(answer);
                getActivity().onBackPressed();
                return true;
            }
            case action_btn_copy_text: {
                copyTextNote();
                break;
            }
            case android.R.id.home: {
                getActivity().onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveFlagOnBD() {
        EventBus.getDefault().post(new BusEvent(MainActivity.CHANGE_ADAPTER));// in MenuNotesFragment changeAdapter
        notesController.saveServerFlag(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            ArrayList<Bitmap> bitmaps = new ArrayList<>();
            ArrayList<String> bitmapsFilePaths = new ArrayList<>();

            if (imageLocationsList.size() > 0) {
                noteImagesList.setVisibility(View.INVISIBLE);
            }

            progressBar.setVisibility(View.VISIBLE);

            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    Uri uriData = data.getData();

                    InputStream in;
                    try {
                        if (uriData != null) {
                            in = getActivity().getContentResolver().openInputStream(uriData);
                            bitmaps.add(cropImage(in, 200, 200, uriData));
                            assert in != null;
                            in.close();
                            bitmapsFilePaths.add(getFilePathFromContentUri(uriData));
                        } else {
                            ClipData clipData = null;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                clipData = data.getClipData();
                            }
                            if (clipData == null) {
                                return;
                            }
                            for (int i = 0; i < clipData.getItemCount(); i++) {
                                ClipData.Item item = clipData.getItemAt(i);
                                uriData = item.getUri();
                                in = App.getInstance(getActivity()).getContentResolver().openInputStream(uriData);
                                bitmaps.add(cropImage(in, 200, 200, uriData));
                                assert in != null;
                                in.close();
                                bitmapsFilePaths.add(getFilePathFromContentUri(uriData));
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case PHOTO_REQUEST_CODE:

                    break;
            }
            try {
                String[] imagesUrl = new UploadManager().uploadBitmaps(bitmaps, bitmapsFilePaths);
                for (String url : imagesUrl) {
                    Log.e("LOG URL:", url);
                }
                if (imagesUrl == null) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Ошибка загрузки изображений на сервер", Toast.LENGTH_SHORT).show();
                } else {
                    //TODO: выяснить зачем setImages
                    //notesController.get().setImages(imagesUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<Notes.ImageLocations> prepareImageLocations(String[] imagesUrl, ArrayList<String> bitmapsFilePaths) {
        List<Notes.ImageLocations> result = new ArrayList<>();
        for (int i = 0; i < imagesUrl.length; i++) {
            Notes.ImageLocations imgLocation = new Notes.ImageLocations(imagesUrl[i], bitmapsFilePaths.get(i));
            result.add(imgLocation);
        }
        Log.d(LOG, result.size() + " imageLocations were prepared");
        return result;
    }

    private String getFilePathFromContentUri(Uri contentUri) {
        Cursor cursor = null;

        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(columnIndex);
            Log.d(LOG, "Content Uri " + contentUri.toString() + " resolved to path => " + path);
            return path;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void onEvent(BusEventNoteImage event) { // Ждем события с uploadManager
        if (event.getImageLocationsList() != null) {
            updateAdapter(event.getImageLocationsList());
        }
    }

    private void updateAdapter(List<ImageLocations> imageLocations) {
        if (imageLocationsListNew == null) {
            imageLocationsListNew = new ArrayList<>();
        }

        List<ImageLocations> gettingImageFromGallery = deleteImageIfAlreadyHave(imageLocations);
        if (gettingImageFromGallery.size() == 0) {
            Toast.makeText(getContext(), "Данное изображение уже имеется", Toast.LENGTH_SHORT).show();
        }

        imageLocationsListNew.addAll(gettingImageFromGallery);
        imageLocationsList.addAll(gettingImageFromGallery);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
        noteImagesList.setVisibility(View.VISIBLE);
    }

    /**
     * Удаляем дубликаты
     */
    private List<ImageLocations> deleteImageIfAlreadyHave(List<ImageLocations> receivedLocations) {
        List<ImageLocations> imagesIsNotPresent = new ArrayList<>();

        for (ImageLocations item : receivedLocations) {
            if (!imageLocationsList.contains(item)) {
                imagesIsNotPresent.add(item);
            }
        }

        return imagesIsNotPresent;
    }


    /**
     * Удаление изображения:
     * Получаем url удаляемого изображения по позиции в списке.
     * Проверяем инициализирован ли списокудаляемых изображений.
     * Добавляем в список удаляемых изображений
     * для подальшего удаления из базы.
     * Удаляем изображение из общего списка.
     */
    @Override
    public void deleteImage(int position) {
        ImageLocations curr = imageLocationsList.get(position);

        if (imageLocationsListDelete == null) {
            imageLocationsListDelete = new ArrayList<>();
        }
        if (!deleteIsNewImage(curr)) {
            imageLocationsListDelete.add(curr);
        }

        imageLocationsList.remove(position);
        adapter.notifyDataSetChanged();

        checkEmptyImageURLs();
    }

    /**
     * Проверка: если общий список пуст то запрятать recyclerView
     */
    private void checkEmptyImageURLs() {
        if (imageLocationsList.isEmpty()) {
            noteImagesList.setVisibility(View.GONE);
        }
    }

    /**
     * Проверяем если удаляемое изображение не загружено из базы а добавлено только что
     * то просто удаляем изображение из списка новых изображений
     */
    private boolean deleteIsNewImage(ImageLocations imgLoc) {
        if (imageLocationsListNew != null && imageLocationsListNew.size() > 0) {
            if (imageLocationsListNew.contains(imgLoc)) {
                imageLocationsListNew.remove(imgLoc);
                return true;
            }
        }
        return false;
    }

    //По примеру из ProfileActivity
    public Bitmap cropImage(InputStream in, int w, int h, Uri selectedImageUri) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        BitmapFactory.decodeStream(in, null, options);
        in.close();
        int origWidth = options.outWidth; //исходная ширина
        int origHeight = options.outHeight; //исходная высота
        int scale; //кратность уменьшения
        if (origWidth > origHeight) {
            scale = Math.round((float) origHeight / (float) h);
        } else {
            scale = Math.round((float) origWidth / (float) w);
        }

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        in = App.getInstance(getActivity()).getContentResolver().openInputStream(selectedImageUri);//Ваш InputStream. Важно - открыть его нужно еще раз, т.к второй раз читать из одного и того же InputStream не разрешается (Проверено на ByteArrayInputStream и FileInputStream).
        Bitmap bitmap = BitmapFactory.decodeStream(in, null, options); //Полученный Bitmap
        Log.e(LOG, "h nad w " + bitmap.getHeight() + " " + bitmap.getWidth() + " " + bitmap.getConfig().toString());
        return bitmap;
    }


}
