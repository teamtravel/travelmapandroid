package com.travelmap3.notes;

import android.util.Log;

import com.travelmap3.api.RetrofitService;
import com.travelmap3.utils.Config;

import java.text.ParseException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andre on 29.03.2016.
 * Класс для синхронизации дневника
 */
public class SyncNote {
    NotesDB notesDB;


    public SyncNote(NotesDB notesDB) {
        this.notesDB = notesDB;
    }

    public void getServer(String token, int uid, int id) {
        RetrofitService.getInstanceAPI()
                .getNote(token, Config.VERSION_API, uid, id, new Callback<ArrayList<Notes>>() {
                    @Override
                    public void success(ArrayList<Notes> notes, Response response) {
                        try {
                            Log.e("LOG travel", notes.size() + " size notes get for sync");
                            notesDB.insertData(notes);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        try {
                            Log.d("syncnote", error.getMessage() + " " + error.getKind());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

}
