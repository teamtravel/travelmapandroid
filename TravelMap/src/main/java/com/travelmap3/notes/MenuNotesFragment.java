/**
 *
 */
package com.travelmap3.notes;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;
import com.travelmap3.App;
import com.travelmap3.MainActivity;
import com.travelmap3.R;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.event.BusEvent;
import com.travelmap3.tips.TIPS;
import com.travelmap3.tips.TipsCreate;
import com.travelmap3.utils.Config;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author An
 */
public class MenuNotesFragment extends android.support.v4.app.Fragment {
    private static final String LOG = " MenuNotesFragment";
    private ArrayList<Notes> listNotes;
    private NoteAdapter adapter;
    private NotesController notesController;
    private ListView list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu_note, container, false);
        TipsCreate.getInstance().show(TIPS.NOTE_MENU, getActivity().getFragmentManager(), getContext());
        list = (ListView) rootView.findViewById(R.id.listViewNotes);
        listNotes = new ArrayList<>();
        notesController = App.getInstance(getActivity()).getComponent().notesController();
        updateUI();

        final FloatingActionButton btnAdd = (FloatingActionButton) rootView.findViewById(
                R.id.notes_fab);
        btnAdd.attachToListView(list);

        setUpListView(list);
        setUpToolbar(rootView);
        setHasOptionsMenu(true);

        App application = (App) getActivity().getApplication();
//        Tracker mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen ListNotesFragment");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        return rootView;
    }

    public void updateUI() {
        try {
            listNotes = notesController.takeAll();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        adapter = new NoteAdapter(getActivity(), listNotes);
        list.setAdapter(adapter);
    }

    private void showDialogIsDelete(final int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(R.string.delete_note_question);
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                long timeCreate = listNotes.get(position).getTimeCreate();
                if (notesController.removeInfo(timeCreate)) {
                    listNotes.remove(position);
                    adapter.setData(listNotes);
                    adapter.notifyDataSetChanged();
                    deleteNoteServer(timeCreate);
                    Toast.makeText(getActivity(), R.string.deleted, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), R.string.delete_error, Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_map_open).setVisible(false);
        menu.findItem(R.id.action_map_note).setVisible(true);
        menu.findItem(R.id.action_btn_save).setVisible(false);
        menu.findItem(R.id.action_btn_copy_text).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_map_note: {
                Intent intMap = new Intent(getActivity().getApplicationContext(),
                        MapGoogleNoteActivity.class);
                startActivity(intMap);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpListView(ListView list) {

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                                           long id) {
                /*
                Вызвать диалог и удалить при желание этот диалог.
                 */
                showDialogIsDelete(position);
                return true;
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /**
             * Устанавливаем значение для заголовка, основного текса, и даты.(доступ)
             * Переходим во фрагмент записи.
             * @param parent
             * @param view
             * @param position
             * @param id
             */
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (listNotes.size() > 0 && listNotes != null
                            && listNotes.get(position) != null) {
                        Notes note = listNotes.get(position);
                        int noteId = note.getId();
                        Intent noteActivity = new Intent(getActivity(), NoteActivity.class);
                        noteActivity.putExtra(NoteActivity.NOTE_ID_EXTRA, noteId);
                        startActivity(noteActivity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    // This method will be called when a BusEvent is posted
    public void onEvent(BusEvent event) {
        Log.d("LOG onEvent ", "Notes");
        if (event.message.compareTo(MainActivity.CHANGE_ADAPTER) == 0) {
            try {
                Log.e("Log onEvent ", "Ok");
                adapter.getData(adapter.getCount() - 1).setServer(true); // устанавливаем иконку в листе что отсинхронено
                adapter.notifyDataSetChanged();
            } catch (Exception e) {// TODO: Исправьте пустой Catch-блок
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        notifyDataAdapter();
        super.onResume();
    }

    private void setUpToolbar(View rootView) {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_burger);
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    private void notifyDataAdapter() {
        if (adapter != null) {
            listNotes = notesController.takeAll();
            Log.e("LOG", "size notes " + listNotes.size() + " ");
            adapter.setData(listNotes);
            adapter.notifyDataSetChanged();
        }
    }

    private void deleteNoteServer(long id) {
        RetrofitService.getInstanceAPI().deleteNote(App.getInstance(getActivity()).getComponent().uController().get().getToken(), Config.VERSION_API, id, App.getInstance(getActivity()).getComponent().uController().get().getId(), new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                Toast.makeText(getActivity(), "Delete from server", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {

                if (error != null)
                    Toast.makeText(getActivity(), "Error send to server" + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
