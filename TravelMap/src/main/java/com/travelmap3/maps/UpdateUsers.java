package com.travelmap3.maps;

import android.util.Log;

import com.travelmap3.BuildConfig;
import com.travelmap3.api.RetrofitService;
import com.travelmap3.model.User;
import com.travelmap3.utils.Config;
import com.travelmap3.utils.InternetConnect;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.UsersDB;

import java.util.HashSet;

import javax.inject.Inject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * new UpdateUsers(getActivity()).currentData();
 * Идет запрос на обновление если это требуется всех пользователей системы.
 * Эта функция как кеш устроенна, если время прошло мало то берем из бд, если много то обновляем
 * бд.
 * При  вставке в бд, мы подсчитываем расстояние до каждого из них. Заносим это в отдельный столбец
 * - расстояние.
 */
public class UpdateUsers {
    private static final String LOG = "UpdateMap";
    private static final long MIN_TIME_UPDATE_DEBUG = 900;
    private static final long MIN_TIME_UPDATE = 15000;
    private final Preferences pref;
    private UsersDB db;

    public InternetConnect internetConnect;

    @Inject
    public UpdateUsers(UsersDB usersDB, InternetConnect internetConnect) {
        pref = Preferences.getInstance();
        db = usersDB;
        this.internetConnect = internetConnect;
    }


    public boolean isTime() {
        return BuildConfig.DEBUG ? System.currentTimeMillis() - pref.loadLastTimeUpdateMap()
                > MIN_TIME_UPDATE_DEBUG
                : System.currentTimeMillis() - pref.loadLastTimeUpdateMap() > MIN_TIME_UPDATE;
    }


    public HashSet<User> getWithServer() throws Exception {
        RetrofitService.getInstanceAPI().getUsers(Config.VERSION_API, new Callback<HashSet<User>>() {
            @Override
            public void success(HashSet<User> users, Response response) {
                db.insert(users);
                saveTime();
            }

            @Override
            public void failure(RetrofitError error) {
                int status = 0;
                try {
                    status = error.getResponse().getStatus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return null;
    }


    public boolean insertInDB(HashSet<User> set) {
        if (set != null) {
            db.insertFast(set);
            return true;
        } else {
            return false;
        }
    }


    public void saveTime() {
        pref.saveTimeUpdateMap(System.currentTimeMillis());

    }

    /**
     * Метод который проверяет нужно ли делать обновления.
     * Проверяет  наличие интернета с учетом настроек, и проверка когда последний раз обновлялись.
     *
     * @return Boolean
     */
    private boolean isUpdate() {
        return internetConnect.isActiveWiFiConnection() && isTime();
    }

    public HashSet<User> currentData() {
        HashSet<User> userSet;
        if (isUpdate()) {
            if (BuildConfig.DEBUG) {
                Log.w(LOG, "Обновляем данные с сервера... ");
            }
            try {
                getWithServer();
                userSet = getFromDB();
            } catch (Exception e) {
                userSet = getFromDB();
                e.printStackTrace();
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.w(LOG,
                        "Времени прошло мало или нет интернета (или у нас отключенна загружка через моб.инет), тогда берем инфу с бд... ");
            }
            userSet = getFromDB();
        }
        if (userSet == null) {
            userSet = new HashSet<>();
            User u = new User();
            u.setFirstName("Сервер не доступен");
            userSet.add(u);
        }
        db.close();
        return userSet;
    }

    public HashSet<User> getFromDB() {
        return db.get();
    }

}
