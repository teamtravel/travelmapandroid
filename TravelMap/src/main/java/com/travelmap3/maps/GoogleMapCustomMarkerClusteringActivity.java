/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.travelmap3.maps;

import android.animation.Animator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.travelmap3.App;
import com.travelmap3.BuildConfig;
import com.travelmap3.R;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.model.User;
import com.travelmap3.profile.CallBackResponse;
import com.travelmap3.profile.ProfileActivity;
import com.travelmap3.utils.Preferences;
import com.travelmap3.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.travelmap3.menu.MainMenuActivity.REQUEST_SENDBIRD_CHAT_ACTIVITY;


/**
 * @author An see for libs: http://googlemaps.github.io/android-maps-utils/#source
 *         <p>
 *         see: https://developers.google.com/maps/documentation/android/map
 */
public class GoogleMapCustomMarkerClusteringActivity extends BaseGoogleMapActivity
        implements ClusterManager.OnClusterClickListener<User>,
        ClusterManager.OnClusterInfoWindowClickListener<User>,
        ClusterManager.OnClusterItemClickListener<User>,
        ClusterManager.OnClusterItemInfoWindowClickListener<User> {

    public static final String ZOOM_CAMERA_USERS_NEARBE = "zoom";
    public static final String NEARBY_LATITUDE = "lat";
    public static final String NEARBY_LONGITUDE = "lon";
    private static final String LOG = "CustomMarkerActivity";
    private static final float ZOOM_CAMERA = 4;
    private static final String FRAGMENT_TAG = "ClusterList";
    private ClusterManager<User> mClusterManager;
    private HashSet<User> userSet;
    private GoogleMap.OnCameraChangeListener cameraChangeListener = new GoogleMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            mClusterManager.onCameraChange(cameraPosition);
            removeClusterInfoFragment();
        }
    };
    private GoogleMap.OnMapClickListener mapClickListener = new GoogleMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            removeClusterInfoFragment();
        }
    };

    @Override
    public boolean onClusterClick(Cluster<User> cluster) {
        // Show a toast with some info when the cluster is clicked.
        ClusterInfoFragment clusterFragment = ClusterInfoFragment.getInstance(
                (ArrayList<User>) cluster.getItems());
        animateFragment(clusterFragment);
        return true;
    }


    private void startChat(String userIds) {
        Intent data = new Intent();
        data.putExtra("userIds", new String[]{userIds});
        openChat(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            openChat(data);
        }
    }

    private void openChat(Intent data) {
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<User> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(User item) {
        // Does nothing, but you could go into the user's profile page, for example.
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(User item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    @Override
    protected void startMap() {
        try {
            pref = Preferences.getInstance();
            mClusterManager = new ClusterManager<User>(this, getMap());
            mClusterManager.setRenderer(new PersonRenderer());
            loadingUserMap();
            //  getMap().setMyLocationEnabled(true);
            Intent intent = getIntent();
            float zoom = ZOOM_CAMERA;
            double lat = App.getInstance(this).getComponent().uController().get().getLatitude();
            double lon = App.getInstance(this).getComponent().uController().get().getLongitude();
            if (intent != null) {
                zoom = (float) intent.getIntExtra(
                        GoogleMapCustomMarkerClusteringActivity.ZOOM_CAMERA_USERS_NEARBE, (int) ZOOM_CAMERA);
                lat = intent.getDoubleExtra(GoogleMapCustomMarkerClusteringActivity.NEARBY_LATITUDE, lat);
                lon = intent.getDoubleExtra(GoogleMapCustomMarkerClusteringActivity.NEARBY_LONGITUDE, lon);
            }
            getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), zoom));
            getMap().setOnCameraChangeListener(mClusterManager);
            getMap().setOnMarkerClickListener(mClusterManager);
            getMap().setOnInfoWindowClickListener(mClusterManager);
            getMap().setOnCameraChangeListener(cameraChangeListener);
            getMap().setOnMapClickListener(mapClickListener);
            mClusterManager.setOnClusterClickListener(this);
            mClusterManager.setOnClusterInfoWindowClickListener(this);
            mClusterManager.setOnClusterItemClickListener(this);
            mClusterManager.setOnClusterItemInfoWindowClickListener(this);
            mClusterManager.cluster();
            listenerInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listenerInfo() {
        getMap().setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                User userMessage = searchSelectedUser(marker.getPosition().latitude,
                        marker.getPosition().longitude, marker.getTitle());
                if (userMessage != null) {
                    //                    if (userMessage.getId() == userMe.getId())
                    //                        showSetStatus();
                    //                    else
                    showMessageDialog(marker.getSnippet(), userMessage);
                }
                if (isMe(marker)) {
                    showSetStatus();
                }
            }
        });

    }

    private boolean isMe(Marker marker) {
        return marker.getPosition().latitude == App.getInstance(this).getComponent().uController().get().getLatitude()
                && marker.getPosition().longitude == App.getInstance(this).getComponent().uController().get().getLongitude();
    }

    private void showSetStatus() {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final EditText editText = new EditText(this);

        DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                send(editText.getText().toString());
                dialog.dismiss();
            }
        };

        dialog = builder.setTitle(getString(R.string.map_marker_clustering_dialog_title))
                .setPositiveButton(getString(R.string.map_marker_clustering_dialog_positive_button),
                        onClickListener)
                .setView(editText).create();
        dialog.show();

    }

    /**
     * Отправляем статус на сервер, в случае успеха сохраняем его в системе,
     * иначе не сохраняем и говорим что все плохо.
     *
     * @param status Новый статус пользователя который установаил Юзер с Карты.
     */
    private void send(final String status) {
        User user = App.getInstance(this).getComponent().uController().get();
        user.setStatusText(status);
        App.getInstance(this).getComponent().uController().setUser(user, new CallBackResponse() {
            @Override
            public void onSuccess(Object obj) {
                Toast.makeText(getBaseContext(),
                        getString(R.string.map_marker_clustering_new_status) + status, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onError(Object obj) {
                toastError();
            }
        });
    }

    protected void toastError() {
        Toast.makeText(getBaseContext(), R.string.map_marker_clustering_error_try_later,
                Toast.LENGTH_SHORT).show();
    }

    protected User searchSelectedUser(double latitude, double longitude, String name) {
        for (User person : userSet) {
            if (checkOnTheKey(person, latitude, longitude, name)) {
                return person;
            }
        }
        return null;
    }

    private boolean checkOnTheKey(User user, double latitude, double longitude, String name) {
        return user.getLatitude() == latitude && user.getLongitude() == longitude &&
                (user.getFirstName() != null) && user.getFirstName().compareTo(name) == 0;
    }

    protected void showMessageDialog(String text, final User userMessage) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(userMessage.getFirstName());

        alertDialog.setPositiveButton(getString(R.string.messages),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (userMessage != null && userMessage.getId() > 0) {
                            startChat(userMessage.getStringId());
                        }
                    }
                });

        alertDialog.setNegativeButton(getString(R.string.map_marker_clustering_message_profile),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        Intent intent = ProfileActivity.newIntent(GoogleMapCustomMarkerClusteringActivity.this,
                                userMessage.getId());
                        startActivityForResult(intent, REQUEST_SENDBIRD_CHAT_ACTIVITY);
                    }
                });
        alertDialog.show();
    }


    private void loadingUserMap() {

        UpdateUsers updMap = App.getInstance(getApplicationContext()).getComponent().updateUsers();
        userSet = updMap.currentData();

        for (User user : userSet) {
            user.setProfilePhoto(R.drawable.no_avatar);
            mClusterManager.addItem(user);
        }
    }

    /*
    * Возвращает true если фрагмент был успешно закрыт
    */
    private boolean removeClusterInfoFragment() {
        final Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        final View container = findViewById(R.id.cluster_info_fragment_container);
        if (fragment != null) {
            if (container != null) {
                container.animate()
                        .translationY(container.getHeight())
                        .setListener(new Utils.CustomAnim() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                            }
                        });
            }
            return true;
        }
        return false;
    }

    private void animateFragment(final Fragment fragment) {
        final View fragmentContainer = findViewById(R.id.cluster_info_fragment_container);
        final Fragment oldFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (oldFragment != null) {
            if (fragmentContainer != null) {
                fragmentContainer.animate()
                        .translationY(fragmentContainer.getHeight())
                        .setListener(new Utils.CustomAnim() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.cluster_info_fragment_container, fragment, FRAGMENT_TAG)
                                        .commitAllowingStateLoss();
                                fragmentContainer.animate().translationY(0);
                            }
                        });
            }
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.cluster_info_fragment_container, fragment, FRAGMENT_TAG).commit();
            if (fragmentContainer != null) {
                fragmentContainer.animate().translationY(0);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!removeClusterInfoFragment()) {
            super.onBackPressed();
            setResult(MainMenuActivity.ITEM_ACTIVITY_RESULT_CODE);
        }
    }

    /**
     * Draws profile photos inside markers (using IconGenerator).
     * When there are multiple people in the cluster, draw multiple photos (using MultiDrawable).
     */
    private class PersonRenderer extends DefaultClusterRenderer<User> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public PersonRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);
            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(User person, final MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            mImageView.setImageResource(person.getDefaultProfilePhoto());
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions
                    .icon(BitmapDescriptorFactory.fromBitmap(icon))
                    .title(person.getFirstName())
                    .visible(true)
                    .snippet(person.getStatusText());
        }

        @Override
        protected void onClusterItemRendered(final User clusterItem, final Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
            try {
                Glide.with(GoogleMapCustomMarkerClusteringActivity.this)
                        .load(clusterItem.getPhoto50())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource,
                                                        GlideAnimation<? super Bitmap> glideAnimation) {
                                try {
                                    mImageView.setImageBitmap(resource);
                                    Bitmap icon = mIconGenerator.makeIcon();
                                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //https://github.com/googlemaps/android-maps-utils/issues/189
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<User> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;
            // Заполняем аватарами по умолчанию.
            try {
                for (int i = 0; i < 4 || i < cluster.getItems().size(); i++) {
                    Drawable stubPhoto = getResources().getDrawable(R.drawable.no_avatar);
                    if (stubPhoto != null) {
                        stubPhoto.setBounds(0, 0, width, height);
                    }
                    profilePhotos.add(stubPhoto);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);
            try {
                mClusterImageView.setImageDrawable(multiDrawable);
                Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
                if (BuildConfig.DEBUG) {
                    Log.e(LOG, "onBeforeClusterRendered finished");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onClusterRendered(final Cluster<User> cluster, final Marker marker) {
            super.onClusterRendered(cluster, marker);
            final List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            final int width = mDimension;
            final int height = mDimension;
            if (BuildConfig.DEBUG) {
                Log.d(LOG, "SIZE OF CLUSTER" + cluster.getSize());
            }
            try {
                //Заполняем маркеры реальными фотографиями
                for (User user : cluster.getItems()) {

                    // Если у пользователя не установлена фотография добавляем в массив фото по умолчанию
                    if (user.getPhoto50().equals("")) {
                        Drawable stubPhoto = getResources().getDrawable(user.getDefaultProfilePhoto());
                        if (stubPhoto != null) {
                            stubPhoto.setBounds(0, 0, width, height);
                        }
                        profilePhotos.add(stubPhoto);
                    }

                    Glide.with(getApplicationContext())
                            .load(user.getPhoto50())
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource,
                                                            GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    resource.setBounds(0, 0, width, height);
                                    profilePhotos.add(resource);
                                    if (profilePhotos.size() == 4 || profilePhotos.size() == cluster.getSize()) {
                                        MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
                                        try {
                                            multiDrawable.setBounds(0, 0, width, height);
                                            mClusterImageView.setImageDrawable(multiDrawable);
                                            Bitmap icon = mClusterIconGenerator.makeIcon(
                                                    String.valueOf(cluster.getSize()));
                                            marker.setIcon(BitmapDescriptorFactory.fromBitmap(icon));
                                            Log.d(LOG, "MultiDrawable for Cluster is sated up");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }
                            });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }
}


