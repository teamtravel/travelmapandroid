package com.travelmap3.maps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.travelmap3.R;
import com.travelmap3.model.User;
import com.travelmap3.profile.ProfileActivity;

import java.util.ArrayList;

/**
 * Created by Andrey on 10.10.2015.
 */
public class ClusterInfoFragment extends Fragment {
  private ArrayList<User> users;

  public ClusterInfoFragment() {
  }

  public static ClusterInfoFragment getInstance(ArrayList<User> users) {
    ClusterInfoFragment fragment = new ClusterInfoFragment();
    fragment.users = users;
    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_cluster_detail, null, false);
    ListView listView = (ListView) rootView.findViewById(R.id.cluster_info_listview);
    try {
      ClusterItemsInfoAdapter adapter = new ClusterItemsInfoAdapter(getActivity(), users);
      listView.setAdapter(adapter);
      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          Intent intent = ProfileActivity.newIntent(getActivity().getBaseContext(),
              users.get(position).getId());
          startActivityForResult(intent, 0);
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }
    return rootView;
  }
}
