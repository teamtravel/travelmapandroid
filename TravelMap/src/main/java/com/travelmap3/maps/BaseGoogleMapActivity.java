/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.travelmap3.maps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.travelmap3.App;
import com.travelmap3.R;
import com.travelmap3.menu.MainMenuActivity;
import com.travelmap3.utils.Preferences;

public abstract class BaseGoogleMapActivity extends AppCompatActivity {
    protected Preferences pref;
    private GoogleMap mMap;


    protected int getLayoutId() {
        return R.layout.activity_google_map_clastering;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setUpMapIfNeeded();
        //setUpMyMarker();

        App application = (App) getApplication();
//        mTracker = application.getDefaultTracker();
//        mTracker.setScreenName("Screen MapUsers");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        setActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpMapIfNeeded();
        // mMap.clear();
    }

    private void setActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_map);
        View back = (View) findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOG", "onClick: base map ");
                startActivity(new Intent(getApplicationContext(), MainMenuActivity.class));
            }
        });
        setSupportActionBar(toolbar);
        ActionBar bar = getSupportActionBar();

        if (null != bar) {
            bar.setDisplayShowTitleEnabled(false);

        }
    }

    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                if (mMap == null) {
                    mMap = googleMap;
                    startMap();
                }
            }
        });

    }

    protected abstract void startMap();

    protected GoogleMap getMap() {
        setUpMapIfNeeded();
        return mMap;
    }
}
