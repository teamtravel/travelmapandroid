package com.travelmap3.maps;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.travelmap3.R;
import com.travelmap3.model.User;
import com.travelmap3.utils.CircleTransform;
import com.travelmap3.utils.Utils;

import java.util.ArrayList;

/**
 * Created by Andrey on 10.10.2015.
 */
public class ClusterItemsInfoAdapter extends ArrayAdapter<User> {
  private final Activity context;
  private final ArrayList<User> users;

  public ClusterItemsInfoAdapter(Activity context, ArrayList<User> users) {
    super(context, R.layout.list_item_object, users);
    this.users = users;
    this.context = context;
  }

  static class ViewHolder {
    public ImageView profileImage;
    public TextView firstAndLastName;
    public TextView profileStatus;
  }

  @Override
  public int getCount() throws NullPointerException {
    return users.size();
  }

  @Override
  public long getItemId(int position) {
    return users.get(position).getId();
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;

    View rowView = convertView;
    if (rowView == null) {
      LayoutInflater inflater = context.getLayoutInflater();
      rowView = inflater.inflate(R.layout.list_item_object, null, true);
      holder = new ViewHolder();
      holder.firstAndLastName = (TextView) rowView.findViewById(R.id.item_object_place_title);
      holder.profileStatus = (TextView) rowView.findViewById(R.id.item_object_place_description);
      holder.profileImage = (ImageView) rowView.findViewById(R.id.item_object_place_image);
      rowView.setTag(holder);
    } else {
      holder = (ViewHolder) rowView.getTag();
    }
    User user = users.get(position);
    holder.firstAndLastName.setText(user.getLastName() + " " + user.getFirstName());
    holder.profileStatus.setText(user.getStatusText());

    if (Utils.isPhotoDefault(user.getPhoto50())) {
      Glide.with(context)
          .load(R.drawable.no_avatar)
          .diskCacheStrategy(DiskCacheStrategy.ALL)
          .into(holder.profileImage);
    } else {
      Glide.with(context)
          .load(user.getPhoto50())
          .diskCacheStrategy(DiskCacheStrategy.RESULT)
          .transform(new CircleTransform(context))
          .into(holder.profileImage);
    }
    return rowView;
  }

}

