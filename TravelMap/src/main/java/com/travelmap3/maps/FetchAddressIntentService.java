package com.travelmap3.maps;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Andrey on 29.11.2015.
 */
public class FetchAddressIntentService extends IntentService {

  public static final int SUCCESS_RESULT = 0;
  public static final int FAILURE_RESULT = 1;
  public static final String RECEIVER = "RECEIVER";
  public static final String LOCATION_DATA_EXTRA = "LOCATION_DATA_EXTRA";
  private static final String LOG = FetchAddressIntentService.class.getSimpleName();

  protected ResultReceiver mReceiver;

  /**
   * Creates an IntentService.Invoked by your subclass's constructor.
   *
   * @param name Used to name the worker thread, important only for debugging.
   */
  public FetchAddressIntentService(String name) {
    super(name);
  }

  public FetchAddressIntentService() {
    super(FetchAddressIntentService.class.getSimpleName());
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
    mReceiver = intent.getParcelableExtra(RECEIVER);

    String errorMessage = "";

    Location location = intent.getParcelableExtra(
        LOCATION_DATA_EXTRA);

    List<Address> addresses = null;

    try {
      addresses = geocoder.getFromLocation(
          location.getLatitude(),
          location.getLongitude(),
          1);
    } catch (IOException | IllegalArgumentException ioException) {
      // TODO: Исправить пустой catch-блок
    }

    if (addresses == null || addresses.size() == 0) {
      if (errorMessage.isEmpty()) {
        errorMessage = "Current location isn't available";
      }
      deliverResultToReceiver(FAILURE_RESULT, errorMessage);
    } else {
      Address address = addresses.get(0);
      ArrayList<String> addressFragments = new ArrayList<String>();

      // Fetch the address lines using getAddressLine,
      // join them, and send them to the thread.
      for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
        addressFragments.add(address.getAddressLine(i));
      }

      deliverResultToReceiver(SUCCESS_RESULT,
          TextUtils.join(System.getProperty("line.separator"),
              addressFragments));
    }
  }

  private void deliverResultToReceiver(int resultCode, String message) {
    Bundle bundle = new Bundle();
    bundle.putString("RESULT OK", message);
    mReceiver.send(resultCode, bundle);
  }

}
