package com.travelmap3.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.travelmap3.BuildConfig;

/**
 * Created by An on 10.07.2015.
 */
public class SqliteDBHelper extends SQLiteOpenHelper implements IDataBase {

    protected static final String DB_TABLE_USERS = "all_usersT";
    protected static final String DB_TABLE_DIALOG_USERS = "t_users";
    protected static final String DB_TABLE_NOTES = "notesT";
    protected static final String DB_TABLE_MESSAGES = "t_messages";
    protected static final String DB_TABLE_GPS_TRACKER = "t_tracker";
    protected static final String DB_TABLE_OBJECT_PLACE = "t_object";
    protected static final String DB_COLUMN_ID = "id";
    protected static final String DB_COLUMN_EMAIL = "email";
    protected static final String DB_COLUMN_NIKNAME = "firstname";
    protected static final String DB_COLUMN_STATUS = "status";
    protected static final String DB_COLUMN_LASTNAME = "lastname";
    protected static final String DB_COLUMN_VISIBLE = "visible";
    protected static final String DB_COLUMN_LASTTIME = "lasttime";
    protected static final String DB_COLUMN_BIRTHDAY = "birthday";
    protected static final String DB_COLUMN_ABOUTME = "aboutme";
    protected static final String DB_COLUMN_PHOTO_50 = "photo50";
    protected static final String DB_COLUMN_PHOTO_100 = "photo100";
    protected static final String DB_COLUMN_PHOTO_200 = "photo200";
    protected static final String DB_COLUMN_FROM_CITY = "fromcity";
    protected static final String DB_COLUMN_NEARBY_DISTANCE = "nearby_users";
    protected static final String DB_COLUMN_TEXT = "text_note";
    protected static final String DB_COLUMN_TIME_CREATE = "d_time_create";
    protected static final String DB_COLUMN_DATA_UPDATE = "d_last_update";
    protected static final String DB_COLUMN_TITLE = "t_title";
    protected static final String DB_COLUMN_ACCESS = "access";
    protected static final String DB_COLIMN_FLAG_VK_SAFE = "vk_flag";
    protected static final String DB_COLUMN_FLAG_SERVER_SAFE = "server_flag";
    protected static final String DB_COLUMN_NUMBER_TRIP = "num_trip"; // Если номер трипа равен == -1 то хначит эти данные пришли с сервера от других чуваков.
    protected static final String DB_COLUMN_ALTITUDE = "altitude";
    protected static final String DB_COLUMN_SPEED = "speed";
    protected static final String DB_COLUMN_PRIVATES = "privatesplace";
    protected static final String DB_COLUMN_TELEGRAM = "telegram";
    public static final String CREATE_ADD_TELEGRAM_COLUMN = " alter table " + DB_TABLE_USERS + " add column " + DB_COLUMN_TELEGRAM + " text";
    protected static final String DB_TABLE_INTERECSING_PLACES = "interestingplace";
    protected static final String DB_COLUMN_DATE = "date";

    /* messages */
    protected static final String DB_COLUMN_TIME = "time_create";
    protected static final String DB_COLUMN_MESSAGE = "text";
    protected static final String DB_COLUMN_RECEIVER_ID = "recipient_id";
    protected static final String DB_COLUMN_SENDER_ID = "sender_id";
    protected static final String DB_COLUMN_READ = "read";
    protected static final String DB_COLUMN_DEFFERED = "def_loс";// Для отложенных сообщений. которые не оправились на сервер
    public static final String DB_CREATE_MESSAGES = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_MESSAGES
            + "( _id integer primary key autoincrement, "
            + DB_COLUMN_ID + " int null, "
            + DB_COLUMN_MESSAGE + " text not null, "
            + DB_COLUMN_RECEIVER_ID + " int not null, "
            + DB_COLUMN_SENDER_ID + " int not null, "
            + DB_COLUMN_READ + " int not null, "
            + DB_COLUMN_DEFFERED + " int not null, "
            + DB_COLUMN_TIME + " long not null );";
    /* object place*/
    protected static final String DB_COLUMN_ADDRESREGION = "adReg";
    protected static final String DB_COLUMN_IMAGURL = "imurl";
    protected static final String DB_COLUMN_IMAGPATH = "impath";
    protected static final String DB_COLUMN_TYPE = "type";
    protected static final String DB_COLUMN_ADDRESAREA = "addarea";
    protected static final String DB_COLUMN_ADDRESLOCALITY = "addreloc";
    protected static final String DB_COLUMN_STREETADRESS = "addlocality";
    protected static final String DB_COLUMN_NAME_OBJECT = "nameobject";
    protected static final String DB_COLUMN_URL = "url";
    protected static final String DB_COLUMN_TELEPHONE = "telephone";
    protected static final String DB_COLUMN_STARTDATA = "startdata";
    protected static final String DB_COLUMN_END_DATA = "enddata";
    protected static final String DB_COLUMN_REVIEW = "review";
    public static final String DB_CREATE_OBJECT_PLACE = " CREATE TABLE IF NOT EXISTS " + DB_TABLE_OBJECT_PLACE
            + "( _id integer primary key autoincrement, "
            + DB_COLUMN_ADDRESREGION + " text , "
            + DB_COLUMN_IMAGURL + " text, "
            + DB_COLUMN_TYPE + " text , "
            + DB_COLUMN_NEARBY_DISTANCE + " real, "
            + DB_COLUMN_ADDRESAREA + " text, "
            + DB_COLUMN_ADDRESLOCALITY + " text, "
            + DB_COLUMN_STREETADRESS + " text  , "
            + DB_COLUMN_NAME_OBJECT + " text  , "
            + DB_COLUMN_URL + " text, "
            + DB_COLUMN_TELEPHONE + " text , "
            + DB_COLUMN_LONG + " real , "
            + DB_COLUMN_STARTDATA + " long , "
            + DB_COLUMN_END_DATA + " long , "
            + DB_COLUMN_REVIEW + " text , "
            + DB_COLUMN_LAT + " real  );";
    private static final String DB_USERS_STRUCTURE = " ( _id integer primary key autoincrement, "
            + DB_COLUMN_ID + " int UNIQUE not null, " // id users with server
            + DB_COLUMN_EMAIL + " text not null, "
            + DB_COLUMN_LONG + " real not null, "
            + DB_COLUMN_LAT + " real not null, "
            + DB_COLUMN_NIKNAME + " text not null, "
            + DB_COLUMN_LASTNAME + " text not null, "
            + DB_COLUMN_VISIBLE + " int not null, "
            + DB_COLUMN_BIRTHDAY + " text not null, "
            + DB_COLUMN_ABOUTME + " text not null, "
            + DB_COLUMN_FROM_CITY + " text not null, "
            + DB_COLUMN_PHOTO_50 + " text not null, "
            + DB_COLUMN_PHOTO_100 + " text not null, "
            + DB_COLUMN_PHOTO_200 + " text not null, "
            + DB_COLUMN_LASTTIME + " long not null, "
            + DB_COLUMN_TELEGRAM + " text not null, "
            + DB_COLUMN_STATUS + " text not null ";
    private static final String DB_CREATE_GPS_TRACKER = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_GPS_TRACKER
            + " ( _id integer primary key autoincrement, "
            + DB_COLUMN_NUMBER_TRIP + " integer not null, "
            + DB_COLUMN_LONG + " real not null, "
            + DB_COLUMN_TIME + " long not null, "
            + DB_COLUMN_SPEED + " real not null, "
            + DB_COLUMN_FLAG_SERVER_SAFE + " integer not null DEFAULT 0, "
            + DB_COLUMN_ALTITUDE + " real not null, "
            + DB_COLUMN_LAT + " real not null )";
    private static final String DB_CREATE_INTERESTING_PLACES = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_INTERECSING_PLACES
            + " ( _id integer primary key autoincrement, "
            + DB_COLUMN_TEXT + " text not null, "
            + DB_COLUMN_TITLE + " text not null, "
            + DB_COLUMN_NUMBER_TRIP + " integer not null, "
            + DB_COLUMN_LONG + " real not null, "
            + DB_COLUMN_TIME + " long not null, "
            + DB_COLUMN_PRIVATES + " int not null, "
            + DB_COLUMN_IMAGURL + " int not null, "
            + DB_COLUMN_FLAG_SERVER_SAFE + " int no null DEFAULT 0, "
            + DB_COLUMN_LAT + " real not null )";
    private static final String DB_CREATE_USERS = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_USERS
            + DB_USERS_STRUCTURE + ","
            + DB_COLUMN_NEARBY_DISTANCE + " int);";
    private static final String DB_CREATE_DIALOG_USERS = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_DIALOG_USERS
            + " ( _id integer primary key autoincrement, "
            + DB_COLUMN_ID + " int UNIQUE not null, "
            + DB_COLUMN_LONG + " real not null, "
            + DB_COLUMN_LAT + " real not null, "
            + DB_COLUMN_PHOTO_50 + " text not null, "
            + DB_COLUMN_NIKNAME + " text not null, "
            + DB_COLUMN_LASTNAME + " text not null ) ";
    private static final String DB_CREATE_NOTES = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_NOTES
            + " (  _id integer primary key autoincrement, "
            + DB_COLUMN_LONG + " real not null, "
            + DB_COLUMN_LAT + " real not null, "
            + DB_COLUMN_TEXT + " text not null, "
            + DB_COLUMN_ACCESS + " int not null, "
            + DB_COLUMN_TITLE + " text not null, "
            + DB_COLIMN_FLAG_VK_SAFE + " integer not null, "
            + DB_COLUMN_FLAG_SERVER_SAFE + " integer not null DEFAULT 0, "
            + DB_COLUMN_DATA_UPDATE + " long not null, "
            + DB_COLUMN_TIME_CREATE + " long not null);";


    /* image for notes */
    protected static final String DB_TABLE_NOTES_IMAGE = "t_notes_image";
    public static final String DB_COLUMN_NOTE_ID = "note_id";

    private static final String DB_CREATE_NOTES_IMAGE = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_NOTES_IMAGE
            + " ( _id integer primary key autoincrement, "
            + DB_COLUMN_NOTE_ID+ " integer, "
            + DB_COLUMN_IMAGPATH + " text, "
            + DB_COLUMN_IMAGURL + " text)";

    protected static final String DB_TABLE_STORY_TRAVEL = "travelstory";
    protected static final String DB_COLUMN_TIME_START = "timestart";
    protected static final String DB_COLUMN_TIME_FINISH = "timefinish";
    protected static final String DB_COLUMN_DISTANCE = "distanc";
    protected static final String DB_COLUMN_LAT_AND_LONG_START = "latlongstr";
    protected static final String DB_COLUMN_LAT_AND_LONG_FINISH = "latlongfin";
    private static final String DB_CREATE_STORY_TRAVEL = "CREATE TABLE IF NOT EXISTS " + DB_TABLE_STORY_TRAVEL
            + " (  _id integer primary key autoincrement, "
            + DB_COLUMN_TIME_START + " long not null DEFAULT 0, "
            + DB_COLUMN_TIME_FINISH + " long not null DEFAULT 0, "
            + DB_COLUMN_NAME_OBJECT + " text not null DEFAULT '', "
            + DB_COLUMN_FLAG_SERVER_SAFE + " integer not null DEFAULT 0, "
            + DB_COLUMN_NUMBER_TRIP + " integer not null DEFAULT 0, "
            + DB_COLUMN_DISTANCE + " real not null DEFAULT 0, "
            + DB_COLUMN_SPEED + " real not null DEFAULT 0, "
            + DB_COLUMN_LAT_AND_LONG_START + " text not null DEFAULT '', "
            + DB_COLUMN_LAT_AND_LONG_FINISH + " text not null DEFAULT '');";
    private static final String DB_NOTES_IMAGE_ADD_COLUMN = "ALTER TABLE " + DB_TABLE_NOTES_IMAGE
            + " ADD " + DB_COLUMN_IMAGPATH + " text;";



    private final String LOG = this.getClass().getName();
    public SQLiteDatabase db;

    public SqliteDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE_USERS);
        db.execSQL(DB_CREATE_DIALOG_USERS);
        db.execSQL(DB_CREATE_NOTES);
        db.execSQL(DB_CREATE_MESSAGES);
        db.execSQL(DB_CREATE_GPS_TRACKER);
        db.execSQL(DB_CREATE_OBJECT_PLACE);
        db.execSQL(DB_CREATE_INTERESTING_PLACES);
        db.execSQL(DB_CREATE_NOTES_IMAGE);
        db.execSQL(DB_CREATE_STORY_TRAVEL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2){
            db.execSQL(DB_CREATE_OBJECT_PLACE);
        }
        if(oldVersion<3){
            db.execSQL(CREATE_ADD_TELEGRAM_COLUMN);
        }
        if(oldVersion<4) {
            db.execSQL(DB_CREATE_NOTES_IMAGE);
        }
        if (oldVersion < 4) {
            db.execSQL(DB_CREATE_STORY_TRAVEL);
        }
        if (oldVersion < 7) {
            db.execSQL(DB_NOTES_IMAGE_ADD_COLUMN);
        }
    }

    public synchronized SQLiteDatabase openDatabase() {
        return getWritableDatabase();
    }

    public synchronized void closeDatabase(SQLiteDatabase db) {
        Log.i(LOG, "Закрываем getWritableDatabase от Message PlaceDB");
        if (db != null) db.close();
    }

    public long insertDB(String t, ContentValues contentValues) {
        Log.i(LOG, "Insert in placeDb");
        db = getWritableDatabase();
        db.beginTransaction();
        long kol = 0;
        try {
            kol = db.insert(t, null, contentValues);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
        Log.i(LOG, "Insert in code=" + kol + " size=" + contentValues.size());
        return kol;
    }

    protected void clearAllT(String table) {
        int p;
        try {
            if ((p = getWritableDatabase().delete(table, null, null)) > 0)
                Log.d("sync", "DELETE " + p);
            if (BuildConfig.DEBUG) Log.v(LOG, "Delete, KOL ROWS = " + p);
            else if (BuildConfig.DEBUG) Log.v(LOG, "CLEAR PlaceDB ROWS=" + p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
