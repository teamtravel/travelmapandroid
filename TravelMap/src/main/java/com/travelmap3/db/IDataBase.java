package com.travelmap3.db;

import com.travelmap3.BuildConfig;

public interface IDataBase {

  String DB_COLUMN_LONG = "longitude";
  String DB_COLUMN_LAT = "latitude";
  String DB_NAME = BuildConfig.DB;
  int DB_VERSION = 8;

}
